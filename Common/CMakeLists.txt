cmake_minimum_required (VERSION 3.21)
set(LIBNAME KawaiiWorlds)
project (${LIBNAME}-Common)

install(DIRECTORY ../src/lib/ DESTINATION include/${LIBNAME} FILES_MATCHING PATTERN "*.hpp")
install(DIRECTORY ../src/qml_wrappers/ DESTINATION include/${LIBNAME} FILES_MATCHING PATTERN "*.hpp")
