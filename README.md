# Vision

The vision is to provide a flexible yet lightweight tool for creating a virtual worlds (e.g. computer games). A world should not contain any platform specific code or logic, so it can run on any software / hardware environment as long as the engine supports the platform. A JSON-based manifest format declares scripts, shaders, textures, 3D models, and other assets / resources.

The engine is a cross platform program to simulate a world. This approach allows the worlds to not have a binary executable file, so they can be easily transfered between systems with different window systems (X11 / Wayland), OS (GNU/Linux or Windows or other) or even CPU architecture (theoretically).

# Building

Install the dependencies (Qt5, GLM, sib\_utils and Kawaii3D) with your system package manager. Then run `cmake ${srcdir} && cmake --build .` in your build directory.

On ArchLinux you can simply call `ArchPackages/deploy.sh` in any directory. This script will package and install the engine with `pacman`.
