cmake_minimum_required (VERSION 3.21)
project (KawaiiWorlds)

cmake_policy(SET CMP0069 NEW)

#________________________________________________________________________________________
#Variables:
set(LIBNAME "${PROJECT_NAME}")

set(${LIBNAME}_MAJOR_VERSION 0)
set(${LIBNAME}_MINOR_VERSION 0)
set(${LIBNAME}_PATCH_VERSION 9)
set(${LIBNAME}_VERSION
  ${${LIBNAME}_MAJOR_VERSION}.${${LIBNAME}_MINOR_VERSION}.${${LIBNAME}_PATCH_VERSION})

string(TOUPPER ${PROJECT_NAME} PROJECT_NAME_UPPER)

# Offer the user the choice of overriding the installation directories
set(INSTALL_LIB_DIR lib CACHE PATH "Installation directory for libraries")
set(INSTALL_BIN_DIR bin CACHE PATH "Installation directory for executables")
set(INSTALL_INCLUDE_DIR include CACHE PATH "Installation directory for header files")
set(DEF_INSTALL_CMAKE_DIR lib/cmake/${LIBNAME})
set(INSTALL_CMAKE_DIR ${DEF_INSTALL_CMAKE_DIR} CACHE PATH "Installation directory for CMake files")

# Make relative paths absolute
foreach(p LIB BIN INCLUDE CMAKE)
  set(var INSTALL_${p}_DIR)
  if(NOT IS_ABSOLUTE "${${var}}")
    set(${var} "${CMAKE_INSTALL_PREFIX}/${${var}}")
  endif()
endforeach()

#________________________________________________________________________________________
#Config compiler:
if(MSVC)
    set (CMAKE_CXX_FLAGS "/std:c++latest /EHcs")
else()
    set (CMAKE_CXX_STANDARD 17)
    set (CMAKE_CXX_STANDARD_REQUIRED ON)
    set (CMAKE_CXX_EXTENSIONS ON)
endif()

if(NOT MSVC)
    set (CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wall -Wextra -Wno-missing-field-initializers")
    set (CMAKE_CXX_FLAGS_DEBUG "${CMAKE_CXX_FLAGS_DEBUG} -g -O0 -Wp,-U_FORTIFY_SOURCE")
    set (CMAKE_CXX_FLAGS_RELEASE "${CMAKE_CXX_FLAGS_RELEASE} -O3")
    set (CMAKE_CXX_FLAGS_RELWITHDEBINFO "${CMAKE_CXX_FLAGS_RELWITHDEBINFO} -g -O3")
    set (CMAKE_CXX_FLAGS_MINSIZEREL "${CMAKE_CXX_FLAGS_MINSIZEREL} -Os")
endif()

if(NOT CMAKE_DEBUG_POSTFIX)
  set(CMAKE_DEBUG_POSTFIX d)
endif()

# Tell CMake to run moc when necessary:
set (CMAKE_AUTOMOC ON)
set (CMAKE_AUTORCC ON)
# As moc files are generated in the binary dir, tell CMake to always look for includes there:
set (CMAKE_INCLUDE_CURRENT_DIR ON)


#________________________________________________________________________________________
#Dependencies:
find_package (Qt6 COMPONENTS Core Gui Multimedia Network Concurrent Quick REQUIRED)
find_package (glm REQUIRED)
find_package (sib_utils REQUIRED)
find_package (Kawaii3D REQUIRED)
find_package (Bullet CONFIG REQUIRED)
find_package (OpenMP)
if (OPENMP_FOUND)
    set (CMAKE_C_FLAGS "${CMAKE_C_FLAGS} ${OpenMP_C_FLAGS}")
    set (CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} ${OpenMP_CXX_FLAGS}")
endif()

file(GLOB_RECURSE SRC src/lib/*.cpp src/lib/*.hpp src/lib/*.qrc)

add_library(${PROJECT_NAME} SHARED ${SRC})

add_library(bullet_interface_lib INTERFACE)
target_compile_definitions(bullet_interface_lib INTERFACE ${BULLET_DEFINITIONS})
foreach(__bullet_include_dir ${BULLET_INCLUDE_DIRS})
    target_include_directories(bullet_interface_lib INTERFACE ${BULLET_ROOT_DIR}/${__bullet_include_dir})
endforeach()
foreach(__bullet_library_dir ${BULLET_LIBRARY_DIRS})
    target_link_directories(bullet_interface_lib INTERFACE ${BULLET_ROOT_DIR}/${__bullet_library_dir})
endforeach()
target_link_libraries(bullet_interface_lib INTERFACE ${BULLET_LIBRARIES})

install(TARGETS bullet_interface_lib
    EXPORT ${LIBNAME}Targets
    DESTINATION lib)

target_include_directories(${PROJECT_NAME} PUBLIC $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/src/lib>)
target_link_libraries(${PROJECT_NAME} PUBLIC Kawaii3D::Kawaii3D Qt6::Multimedia Qt6::Network bullet_interface_lib)

target_compile_definitions(${PROJECT_NAME}
    PRIVATE KAWAIIWORLDS_LIBRARY
    PUBLIC _USE_MATH_DEFINES ${BULLET_DEFINITIONS}
)

target_include_directories(${PROJECT_NAME} INTERFACE
    $<INSTALL_INTERFACE:include/${PROJECT_NAME}>)

if(UNIX)
    find_program(WAYLAND_SCANNER wayland-scanner)
    set(GEN_HEADERS_DIR ${KawaiiWorlds_BINARY_DIR}/generated)
    file(MAKE_DIRECTORY ${GEN_HEADERS_DIR})

    add_custom_command(
        OUTPUT "${GEN_HEADERS_DIR}/wl_relative-pointer-unstable-v1.h"
        DEPENDS "/usr/share/wayland-protocols/unstable/relative-pointer/relative-pointer-unstable-v1.xml"
        COMMAND
        ${WAYLAND_SCANNER}
        "client-header"
        "/usr/share/wayland-protocols/unstable/relative-pointer/relative-pointer-unstable-v1.xml"
        "${GEN_HEADERS_DIR}/wl_relative-pointer-unstable-v1.h"
    )

    add_custom_command(
        OUTPUT "${GEN_HEADERS_DIR}/wl_relative-pointer-unstable-v1.c"
        DEPENDS "/usr/share/wayland-protocols/unstable/relative-pointer/relative-pointer-unstable-v1.xml"
        COMMAND
        ${WAYLAND_SCANNER}
        "private-code"
        "/usr/share/wayland-protocols/unstable/relative-pointer/relative-pointer-unstable-v1.xml"
        "${GEN_HEADERS_DIR}/wl_relative-pointer-unstable-v1.c"
    )
    if(NOT EXISTS "${GEN_HEADERS_DIR}/wl_relative-pointer-unstable-v1.h")
        execute_process(COMMAND ${WAYLAND_SCANNER}
            "client-header"
            "/usr/share/wayland-protocols/unstable/relative-pointer/relative-pointer-unstable-v1.xml"
            "${GEN_HEADERS_DIR}/wl_relative-pointer-unstable-v1.h")
    endif()
    if(NOT EXISTS "${GEN_HEADERS_DIR}/wl_relative-pointer-unstable-v1.c")
        execute_process(COMMAND ${WAYLAND_SCANNER}
            "private-code"
            "/usr/share/wayland-protocols/unstable/relative-pointer/relative-pointer-unstable-v1.xml"
            "${GEN_HEADERS_DIR}/wl_relative-pointer-unstable-v1.c")
    endif()
    add_library(${PROJECT_NAME}_wl_relative-pointer-unstable-v1 STATIC "${GEN_HEADERS_DIR}/wl_relative-pointer-unstable-v1.c")

    add_custom_command(
        OUTPUT "${GEN_HEADERS_DIR}/wl_pointer-constraints-unstable-v1.h"
        DEPENDS "/usr/share/wayland-protocols/unstable/pointer-constraints/pointer-constraints-unstable-v1.xml"
        COMMAND
        ${WAYLAND_SCANNER}
        "client-header"
        "/usr/share/wayland-protocols/unstable/pointer-constraints/pointer-constraints-unstable-v1.xml"
        "${GEN_HEADERS_DIR}/wl_pointer-constraints-unstable-v1.h"
    )

    add_custom_command(
        OUTPUT "${GEN_HEADERS_DIR}/wl_pointer-constraints-unstable-v1.c"
        DEPENDS "/usr/share/wayland-protocols/unstable/pointer-constraints/pointer-constraints-unstable-v1.xml"
        COMMAND
        ${WAYLAND_SCANNER}
        "private-code"
        "/usr/share/wayland-protocols/unstable/pointer-constraints/pointer-constraints-unstable-v1.xml"
        "${GEN_HEADERS_DIR}/wl_pointer-constraints-unstable-v1.c"
    )
    if(NOT EXISTS "${GEN_HEADERS_DIR}/wl_pointer-constraints-unstable-v1.h")
        execute_process(COMMAND ${WAYLAND_SCANNER}
            "client-header"
            "/usr/share/wayland-protocols/unstable/pointer-constraints/pointer-constraints-unstable-v1.xml"
            "${GEN_HEADERS_DIR}/wl_pointer-constraints-unstable-v1.h")
    endif()
    if(NOT EXISTS "${GEN_HEADERS_DIR}/wl_pointer-constraints-unstable-v1.h")
        execute_process(COMMAND ${WAYLAND_SCANNER}
            "private-code"
            "/usr/share/wayland-protocols/unstable/pointer-constraints/pointer-constraints-unstable-v1.xml"
            "${GEN_HEADERS_DIR}/wl_pointer-constraints-unstable-v1.c")
    endif()
    add_library(${PROJECT_NAME}_wl_pointer-constraints-v1 STATIC "${GEN_HEADERS_DIR}/wl_pointer-constraints-unstable-v1.c")

    target_include_directories(${PROJECT_NAME}
        PRIVATE ${Qt6Gui_PRIVATE_INCLUDE_DIRS}
        PRIVATE ${GEN_HEADERS_DIR}
        )
    target_link_libraries(${PROJECT_NAME}
        PRIVATE ${PROJECT_NAME}_wl_relative-pointer-unstable-v1 ${PROJECT_NAME}_wl_pointer-constraints-v1
        PUBLIC wayland-client)
endif()

option(FORBID_LTO "Force disable LTO for ${LIBNAME}" OFF)
if(NOT FORBID_LTO)
    set_property(TARGET ${PROJECT_NAME} PROPERTY INTERPROCEDURAL_OPTIMIZATION TRUE)
    message(STATUS "${PROJECT_NAME}: Enabled LTO")
else()
    set_property(TARGET ${PROJECT_NAME} PROPERTY INTERPROCEDURAL_OPTIMIZATION FALSE)
    message(STATUS "${PROJECT_NAME}: Disabled LTO")
endif()

install(TARGETS ${LIBNAME}
    EXPORT ${LIBNAME}Targets
    ARCHIVE DESTINATION lib
    LIBRARY DESTINATION lib
    RUNTIME DESTINATION bin)

set(_src_root_path ${PROJECT_SOURCE_DIR}/src/lib)
set(_source_list ${SRC})
foreach(_source IN ITEMS ${_source_list})
    get_filename_component(_source_path "${_source}" PATH)
    file(RELATIVE_PATH _source_path_rel "${_src_root_path}" "${_source_path}")
    string(REPLACE "/" "\\" _group_path "${_source_path_rel}")
    source_group("${_group_path}" FILES "${_source}")
endforeach()

file(GLOB_RECURSE SRC src/qml_wrappers/*.cpp src/qml_wrappers/*.hpp)

add_library(${PROJECT_NAME}_qml SHARED ${SRC})
target_include_directories(${PROJECT_NAME}_qml PUBLIC $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/src/qml_wrappers>)
target_link_libraries(${PROJECT_NAME}_qml PUBLIC ${PROJECT_NAME} Qt6::Qml)

target_compile_definitions(${PROJECT_NAME}_qml PRIVATE KAWAIIWORLDS_QML_LIBRARY)

if(NOT FORBID_LTO)
    set_property(TARGET ${PROJECT_NAME}_qml PROPERTY INTERPROCEDURAL_OPTIMIZATION TRUE)
    message(STATUS "${PROJECT_NAME}_qml: Enabled LTO")
else()
    set_property(TARGET ${PROJECT_NAME}_qml PROPERTY INTERPROCEDURAL_OPTIMIZATION FALSE)
    message(STATUS "${PROJECT_NAME}_qml: Disabled LTO")
endif()

install(TARGETS ${LIBNAME}_qml
    EXPORT ${LIBNAME}Targets
    ARCHIVE DESTINATION lib
    LIBRARY DESTINATION lib
    RUNTIME DESTINATION bin)

set(_src_root_path ${PROJECT_SOURCE_DIR}/src/qml_wrappers)
set(_source_list ${SRC})
foreach(_source IN ITEMS ${_source_list})
    get_filename_component(_source_path "${_source}" PATH)
    file(RELATIVE_PATH _source_path_rel "${_src_root_path}" "${_source_path}")
    string(REPLACE "/" "\\" _group_path "${_source_path_rel}")
    source_group("${_group_path}" FILES "${_source}")
endforeach()


file(GLOB_RECURSE SRC src/viewer/*.cpp src/viewer/*.hpp src/viewer/*.qrc)

add_executable(${PROJECT_NAME}Viewer ${SRC})
target_link_libraries(${PROJECT_NAME}Viewer PUBLIC ${PROJECT_NAME}_qml Qt6::Quick)

if(NOT FORBID_LTO)
    set_property(TARGET ${PROJECT_NAME}Viewer PROPERTY INTERPROCEDURAL_OPTIMIZATION TRUE)
    message(STATUS "${PROJECT_NAME}Viewer: Enabled LTO")
else()
    set_property(TARGET ${PROJECT_NAME}Viewer PROPERTY INTERPROCEDURAL_OPTIMIZATION FALSE)
    message(STATUS "${PROJECT_NAME}Viewer: Disabled LTO")
endif()

target_compile_definitions(${PROJECT_NAME}Viewer
    PRIVATE
    -DAPP_NAME="${PROJECT_NAME}Viewer"
    $<$<OR:$<CONFIG:Debug>,$<CONFIG:RelWithDebInfo>>:QT_QML_DEBUG>
)

#set_target_properties(${PROJECT_NAME}Viewer PROPERTIES
#    WIN32_EXECUTABLE ON
#    MACOSX_BUNDLE ON
#)
install(TARGETS ${PROJECT_NAME}Viewer
    EXPORT ${PROJECT_NAME}Targets
    DESTINATION bin)

option(Kawaii3D_Bundle "Make a standalone bundle with the bare minimum of external dependencies" OFF)
if(Kawaii3D_Bundle)
    install(CODE [[
        file(GET_RUNTIME_DEPENDENCIES
            LIBRARIES $<TARGET_FILE:KawaiiWorlds_qml>
            EXECUTABLES $<TARGET_FILE:KawaiiWorldsViewer>
            RESOLVED_DEPENDENCIES_VAR _r_deps
            UNRESOLVED_DEPENDENCIES_VAR _u_deps
        )

    foreach(_file ${_r_deps})
        message(STATUS "${_file}")
        file(INSTALL
            DESTINATION bin
            TYPE SHARED_LIBRARY
            FOLLOW_SYMLINK_CHAIN
            FILES "${_file}")
    endforeach()
    list(LENGTH _u_deps _u_length)
    if("${_u_length}" GREATER 0)
        message(WARNING "Unresolved dependencies detected!")
    endif()
]])
endif()


set(_src_root_path ${PROJECT_SOURCE_DIR}/src/viewer)
set(_source_list ${SRC})
foreach(_source IN ITEMS ${_source_list})
    get_filename_component(_source_path "${_source}" PATH)
    file(RELATIVE_PATH _source_path_rel "${_src_root_path}" "${_source_path}")
    string(REPLACE "/" "\\" _group_path "${_source_path_rel}")
    source_group("${_group_path}" FILES "${_source}")
endforeach()

include("Common/CPackConfig.cmake")
