#ifndef RESOURCEPACK_QML_HPP
#define RESOURCEPACK_QML_HPP

#include "KawaiiWorlds_qml_global.hpp"
#include <ResourcePack.hpp>

#include <sib_utils/QtToStdHash.hpp>
#include <QJSValue>

namespace KawaiiJSWrappers {
  class KAWAIIWORLDS_QML_SHARED_EXPORT ResourcePack: public QObject
  {
    Q_OBJECT

    Q_PROPERTY(float gamma READ getGamma WRITE setGamma SCRIPTABLE true)
    Q_PROPERTY(float exposure READ getExposure WRITE setExposure SCRIPTABLE true)
    Q_PROPERTY(float brightness READ getBrightness WRITE setBrightness SCRIPTABLE true)
    Q_PROPERTY(float contrast READ getContrast WRITE setContrast SCRIPTABLE true)

    Q_PROPERTY(QJSValue on_sound_finished READ getOnSoundFinished WRITE setOnSoundFinished SCRIPTABLE true)

  public:
    inline ResourcePack(::ResourcePack &res):
      obj(res)
    {}

    Q_INVOKABLE inline float getGamma() const
    { return obj.getGamma(); }

    Q_INVOKABLE inline float getExposure() const
    { return obj.getExposure(); }

    Q_INVOKABLE inline float getBrightness() const
    { return obj.getBrightness(); }

    Q_INVOKABLE inline float getContrast() const
    { return obj.getContrast(); }

    Q_INVOKABLE inline QJSValue getOnSoundFinished() const
    {
      return soundFinishedFunc;
    }

  public slots:
    void playSound(const QString &soundName, const QJSValue &secondsStart = 0.0, int volume = 33);

    void loopSound(const QString &soundName, const QJSValue &secondsStart = 0.0, int volume = 33);

    inline void pauseSound(const QString &soundName)
    {
      auto snd = obj.getSound(soundName);
      if(snd)
        snd->pause();
    }

    inline void stopSound(const QString &soundName)
    {
      auto snd = obj.getSound(soundName);
      if(snd)
        snd->stop();
    }

    inline void setGamma(float value)
    {
      obj.setGamma(value);
    }

    inline void setExposure(float value)
    {
      obj.setExposure(value);
    }

    inline void setBrightness(float value)
    {
      obj.setBrightness(value);
    }

    inline void setContrast(float value)
    {
      obj.setContrast(value);
    }

    inline void setOnSoundFinished(const QJSValue &func)
    {
      if(func.isCallable() || func.isUndefined() || func.isNull())
        soundFinishedFunc = func;
    }

    void startSkeletalAnimation(const QString &model, const QString &animationName);
    void stopSkeletalAnimation(const QString &model);
    void setSkeletalAnimationPosition(const QString &model, const QString &animationName, double secElapsed);


    //IMPLEMENT
  private:
    struct SkeletalAnimationPermanentInstance
    {
      std::unique_ptr<KawaiiSkeletalAnimation::Instance> anim;
      QString animName;

      SkeletalAnimationPermanentInstance() = default;
      SkeletalAnimationPermanentInstance(SkeletalAnimationPermanentInstance &&orig) = default;
      SkeletalAnimationPermanentInstance& operator=(SkeletalAnimationPermanentInstance &&orig) = default;

      inline SkeletalAnimationPermanentInstance(std::unique_ptr<KawaiiSkeletalAnimation::Instance> &&anim, const QString &animName):
        anim(std::move(anim)),
        animName(animName)
      {}
    };

    ::ResourcePack &obj;
    QJSValue soundFinishedFunc;
    std::unordered_map<QString, SkeletalAnimationPermanentInstance, sib_utils::QtHashFunc<QString>> activeSkeletalAnimations;

    void dropActiveSkeletalAnimation(const QString &model);

    void soundFinished(Sound *snd);
  };
}

#endif // RESOURCEPACK_QML_HPP
