#include "Gearbox_qml.hpp"
#include "Clockwork_qml.hpp"

#include <QJSEngine>
#include <QMetaMethod>
#include <QDynamicPropertyChangeEvent>

#include <clockworks/BarierClockwork.hpp>
#include <clockworks/FrictionClockwork.hpp>
#include <clockworks/HookesLawClockwork.hpp>
#include <clockworks/ViscosityClockwork.hpp>
#include <clockworks/SolidizerClockwork.hpp>
#include <clockworks/GravityAccelerationClockwork.hpp>

KawaiiJSWrappers::Gearbox::Gearbox(::Gearbox &obj, ::World &world):
  world(world),
  obj(&obj),
  type(obj.getSerializableType().data())
{ }

void KawaiiJSWrappers::Gearbox::setType(const QString &newType)
{
  if(type == newType) return;

  type = newType;
  obj = ::Gearbox::create(type, world).release();

  emit typeChanged();
}

const QString &KawaiiJSWrappers::Gearbox::getType() const
{
  return type;
}

Gearbox *KawaiiJSWrappers::Gearbox::getObj() const
{
  return obj;
}

void KawaiiJSWrappers::Gearbox::setObj(::Gearbox *newObj)
{
  obj = newObj;
  if(obj)
    {
      if(auto newType = QString(newObj->getSerializableType().data()); type != newType)
        {
          type = newType;
          emit typeChanged();
        }
    } else
    {
      if(!type.isNull())
        {
          type = QString();
          emit typeChanged();
        }
    }
}

namespace {
  std::optional<float> floatProperty(const QJSValue &val)
  {
    if(val.isNumber())
      return val.toNumber();
    else
      return std::nullopt;
  }

  QVector3D vec3FromJs(QJSValue val)
  {
    return QVector3D(floatProperty(val.property(0)).value_or(0),
                     floatProperty(val.property(1)).value_or(0),
                     floatProperty(val.property(2)).value_or(0));
  }

  std::unique_ptr<Clockwork> createBarierClockwork(const QJSValue &arg)
  {
    auto val = std::make_unique<BarierClockwork>();
    val->min_x = floatProperty(arg.property(QStringLiteral("min_x")));
    val->max_x = floatProperty(arg.property(QStringLiteral("max_x")));
    val->min_y = floatProperty(arg.property(QStringLiteral("min_y")));
    val->max_y = floatProperty(arg.property(QStringLiteral("max_y")));
    val->min_z = floatProperty(arg.property(QStringLiteral("min_z")));
    val->max_z = floatProperty(arg.property(QStringLiteral("max_z")));
    val->frictionFactor = floatProperty(arg.property(QStringLiteral("friction_factor")));
    return val;
  }
}

quint64 KawaiiJSWrappers::Gearbox::addClockwork(const QJSValue &argv)
{
  const auto typeProperty = argv.property(QStringLiteral("type"));
  if(!typeProperty.isString())
    {
      qWarning("WARN: KawaiiJSWrappers::Gearbox::addClockwork: argument must specify the clockwork type!");
      return obj->addClockwork({});
    }

  static const std::unordered_map<std::string, std::function<std::unique_ptr<::Clockwork>(const QJSValue &)>> creators = {
  {"barier", createBarierClockwork },

  {"friction", [](const QJSValue &) { return std::make_unique<FrictionClockwork>(); }},

  {"gravity_acceleration", [](const QJSValue &arg) {
      return std::make_unique<GravityAccelerationClockwork>(vec3FromJs(arg.property(QStringLiteral("acceleration"))));
    }},

  {"hookes_law", [](const QJSValue &) { return std::make_unique<HookesLawClockwork>(); }},

  {"viscosity", [](const QJSValue &arg) {
      return std::make_unique<ViscosityClockwork>(floatProperty(arg.property(QStringLiteral("factor"))).value_or(0.05));
    }},

  {"solidizer", [](const QJSValue &) { return std::make_unique<SolidizerClockwork>(); }},
};

  return obj->addClockwork(creators.at(typeProperty.toString().toLower().toStdString())(argv));
}

quint64 KawaiiJSWrappers::Gearbox::addCustomClockwork(const QString &singleFuncSrc,
                                                      const QString &pairFuncSrc,
                                                      const QString &collisionFuncSrc)
{
  if(auto engine = qjsEngine(this); engine)
    return obj->addClockwork(std::make_unique<Clockwork_qml>(engine, singleFuncSrc, pairFuncSrc, collisionFuncSrc));
  else
    return obj->addClockwork({});
}

QVariant KawaiiJSWrappers::Gearbox::getProperty(const QString &propName) const
{
  const auto propNameUtf8 = propName.toUtf8();
  if(Q_LIKELY(obj))
    {
      const auto &metaObj = obj->metaObject();
      int propertyIndex = metaObj->indexOfProperty(propNameUtf8.constData());
      if(propertyIndex > -1)
        return metaObj->property(propertyIndex).read(obj);
    }
  return QVariant::Invalid;
}

void KawaiiJSWrappers::Gearbox::swapClockworks(quint64 i1, quint64 i2)
{
  obj->swapClockworks(i1, i2);
}

void KawaiiJSWrappers::Gearbox::removeClockwork(quint64 i)
{
  obj->removeClockwork(i);
}

void KawaiiJSWrappers::Gearbox::setProperty(const QString &propName, const QVariant &value)
{
  const auto propNameUtf8 = propName.toUtf8();
  QObject::setProperty(propNameUtf8.constData(), value);
}
