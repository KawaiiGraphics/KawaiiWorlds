#include "Entity_qml.hpp"

#include "Body_qml.hpp"
#include "Landscape_qml.hpp"
#include "LandscapeInfo_qml.hpp"

#include <QBindable>
#include <QQmlEngine>

KawaiiJSWrappers::Entity *KawaiiJSWrappers::Entity::createWrapperObj(::Entity &e, QJSEngine *engine)
{
  KawaiiJSWrappers::Entity *e_wrapped;
  e.changeScriptWrapper([&e, engine, &e_wrapped] (QObject *&wrapper) {
      if(e_wrapped = qobject_cast<KawaiiJSWrappers::Entity*>(wrapper); !e_wrapped)
        {
          if(wrapper)
            delete wrapper;
          wrapper = e_wrapped = new KawaiiJSWrappers::Entity(e, engine);
          if(auto qmlEngine = dynamic_cast<QQmlEngine*>(engine); Q_LIKELY(qmlEngine))
            qmlEngine->setObjectOwnership(wrapper, QQmlEngine::CppOwnership);
        }
    });
  return e_wrapped;
}

QJSValue KawaiiJSWrappers::Entity::createWrapper(::Entity &e, QJSEngine *engine)
{
  if(!engine) return QJSValue::UndefinedValue;

  QJSValue jsVal;
  e.changeScriptWrapper([engine, &jsVal, &e] (QObject *&wrapper) {
      if(!qobject_cast<KawaiiJSWrappers::Entity*>(wrapper))
        {
          if(wrapper)
            delete wrapper;
          wrapper = new KawaiiJSWrappers::Entity(e, engine);
          if(auto qmlEngine = dynamic_cast<QQmlEngine*>(engine); Q_LIKELY(qmlEngine))
            qmlEngine->setObjectOwnership(wrapper, QQmlEngine::CppOwnership);
        }

      jsVal = engine->newQObject(wrapper);
      if(jsVal.isNull())
        {
          delete wrapper;
          wrapper = new KawaiiJSWrappers::Entity(e, engine);
          if(auto qmlEngine = dynamic_cast<QQmlEngine*>(engine); Q_LIKELY(qmlEngine))
            qmlEngine->setObjectOwnership(wrapper, QQmlEngine::CppOwnership);
          jsVal = engine->newQObject(wrapper);
        }
    });

  return jsVal;
}

KawaiiJSWrappers::Entity::Entity(::Entity &e, QJSEngine *engine):
  obj(e),
  engine(engine)
{
  bindableObjectName().setBinding([this] { return obj.objectName(); });

  connect(&obj, &::Entity::objectNameChanged,            this, &Entity::idChanged);
  connect(&obj, &::Entity::rotationChanged,              this, &Entity::rotationChanged);
  connect(&obj, &::Entity::positionChanged,              this, &Entity::positionChanged);
  connect(&obj, &::Entity::velocityChanged,              this, &Entity::velocityChanged);
  connect(&obj, &::Entity::angularVelChanged,            this, &Entity::angularVelChanged);
  connect(&obj, &::Entity::massChanged,                  this, &Entity::massChanged);
  connect(&obj, &::Entity::restitutionChanged,           this, &Entity::restitutionChanged);
  connect(&obj, &::Entity::frictionChanged,              this, &Entity::frictionChanged);
  connect(&obj, &::Entity::frictionRollingChanged,       this, &Entity::frictionRollingChanged);
  connect(&obj, &::Entity::frictionSpinningChanged,       this, &Entity::frictionSpinningChanged);
  connect(&obj, &::Entity::lookForwardChanged,           this, &Entity::lookForwardChanged);
  connect(&obj, &::Entity::clickOpacityChanged,          this, &Entity::clickOpacityChanged);
  connect(&obj, &::Entity::hasContactsChanged,           this, &Entity::hasContactsChanged);
  connect(&obj, &::Entity::lookingForwardFactorXChanged, this, &Entity::lookingForwardFactorXChanged);
  connect(&obj, &::Entity::lookingForwardFactorYChanged, this, &Entity::lookingForwardFactorYChanged);
  connect(&obj, &::Entity::lookingForwardFactorZChanged, this, &Entity::lookingForwardFactorZChanged);

  connect(&obj, &::Entity::tick, this, [engine](::Entity *e, float secElapsed) {
      auto *e_wrapped = createWrapperObj(*e, engine);
      emit e_wrapped->tick(e_wrapped, secElapsed);
    });

  connect(&obj, &::Entity::contact, this, [engine](::Entity *e0, ::Entity *e1) {
      auto *e0_wrapped = createWrapperObj(*e0, engine);
      auto *e1_wrapped = createWrapperObj(*e1, engine);
      emit e0_wrapped->contact(e0_wrapped, e1_wrapped);
    });

  connect(&obj, &::Entity::contactLost, this, [engine](::Entity *e0, ::Entity *e1) {
      auto *e0_wrapped = createWrapperObj(*e0, engine);
      auto *e1_wrapped = createWrapperObj(*e1, engine);
      emit e0_wrapped->contactLost(e0_wrapped, e1_wrapped);
    });

  connect(&obj, &::Entity::pointerPress, this, [engine](::Entity *e) {
      auto *e_wrapped = createWrapperObj(*e, engine);
      emit e_wrapped->pointerPress(e_wrapped);
    });

  connect(&obj, &::Entity::pointerRelease, this, [engine](::Entity *e) {
      auto *e_wrapped = createWrapperObj(*e, engine);
      emit e_wrapped->pointerRelease(e_wrapped);
    });

  connect(&obj, &::Entity::pointerDblClicked, this, [engine](::Entity *e) {
      auto *e_wrapped = createWrapperObj(*e, engine);
      emit e_wrapped->pointerDblClicked(e_wrapped);
    });
}

KawaiiJSWrappers::Entity::~Entity()
{
  obj.changeScriptWrapper([this](QObject*& wrapper) {
    if(wrapper == this)
      wrapper = nullptr;
  });
}

float KawaiiJSWrappers::Entity::getPitch() const
{
  return obj.getEulerAngles().x();
}

float KawaiiJSWrappers::Entity::getYaw() const
{
  return obj.getEulerAngles().y();
}

float KawaiiJSWrappers::Entity::getRoll() const
{
  return obj.getEulerAngles().z();
}

float KawaiiJSWrappers::Entity::getWidth() const
{
  auto bbox = obj.getBBox();
  return bbox.second.x() - bbox.first.x();
}

float KawaiiJSWrappers::Entity::getHeight() const
{
  auto bbox = obj.getBBox();
  return bbox.second.y() - bbox.first.y();
}

float KawaiiJSWrappers::Entity::getDepth() const
{
  auto bbox = obj.getBBox();
  return bbox.second.z() - bbox.first.z();
}

QJSValue KawaiiJSWrappers::Entity::getMetadata(const QString &key) const
{
  if(auto str = obj.getMetadata(key); !str.isNull())
    return str;
  else
    return QJSValue::NullValue;
}

QJSValue KawaiiJSWrappers::Entity::getBody() const
{
  if(auto *body = obj.getBody(); body)
    {
      auto *bodyWrapper = new Body(*body);
      connect(bodyWrapper, &Body::typeChanged, this, [this, bodyWrapper]() { obj.setBody(bodyWrapper->getObj()); });
      connect(&obj, &::Entity::bodyChanged, bodyWrapper, [this, bodyWrapper]() { bodyWrapper->setObj(obj.getBody()); });
      connect(this, &QObject::destroyed, bodyWrapper, &QObject::deleteLater, Qt::DirectConnection);
      return engine->newQObject(bodyWrapper);
    } else
    return QJSValue::NullValue;
}

QJSValue KawaiiJSWrappers::Entity::createBody(const QString &type)
{
  obj.setBody(::Body::create(type).release());
  return getBody();
}

float KawaiiJSWrappers::Entity::distanceToLand(const QJSValue &val)
{
  if(!val.isQObject())
    return std::numeric_limits<float>::quiet_NaN();

  if(auto landscape = qobject_cast<KawaiiJSWrappers::Landscape*>(val.toQObject()); landscape)
    return obj.distanceToLand(&landscape->getObj());
  else if(auto landscapeInfo = qobject_cast<KawaiiJSWrappers::LandscapeInfo*>(val.toQObject()); landscapeInfo)
    return obj.distanceToLand(&landscapeInfo->getObj());
  else
    return std::numeric_limits<float>::quiet_NaN();
}

void KawaiiJSWrappers::Entity::removeBody()
{
  obj.setBody(nullptr);
}

void KawaiiJSWrappers::Entity::setLocation(float x, float y, float z, float pitch, float yaw, float roll)
{
  obj.setEulerAngles(QVector3D(pitch, yaw, roll));
  obj.setPosition(QVector3D(x,y,z));
}

void KawaiiJSWrappers::Entity::setRotation(float pitch, float yaw, float roll)
{
  obj.setEulerAngles(QVector3D(pitch, yaw, roll));
}

void KawaiiJSWrappers::Entity::rotate(float pitch, float yaw, float roll)
{
  obj.changeEulerAngles([pitch, yaw, roll](QVector3D &eulerAngles) {
      eulerAngles += QVector3D(pitch, yaw, roll);
      return true;
    });
}

void KawaiiJSWrappers::Entity::setPitch(float pitch)
{
  setRotation(pitch, getYaw(), getRoll());
}

void KawaiiJSWrappers::Entity::setYaw(float yaw)
{
  setRotation(getPitch(), yaw, getRoll());
}

void KawaiiJSWrappers::Entity::setRoll(float roll)
{
  setRotation(getPitch(), getYaw(), roll);
}

void KawaiiJSWrappers::Entity::setX(float x)
{
  const QVector3D pos = obj.getPosition();
  setPosition(x, pos.y(), pos.z());
}

void KawaiiJSWrappers::Entity::setY(float y)
{
  const QVector3D pos = obj.getPosition();
  setPosition(pos.x(), y, pos.z());
}

void KawaiiJSWrappers::Entity::setZ(float z)
{
  const QVector3D pos = obj.getPosition();
  setPosition(pos.x(), pos.y(), z);
}

void KawaiiJSWrappers::Entity::setVelX(float x)
{
  const QVector3D &vel = obj.getVelocity();
  setVelocity(x, vel.y(), vel.z());
}

void KawaiiJSWrappers::Entity::setVelY(float y)
{
  const QVector3D &vel = obj.getVelocity();
  setVelocity(vel.x(), y, vel.z());
}

void KawaiiJSWrappers::Entity::setVelZ(float z)
{
  const QVector3D &vel = obj.getVelocity();
  setVelocity(vel.x(), vel.y(), z);
}

void KawaiiJSWrappers::Entity::setAngularVelX(float x)
{
  const QVector3D &angleVel = obj.getAngularVel();
  setAngularVelocity(x, angleVel.y(), angleVel.z());
}

void KawaiiJSWrappers::Entity::setAngularVelY(float y)
{
  const QVector3D &angleVel = obj.getAngularVel();
  setAngularVelocity(angleVel.x(), y, angleVel.z());
}

void KawaiiJSWrappers::Entity::setAngularVelZ(float z)
{
  const QVector3D &angleVel = obj.getAngularVel();
  setAngularVelocity(angleVel.x(), angleVel.y(), z);
}

void KawaiiJSWrappers::Entity::setMetadata(const QString &key, const QJSValue &value)
{
  if(value.isUndefined() || value.isNull())
    obj.setMetadata(key, QString());
  else
    obj.setMetadata(key, value.toString());
}

void KawaiiJSWrappers::Entity::allignToLand(const QJSValue &val)
{
  if(!val.isQObject()) return;

  if(auto landscape = qobject_cast<KawaiiJSWrappers::Landscape*>(val.toQObject()); landscape)
    obj.allignToLand(&landscape->getObj());
  else if(auto landscapeInfo = qobject_cast<KawaiiJSWrappers::LandscapeInfo*>(val.toQObject()); landscapeInfo)
    obj.allignToLand(&landscapeInfo->getObj());
}

void KawaiiJSWrappers::Entity::flush()
{
  obj.writeBulletObj();
}

QJSEngine *KawaiiJSWrappers::Entity::getEngine()
{
  if(!engine)
    engine = qjsEngine(this);
  return engine;
}

QJSEngine *KawaiiJSWrappers::Entity::getEngine() const
{
  if(!engine)
    return qjsEngine(this);
  else
    return engine;
}
