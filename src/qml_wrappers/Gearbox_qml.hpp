#ifndef GEARBOX_QML_HPP
#define GEARBOX_QML_HPP


#include "KawaiiWorlds_qml_global.hpp"

#include <QObject>
#include <QJSValue>
#include <QVariant>
#include <clockworks/Gearbox.hpp>

namespace KawaiiJSWrappers {
  class KAWAIIWORLDS_QML_SHARED_EXPORT Gearbox : public QObject
  {
    Q_OBJECT

    Q_PROPERTY(QString type READ getType WRITE setType NOTIFY typeChanged FINAL)

  public:
    Gearbox(::Gearbox &obj, ::World &world);
    virtual ~Gearbox() = default;

    void setType(const QString &newType);
    const QString &getType() const;

    ::Gearbox *getObj() const;
    void setObj(::Gearbox *newObj);

    Q_INVOKABLE quint64 addClockwork(const QJSValue &argv);
    Q_INVOKABLE quint64 addCustomClockwork(const QString &singleFuncSrc, const QString &pairFuncSrc, const QString &collisionFuncSrc);

    Q_INVOKABLE QVariant getProperty(const QString &propName) const;

  public slots:
    void swapClockworks(quint64 i1, quint64 i2);
    void removeClockwork(quint64 i);
    void setProperty(const QString &propName, const QVariant &value);

  signals:
    void typeChanged();



    // IMPLEMENT
  private:
    ::World &world;
    ::Gearbox *obj;
    QString type;
  };
}

#endif // GEARBOX_QML_HPP
