#include "Camera_qml.hpp"
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/quaternion.hpp>
using namespace KawaiiJSWrappers;

Camera::Camera(KawaiiCamera &cam):
  obj(cam),
  owner(true)
{
  connect(&obj, &QObject::destroyed, this, [this]{ owner = false; });
}

Camera::~Camera()
{
  if(owner)
    delete &obj;
}

float Camera::getX() const
{
  return obj.getEyePos().x;
}

float Camera::getY() const
{
  return obj.getEyePos().y;
}

float Camera::getZ() const
{
  return obj.getEyePos().z;
}

QVector3D Camera::getPos() const
{
  return obj.getQEyePos();
}

float Camera::getViewDirX() const
{
  return obj.getViewDir().x;
}

float Camera::getViewDirY() const
{
  return obj.getViewDir().y;
}

float Camera::getViewDirZ() const
{
  return obj.getViewDir().z;
}

QVector3D Camera::getViewDir() const
{
  return obj.getQViewDir();
}

float Camera::getUpDirX() const
{
  return obj.getUpVec().x;
}

float Camera::getUpDirY() const
{
  return obj.getUpVec().y;
}

float Camera::getUpDirZ() const
{
  return obj.getUpVec().z;
}

QVector3D Camera::getUpDir() const
{
  return obj.getQUpVec();
}

KawaiiCamera &Camera::getObj()
{
  return obj;
}

const KawaiiCamera &Camera::getObj() const
{
  return obj;
}

void Camera::setX(float value)
{
  auto pos = obj.getEyePos();
  pos.x = value;
  const auto viewDir = obj.getViewDir();
  const auto upDir = obj.getUpVec();

  obj.setViewMat(glm::lookAt(pos, pos + viewDir, upDir));
}

void Camera::setY(float value)
{
  auto pos = obj.getEyePos();
  pos.y = value;
  const auto viewDir = obj.getViewDir();
  const auto upDir = obj.getUpVec();

  obj.setViewMat(glm::lookAt(pos, pos + viewDir, upDir));
}

void Camera::setZ(float value)
{
  auto pos = obj.getEyePos();
  pos.z = value;
  const auto viewDir = obj.getViewDir();
  const auto upDir = obj.getUpVec();

  obj.setViewMat(glm::lookAt(pos, pos + viewDir, upDir));
}

void Camera::setPos(const QVector3D &value)
{
  const glm::vec3 pos = {value.x(), value.y(), value.z()};
  const auto viewDir = obj.getViewDir();
  const auto upDir = obj.getUpVec();

  obj.setViewMat(glm::lookAt(pos, pos + viewDir, upDir));
}

void Camera::setViewDirX(float value)
{
  const auto pos = obj.getEyePos();
  auto viewDir = obj.getViewDir();
  viewDir.x = value;
  const auto upDir = obj.getUpVec();

  obj.setViewMat(glm::lookAt(pos, pos + viewDir, upDir));
}

void Camera::setViewDirY(float value)
{
  const auto pos = obj.getEyePos();
  auto viewDir = obj.getViewDir();
  viewDir.y = value;
  const auto upDir = obj.getUpVec();

  obj.setViewMat(glm::lookAt(pos, pos + viewDir, upDir));
}

void Camera::setViewDirZ(float value)
{
  const auto pos = obj.getEyePos();
  auto viewDir = obj.getViewDir();
  viewDir.z = value;
  const auto upDir = obj.getUpVec();

  obj.setViewMat(glm::lookAt(pos, pos + viewDir, upDir));
}

void Camera::setViewDir(const QVector3D &value)
{
  const auto pos = obj.getEyePos();
  const glm::vec3 viewDir = {value.x(), value.y(), value.z()};
  const auto upDir = obj.getUpVec();

  obj.setViewMat(glm::lookAt(pos, pos + viewDir, upDir));
}

void Camera::setUpDirX(float value)
{
  const auto pos = obj.getEyePos();
  const auto viewDir = obj.getViewDir();
  auto upDir = obj.getUpVec();
  upDir.x = value;

  obj.setViewMat(glm::lookAt(pos, pos + viewDir, upDir));
}

void Camera::setUpDirY(float value)
{
  const auto pos = obj.getEyePos();
  const auto viewDir = obj.getViewDir();
  auto upDir = obj.getUpVec();
  upDir.y = value;

  obj.setViewMat(glm::lookAt(pos, pos + viewDir, upDir));
}

void Camera::setUpDirZ(float value)
{
  const auto pos = obj.getEyePos();
  const auto viewDir = obj.getViewDir();
  auto upDir = obj.getUpVec();
  upDir.z = value;

  obj.setViewMat(glm::lookAt(pos, pos + viewDir, upDir));
}

void Camera::setUpDir(const QVector3D &value)
{
  const auto pos = obj.getEyePos();
  const auto viewDir = obj.getViewDir();

  obj.setViewMat(glm::lookAt(pos, pos + viewDir, {value.x(), value.y(), value.z()}));
}

void Camera::setMatrix(float m00, float m01, float m02, float m03, float m10, float m11, float m12, float m13, float m20, float m21, float m22, float m23, float m30, float m31, float m32, float m33)
{
  obj.setViewMat(glm::mat4(m00, m01, m02, m03,
                           m10, m11, m12, m13,
                           m20, m21, m22, m23,
                           m30, m31, m32, m33));
}

void Camera::lookAt(float x, float y, float z, float viewDirX, float viewDirY, float viewDirZ, float upDirX, float upDirY, float upDirZ)
{
  const auto pos = glm::vec3(x,y,z);
  obj.setViewMat(glm::lookAt(pos,
                             pos + glm::vec3(viewDirX, viewDirY, viewDirZ),
                             glm::vec3(upDirX, upDirY, upDirZ)));
}

void Camera::translate(float dx, float dy, float dz)
{
  const auto pos = obj.getEyePos() + glm::vec3(dx, dy, dz);
  const auto viewDir = obj.getViewDir();
  const auto upDir = obj.getUpVec();

  obj.setViewMat(glm::lookAt(pos, pos + viewDir, upDir));
}

namespace {
  static constexpr float toRad = 3.14159265358979323846 / 180.0;
}

void Camera::rotateExternalAngles(float pitch, float yaw, float roll)
{
  if(!pitch && !yaw && !roll) return;

  const auto angles0 = glm::eulerAngles(glm::quat(glm::vec3(0,0,1), obj.getViewDir()));

  const auto angles = (glm::vec3(pitch, -yaw, roll) * toRad);

  const auto q = glm::quat(angles0 + angles);

  const auto pos = obj.getEyePos();
  const auto viewDir = q * glm::vec3(0,0,1);
  const auto upDir = q * glm::vec3(0,1,0);

  obj.setViewMat(glm::lookAt(pos, pos + viewDir, upDir));
}

void Camera::rotateInternalAngles(float pitch, float yaw, float roll)
{
  if(!pitch && !yaw && !roll) return;

  const auto angles = (glm::vec3(pitch, yaw, roll) * toRad);

  const auto q = glm::quat(angles);

  const auto pos = obj.getEyePos();
  const auto viewDir = q * obj.getViewDir();
  const auto upDir = q * obj.getUpVec();

  obj.setViewMat(glm::lookAt(pos, pos + viewDir, upDir));
}

void Camera::setAngles(float pitch, float yaw, float roll)
{
  const auto angles = (glm::vec3(pitch, yaw, roll) * toRad);

  const auto q = glm::quat(angles);

  const auto pos = obj.getEyePos();
  const auto viewDir = q * glm::vec3(0,0,1);
  const auto upDir = q * glm::vec3(0,1,0);

  obj.setViewMat(glm::lookAt(pos, pos + viewDir, upDir));
}
