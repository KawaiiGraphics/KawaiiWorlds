#include "Clockwork_qml.hpp"
#include "Entity_qml.hpp"
#include <QJSEngine>

Clockwork::AdapterRegistrator<KawaiiJSWrappers::Clockwork_qml> KawaiiJSWrappers::Clockwork_qml::reg("qml_js");


KawaiiJSWrappers::Clockwork_qml::Clockwork_qml():
  ::Clockwork(reg.adapterId)
{ }

KawaiiJSWrappers::Clockwork_qml::Clockwork_qml(QJSEngine *engine,
                                               const QString &singleFuncSrc, const QString &pairFuncSrc, const QString &collisionFuncSrc):
  ::Clockwork(reg.adapterId),
  engine(engine),
  singleFuncSrc(singleFuncSrc),
  pairFuncSrc(pairFuncSrc),
  collisionFuncSrc(collisionFuncSrc),

  singleFunc(engine->evaluate(singleFuncSrc)),
  pairFunc(engine->evaluate(pairFuncSrc)),
  collisionFunc(engine->evaluate(collisionFuncSrc))
{
}

QString KawaiiJSWrappers::Clockwork_qml::getSingleFuncSrc() const
{
  return singleFuncSrc;
}

void KawaiiJSWrappers::Clockwork_qml::setSingleFuncSrc(const QString &newSingleFuncSrc)
{
  if(singleFuncSrc == newSingleFuncSrc) return;

  singleFuncSrc = newSingleFuncSrc;
  if(Q_LIKELY(engine && !singleFuncSrc.isNull()))
    singleFunc = engine->evaluate(singleFuncSrc);
  else
    singleFunc = QJSValue::UndefinedValue;
}

QString KawaiiJSWrappers::Clockwork_qml::getPairFuncSrc() const
{
  return pairFuncSrc;
}

void KawaiiJSWrappers::Clockwork_qml::setPairFuncSrc(const QString &newPairFuncSrc)
{
  if(pairFuncSrc == newPairFuncSrc) return;

  pairFuncSrc = newPairFuncSrc;
  if(Q_LIKELY(engine && !pairFuncSrc.isNull()))
    pairFunc = engine->evaluate(pairFuncSrc);
  else
    pairFunc = QJSValue::UndefinedValue;
}

QString KawaiiJSWrappers::Clockwork_qml::getCollisionFuncSrc() const
{
  return collisionFuncSrc;
}

void KawaiiJSWrappers::Clockwork_qml::setCollisionFuncSrc(const QString &newCollisionFuncSrc)
{
  if(collisionFuncSrc == newCollisionFuncSrc) return;

  collisionFuncSrc = newCollisionFuncSrc;
  if(Q_LIKELY(engine && !collisionFuncSrc.isNull()))
    collisionFunc = engine->evaluate(collisionFuncSrc);
  else
    collisionFunc = QJSValue::UndefinedValue;
}

void KawaiiJSWrappers::Clockwork_qml::applySingle(::Entity *a, float sec_elapsed)
{
  if(Q_UNLIKELY(!engine))
    if(auto wrapper = a->getScriptWrapper(); wrapper)
      {
        engine = qjsEngine(wrapper);
        if(!singleFuncSrc.isNull())
          singleFunc = engine->evaluate(singleFuncSrc);
      }

  if(Q_LIKELY(engine && singleFunc.isCallable()))
    singleFunc.call({Entity::createWrapper(*a, engine), sec_elapsed});
}

void KawaiiJSWrappers::Clockwork_qml::applyPair(::Entity *a, ::Entity *b, float sec_elapsed)
{
  if(Q_UNLIKELY(!engine))
    if(auto wrapper = a->getScriptWrapper(); wrapper)
      {
        engine = qjsEngine(wrapper);
        if(!pairFuncSrc.isNull())
          pairFunc = engine->evaluate(pairFuncSrc);
      }

  if(Q_LIKELY(engine && pairFunc.isCallable()))
    pairFunc.call({Entity::createWrapper(*a, engine), Entity::createWrapper(*b, engine), sec_elapsed});
}

void KawaiiJSWrappers::Clockwork_qml::applyCollision(const CollisionInfo &collision, float sec_elapsed)
{
  if(Q_UNLIKELY(!engine))
    {
      ::Entity *a = collision.a;
      if(!a)
        a = collision.b;
      if(a)
        if(auto wrapper = a->getScriptWrapper(); wrapper)
          {
            engine = qjsEngine(wrapper);
            if(!collisionFuncSrc.isNull())
              collisionFunc = engine->evaluate(collisionFuncSrc);
          }
    }

  if(Q_LIKELY(engine && collisionFunc.isCallable()))
    collisionFunc.call({collisionToJs(collision), sec_elapsed});
}

QJSValue KawaiiJSWrappers::Clockwork_qml::vec3ToJs(const QVector3D &a) const
{
  QJSValue vec = engine->newArray(3);
  vec.setProperty(0, a.x());
  vec.setProperty(1, a.y());
  vec.setProperty(2, a.z());
  vec.setProperty(QStringLiteral("x"), a.x());
  vec.setProperty(QStringLiteral("y"), a.y());
  vec.setProperty(QStringLiteral("z"), a.z());
  return vec;
}

QJSValue KawaiiJSWrappers::Clockwork_qml::collisionToJs(const CollisionInfo &collision) const
{
  QJSValue obj = engine->newObject();
  obj.setProperty(QStringLiteral("a"),
                  collision.a
                  ? Entity::createWrapper(*collision.a, engine)
                  : QJSValue::NullValue);

  obj.setProperty(QStringLiteral("b"),
                  collision.b
                  ? Entity::createWrapper(*collision.b, engine)
                  : QJSValue::NullValue);

  obj.setProperty(QStringLiteral("normal"), vec3ToJs(collision.normal));
  obj.setProperty(QStringLiteral("overlap"), vec3ToJs(collision.overlap));
  obj.setProperty(QStringLiteral("friction"), collision.friction);
  return obj;
}


namespace KawaiiJSWrappers {
KAWAIIWORLDS_QML_SHARED_EXPORT QDataStream& operator<<(QDataStream &st, const Clockwork_qml &obj)
{
  return st << obj.getSingleFuncSrc() << obj.getPairFuncSrc() << obj.getCollisionFuncSrc();
}

KAWAIIWORLDS_QML_SHARED_EXPORT QDataStream& operator>>(QDataStream &st, Clockwork_qml &obj)
{
  QString str;
  st >> str;
  obj.setSingleFuncSrc(str);

  st >> str;
  obj.setPairFuncSrc(str);

  st >> str;
  obj.setCollisionFuncSrc(str);

  return st;
}
}
