#include "Body_qml.hpp"

#include <QMetaMethod>
#include <QDynamicPropertyChangeEvent>

KawaiiJSWrappers::Body::Body(::Body &obj):
  obj(&obj),
  type(obj.getSerializableType().data())
{ }

void KawaiiJSWrappers::Body::setType(const QString &newType)
{
  if(type == newType) return;

  type = newType;
  obj = ::Body::create(type).release();

  emit typeChanged();
}

const QString &KawaiiJSWrappers::Body::getType() const
{
  return type;
}

Body *KawaiiJSWrappers::Body::getObj() const
{
  return obj;
}

void KawaiiJSWrappers::Body::setObj(::Body *newObj)
{
  obj = newObj;
  if(obj)
    {
      if(auto newType = QString(newObj->getSerializableType().data()); type != newType)
        {
          type = newType;
          emit typeChanged();
        }
    } else
    {
      if(!type.isNull())
        {
          type = QString();
          emit typeChanged();
        }
    }
}

QVariant KawaiiJSWrappers::Body::getProperty(const QString &propName) const
{
  if(Q_LIKELY(obj))
    return obj->getProperty(propName.toUtf8());
  return {};
}

void KawaiiJSWrappers::Body::setProperty(const QString &propName, const QVariant &value)
{
  if(Q_LIKELY(obj))
    obj->setProperty(propName.toUtf8(), value);
}
