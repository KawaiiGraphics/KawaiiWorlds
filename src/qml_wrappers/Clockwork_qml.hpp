#ifndef CLOCKWORK_QML_HPP
#define CLOCKWORK_QML_HPP

#include <clockworks/Clockwork.hpp>
#include <QJSValue>
#include "KawaiiWorlds_qml_global.hpp"

namespace KawaiiJSWrappers {
  class KAWAIIWORLDS_QML_SHARED_EXPORT Clockwork_qml: public ::Clockwork
  {
    static AdapterRegistrator<Clockwork_qml> reg;
  public:
    Clockwork_qml();
    Clockwork_qml(QJSEngine *engine, const QString &singleFuncSrc, const QString &pairFuncSrc, const QString &collisionFuncSrc);

    QString getSingleFuncSrc() const;
    void setSingleFuncSrc(const QString &newSingleFuncSrc);

    QString getPairFuncSrc() const;
    void setPairFuncSrc(const QString &newPairFuncSrc);

    QString getCollisionFuncSrc() const;
    void setCollisionFuncSrc(const QString &newCollisionFuncSrc);

    // Clockwork interface
  public:
    void applySingle(::Entity *a, float sec_elapsed) override final;
    void applyPair(::Entity *a, ::Entity *b, float sec_elapsed) override final;
    void applyCollision(const CollisionInfo &collision, float sec_elapsed) override final;



    // IMPLEMENT
  private:
    QJSEngine *engine;

    QString singleFuncSrc;
    QString pairFuncSrc;
    QString collisionFuncSrc;

    QJSValue singleFunc;
    QJSValue pairFunc;
    QJSValue collisionFunc;

    QJSValue vec3ToJs(const QVector3D &a) const;
    QJSValue collisionToJs(const CollisionInfo &collision) const;
  };

  KAWAIIWORLDS_QML_SHARED_EXPORT QDataStream& operator<<(QDataStream &st, const Clockwork_qml &obj);
  KAWAIIWORLDS_QML_SHARED_EXPORT QDataStream& operator>>(QDataStream &st, Clockwork_qml &obj);
}


#endif // CLOCKWORK_QML_HPP
