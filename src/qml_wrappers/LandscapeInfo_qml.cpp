#include "LandscapeInfo_qml.hpp"

KawaiiJSWrappers::LandscapeInfo::LandscapeInfo(::LandscapeInfo &info):
  info(info)
{
}

KawaiiJSWrappers::LandscapeInfo::~LandscapeInfo()
{
}

float KawaiiJSWrappers::LandscapeInfo::getX() const
{
  return info.pos.x();
}

float KawaiiJSWrappers::LandscapeInfo::getY() const
{
  return info.pos.y();
}

float KawaiiJSWrappers::LandscapeInfo::getZ() const
{
  return info.pos.z();
}

float KawaiiJSWrappers::LandscapeInfo::getWidth() const
{
  return info.size.x();
}

float KawaiiJSWrappers::LandscapeInfo::getHeight() const
{
  return info.size.y();
}

float KawaiiJSWrappers::LandscapeInfo::getDepth() const
{
  return info.size.z();
}

QString KawaiiJSWrappers::LandscapeInfo::getMaterial() const
{
  return info.material;
}

QString KawaiiJSWrappers::LandscapeInfo::getHeightMap() const
{
  return info.heightMapTexture;
}

QString KawaiiJSWrappers::LandscapeInfo::getId() const
{
  return info.id;
}

float KawaiiJSWrappers::LandscapeInfo::getStep() const
{
  return info.step;
}

QJSValue KawaiiJSWrappers::LandscapeInfo::getMetadata(const QString &key) const
{
  if(auto el = info.metadata.find(key); el != info.metadata.end())
    return el.value();
  else
    return QJSValue::UndefinedValue;
}

QVector2D KawaiiJSWrappers::LandscapeInfo::getChunkSize() const
{
  return info.chunkSize;
}

void KawaiiJSWrappers::LandscapeInfo::setX(float val)
{
  info.pos.setX(val);
}

void KawaiiJSWrappers::LandscapeInfo::setY(float val)
{
  info.pos.setY(val);
}

void KawaiiJSWrappers::LandscapeInfo::setZ(float val)
{
  info.pos.setZ(val);
}

void KawaiiJSWrappers::LandscapeInfo::setWidth(float val)
{
  info.size.setX(val);
}

void KawaiiJSWrappers::LandscapeInfo::setHeight(float val)
{
  info.size.setY(val);
}

void KawaiiJSWrappers::LandscapeInfo::setDepth(float val)
{
  info.size.setZ(val);
}

void KawaiiJSWrappers::LandscapeInfo::setMaterial(const QString &val)
{
  info.material = val;
}

void KawaiiJSWrappers::LandscapeInfo::setHeightMap(const QString &val)
{
  info.heightMapTexture = val;
}

void KawaiiJSWrappers::LandscapeInfo::setId(const QString &value)
{
  info.id = value;
}

void KawaiiJSWrappers::LandscapeInfo::setStep(float step)
{
  info.step = step;
}

void KawaiiJSWrappers::LandscapeInfo::setMetadata(const QString &key, QJSValue value)
{
  if(value.isUndefined() || value.isNull())
    info.metadata.remove(key);
  else
    info.metadata[key] = value.toString();
}

void KawaiiJSWrappers::LandscapeInfo::setChunkSize(const QVector2D &newChunkSize)
{
  info.chunkSize = newChunkSize;
}
