#include "ResourcePack_qml.hpp"
#include <QJSValue>

void KawaiiJSWrappers::ResourcePack::playSound(const QString &soundName, const QJSValue &secondsStart, int volume)
{
  if(soundName.isNull())
    {
      qWarning().noquote() << "Attempt to play NULL sound";
      return;
    }

  auto snd = obj.getSound(soundName);
  if(snd)
    {
      snd->setAutoRepeat(false);
      connect(snd, &Sound::finished, this, &ResourcePack::soundFinished, Qt::UniqueConnection);
      if(secondsStart.isUndefined() || (secondsStart.isString() && secondsStart.toString() == QLatin1String("random")))
        snd->play(snd->random_pos, volume);
      else if(secondsStart.isNull())
        snd->play(std::chrono::milliseconds(0), volume);
      else
        snd->play(std::chrono::milliseconds(static_cast<quint64>(secondsStart.toNumber() * 1000.0)), volume);
    } else
    qCritical().noquote() << "Sound \"" << soundName << "\" not found!";
}

void KawaiiJSWrappers::ResourcePack::loopSound(const QString &soundName, const QJSValue &secondsStart, int volume)
{
  auto snd = obj.getSound(soundName);
  if(snd)
    {
      snd->setAutoRepeat(true);
      if(secondsStart.isUndefined() || (secondsStart.isString() && secondsStart.toString() == QLatin1String("random")))
        snd->play(snd->random_pos, volume);
      else if(secondsStart.isNull())
        snd->play(std::chrono::milliseconds(0), volume);
      else
        snd->play(std::chrono::milliseconds(static_cast<quint64>(secondsStart.toNumber() * 1000.0)), volume);
    } else
    qCritical().noquote() << "Sound \"" << soundName << "\" not found!";
}

void KawaiiJSWrappers::ResourcePack::startSkeletalAnimation(const QString &model, const QString &animationName)
{
  dropActiveSkeletalAnimation(model);
  obj.startSkeletalAnimation(model, animationName);
}

void KawaiiJSWrappers::ResourcePack::stopSkeletalAnimation(const QString &model)
{
  dropActiveSkeletalAnimation(model);
  obj.stopSkeletalAnimation(model);
}

void KawaiiJSWrappers::ResourcePack::setSkeletalAnimationPosition(const QString &model, const QString &animationName, double secElapsed)
{
  auto el = activeSkeletalAnimations.find(model);
  if(el == activeSkeletalAnimations.end())
    el = activeSkeletalAnimations.emplace(
          std::piecewise_construct,
          std::forward_as_tuple(model),
          std::forward_as_tuple(obj.getSkeletalAnimation(model, animationName), animationName)
          ).first;

  if(el->second.animName != animationName)
    {
      el->second.anim = obj.getSkeletalAnimation(model, animationName);
      el->second.animName = animationName;
    }
  el->second.anim->tick(secElapsed);
}

void KawaiiJSWrappers::ResourcePack::dropActiveSkeletalAnimation(const QString &model)
{
  if(auto el = activeSkeletalAnimations.find(model); el != activeSkeletalAnimations.end())
    activeSkeletalAnimations.erase(el);
}

void KawaiiJSWrappers::ResourcePack::soundFinished(Sound *snd)
{
  if(soundFinishedFunc.isCallable())
    {
      QJSValue arg = snd? QJSValue(snd->objectName()): QJSValue::NullValue;
      soundFinishedFunc.call({ arg });
    }
}
