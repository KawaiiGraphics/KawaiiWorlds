#ifndef LOCATION_QML_HPP
#define LOCATION_QML_HPP

#include "KawaiiWorlds_qml_global.hpp"
#include <Location.hpp>
#include <QJSValue>

namespace KawaiiJSWrappers {
  class KAWAIIWORLDS_QML_SHARED_EXPORT Location: public QObject
  {
    Q_OBJECT

  public:
    static QJSValue createWrapper(::Location &l, QJSEngine *engine);

    Location(::Location &l, QJSEngine *engine);
    ~Location();

    inline ::Location& getObj() const
    { return obj; }

    Q_INVOKABLE QJSValue createEntity(const QJSValue &body, const QString &id, float mass = 1);

    Q_INVOKABLE QJSValue createLandscapeInfo(const QString &material, const QString &heightMapTexture, float width, float height, float depth, bool createCollisionShape = false);

    Q_INVOKABLE QJSValue findEntity(const QString &id);
    Q_INVOKABLE QJSValue findLandscapeInfo(const QString &id);

  public slots:
    void removeEntity(const QJSValue &entity);
    void forallEntities(const QJSValue &func);

    void removeLandscapeInfo(const QJSValue &landscape);
    void forallLandscapes(const QJSValue &func);

    void remove();

  signals:
    void entityAdded(const QJSValue &e);
    void entityRemoved(const QJSValue &e);

    void landscapeAdded(const QJSValue &landscape);
    void landscapeRemoved(const QJSValue &landscape);



  private:
    ::Location &obj;
    QJSEngine *engine;

    QJSEngine *getEngine();
  };
}

#endif // LOCATION_QML_HPP
