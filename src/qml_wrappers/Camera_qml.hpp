#ifndef CAMERAWRAPPER_HPP
#define CAMERAWRAPPER_HPP

#include "KawaiiWorlds_qml_global.hpp"

#include <QObject>
#include <QVector3D>

#include <Kawaii3D/KawaiiCamera.hpp>

namespace KawaiiJSWrappers {
  class KAWAIIWORLDS_QML_SHARED_EXPORT Camera: public QObject
  {
    Q_OBJECT

    Q_PROPERTY(float x       READ getX   WRITE setX   SCRIPTABLE true)
    Q_PROPERTY(float y       READ getY   WRITE setY   SCRIPTABLE true)
    Q_PROPERTY(float z       READ getZ   WRITE setZ   SCRIPTABLE true)
    Q_PROPERTY(QVector3D pos READ getPos WRITE setPos SCRIPTABLE true)

    Q_PROPERTY(float viewDirX    READ getViewDirX WRITE setViewDirX SCRIPTABLE true)
    Q_PROPERTY(float viewDirY    READ getViewDirY WRITE setViewDirY SCRIPTABLE true)
    Q_PROPERTY(float viewDirZ    READ getViewDirZ WRITE setViewDirZ SCRIPTABLE true)
    Q_PROPERTY(QVector3D viewDir READ getViewDir  WRITE setViewDir  SCRIPTABLE true)

    Q_PROPERTY(float     upDirX READ getUpDirX WRITE setUpDirX SCRIPTABLE true)
    Q_PROPERTY(float     upDirY READ getUpDirY WRITE setUpDirY SCRIPTABLE true)
    Q_PROPERTY(float     upDirZ READ getUpDirZ WRITE setUpDirZ SCRIPTABLE true)
    Q_PROPERTY(QVector3D upDir  READ getUpDir  WRITE setUpDir  SCRIPTABLE true)

  public:
    Camera(KawaiiCamera &cam);
    virtual ~Camera();

    Q_INVOKABLE float getX() const;
    Q_INVOKABLE float getY() const;
    Q_INVOKABLE float getZ() const;
    Q_INVOKABLE QVector3D getPos() const;

    Q_INVOKABLE float getViewDirX() const;
    Q_INVOKABLE float getViewDirY() const;
    Q_INVOKABLE float getViewDirZ() const;
    Q_INVOKABLE QVector3D getViewDir() const;

    Q_INVOKABLE float getUpDirX() const;
    Q_INVOKABLE float getUpDirY() const;
    Q_INVOKABLE float getUpDirZ() const;
    Q_INVOKABLE QVector3D getUpDir() const;

    KawaiiCamera &getObj();
    const KawaiiCamera &getObj() const;

  public slots:
    void setX(float value);
    void setY(float value);
    void setZ(float value);
    void setPos(const QVector3D &value);

    void setViewDirX(float value);
    void setViewDirY(float value);
    void setViewDirZ(float value);
    void setViewDir(const QVector3D &value);

    void setUpDirX(float value);
    void setUpDirY(float value);
    void setUpDirZ(float value);
    void setUpDir(const QVector3D &value);

    void setMatrix(float m00, float m01, float m02, float m03,
                   float m10, float m11, float m12, float m13,
                   float m20, float m21, float m22, float m23,
                   float m30, float m31, float m32, float m33);

    void lookAt(float x, float y, float z,
                float viewDirX, float viewDirY, float viewDirZ,
                float upDirX, float upDirY, float upDirZ);

    void translate(float dx, float dy, float dz);

    void rotateExternalAngles(float pitch, float yaw, float roll);
    void rotateInternalAngles(float pitch, float yaw, float roll);
    void setAngles(float pitch, float yaw, float roll);



    //IMPLEMENT
  private:
    KawaiiCamera &obj;
    bool owner;
  };
}

#endif // CAMERAWRAPPER_HPP
