#ifndef ILLUMINATIONMODEL_QML_HPP
#define ILLUMINATIONMODEL_QML_HPP

#include <QObject>
#include <QJSValue>

#include <IlluminationModel.hpp>
#include "KawaiiWorlds_qml_global.hpp"

namespace KawaiiJSWrappers {
  class KAWAIIWORLDS_QML_SHARED_EXPORT IlluminationModel: public QObject
  {
    Q_OBJECT

    Q_PROPERTY(quint64 dir_light_count READ getDirLightCount SCRIPTABLE true)
    Q_PROPERTY(quint64 dot_light_count READ getDotLightCount SCRIPTABLE true)
    Q_PROPERTY(quint64 spot_light_count READ getSpotLightCount SCRIPTABLE true)
    Q_PROPERTY(QVector3D dir_lights_shadow_center READ getDirLightsShadowCenter WRITE setDirLightsShadowCenter SCRIPTABLE true)
    Q_PROPERTY(QVector3D dir_lights_shadow_area READ getDirLightsShadowArea WRITE setDirLightsShadowArea SCRIPTABLE true)

  public:
    static QJSValue createWrapper(::IlluminationModelBase &illum, QJSEngine *engine);

    inline IlluminationModel(::IlluminationModelBase &obj):
      obj(obj),
      engine(nullptr)
    {
      setObjectName(obj.getName());
    }

    ~IlluminationModel()
    {}

    Q_INVOKABLE QJSValue readDirLight(size_t i) const;
    Q_INVOKABLE QJSValue readDotLight(size_t i) const;
    Q_INVOKABLE QJSValue readSpotLight(size_t i) const;

    inline quint64 getDirLightCount() const
    { return obj.getDirLightCount(); }

    inline quint64 getDotLightCount() const
    { return obj.getDotLightCount(); }

    inline quint64 getSpotLightCount() const
    { return obj.getSpotLightCount(); }

    Q_INVOKABLE QVector3D getDirLightsShadowCenter() const;
    Q_INVOKABLE QVector3D getDirLightsShadowArea() const;

  public slots:
    void createDirLight(const QJSValue &val);
    void createDotLight(const QJSValue &val);
    void createSpotLight(const QJSValue &val);

    void updateDirLight(quint64 i, const QJSValue &val);
    void updateDotLight(quint64 i, const QJSValue &val);
    void updateSpotLight(quint64 i, const QJSValue &val);

    inline void deleteDirLight(quint64 i)
    { obj.removeDirLight(i); }
    inline void deleteDotLight(quint64 i)
    { obj.removeDotLight(i); }
    inline void deleteSpotLight(quint64 i)
    { obj.removeSpotLight(i); }

    void setDirLightsShadowCenter(const QVector3D &newDirLightsShadowCenter);
    void setDirLightsShadowArea(const QVector3D &newDirLightsShadowArea);

  private:
    IlluminationModelBase &obj;
    QJSEngine *engine;

    QJSEngine *getEngine();
    QJSEngine *getEngine() const;
  };
}

#endif // ILLUMINATIONMODEL_QML_HPP
