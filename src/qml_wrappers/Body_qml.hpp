#ifndef BODY_QML_HPP
#define BODY_QML_HPP

#include "KawaiiWorlds_qml_global.hpp"

#include <bodies/Body.hpp>
#include <QVariant>
#include <QObject>

namespace KawaiiJSWrappers {

  class KAWAIIWORLDS_QML_SHARED_EXPORT Body : public QObject
  {
    Q_OBJECT

    Q_PROPERTY(QString type READ getType WRITE setType NOTIFY typeChanged FINAL)

  public:
    Body(::Body &obj);

    void setType(const QString &newType);
    const QString &getType() const;

    ::Body *getObj() const;
    void setObj(::Body *newObj);

    Q_INVOKABLE QVariant getProperty(const QString &propName) const;

  public slots:
    void setProperty(const QString &propName, const QVariant &value);

  signals:
    void typeChanged();



  private:
    ::Body *obj;
    QString type;
  };

}

#endif // BODY_QML_HPP
