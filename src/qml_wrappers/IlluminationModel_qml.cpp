#include "IlluminationModel_qml.hpp"
#include <Kawaii3D/Illumination/KawaiiPBR.hpp>
#include <Kawaii3D/Illumination/KawaiiCel.hpp>
#include <Kawaii3D/Illumination/KawaiiPhong.hpp>
#include <QJSEngine>

namespace {
  static const constexpr double pi = 3.141592653589793238462643383279502884L;
  static const constexpr double toDegrees = 180.0L / pi;
  static const constexpr double toRadians = pi / 180.0L;

  QJSValue vec3_to_JS(const QVector3D &val, QJSEngine &engine)
  {
    auto result = engine.newArray(3);
    result.setProperty(0, val.x());
    result.setProperty(1, val.y());
    result.setProperty(2, val.z());
    return result;
  }

  template<typename T>
  void readLight(QJSEngine &engine, QJSValue &val, const T &origin);

  template<>
  void readLight(QJSEngine &engine, QJSValue &val, const KawaiiPbrLight &origin)
  {
    val.setProperty(QStringLiteral("color"), vec3_to_JS(origin.color, engine));
  }

  template<>
  void readLight(QJSEngine &engine, QJSValue &val, const KawaiiPhongLight &origin)
  {
    val.setProperty(QStringLiteral("diffuse"), vec3_to_JS(origin.diffuse, engine));
    val.setProperty(QStringLiteral("specular"), vec3_to_JS(origin.specular, engine));
  }

  template<>
  void readLight(QJSEngine &engine, QJSValue &val, const KawaiiCelLight &origin)
  {
    val.setProperty(QStringLiteral("diffuse"), vec3_to_JS(origin.diffuse, engine));
    val.setProperty(QStringLiteral("specular"), vec3_to_JS(origin.specular, engine));
  }

  template<typename T>
  std::optional<QJSValue> readDirLight(QJSEngine *engine, size_t i, IlluminationModelBase &obj)
  {
    if(auto *ptr = dynamic_cast<::IlluminationModel<T>*>(&obj); ptr)
      {
        auto readDirL = [engine] (QJSValue &val, const KawaiiDirLight &l) {
          val.setProperty(QStringLiteral("ambient"), vec3_to_JS(l.ambient, *engine));
          val.setProperty(QStringLiteral("direction"), vec3_to_JS(l.direction, *engine));
        };

        auto val = engine->newObject();
        auto &origin = ptr->getDirLight(i);
        readLight<T>(*engine, val, origin);
        readDirL(val, origin);
        return val;
      } else
      return std::nullopt;
  }

  template<typename T>
  std::optional<QJSValue> readDotLight(QJSEngine *engine, size_t i, IlluminationModelBase &obj)
  {
    if(auto *ptr = dynamic_cast<::IlluminationModel<T>*>(&obj); ptr)
      {
        auto readDotL = [engine] (QJSValue &val, const KawaiiLamp &l) {
          val.setProperty(QStringLiteral("ambient"), vec3_to_JS(l.ambient, *engine));
          val.setProperty(QStringLiteral("position"), vec3_to_JS(l.position, *engine));
          val.setProperty(QStringLiteral("const_attenuation"), l.constAttenuation);
          val.setProperty(QStringLiteral("linear_attenuation"), l.linearAttenuation);
          val.setProperty(QStringLiteral("quadratic_attenuation"), l.quadraticAttenuation);
        };

        auto val = engine->newObject();
        auto &origin = ptr->getDotLight(i);
        readLight<T>(*engine, val, origin);
        readDotL(val, origin);
        return val;
      } else
      return std::nullopt;
  }

  template<typename T>
  std::optional<QJSValue> readSpotLight(QJSEngine *engine, size_t i, IlluminationModelBase &obj)
  {
    if(auto *ptr = dynamic_cast<::IlluminationModel<T>*>(&obj); ptr)
      {
        auto readSpotL = [engine] (QJSValue &val, const KawaiiProjector &l) {
          val.setProperty(QStringLiteral("ambient"), vec3_to_JS(l.ambient, *engine));
          val.setProperty(QStringLiteral("direction"), vec3_to_JS(l.direction, *engine));
          val.setProperty(QStringLiteral("position"), vec3_to_JS(l.position, *engine));
          val.setProperty(QStringLiteral("const_attenuation"), l.constAttenuation);
          val.setProperty(QStringLiteral("linear_attenuation"), l.linearAttenuation);
          val.setProperty(QStringLiteral("quadratic_attenuation"), l.quadraticAttenuation);
          val.setProperty(QStringLiteral("spot_exponent"), l.spotExponent);
          val.setProperty(QStringLiteral("spot_cutoff"), l.spotCutoffRadians * toDegrees);
        };

        auto val = engine->newObject();
        auto &origin = ptr->getSpotLight(i);
        readLight<T>(*engine, val, origin);
        readSpotL(val, origin);
        return val;
      } else
      return std::nullopt;
  }
}

QJSValue KawaiiJSWrappers::IlluminationModel::createWrapper(IlluminationModelBase &illum, QJSEngine *engine)
{
  if(!engine) return QJSValue::UndefinedValue;

  if(!illum.jsWrapper)
    {
      auto ptr = new KawaiiJSWrappers::IlluminationModel(illum);
      ptr->engine = engine;
      illum.jsWrapper = ptr;
    }
  QJSValue jsVal = engine->newQObject(illum.jsWrapper.data());
  if(jsVal.isNull())
    {
      delete illum.jsWrapper;
      auto wrapper = new KawaiiJSWrappers::IlluminationModel(illum);
      wrapper->engine = engine;
      illum.jsWrapper = wrapper;
      jsVal = engine->newQObject(illum.jsWrapper.data());
    }
  return jsVal;
}

QJSValue KawaiiJSWrappers::IlluminationModel::readDirLight(size_t i) const
{
  auto engine = getEngine();
  if(!engine || i >= obj.getDirLightCount())
    return QJSValue::UndefinedValue;

  if(auto val = ::readDirLight<KawaiiPbrLight>(engine, i, obj); val.has_value())
    return *val;
  else if(auto val = ::readDirLight<KawaiiPhongLight>(engine, i, obj); val.has_value())
    return *val;
  else if(auto val = ::readDirLight<KawaiiCelLight>(engine, i, obj); val.has_value())
    return *val;
  else
    return QJSValue::UndefinedValue;
}

QJSValue KawaiiJSWrappers::IlluminationModel::readDotLight(size_t i) const
{
  auto engine = getEngine();
  if(!engine || i >= obj.getDotLightCount())
    return QJSValue::UndefinedValue;

  if(auto val = ::readDotLight<KawaiiPbrLight>(engine, i, obj); val.has_value())
    return *val;
  else if(auto val = ::readDotLight<KawaiiPhongLight>(engine, i, obj); val.has_value())
    return *val;
  else if(auto val = ::readDotLight<KawaiiCelLight>(engine, i, obj); val.has_value())
    return *val;
  else
    return QJSValue::UndefinedValue;
}

QJSValue KawaiiJSWrappers::IlluminationModel::readSpotLight(size_t i) const
{
  auto engine = getEngine();
  if(!engine || i >= obj.getSpotLightCount())
    return QJSValue::UndefinedValue;

  if(auto val = ::readSpotLight<KawaiiPbrLight>(engine, i, obj); val.has_value())
    return *val;
  else if(auto val = ::readSpotLight<KawaiiPhongLight>(engine, i, obj); val.has_value())
    return *val;
  else if(auto val = ::readSpotLight<KawaiiCelLight>(engine, i, obj); val.has_value())
    return *val;
  else
    return QJSValue::UndefinedValue;
}

QVector3D KawaiiJSWrappers::IlluminationModel::getDirLightsShadowCenter() const
{
  if(obj.getShadowMapper())
    return obj.getShadowMapper()->getDirLightsCenter();
  else
    return {std::numeric_limits<float>::quiet_NaN(),
            std::numeric_limits<float>::quiet_NaN(),
            std::numeric_limits<float>::quiet_NaN()};
}

QVector3D KawaiiJSWrappers::IlluminationModel::getDirLightsShadowArea() const
{
  if(obj.getShadowMapper())
    return obj.getShadowMapper()->getDirLightsArea();
  else
    return {std::numeric_limits<float>::quiet_NaN(),
            std::numeric_limits<float>::quiet_NaN(),
            std::numeric_limits<float>::quiet_NaN()};
}

void KawaiiJSWrappers::IlluminationModel::createDirLight(const QJSValue &val)
{
  if(!val.isObject()) return;

  size_t i = obj.getDirLightCount();
  obj.addDirLight();
  updateDirLight(i, val);
}

void KawaiiJSWrappers::IlluminationModel::createDotLight(const QJSValue &val)
{
  if(!val.isObject()) return;

  size_t i = obj.getDotLightCount();
  obj.addDotLight();
  updateDotLight(i, val);
}

void KawaiiJSWrappers::IlluminationModel::createSpotLight(const QJSValue &val)
{
  if(!val.isObject()) return;

  size_t i = obj.getDotLightCount();
  obj.addSpotLight();
  updateSpotLight(i, val);
}

namespace {
  QVector3D vec3_from_JS(const QJSValue &val, const QVector3D &defaultVal)
  {
    if(val.isArray())
      {
        if(val.property(QStringLiteral("length")).toUInt() == 3)
          {
            return QVector3D(val.property(0).toNumber(),
                             val.property(1).toNumber(),
                             val.property(2).toNumber());
          } else
          if(val.property(QStringLiteral("length")).toUInt() == 1)
            {
              return QVector3D(val.property(0).toNumber(),
                               val.property(0).toNumber(),
                               val.property(0).toNumber());
            }
      } else if(val.isNumber())
      {
        return QVector3D(val.toNumber(),
                         val.toNumber(),
                         val.toNumber());
      }

    return defaultVal;
  }

  float float_from_JS(const QJSValue &val, float defaultVal)
  {
    if(val.isUndefined())
      return defaultVal;
    else
      return val.toNumber();
  }

  template<typename T>
  void changeLight(const QJSValue &val, T &l);

  template<>
  void changeLight(const QJSValue &val, KawaiiPbrLight &l)
  {
    l.color = vec3_from_JS(val.property(QStringLiteral("color")), l.color);
  }

  template<>
  void changeLight(const QJSValue &val, KawaiiPhongLight &l)
  {
    l.diffuse = vec3_from_JS(val.property(QStringLiteral("diffuse")), l.diffuse);
    l.specular = vec3_from_JS(val.property(QStringLiteral("specular")), l.specular);
  }

  template<>
  void changeLight(const QJSValue &val, KawaiiCelLight &l)
  {
    l.diffuse = vec3_from_JS(val.property(QStringLiteral("diffuse")), l.diffuse);
    l.specular = vec3_from_JS(val.property(QStringLiteral("specular")), l.specular);
  }

  template<typename T>
  bool changeDirLight(IlluminationModelBase &obj, size_t i, const QJSValue &val)
  {
    if(auto *ptr = dynamic_cast<::IlluminationModel<T>*>(&obj); ptr)
      {
        ptr->changeDirLight(i, [&val] (typename KawaiiIlluminationStructs<T>::DirLight &l) {
            changeLight<T>(val, l);
            l.ambient = vec3_from_JS(val.property(QStringLiteral("ambient")), l.ambient);
            l.direction = vec3_from_JS(val.property(QStringLiteral("direction")), l.direction).normalized();
            return true;
          });
        return true;
      } else
      return false;
  }

  template<typename T>
  bool changeDotLight(IlluminationModelBase &obj, size_t i, const QJSValue &val)
  {
    if(auto *ptr = dynamic_cast<::IlluminationModel<T>*>(&obj); ptr)
      {
        ptr->changeDotLight(i, [&val] (typename KawaiiIlluminationStructs<T>::Lamp &l) {
            changeLight<T>(val, l);
            l.ambient = vec3_from_JS(val.property(QStringLiteral("ambient")), l.ambient);
            l.position = vec3_from_JS(val.property(QStringLiteral("position")), l.position);
            l.constAttenuation = float_from_JS(val.property(QStringLiteral("const_attenuation")), l.constAttenuation);
            l.linearAttenuation = float_from_JS(val.property(QStringLiteral("linear_attenuation")), l.linearAttenuation);
            l.quadraticAttenuation = float_from_JS(val.property(QStringLiteral("quadratic_attenuation")), l.quadraticAttenuation);
            return true;
          });
        return true;
      } else
      return false;
  }

  template<typename T>
  bool changeSpotLight(IlluminationModelBase &obj, size_t i, const QJSValue &val)
  {
    if(auto *ptr = dynamic_cast<::IlluminationModel<T>*>(&obj); ptr)
      {
        ptr->changeSpotLight(i, [&val] (typename KawaiiIlluminationStructs<T>::Projector &l) {
            changeLight<T>(val, l);
            l.ambient = vec3_from_JS(val.property(QStringLiteral("ambient")), l.ambient);
            l.position = vec3_from_JS(val.property(QStringLiteral("position")), l.position);
            l.constAttenuation = float_from_JS(val.property(QStringLiteral("const_attenuation")), l.constAttenuation);
            l.linearAttenuation = float_from_JS(val.property(QStringLiteral("linear_attenuation")), l.linearAttenuation);
            l.quadraticAttenuation = float_from_JS(val.property(QStringLiteral("quadratic_attenuation")), l.quadraticAttenuation);
            l.direction = vec3_from_JS(val.property(QStringLiteral("direction")), l.direction).normalized();
            l.spotExponent = float_from_JS(val.property(QStringLiteral("spot_exponent")), l.spotExponent);
            l.spotCutoffRadians = toRadians * float_from_JS(val.property(QStringLiteral("spot_cutoff")), l.spotCutoffRadians * toDegrees);
            return true;
          });
        return true;
      } else
      return false;
  }
}

void KawaiiJSWrappers::IlluminationModel::updateDirLight(quint64 i, const QJSValue &val)
{
  if(changeDirLight<KawaiiPbrLight>(obj, i, val))
    return;
  else if(changeDirLight<KawaiiPhongLight>(obj, i, val))
    return;
  else if(changeDirLight<KawaiiCelLight>(obj, i, val))
    return;
}

void KawaiiJSWrappers::IlluminationModel::updateDotLight(quint64 i, const QJSValue &val)
{
  if(changeDotLight<KawaiiPbrLight>(obj, i, val))
    return;
  else if(changeDotLight<KawaiiPhongLight>(obj, i, val))
    return;
  else if(changeDotLight<KawaiiCelLight>(obj, i, val))
    return;
}

void KawaiiJSWrappers::IlluminationModel::updateSpotLight(quint64 i, const QJSValue &val)
{
  if(changeSpotLight<KawaiiPbrLight>(obj, i, val))
    return;
  else if(changeSpotLight<KawaiiPhongLight>(obj, i, val))
    return;
  else if(changeSpotLight<KawaiiCelLight>(obj, i, val))
    return;
}

void KawaiiJSWrappers::IlluminationModel::setDirLightsShadowCenter(const QVector3D &newDirLightsShadowCenter)
{
  if(obj.getShadowMapper())
    obj.getShadowMapper()->setDirLightsCenter(newDirLightsShadowCenter);
}

void KawaiiJSWrappers::IlluminationModel::setDirLightsShadowArea(const QVector3D &newDirLightsShadowArea)
{
  if(obj.getShadowMapper())
    obj.getShadowMapper()->setDirLightsArea(newDirLightsShadowArea);
}

QJSEngine *KawaiiJSWrappers::IlluminationModel::getEngine()
{
  if(!engine)
    engine = qjsEngine(this);
  return engine;
}

QJSEngine *KawaiiJSWrappers::IlluminationModel::getEngine() const
{
  if(!engine)
    return qjsEngine(this);
  return engine;
}
