#include "Location_qml.hpp"

#include <QJSEngine>
#include <QBindable>

#include "Body_qml.hpp"
#include "Entity_qml.hpp"
#include "LandscapeInfo_qml.hpp"

QJSValue KawaiiJSWrappers::Location::createWrapper(::Location &l, QJSEngine *engine)
{
  if(!engine) return QJSValue::UndefinedValue;

  QJSValue jsVal;
  l.changeScriptWrapper([engine, &jsVal, &l] (QObject *&wrapper) {
      if(!qobject_cast<KawaiiJSWrappers::Location*>(wrapper))
        {
          if(wrapper)
            delete wrapper;
          wrapper = new KawaiiJSWrappers::Location(l, engine);
        }

      jsVal = engine->newQObject(wrapper);
      if(jsVal.isNull())
        {
          delete wrapper;
          wrapper = new KawaiiJSWrappers::Location(l, engine);
          jsVal = engine->newQObject(wrapper);
        }
    });

  return jsVal;
}

KawaiiJSWrappers::Location::Location(::Location &l, QJSEngine *engine):
  obj(l),
  engine(engine)
{
  bindableObjectName().setBinding([this] { return obj.objectName(); });

  connect(&obj, &::Location::entityAdded, this, [this](::Entity *e) {
      emit entityAdded(Entity::createWrapper(*e, getEngine()));
    });

  connect(&obj, &::Location::entityRemoved, this, [this](::Entity *e) {
      emit entityRemoved(Entity::createWrapper(*e, getEngine()));
    });

  connect(&obj, &::Location::landscapeAdded, this, [this](::LandscapeInfo *landscape) {
      emit landscapeAdded(getEngine()->newQObject(new LandscapeInfo(*landscape)));
    });

  connect(&obj, &::Location::landscapeRemoved, this, [this](::LandscapeInfo *landscape) {
      emit landscapeRemoved(getEngine()->newQObject(new LandscapeInfo(*landscape)));
    });
}

KawaiiJSWrappers::Location::~Location()
{
  obj.changeScriptWrapper([this](QObject*& wrapper) {
    if(wrapper == this)
      wrapper = nullptr;
  });
}

QJSValue KawaiiJSWrappers::Location::createEntity(const QJSValue &body, const QString &id, float mass)
{
  auto engine = getEngine();
  if(!engine)
    return QJSValue::UndefinedValue;

  ::Body *newBody = nullptr;

  if(body.isQObject())
    {
      auto *bodyWrapper = qobject_cast<Body*>(body.toQObject());
      newBody = bodyWrapper->getObj();
    }

  if(body.isObject())
    {
      const auto bodyData = body.toVariant().toMap();
      newBody = ::Body::create(bodyData).release();
    }

  return KawaiiJSWrappers::Entity::createWrapper(*obj.createEntity(newBody, id, mass), engine);
}

QJSValue KawaiiJSWrappers::Location::createLandscapeInfo(const QString &material, const QString &heightMapTexture, float width, float height, float depth, bool createCollisionShape)
{
    auto engine = getEngine();
    if(!engine)
      return QJSValue::UndefinedValue;

    auto newLandscape = obj.addLandscapeInfo(material, heightMapTexture, QVector3D(width, height, depth), createCollisionShape);
    return engine->newQObject(new LandscapeInfo(*newLandscape));
}

QJSValue KawaiiJSWrappers::Location::findEntity(const QString &id)
{
  auto engine = getEngine();
  if(!engine)
    return QJSValue::UndefinedValue;

  auto resultObj = obj.findEntity(id);
  if(resultObj)
    return Entity::createWrapper(*resultObj, engine);
  else
    return QJSValue::NullValue;
}

QJSValue KawaiiJSWrappers::Location::findLandscapeInfo(const QString &id)
{
  auto engine = getEngine();
  if(!engine)
    return QJSValue::UndefinedValue;

  auto resultObj = obj.findLandscapeInfo(id);
  if(resultObj)
    return engine->newQObject(new LandscapeInfo(*resultObj));
  else
    return QJSValue::NullValue;
}

void KawaiiJSWrappers::Location::removeEntity(const QJSValue &entity)
{
  if(entity.isString())
    if(auto *e = obj.findEntity(entity.toString()); e)
      delete e;

  if(!entity.isQObject()) return;

  auto entityObjWrapper = qobject_cast<Entity*>(entity.toQObject());
  if(entityObjWrapper)
    entityObjWrapper->remove();
}

void KawaiiJSWrappers::Location::forallEntities(const QJSValue &func)
{
  if(Q_LIKELY(func.isCallable()))
    obj.forallEntities([func, this] (::Entity &e) {
        QJSValue(func).call({Entity::createWrapper(e, getEngine())});
      });
}

void KawaiiJSWrappers::Location::removeLandscapeInfo(const QJSValue &landscape)
{
  if(landscape.isString())
    if(auto *l = obj.findLandscapeInfo(landscape.toString()); l)
      obj.removeLandscapeInfo(*l);

  if(!landscape.isQObject()) return;

  auto landscapeInfo = qobject_cast<LandscapeInfo*>(landscape.toQObject());
  if(landscapeInfo)
    {
      obj.removeLandscapeInfo(landscapeInfo->getObj());
      landscapeInfo->deleteLater();
    }
}

void KawaiiJSWrappers::Location::forallLandscapes(const QJSValue &func)
{
  if(Q_LIKELY(func.isCallable()))
    obj.forallLandscapes([func, this] (::LandscapeInfo &landscape) {
        QJSValue(func).call({engine->newQObject(new LandscapeInfo(landscape))});
      });
}

void KawaiiJSWrappers::Location::remove()
{
  obj.deleteLater();
}

QJSEngine *KawaiiJSWrappers::Location::getEngine()
{
  if(!engine)
    engine = qjsEngine(this);
  return engine;
}
