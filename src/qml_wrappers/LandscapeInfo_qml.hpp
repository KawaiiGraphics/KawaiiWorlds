#ifndef LANDSCAPEINFO_QML_HPP
#define LANDSCAPEINFO_QML_HPP

#include "KawaiiWorlds_qml_global.hpp"
#include <LandscapeInfo.hpp>
#include <QJSValue>
#include <QObject>

namespace KawaiiJSWrappers {
  class KAWAIIWORLDS_QML_SHARED_EXPORT LandscapeInfo : public QObject
  {
    Q_OBJECT

    Q_PROPERTY(float x READ getX WRITE setX FINAL SCRIPTABLE true)
    Q_PROPERTY(float y READ getY WRITE setY FINAL SCRIPTABLE true)
    Q_PROPERTY(float z READ getZ WRITE setZ FINAL SCRIPTABLE true)

    Q_PROPERTY(float width  READ getWidth  WRITE setWidth  FINAL SCRIPTABLE true)
    Q_PROPERTY(float height READ getHeight WRITE setHeight FINAL SCRIPTABLE true)
    Q_PROPERTY(float depth  READ getDepth  WRITE setDepth  FINAL SCRIPTABLE true)

    Q_PROPERTY(QString   material  READ getMaterial  WRITE setMaterial  FINAL SCRIPTABLE true)
    Q_PROPERTY(QString   heightMap READ getHeightMap WRITE setHeightMap FINAL SCRIPTABLE true)
    Q_PROPERTY(float     step      READ getStep      WRITE setStep      FINAL SCRIPTABLE true)
    Q_PROPERTY(QVector2D chunkSize READ getChunkSize WRITE setChunkSize FINAL SCRIPTABLE true)

    Q_PROPERTY(float friction         READ getFriction         WRITE setFriction         FINAL SCRIPTABLE true)
    Q_PROPERTY(float frictionRolling  READ getFrictionRolling  WRITE setFrictionRolling  FINAL SCRIPTABLE true)
    Q_PROPERTY(float frictionSpinning READ getFrictionSpinning WRITE setFrictionSpinning FINAL SCRIPTABLE true)
    Q_PROPERTY(float restitution      READ getRestitution      WRITE setRestitution      FINAL SCRIPTABLE true)

    Q_PROPERTY(QString id READ getId WRITE setId SCRIPTABLE true)
  public:
    LandscapeInfo(::LandscapeInfo &info);
    ~LandscapeInfo();

    inline ::LandscapeInfo& getObj()
    { return info; }

    Q_INVOKABLE float getX() const;
    Q_INVOKABLE float getY() const;
    Q_INVOKABLE float getZ() const;

    Q_INVOKABLE float getWidth() const;
    Q_INVOKABLE float getHeight() const;
    Q_INVOKABLE float getDepth() const;

    Q_INVOKABLE QString getMaterial() const;
    Q_INVOKABLE QString getHeightMap() const;
    Q_INVOKABLE QString getId() const;

    Q_INVOKABLE float getStep() const;
    Q_INVOKABLE QJSValue getMetadata(const QString &key) const;

    Q_INVOKABLE inline float getRestitution() const
    { return info.restitution; }

    Q_INVOKABLE inline float getFriction() const
    { return info.friction; }

    Q_INVOKABLE inline float getFrictionRolling() const
    { return info.frictionRolling; }

    Q_INVOKABLE inline float getFrictionSpinning() const
    { return info.frictionSpinning; }

    Q_INVOKABLE QVector2D getChunkSize() const;

  public slots:
    void setX(float val);
    void setY(float val);
    void setZ(float val);

    void setWidth(float val);
    void setHeight(float val);
    void setDepth(float val);

    void setMaterial(const QString &val);
    void setHeightMap(const QString &val);
    void setId(const QString &value);

    void setStep(float step);
    void setMetadata(const QString &key, QJSValue value);

    void setChunkSize(const QVector2D &newChunkSize);

    inline void setRestitution(float value)
    { info.restitution = value; }

    inline void setFriction(float value)
    { info.friction = value; }

    inline void setFrictionRolling(float newFrictionRolling)
    { info.frictionRolling = newFrictionRolling; }

    inline void setFrictionSpinning(float newFrictionSpinning)
    { info.frictionSpinning = newFrictionSpinning; }



    //IMPLEMENT
  private:
    ::LandscapeInfo &info;
  };
}

#endif // LANDSCAPEINFO_QML_HPP
