#ifndef ENTITY_QML_HPP
#define ENTITY_QML_HPP

#include "KawaiiWorlds_qml_global.hpp"
#include <Entity.hpp>
#include <QJSEngine>

namespace KawaiiJSWrappers {
  class KAWAIIWORLDS_QML_SHARED_EXPORT Entity: public QObject
  {
    Q_OBJECT

    Q_PROPERTY(QString id READ getId WRITE setId NOTIFY idChanged FINAL)

    Q_PROPERTY(float pitch READ getPitch WRITE setPitch NOTIFY rotationChanged FINAL)
    Q_PROPERTY(float yaw   READ getYaw   WRITE setYaw   NOTIFY rotationChanged FINAL)
    Q_PROPERTY(float roll  READ getRoll  WRITE setRoll  NOTIFY rotationChanged FINAL)

    Q_PROPERTY(float x READ getX WRITE setX NOTIFY positionChanged FINAL)
    Q_PROPERTY(float y READ getY WRITE setY NOTIFY positionChanged FINAL)
    Q_PROPERTY(float z READ getZ WRITE setZ NOTIFY positionChanged FINAL)

    Q_PROPERTY(float width  READ getWidth  FINAL)
    Q_PROPERTY(float height READ getHeight FINAL)
    Q_PROPERTY(float depth  READ getDepth  FINAL)

    Q_PROPERTY(float vel_x READ getVelX WRITE setVelX NOTIFY velocityChanged FINAL)
    Q_PROPERTY(float vel_y READ getVelY WRITE setVelY NOTIFY velocityChanged FINAL)
    Q_PROPERTY(float vel_z READ getVelZ WRITE setVelZ NOTIFY velocityChanged FINAL)

    Q_PROPERTY(float angle_vel_x READ getAngularVelX WRITE setAngularVelX NOTIFY angularVelChanged FINAL)
    Q_PROPERTY(float angle_vel_y READ getAngularVelY WRITE setAngularVelY NOTIFY angularVelChanged FINAL)
    Q_PROPERTY(float angle_vel_z READ getAngularVelZ WRITE setAngularVelZ NOTIFY angularVelChanged FINAL)

    Q_PROPERTY(float mass            READ getMass            WRITE setMass            NOTIFY massChanged            FINAL)
    Q_PROPERTY(float friction        READ getFriction        WRITE setFriction        NOTIFY frictionChanged        FINAL)
    Q_PROPERTY(float frictionRolling READ getFrictionRolling WRITE setFrictionRolling NOTIFY frictionRollingChanged FINAL)
    Q_PROPERTY(float frictionSpinning READ getFrictionSpinning WRITE setFrictionSpinning NOTIFY frictionSpinningChanged FINAL)
    Q_PROPERTY(float restitution     READ getRestitution     WRITE setRestitution     NOTIFY restitutionChanged     FINAL)

    Q_PROPERTY(float lookingForwardFactorX READ getLookingForwardFactorX WRITE setLookingForwardFactorX NOTIFY lookingForwardFactorXChanged FINAL)
    Q_PROPERTY(float lookingForwardFactorY READ getLookingForwardFactorY WRITE setLookingForwardFactorY NOTIFY lookingForwardFactorYChanged FINAL)
    Q_PROPERTY(float lookingForwardFactorZ READ getLookingForwardFactorZ WRITE setLookingForwardFactorZ NOTIFY lookingForwardFactorZChanged FINAL)

    Q_PROPERTY(bool click_opacity READ getClickOpacity WRITE setClickOpacity NOTIFY clickOpacityChanged FINAL)
    Q_PROPERTY(bool look_forward  READ getLookForward  WRITE setLookForward  NOTIFY lookForwardChanged  FINAL)
    Q_PROPERTY(bool hasContacts   READ hasContacts NOTIFY hasContactsChanged FINAL)

    Q_PROPERTY(QJSValue body READ getBody NOTIFY bodyChanged FINAL)

  public:
    static Entity* createWrapperObj(::Entity &e, QJSEngine *engine);
    static QJSValue createWrapper(::Entity &e, QJSEngine *engine);

    Entity(::Entity &e, QJSEngine *engine);
    ~Entity();

    inline ::Entity& getObj() const
    { return obj; }

    Q_INVOKABLE inline QString getId() const
    { return obj.objectName(); }

    Q_INVOKABLE float getPitch() const;

    Q_INVOKABLE float getYaw() const;

    Q_INVOKABLE float getRoll() const;


    Q_INVOKABLE inline float getX() const
    { return obj.getPosition().x(); }

    Q_INVOKABLE inline float getY() const
    { return obj.getPosition().y(); }

    Q_INVOKABLE inline float getZ() const
    { return obj.getPosition().z(); }

    Q_INVOKABLE float getWidth() const;
    Q_INVOKABLE float getHeight() const;
    Q_INVOKABLE float getDepth() const;


    Q_INVOKABLE inline float getMass() const
    { return obj.getMass(); }


    Q_INVOKABLE inline float getVelX() const
    { return obj.getVelocity().x(); }

    Q_INVOKABLE inline float getVelY() const
    { return obj.getVelocity().y(); }

    Q_INVOKABLE inline float getVelZ() const
    { return obj.getVelocity().z(); }


    Q_INVOKABLE inline float getAngularVelX() const
    { return obj.getAngularVel().x(); }

    Q_INVOKABLE inline float getAngularVelY() const
    { return obj.getAngularVel().y(); }

    Q_INVOKABLE inline float getAngularVelZ() const
    { return obj.getAngularVel().z(); }


    Q_INVOKABLE inline float getRestitution() const
    { return obj.getRestitution(); }

    Q_INVOKABLE inline float getFriction() const
    { return obj.getFriction(); }

    Q_INVOKABLE inline float getFrictionRolling() const
    { return obj.getFrictionRolling(); }

    Q_INVOKABLE inline float getFrictionSpinning() const
    { return obj.getFrictionSpinning(); }

    Q_INVOKABLE QJSValue getMetadata(const QString &key) const;

    inline Q_INVOKABLE bool getLookForward() const
    { return obj.getLookForward(); }

    inline Q_INVOKABLE float getLookingForwardFactorX() const
    { return obj.getLookingForwardFactorX(); }

    inline Q_INVOKABLE float getLookingForwardFactorY() const
    { return obj.getLookingForwardFactorY(); }

    inline Q_INVOKABLE float getLookingForwardFactorZ() const
    { return obj.getLookingForwardFactorZ(); }

    inline Q_INVOKABLE bool getClickOpacity() const
    { return obj.getClickOpacity(); }

    inline Q_INVOKABLE bool hasContact(Entity *withEntity) const
    { return obj.hasContact(&withEntity->obj); }

    inline Q_INVOKABLE bool hasContacts() const
    { return obj.hasContacts(); }

    Q_INVOKABLE QJSValue getBody() const;
    Q_INVOKABLE QJSValue createBody(const QString &type);

    Q_INVOKABLE float distanceToLand(const QJSValue &landscape);

  public slots:
    inline void setLookingForwardFactorX(float newLookingForwardFactorX)
    { obj.setLookingForwardFactorX(newLookingForwardFactorX); }

    inline void setLookingForwardFactorY(float newLookingForwardFactorY)
    { obj.setLookingForwardFactorY(newLookingForwardFactorY); }

    inline void setLookingForwardFactorZ(float newLookingForwardFactorZ)
    { obj.setLookingForwardFactorZ(newLookingForwardFactorZ); }

    void removeBody();

    void setLocation(float x, float y, float z, float pitch, float yaw, float roll);

    void setRotation(float pitch, float yaw, float roll);

    void rotate(float pitch, float yaw, float roll);

    void setPitch(float pitch);
    void setYaw(float yaw);
    void setRoll(float roll);

    inline void setPosition(float x, float y, float z)
    { obj.setPosition(QVector3D(x,y,z)); }

    void setX(float x);
    void setY(float y);
    void setZ(float z);

    inline void setMass(float mass)
    { obj.setMass(mass); }

    inline void setVelocity(float x, float y, float z)
    { obj.setVelocity(QVector3D(x,y,z)); }

    void setVelX(float x);
    void setVelY(float y);
    void setVelZ(float z);

    inline void setAngularVelocity(float x, float y, float z)
    { obj.setAngularVel(QVector3D(x,y,z)); }

    void setAngularVelX(float x);
    void setAngularVelY(float y);
    void setAngularVelZ(float z);

    inline void push(float x, float y, float z)
    { obj.push(QVector3D(x,y,z)); }


    inline void setRestitution(float value)
    { obj.setRestitution(value); }

    inline void setFriction(float value)
    { obj.setFriction(value); }

    inline void setFrictionRolling(float newFrictionRolling)
    { obj.setFrictionRolling(newFrictionRolling); }

    inline void setFrictionSpinning(float newFrictionSpinning)
    { obj.setFrictionSpinning(newFrictionSpinning); }

    inline void remove()
    { obj.deleteLater(); }

    inline void setLookForward(bool newLookForward)
    { obj.setLookForward(newLookForward); }

    inline void setClickOpacity(bool newLookForward)
    { obj.setClickOpacity(newLookForward); }

    void setMetadata(const QString &key, const QJSValue &value);

    inline void setId(const QString &val)
    { obj.setObjectName(val); }

    void allignToLand(const QJSValue &landscape);

    void flush();

  signals:
    void tick(Entity *e, float secElapsed);
    void contact(Entity *e0, Entity *e1);
    void contactLost(Entity *e0, Entity *e1);

    void pointerPress(Entity *e);
    void pointerRelease(Entity *e);
    void pointerDblClicked(Entity *e);

    void idChanged();

    void rotationChanged();
    void positionChanged();

    void velocityChanged();
    void angularVelChanged();

    void bodyIsAboutToBeChanged();
    void bodyChanged();

    void worldChanged();
    void locationChanged();

    void massChanged();
    void restitutionChanged();
    void frictionChanged();
    void frictionRollingChanged();
    void frictionSpinningChanged();

    void lookingForwardFactorXChanged();
    void lookingForwardFactorYChanged();
    void lookingForwardFactorZChanged();

    void lookForwardChanged();
    void clickOpacityChanged();
    void hasContactsChanged();



    //IMPLEMENT
  private:
    ::Entity &obj;
    QJSEngine *engine;

    QJSEngine *getEngine();
    QJSEngine *getEngine() const;
  };
}

#endif // ENTITY_QML_HPP
