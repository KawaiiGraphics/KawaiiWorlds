#ifndef LANDSCAPE_QML_HPP
#define LANDSCAPE_QML_HPP

#include "KawaiiWorlds_qml_global.hpp"
#include <Landscape.hpp>
#include <QJSValue>
#include <QObject>

namespace KawaiiJSWrappers {
  class KAWAIIWORLDS_QML_SHARED_EXPORT Landscape : public QObject
  {
    Q_OBJECT
  public:
    static QJSValue createWrapper(::Landscape &obj, QJSEngine *engine);

    Landscape(::Landscape &obj);
    ~Landscape();

    ::Landscape &getObj();

    Q_INVOKABLE float getHeight(float x, float z) const;
    Q_INVOKABLE float getHeight(float x0, float z0, float x1, float z1) const;
    Q_INVOKABLE float getSlope(float x0, float z0, float x1, float z1) const;

  public slots:
    void setMetadata(const QString &key, const QJSValue &value);
    void bump(float x, float y, float val, float radius, float hardness, float pressure);



    //IMPLEMENT
  private:
    ::Landscape &obj;
  };
}

#endif // LANDSCAPE_QML_HPP
