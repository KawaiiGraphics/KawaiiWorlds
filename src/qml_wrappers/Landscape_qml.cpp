#include "Landscape_qml.hpp"
#include <QJSEngine>

QJSValue KawaiiJSWrappers::Landscape::createWrapper(::Landscape &obj, QJSEngine *engine)
{
  if(!engine) return QJSValue::UndefinedValue;

  if(!obj.jsWrapper)
    obj.jsWrapper = new KawaiiJSWrappers::Landscape(obj);

  QJSValue jsVal = engine->newQObject(obj.jsWrapper);
  if(jsVal.isNull())
    {
      delete obj.jsWrapper;
      auto wrapper = new KawaiiJSWrappers::Landscape(obj);
      obj.jsWrapper = wrapper;
      jsVal = engine->newQObject(obj.jsWrapper);
    }
  return jsVal;
}

KawaiiJSWrappers::Landscape::Landscape(::Landscape &obj):
  obj(obj)
{
  obj.jsWrapper = this;
}

KawaiiJSWrappers::Landscape::~Landscape()
{
  if(obj.jsWrapper == this)
    obj.jsWrapper = nullptr;
}

Landscape &KawaiiJSWrappers::Landscape::getObj()
{
  return obj;
}

float KawaiiJSWrappers::Landscape::getHeight(float x, float z) const
{
  return obj.getHeight(QVector2D(x, z));
}

float KawaiiJSWrappers::Landscape::getHeight(float x0, float z0, float x1, float z1) const
{
  return obj.getHeight(QVector2D(x0, z0), QVector2D(x1, z1));
}

float KawaiiJSWrappers::Landscape::getSlope(float x0, float z0, float x1, float z1) const
{
  return obj.getSlope(QVector2D(x0, z0), QVector2D(x1, z1));
}

void KawaiiJSWrappers::Landscape::setMetadata(const QString &key, const QJSValue &value)
{
  obj.metadata[key] = value.toString();
}

void KawaiiJSWrappers::Landscape::bump(float x, float y, float val, float radius, float hardness, float pressure)
{
  obj.bump(QVector2D(x,y), val, radius, hardness, pressure);
}
