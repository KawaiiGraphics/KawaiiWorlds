#include "NetworkClient_qml.hpp"

KawaiiJSWrappers::NetworkClient::NetworkClient(::NetworkClient &obj):
  obj(obj)
{
  connect(&obj, &::NetworkClient::clientConnected, this, &NetworkClient::clientConnectedSlot);
  connect(&obj, &::NetworkClient::clientDisconnected, this, &NetworkClient::clientDisconnectedSlot);
  connect(&obj, &::NetworkClient::customMessageReceived, this, &NetworkClient::customMessageReceivedSlot);
}

bool KawaiiJSWrappers::NetworkClient::isLocalAddress(const QString &addr)
{
  return obj.serverAddress().isEqual(QHostAddress(addr), QHostAddress::TolerantConversion);
}

void KawaiiJSWrappers::NetworkClient::setOnClientConnected(const QJSValue &func)
{
  if(func.isCallable())
    onClientConnectedFunc = func;
  else
    onClientConnectedFunc = QJSValue::NullValue;
}

void KawaiiJSWrappers::NetworkClient::setOnClientDisconnected(const QJSValue &func)
{
  if(func.isCallable())
    onClientDisconnectedFunc = func;
  else
    onClientDisconnectedFunc = QJSValue::NullValue;
}

void KawaiiJSWrappers::NetworkClient::setOnCustomMessage(const QJSValue &func)
{
  if(func.isCallable())
    onCustomMessageFunc = func;
  else
    onCustomMessageFunc = QJSValue::NullValue;
}

void KawaiiJSWrappers::NetworkClient::clientConnectedSlot(const QHostAddress &addr)
{
  if(onClientConnectedFunc.isCallable())
    onClientConnectedFunc.call({ addr.toString() });
}

void KawaiiJSWrappers::NetworkClient::clientDisconnectedSlot(const QHostAddress &addr)
{
  if(onClientDisconnectedFunc.isCallable())
    onClientDisconnectedFunc.call({ addr.toString() });
}

void KawaiiJSWrappers::NetworkClient::customMessageReceivedSlot(const QByteArray &message)
{
  if(onCustomMessageFunc.isCallable())
    onCustomMessageFunc.call({ obj.serverAddress().toString(), QString::fromUtf8(message) });
}
