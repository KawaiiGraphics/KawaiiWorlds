#ifndef NETWORKSERVER_QML_HPP
#define NETWORKSERVER_QML_HPP

#include <QObject>
#include "KawaiiWorlds_qml_global.hpp"
#include <networking/NetworkServer.hpp>
#include <QJSValue>

namespace KawaiiJSWrappers {
  class KAWAIIWORLDS_QML_SHARED_EXPORT NetworkServer: public QObject
  {
    Q_OBJECT

    Q_PROPERTY(QJSValue onClientConnected READ getOnClientConnected WRITE setOnClientConnected SCRIPTABLE true)
    Q_PROPERTY(QJSValue onClientDisconnected READ getOnClientDisconnected WRITE setOnClientDisconnected SCRIPTABLE true)
    Q_PROPERTY(QJSValue onCustomMessage READ getOnCustomMessage WRITE setOnCustomMessage SCRIPTABLE true)

  public:
    NetworkServer(::NetworkServer &obj);

    Q_INVOKABLE bool isLocalAddress(const QString &addr);

    Q_INVOKABLE inline QJSValue getOnClientConnected() const
    { return onClientConnectedFunc; }

    Q_INVOKABLE inline QJSValue getOnClientDisconnected() const
    { return onClientDisconnectedFunc; }

    Q_INVOKABLE inline QJSValue getOnCustomMessage() const
    { return onCustomMessageFunc; }

  public slots:
    void setOnClientConnected(const QJSValue &func);
    void setOnClientDisconnected(const QJSValue &func);
    void setOnCustomMessage(const QJSValue &func);



    //IMPLEMENT
  private:
    ::NetworkServer &obj;
    QJSValue onClientConnectedFunc;
    QJSValue onClientDisconnectedFunc;
    QJSValue onCustomMessageFunc;

    void clientConnectedSlot(QTcpSocket &client);
    void clientDisconnectedSlot(QTcpSocket &client);
    void customMessageReceivedSlot(QTcpSocket &client, const QByteArray &message);
  };
}

#endif // NETWORKSERVER_QML_HPP
