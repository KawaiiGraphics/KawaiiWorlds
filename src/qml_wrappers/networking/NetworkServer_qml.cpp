#include "NetworkServer_qml.hpp"
#include <QTcpSocket>

KawaiiJSWrappers::NetworkServer::NetworkServer(::NetworkServer &obj):
  obj(obj)
{
  connect(&obj, &::NetworkServer::clientConnected, this, &NetworkServer::clientConnectedSlot);
  connect(&obj, &::NetworkServer::clientDisconnected, this, &NetworkServer::clientDisconnectedSlot);
  connect(&obj, &::NetworkServer::customMessageReceived, this, &NetworkServer::customMessageReceivedSlot);
}

bool KawaiiJSWrappers::NetworkServer::isLocalAddress(const QString &addr)
{
  if(addr == QLatin1String("SERVER")) return true;
  else return false;

  const QHostAddress host(addr);
  return host.isLoopback() || obj.serverAddress().isEqual(host, QHostAddress::TolerantConversion);
}

void KawaiiJSWrappers::NetworkServer::setOnClientConnected(const QJSValue &func)
{
  if(func.isCallable())
    onClientConnectedFunc = func;
  else
    onClientConnectedFunc = QJSValue::NullValue;
}

void KawaiiJSWrappers::NetworkServer::setOnClientDisconnected(const QJSValue &func)
{
  if(func.isCallable())
    onClientDisconnectedFunc = func;
  else
    onClientDisconnectedFunc = QJSValue::NullValue;
}

void KawaiiJSWrappers::NetworkServer::setOnCustomMessage(const QJSValue &func)
{
  if(func.isCallable())
    onCustomMessageFunc = func;
  else
    onCustomMessageFunc = QJSValue::NullValue;
}

void KawaiiJSWrappers::NetworkServer::clientConnectedSlot(QTcpSocket &client)
{
  if(onClientConnectedFunc.isCallable())
    onClientConnectedFunc.call({ client.peerAddress().toString() });
}

void KawaiiJSWrappers::NetworkServer::clientDisconnectedSlot(QTcpSocket &client)
{
  if(onClientDisconnectedFunc.isCallable())
    onClientDisconnectedFunc.call({ client.peerAddress().toString() });
}

void KawaiiJSWrappers::NetworkServer::customMessageReceivedSlot(QTcpSocket &client, const QByteArray &message)
{
  if(onCustomMessageFunc.isCallable())
    onCustomMessageFunc.call({ client.peerAddress().toString(), QString::fromUtf8(message) });
}
