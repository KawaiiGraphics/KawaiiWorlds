#ifndef NETWORKCLIENT_QML_HPP
#define NETWORKCLIENT_QML_HPP

#include <QObject>
#include "KawaiiWorlds_qml_global.hpp"
#include <networking/NetworkClient.hpp>
#include <QJSValue>

namespace KawaiiJSWrappers {
  class KAWAIIWORLDS_QML_SHARED_EXPORT NetworkClient: public QObject
  {
    Q_OBJECT

    Q_PROPERTY(QJSValue onClientConnected READ getOnClientConnected WRITE setOnClientConnected SCRIPTABLE true)
    Q_PROPERTY(QJSValue onClientDisconnected READ getOnClientDisconnected WRITE setOnClientDisconnected SCRIPTABLE true)
    Q_PROPERTY(QJSValue onCustomMessage READ getOnCustomMessage WRITE setOnCustomMessage SCRIPTABLE true)

  public:
    NetworkClient(::NetworkClient &obj);

    inline ::NetworkClient& getObj()
    { return obj; }

    Q_INVOKABLE bool isLocalAddress(const QString &addr);

    Q_INVOKABLE inline QJSValue getOnClientConnected() const
    { return onClientConnectedFunc; }

    Q_INVOKABLE inline QJSValue getOnClientDisconnected() const
    { return onClientDisconnectedFunc; }

    Q_INVOKABLE inline QJSValue getOnCustomMessage() const
    { return onCustomMessageFunc; }

  public slots:
    void setOnClientConnected(const QJSValue &func);
    void setOnClientDisconnected(const QJSValue &func);
    void setOnCustomMessage(const QJSValue &func);



    //IMPLEMENT
  private:
    ::NetworkClient &obj;
    QJSValue onClientConnectedFunc;
    QJSValue onClientDisconnectedFunc;
    QJSValue onCustomMessageFunc;

    void clientConnectedSlot(const QHostAddress &addr);
    void clientDisconnectedSlot(const QHostAddress &addr);
    void customMessageReceivedSlot(const QByteArray &message);
  };
}

#endif // NETWORKCLIENT_QML_HPP
