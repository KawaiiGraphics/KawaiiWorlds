#ifndef WORLD_QML_HPP
#define WORLD_QML_HPP

#include <Kawaii3D/Surfaces/KawaiiSurface.hpp>
#include "KawaiiWorlds_qml_global.hpp"
#include <World.hpp>
#include <QJSValue>

namespace KawaiiJSWrappers {
  class KAWAIIWORLDS_QML_SHARED_EXPORT World: public QObject
  {
    Q_OBJECT

    Q_PROPERTY(float age READ getAge SCRIPTABLE true)
  public:
    inline World(::World &w):
      obj(w),
      engine(nullptr)
    { }

    Q_INVOKABLE QJSValue createEntity(const QJSValue &body, const QString &id, float mass = 1);

    Q_INVOKABLE QJSValue getEntity(const QString &id) const;

    Q_INVOKABLE QJSValue getEntityAt(float screenX, float screenY) const;
    Q_INVOKABLE QJSValue getEntitiesAt(float screenX, float screenY) const;

    Q_INVOKABLE QJSValue getEntityAABB(const QJSValue &e) const;

    Q_INVOKABLE QJSValue rotatedVector(float x, float y, float z, float pitch, float yaw, float roll);

    Q_INVOKABLE QJSValue toScreenAxis(float x, float y, float z);
    Q_INVOKABLE QJSValue toWorldAxis(float screenX, float screenY, float depth);
    Q_INVOKABLE float readScreenDepth(float screenX, float screenY);
    Q_INVOKABLE inline QJSValue toWorldAxis(float screenX, float screenY) { return toWorldAxis(screenX, screenY,
                                                                                               readScreenDepth(screenX, screenY)); }

    Q_INVOKABLE inline float getAge() const
    { return obj.getAgeSeconds(); }

    Q_INVOKABLE quint64 addInput(const QJSValue &arg);

    Q_INVOKABLE QJSValue getIllumination(quint64 i) const;
    Q_INVOKABLE QJSValue findIllumination(const QString &name) const;

    Q_INVOKABLE QJSValue createLocation(const QString &name);

    Q_INVOKABLE QJSValue getBringedLandscape(const QJSValue &landscapeInfo) const;

    Q_INVOKABLE QJSValue getMetadata(const QString &key) const;

    Q_INVOKABLE bool canPlace(const QJSValue &entity, float x, float y, float z) const;

    Q_INVOKABLE QJSValue getGearbox() const;
    Q_INVOKABLE QJSValue createGearbox(const QString &type);

  public slots:
    void removeGearbox();

    void forallEntities(const QJSValue &func) const;

    void swapInputs(quint64 i1, quint64 i2);
    void removeInput(quint64 i);
    void enableInput(quint64 i);
    void disableInput(quint64 i);

    void setLocation(const QJSValue &l);

    void setMetadata(const QString &key, const QJSValue &value);

    void onPointerPress(float screenX, float screenY);
    void onPointerRelease(float screenX, float screenY);
    void onPointerDoubleClick(float screenX, float screenY);



  private:
    ::World &obj;
    QJSEngine *engine;

    QJSEngine *getEngine();
    QJSEngine *getEngine() const;
  };
}

#endif // WORLD_QML_HPP
