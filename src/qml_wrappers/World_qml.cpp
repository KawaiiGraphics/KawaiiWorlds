#include "World_qml.hpp"

#include "Body_qml.hpp"
#include "Entity_qml.hpp"
#include "Gearbox_qml.hpp"
#include "Location_qml.hpp"
#include "Landscape_qml.hpp"
#include "LandscapeInfo_qml.hpp"
#include "IlluminationModel_qml.hpp"

#include <input/RelativePointerInput.hpp>
#include <input/KeyboardInput.hpp>

#include <QJSValueIterator>
#include <QJSEngine>
#include <iostream>

QJSValue KawaiiJSWrappers::World::createEntity(const QJSValue &body, const QString &id, float mass)
{
  auto engine = getEngine();
  if(!engine)
    return QJSValue::UndefinedValue;

  ::Body *newBody = nullptr;

  if(body.isQObject())
    {
      auto *bodyWrapper = qobject_cast<Body*>(body.toQObject());
      newBody = bodyWrapper->getObj();
    }

  if(body.isObject())
    {
      const auto bodyData = body.toVariant().toMap();
      newBody = ::Body::create(bodyData).release();
    }

  return KawaiiJSWrappers::Entity::createWrapper(*obj.createEntity(newBody, id, mass), engine);
}

QJSValue KawaiiJSWrappers::World::getEntity(const QString &id) const
{
  auto engine = getEngine();
  if(!engine)
    return QJSValue::UndefinedValue;

  auto entity = obj.getEntity(id);
  if(!entity)
    return QJSValue::NullValue;

  return Entity::createWrapper(*entity, engine);
}

QJSValue KawaiiJSWrappers::World::getEntityAt(float screenX, float screenY) const
{
  auto engine = getEngine();
  if(!engine)
    return QJSValue::UndefinedValue;

  const auto sz = obj.res().getMainRenderpass()->getRenderableSize();

  const QPointF screenCoords(
        screenX / static_cast<float>(sz.x),
        screenY / static_cast<float>(sz.y)
        );

  auto entity = obj.getEntityAt(screenCoords);
  if(!entity)
    return QJSValue::NullValue;
  return Entity::createWrapper(*entity, engine);
}

QJSValue KawaiiJSWrappers::World::getEntitiesAt(float screenX, float screenY) const
{
  auto engine = getEngine();
  if(!engine)
    return QJSValue::UndefinedValue;

  const auto sz = obj.res().getMainRenderpass()->getRenderableSize();

  const QPointF screenCoords(
        screenX / static_cast<float>(sz.x),
        screenY / static_cast<float>(sz.y)
        );

  auto entitys = obj.getEntitiesAt(screenCoords);
  if(entitys.empty())
    return QJSValue::NullValue;

  auto arr = engine->newArray(entitys.size());
  quint32 i = 0;
  for(auto *e: entitys)
    arr.setProperty(i, Entity::createWrapper(*e, engine));
  return arr;
}

QJSValue KawaiiJSWrappers::World::getEntityAABB(const QJSValue &e) const
{
  auto engine = getEngine();
  if(!engine)
    return QJSValue::UndefinedValue;

  const auto sz = obj.res().getMainRenderpass()->getRenderableSize();

  auto entity = qobject_cast<KawaiiJSWrappers::Entity*>(e.toQObject());
  if(!entity)
    return QJSValue::UndefinedValue;

  const auto aabbVec = obj.getEntityAABB(entity->getObj());
  auto result = engine->newArray(aabbVec.size());
  for(quint32 i = 0; i < aabbVec.size(); ++i)
    {
      const auto &aabb = aabbVec[i];
      auto el = engine->newObject();
      el.setProperty(QStringLiteral("x"), aabb.first.x() * static_cast<float>(sz.x));
      el.setProperty(QStringLiteral("y"), aabb.first.y() * static_cast<float>(sz.y));
      el.setProperty(QStringLiteral("z"), aabb.first.z());
      el.setProperty(QStringLiteral("width"), (aabb.second.x() - aabb.first.x()) * static_cast<float>(sz.x));
      el.setProperty(QStringLiteral("height"), (aabb.second.y() - aabb.first.y()) * static_cast<float>(sz.y));
      el.setProperty(QStringLiteral("depth"), (aabb.second.z() - aabb.first.z()));
      result.setProperty(i, el);
    }
  return result;
}

QJSValue KawaiiJSWrappers::World::rotatedVector(float x, float y, float z, float pitch, float yaw, float roll)
{
  auto engine = getEngine();
  if(!engine)
    return QJSValue::UndefinedValue;

  static constexpr float toRad = 3.14159265358979323846 / 180.0;
  const auto angles = glm::vec3(pitch, yaw, roll) * toRad;

  const glm::vec3 v = glm::quat(angles) * glm::vec3(x,y,z);

  auto val = engine->newArray(3);
  val.setProperty(0, v.x);
  val.setProperty(1, v.y);
  val.setProperty(2, v.z);
  return val;
}

QJSValue KawaiiJSWrappers::World::toScreenAxis(float x, float y, float z)
{
  auto engine = getEngine();
  if(!engine)
    return QJSValue::UndefinedValue;

  const auto result = obj.toScreenAxis(QVector3D(x,y,z));
  auto val = engine->newArray(result.size());
  for(quint32 i = 0; i < result.size(); ++i)
    {
      auto el = engine->newArray(2);
      el.setProperty(0, result[i].x());
      el.setProperty(1, result[i].y());
      val.setProperty(i, el);
    }
  return val;
}

QJSValue KawaiiJSWrappers::World::toWorldAxis(float screenX, float screenY, float depth)
{
  if(std::isnan(depth))
    return QJSValue::UndefinedValue;

  auto engine = getEngine();
  if(Q_UNLIKELY(!engine))
    return QJSValue::UndefinedValue;

  const auto result = obj.toWorldAxis(QPointF(screenX, screenY), depth);
  auto val = engine->newArray(result.size());
  for(quint32 i = 0; i < result.size(); ++i)
    {
      auto el = engine->newArray(3);
      el.setProperty(0, result[i].x());
      el.setProperty(1, result[i].y());
      el.setProperty(2, result[i].z());
      val.setProperty(i, el);
    }
  return val;
}

float KawaiiJSWrappers::World::readScreenDepth(float screenX, float screenY)
{
  return obj.readDepth(QPointF(screenX, screenY));
}

namespace {
  std::unique_ptr<Input> createRelativePointerInput(const QJSValue &arg)
  {
    auto val = std::make_unique<RelativePointerInput>();
    if(auto jsFunc = arg.property(QStringLiteral("func")); jsFunc.isCallable())
      val->func = [jsFunc] (const QPoint &delta) { QJSValue(jsFunc).call({delta.x(), delta.y()}); };
    return val;
  }

  std::unique_ptr<Input> createKeyboardInput(const QJSValue &arg)
  {
    auto getKeyBinding = [] (QJSValue &el) {
      KeyboardInput::KeyBinding binding;
      QJSValue jsOnTick = el.property(QStringLiteral("on_tick"));
      QJSValue jsOnPressed = el.property(QStringLiteral("on_pressed"));
      QJSValue jsOnReleased = el.property(QStringLiteral("on_released"));

      if(jsOnTick.isCallable())
        binding.onTick = [jsOnTick](float sec_elapsed) { QJSValue(jsOnTick).call({sec_elapsed}); };
      if(jsOnPressed.isCallable())
        binding.onPressed = [jsOnPressed] { QJSValue(jsOnPressed).call({}); };
      if(jsOnReleased.isCallable())
        binding.onReleased = [jsOnReleased] { QJSValue(jsOnReleased).call({}); };

      return binding;
    };

    auto val = std::make_unique<KeyboardInput>();
    if(auto bindings = arg.property(QStringLiteral("bindings")); bindings.isArray())
      for(int i = 0, length = bindings.property(QStringLiteral("length")).toInt(); i < length; ++i)
        if(auto el = bindings.property(i); el.isObject())
          {
            if(QJSValue code = el.property(QStringLiteral("code")); code.isNumber())
              val->bindKey(code.toInt(), getKeyBinding(el));

            else if(QJSValue text = el.property(QStringLiteral("text")); text.isString())
              val->bindKey(text.toString(), getKeyBinding(el));
          }
    if(auto postTickJs = arg.property(QStringLiteral("post_tick")); postTickJs.isCallable())
      val->postTick = [postTickJs] (float sec_elapsed) { QJSValue(postTickJs).call({sec_elapsed}); };
    return val;
  }
}

quint64 KawaiiJSWrappers::World::addInput(const QJSValue &arg)
{
  const auto typeProperty = arg.property(QStringLiteral("type"));
  if(!typeProperty.isString())
    {
      std::cerr << "WARN: KawaiiJSWrappers::World::addInput: \'type\' argument must be the input name!" << std::endl;
      return obj.addInput({});
    }

  static const std::unordered_map<std::string, std::function<std::unique_ptr<::Input>(const QJSValue &)>> creators = {
  {"relative_pointer", createRelativePointerInput},
  {"keyboard", createKeyboardInput},
};

  return obj.addInput(creators.at(typeProperty.toString().toLower().toStdString())(arg));
}

QJSValue KawaiiJSWrappers::World::getIllumination(quint64 i) const
{
  if(auto engine = getEngine(); engine)
    return IlluminationModel::createWrapper(obj.getIlluminationModelBase(i), engine);
  else
    return QJSValue::UndefinedValue;
}

QJSValue KawaiiJSWrappers::World::findIllumination(const QString &name) const
{
  if(auto engine = getEngine(); engine)
    return IlluminationModel::createWrapper(obj.findIlluminationModelBase(name), engine);
  else
    return QJSValue::UndefinedValue;
}

QJSValue KawaiiJSWrappers::World::createLocation(const QString &name)
{
  if(auto engine = getEngine(); engine)
    {
      auto l = obj.createChild<::Location>();
      l->setObjectName(name);
      return Location::createWrapper(*l, engine);
    } else
    return QJSValue::UndefinedValue;
}

QJSValue KawaiiJSWrappers::World::getBringedLandscape(const QJSValue &landscapeInfo) const
{
  if(!landscapeInfo.isQObject()) return QJSValue::UndefinedValue;

  if(auto engine = getEngine(); engine)
    {
      auto landscapeInfoObj = qobject_cast<LandscapeInfo*>(landscapeInfo.toQObject());
      if(landscapeInfoObj)
        {
          auto landscape = obj.getBringedLandscape(&landscapeInfoObj->getObj());
          if(landscape)
            return Landscape::createWrapper(*landscape, engine);
        }
      return QJSValue::NullValue;
    } else
    return QJSValue::UndefinedValue;
}

QJSValue KawaiiJSWrappers::World::getMetadata(const QString &key) const
{
  const auto val = obj.getMetadata(key);
  if(!val.isNull())
    return val;
  else
    return QJSValue::NullValue;
}

bool KawaiiJSWrappers::World::canPlace(const QJSValue &entity, float x, float y, float z) const
{
  if(entity.isString())
    return obj.canPlace(entity.toString(), QVector3D(x,y,z));

  if(!entity.isQObject())
    return false;

  auto *qobj = entity.toQObject();

  if(auto *e = qobject_cast<Entity*>(qobj); e)
    return obj.canPlace(e->getObj(), QVector3D(x,y,z));

  else
    return false;
}

QJSValue KawaiiJSWrappers::World::getGearbox() const
{
  if(obj.hasGearbox())
    {
      auto &gearbox = obj.getGearbox();
      auto *gearboxWrapper = new Gearbox(gearbox, obj);

      connect(gearboxWrapper, &Gearbox::typeChanged, this, [this, gearboxWrapper]() {
          obj.setGearbox(
                std::unique_ptr<::Gearbox>(gearboxWrapper->getObj())
                );
        });

      connect(&obj, &::World::gearboxChanged, gearboxWrapper, [this, gearboxWrapper]() { gearboxWrapper->setObj(&obj.getGearbox()); });
      connect(this, &QObject::destroyed, gearboxWrapper, &QObject::deleteLater, Qt::DirectConnection);
      return engine->newQObject(gearboxWrapper);
    } else
    return QJSValue::NullValue;
}

QJSValue KawaiiJSWrappers::World::createGearbox(const QString &type)
{
  obj.setGearbox(::Gearbox::create(type, obj));
  return getGearbox();
}

void KawaiiJSWrappers::World::removeGearbox()
{
  obj.setGearbox({});
}

void KawaiiJSWrappers::World::forallEntities(const QJSValue &func) const
{
  if(auto engine = getEngine(); engine)
    if(func.isCallable())
      obj.forallEntities([&func, engine] (::Entity &e) { QJSValue(func).call({ Entity::createWrapper(e, engine) }); });
}

void KawaiiJSWrappers::World::swapInputs(quint64 i1, quint64 i2)
{
  obj.swapInputs(i1, i2);
}

void KawaiiJSWrappers::World::removeInput(quint64 i)
{
  obj.removeInput(i);
}

void KawaiiJSWrappers::World::enableInput(quint64 i)
{
  obj.getInput(i).active = true;
}

void KawaiiJSWrappers::World::disableInput(quint64 i)
{
  obj.getInput(i).active = false;
}

void KawaiiJSWrappers::World::setLocation(const QJSValue &l)
{
  if(l.isUndefined()) return;

  if(auto *locationJs = qobject_cast<KawaiiJSWrappers::Location*>(l.toQObject()); locationJs)
    obj.setLocation(&locationJs->getObj());
  else
    obj.setLocation(nullptr);
}

void KawaiiJSWrappers::World::setMetadata(const QString &key, const QJSValue &value)
{
  if(value.isUndefined() || value.isNull())
    obj.setMetadata(key, QString());
  else
    obj.setMetadata(key, value.toString());
}

void KawaiiJSWrappers::World::onPointerPress(float screenX, float screenY)
{
  obj.onPointerPress({screenX, screenY});
}

void KawaiiJSWrappers::World::onPointerRelease(float screenX, float screenY)
{
  obj.onPointerRelease({screenX, screenY});
}

void KawaiiJSWrappers::World::onPointerDoubleClick(float screenX, float screenY)
{
  obj.onPointerDoubleClick({screenX, screenY});
}

QJSEngine *KawaiiJSWrappers::World::getEngine()
{
  if(!engine)
    engine = qjsEngine(this);
  return engine;
}

QJSEngine *KawaiiJSWrappers::World::getEngine() const
{
  if(!engine)
    return qjsEngine(this);
  else
    return engine;
}
