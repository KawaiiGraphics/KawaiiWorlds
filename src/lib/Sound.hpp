#ifndef SOUND_HPP
#define SOUND_HPP

#include "KawaiiWorlds_global.hpp"
#include <Kawaii3D/KawaiiDataUnit.hpp>

#include <QMediaPlayer>
#include <QAudioOutput>
#include <QBuffer>

#include <optional>
#include <random>

class KAWAIIWORLDS_SHARED_EXPORT Sound: public KawaiiDataUnit
{
  Q_OBJECT
  KAWAII_UNIT_DEF(Sound);
  Q_PROPERTY(const QString MementoType MEMBER nodeType DESIGNABLE false CONSTANT);

  QUrl url;
  QMediaPlayer player;
  QAudioOutput audioOut;

  QMetaObject::Connection onPlayerDurationChanged;

  std::unique_ptr<QBuffer> extractedMemento;
  std::unique_ptr<std::uniform_int_distribution<uint64_t>> positionsDistribution;
  std::optional<uint64_t> pendingPosition;


  Sound(const Sound &) = delete;
  Sound& operator=(const Sound&) = delete;
  Sound() = default;

public:
  struct random_pos_t {};
  static constexpr random_pos_t random_pos = {};

  Sound(const QString &fName);
  ~Sound();

  void play(const std::chrono::milliseconds &position, int volume = 33);
  void play(random_pos_t, int volume = 33);
  void pause();
  void stop();

  inline bool autoRepeat() const
  { return loopEnabled; }

  void setAutoRepeat(bool val);

  static Sound* createFromMemento(sib_utils::memento::Memento::DataReader &mem);

signals:
  void finished(Sound *);

  // TreeNode interface
protected:
  void writeBinary(sib_utils::memento::Memento::DataMutator &mem) const override;
  void writeText(sib_utils::memento::Memento::DataMutator &mem) const override;
  void read(sib_utils::memento::Memento::DataReader &mem) override;



  //IMPLEMENT
private:
  void playRequest(int volume);
  void setPlaybackPos(uint64_t pos);
  void seek(uint64_t pos);

  int requestedVolume;
  bool playRequested;
  bool loopEnabled;
};

#endif // SOUND_HPP
