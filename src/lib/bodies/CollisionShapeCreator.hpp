#ifndef COLLISIONSHAPECREATOR_HPP
#define COLLISIONSHAPECREATOR_HPP

#include "KawaiiWorlds_global.hpp"

#include <memory>
#include <sib_utils/Serializable.hpp>
#include <Kawaii3D/Geometry/KawaiiModel3D.hpp>
#include <BulletCollision/CollisionShapes/btCollisionShape.h>

class KAWAIIWORLDS_SHARED_EXPORT CollisionShapeCreator: public sib_utils::Serializable
{
public:
  CollisionShapeCreator(std::string_view adapterId);

  virtual std::shared_ptr<btCollisionShape> createCollisionShape(KawaiiModel3D *model) const = 0;



  inline static std::unique_ptr<CollisionShapeCreator> loadFrom(QDataStream &st)
  {
    auto res = sib_utils::Serializable::loadFrom(st, adapters);
    return std::unique_ptr<CollisionShapeCreator>(static_cast<CollisionShapeCreator*>(res.release()));
  }

  inline static std::unique_ptr<CollisionShapeCreator> create(std::string_view type)
  {
    auto el = adapters.find(type);
    if(el == adapters.end())
      return std::unique_ptr<CollisionShapeCreator>();

    auto result = el->second.creator();
    return std::unique_ptr<CollisionShapeCreator>(static_cast<CollisionShapeCreator*>(result.release()));
  }

  inline static std::unique_ptr<CollisionShapeCreator> create(const QString &type)
  {
    const auto typeUtf8 = type.toUtf8();
    return create({typeUtf8.constData(), static_cast<size_t>(typeUtf8.size())});
  }

protected:
  static std::unordered_map<std::string_view, ByteAdapter> adapters;

  template<class T> using AdapterRegistrator = sib_utils::Serializable::AdapterRegistrator<T, adapters>;
};

#endif // COLLISIONSHAPECREATOR_HPP
