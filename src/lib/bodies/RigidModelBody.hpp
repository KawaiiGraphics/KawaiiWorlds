#ifndef RIGIDMODELBODY_HPP
#define RIGIDMODELBODY_HPP

#include "ModelInstanceBody.hpp"
#include "AbstractRigidBody.hpp"

class KAWAIIWORLDS_SHARED_EXPORT RigidModelBody: public ModelInstanceBody, public AbstractRigidBody
{
  Q_GADGET

  static AdapterRegistrator<RigidModelBody> reg;

public:
  RigidModelBody();
  ~RigidModelBody();

  inline const QMetaObject& metaObject() const override { return staticMetaObject; }

  // ModelInstanceBody interface
protected:
  void onShapeChanged() override;
};


KAWAIIWORLDS_SHARED_EXPORT QDataStream& operator<<(QDataStream &st, const RigidModelBody &obj);
KAWAIIWORLDS_SHARED_EXPORT QDataStream& operator>>(QDataStream &st, RigidModelBody &obj);

#endif // RIGIDMODELBODY_HPP
