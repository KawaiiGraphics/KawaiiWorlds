#ifndef BODY_HPP
#define BODY_HPP

#include "CollisionShapeCreator.hpp"

#include <sib_utils/Serializable.hpp>

#include <Kawaii3D/Geometry/KawaiiModel3D.hpp>
#include <btBulletCollisionCommon.h>
#include <functional>
#include <QVector3D>
#include <memory>
#include <atomic>

class World;

class KAWAIIWORLDS_SHARED_EXPORT Body: public sib_utils::Serializable
{
  Q_GADGET

  Q_PROPERTY(QString collisionShapeCreator READ collisionShapeCreatorType
             WRITE setCollisionShapeCreatorType RESET removeCollisionShapeCreator)

  friend KAWAIIWORLDS_SHARED_EXPORT QDataStream& operator>>(QDataStream &st, Body &obj);

public:
  Body(std::string_view adapterId);
  virtual ~Body();

  inline bool hasCollisionObject() const { return !!btCollisionObj; }
  inline btCollisionObject& getBtCollisionObject() const { return *btCollisionObj; }

  virtual void setTransformation(const QVector3D &pos, const QQuaternion &rotation);
  void getTransformation(QVector3D &pos, QQuaternion &rotation);
  virtual bool setMotionState(btMotionState *motionState);
  inline virtual void setVisualTransformation([[maybe_unused]] const QVector3D &pos, [[maybe_unused]] const QQuaternion &rotation) {}

  virtual void setVelocity(const QVector3D &vel);
  virtual void setAngularVelocity(const QVector3D &angularVel);
  virtual void getVelocity(QVector3D &vel, QVector3D &angularVel);

  virtual void enable(World &world) = 0;
  virtual void disable() = 0;
  virtual void reenable(World &world) = 0;

  inline virtual void setMass([[maybe_unused]] float newMass) {}

  virtual btTransform getWorldTrsf() const;

  bool reenableIfNeeds(World &world);

  void handleCollisionObjChange(const std::function<void (btCollisionObject *, btCollisionObject *)> &func);
  void markCollisionObjUpdated();

  void handleOverlapingPairCacheInvalidated(const std::function<void()> &func);


  std::shared_ptr<btCollisionShape> getCollisionShape(KawaiiModel3D *model) const;


  template<typename T, typename... ArgV,
           class = std::enable_if_t<std::is_base_of_v<CollisionShapeCreator, T>>>
  void setCollisionShapeCreator(ArgV&&... args)
  {
    collisionShapeCreator = std::make_unique<T>(std::forward<ArgV>(args)...);
  }

  void removeCollisionShapeCreator();

  void setCollisionShapeCreatorType(const QString &collisionShapeCreatorType);
  QString collisionShapeCreatorType() const;

  inline bool hasCollisionShapeCreator() const { return !!collisionShapeCreator; }
  inline CollisionShapeCreator& collisionShapeCreatorRef() { return *collisionShapeCreator; }
  inline const CollisionShapeCreator& collisionShapeCreatorRef() const { return *collisionShapeCreator; }

  bool getAabb(const btTransform& t, btVector3& aabbMin, btVector3& aabbMax) const;



  inline virtual const QMetaObject& metaObject() const { return staticMetaObject; }

  inline static std::unique_ptr<Body> loadFrom(QDataStream &st)
  {
    auto res = sib_utils::Serializable::loadFrom(st, adapters);
    return std::unique_ptr<Body>(static_cast<Body*>(res.release()));
  }

  inline static std::unique_ptr<Body> create(std::string_view type)
  {
    auto el = adapters.find(type);
    if(el == adapters.end())
      return std::unique_ptr<Body>();

    auto result = el->second.creator();
    return std::unique_ptr<Body>(static_cast<Body*>(result.release()));
  }

  inline static std::unique_ptr<Body> create(const QString &type)
  {
    const auto typeUtf8 = type.toUtf8();
    return create(std::string_view(typeUtf8.constData(), static_cast<size_t>(typeUtf8.size())));
  }

  static std::unique_ptr<Body> create(const QVariantMap &data);

  void setProperty(const QByteArray &propName, const QVariant &value);
  QVariant getProperty(const QByteArray &propName);

protected:
  static std::unordered_map<std::string_view, ByteAdapter> adapters;

  template<class T> using AdapterRegistrator = sib_utils::Serializable::AdapterRegistrator<T, adapters>;


  std::atomic_flag reenableNotNeeded;

  std::atomic_flag collisionObjNotChanged;

  std::atomic_flag overlapingPairCacheValid;


  /// Collision Object should be created as soon as possible and should never be destroyed (but obviously will be destroyed in the ~Body())
  void setBtCollisionObject(std::unique_ptr<btCollisionObject> &&newBtCollisionObject);

private:
  std::unique_ptr<btCollisionObject> btCollisionObj;
  std::unique_ptr<btCollisionObject> oldBtCollisionObj;

  std::unique_ptr<CollisionShapeCreator> collisionShapeCreator;
};


KAWAIIWORLDS_SHARED_EXPORT QDataStream& operator<<(QDataStream &st, const Body &obj);
KAWAIIWORLDS_SHARED_EXPORT QDataStream& operator>>(QDataStream &st, Body &obj);

#endif // BODY_HPP
