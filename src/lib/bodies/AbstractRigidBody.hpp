#ifndef ABSTRACTRIGIDBODY_HPP
#define ABSTRACTRIGIDBODY_HPP

#include "Body.hpp"

class KAWAIIWORLDS_SHARED_EXPORT AbstractRigidBody : public virtual Body
{
public:
  AbstractRigidBody();
  ~AbstractRigidBody();

  bool rigidBodyCreated() const;

  // Body interface
  void setMass(float newMass) override;
  bool setMotionState(btMotionState *motionState) override;

  void setVelocity(const QVector3D &vel) override;
  void setAngularVelocity(const QVector3D &angularVel) override;
  void getVelocity(QVector3D &vel, QVector3D &angularVel) override;

protected:
  void onShapeChanged(btCollisionShape *newShape);



  // IMPLEMENT
private:
  btRigidBody *btRigid;
};

#endif // ABSTRACTRIGIDBODY_HPP
