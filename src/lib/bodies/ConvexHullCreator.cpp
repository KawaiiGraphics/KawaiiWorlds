#include "ConvexHullCreator.hpp"

#include <BulletCollision/CollisionShapes/btConvexHullShape.h>
#include <Kawaii3D/Geometry/KawaiiMesh3D.hpp>
#include <QtConcurrent>
#include <cstring>

Q_DECLARE_METATYPE(std::weak_ptr<btConvexHullShape>)

namespace {
  class ModelConvexHullShape: public btConvexHullShape
  {
    QMetaObject::Connection onModelTopologyChanged;
    QMetaObject::Connection onModelGeometryChanged;

    struct AssignedMesh
    {
      QMetaObject::Connection onGeometryChange;
      std::vector<std::pair<btVector3*, const QVector4D*>> vertexData;
    };
    std::unordered_map<KawaiiMesh3D*, AssignedMesh> assignedMeshes;

  public:
    ModelConvexHullShape(KawaiiModel3D *model, bool hullOptimized)
    {
      auto invalidateBtShape = [model] { model->setProperty("btConvexHull", {}); };

      onModelTopologyChanged = QObject::connect(model, &KawaiiModel3D::topologyChanged, invalidateBtShape);
      if(!hullOptimized)
        {
          model->forallMeshes([this](KawaiiMesh3D &mesh) {
            assignedMeshes[&mesh].onGeometryChange = QObject::connect(&mesh, &KawaiiMesh3D::verticesUpdated,
                                                                      [&mesh, this] { updateHullGeometry(mesh); });
          });
        } else
        onModelGeometryChanged = QObject::connect(model, &KawaiiModel3D::geometryChanged, invalidateBtShape);
    }

    ~ModelConvexHullShape()
    {
      QObject::disconnect(onModelTopologyChanged);
      QObject::disconnect(onModelGeometryChanged);
      for(auto& i: assignedMeshes)
        QObject::disconnect(i.second.onGeometryChange);
    }

    void createHullGeometry(KawaiiModel3D *model)
    {
      setLocalScaling({1,1,1});
      setMargin(0.03);
      model->forallMeshes([this](KawaiiMesh3D &mesh) {
        for(const auto &vert: mesh.getVertices()) {
            const auto *pos = &vert.positionRef();
            addPoint(btVector3(pos->x(), pos->y(), pos->z()), false);
          }
      });
    }

    void assignMeshes(KawaiiModel3D *model)
    {
      auto *hullVert = getUnscaledPoints();
      model->forallMeshes([this, &hullVert](KawaiiMesh3D &mesh) {
          auto &vertexData = assignedMeshes[&mesh].vertexData;
          vertexData.reserve(vertexData.size() + mesh.vertexCount());
          for(const auto &vert: mesh.getVertices()) {
              const auto *pos = &vert.positionRef();
              vertexData.emplace_back(hullVert++, pos);
            }
        });
    }

    void updateHullGeometry(KawaiiMesh3D &mesh)
    {
      if(auto el = assignedMeshes.find(&mesh); el != assignedMeshes.end())
        {
          const auto &vertexData = el->second.vertexData;
          QtConcurrent::map(vertexData, [](const std::pair<btVector3*, const QVector4D*> &i) {
              i.first->setX(i.second->x());
              i.first->setY(i.second->y());
              i.first->setZ(i.second->x());
            }).waitForFinished();
          setUserIndex(1);
        }
    }
  };
}

ConvexHullCreator::ConvexHullCreator():
  CollisionShapeCreator(reg.adapterId)
{ }

std::shared_ptr<btCollisionShape> ConvexHullCreator::createCollisionShape(KawaiiModel3D *model) const
{
  auto btModel = model->property("btConvexHull").value<std::weak_ptr<btConvexHullShape>>().lock();
  if(!btModel)
    {
      btModel = std::make_shared<ModelConvexHullShape>(model, false);
      model->setProperty("btConvexHull", QVariant::fromValue<std::weak_ptr<btConvexHullShape>>(btModel));

      static_cast<ModelConvexHullShape&>(*btModel).createHullGeometry(model);
      static_cast<ModelConvexHullShape&>(*btModel).assignMeshes(model);
      btModel->recalcLocalAabb();
    }
  return btModel;
}

OptimizedConvexHullCreator::OptimizedConvexHullCreator():
  CollisionShapeCreator(reg.adapterId)
{ }

std::shared_ptr<btCollisionShape> OptimizedConvexHullCreator::createCollisionShape(KawaiiModel3D *model) const
{
  auto btModel = model->property("btConvexHull").value<std::weak_ptr<btConvexHullShape>>().lock();
  if(!btModel)
    {
      btModel = std::make_shared<ModelConvexHullShape>(model, true);
      model->setProperty("btConvexHull", QVariant::fromValue<std::weak_ptr<btConvexHullShape>>(btModel));

      static_cast<ModelConvexHullShape&>(*btModel).createHullGeometry(model);
      btModel->optimizeConvexHull();
      btModel->recalcLocalAabb();
    }
  return btModel;
}
