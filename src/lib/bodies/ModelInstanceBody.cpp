#include "ModelInstanceBody.hpp"
#include "World.hpp"

ModelInstanceBody::ModelInstanceBody():
  Body(reg.adapterId),
  lastModel(nullptr),
  positionDirty(true),
  rotationDirty(true),
  onShapeChangedQueued(false)
{ }

ModelInstanceBody::~ModelInstanceBody()
{
  freeGpuBuffers();
  btShape.reset();
  QObject::disconnect(onModelGeometryChanged);
}

void ModelInstanceBody::setVisualTransformation(const QVector3D &pos, const QQuaternion &rotation)
{
  if(!qFuzzyCompare(pos, instance.position().toVector3D()))
    {
      instance.position() = QVector4D(pos, 0);
      positionDirty = true;
    }
  if(!qFuzzyCompare(rotation, instance.rotation()))
    {
      instance.rotation() = rotation;
      rotationDirty = true;
    }
  writeGpuBufs();
}

void ModelInstanceBody::enable(World &world)
{
  reenableNotNeeded.test_and_set();

  if(instance.meshInstances)
    return reenable(world);

  auto *model = world.res().getModel(modelName);
  if(!model)
    throw std::runtime_error("ModelInstanceBody::enable: model not exists");

  auto *material = world.res().getMaterial(materialName);
  if(!material)
    throw std::invalid_argument("ModelInstanceBody::enable: material not exists");

  instanceModel(world, model, material);
}

void ModelInstanceBody::disable()
{
  freeGpuBuffers();
}

void ModelInstanceBody::reenable(World &world)
{
  auto *model = world.res().getModel(modelName);
  if(!model)
    throw std::runtime_error("ModelInstanceBody::enable: model not exists");

  auto *material = world.res().getMaterial(materialName);
  if(!material)
    throw std::invalid_argument("ModelInstanceBody::enable: material not exists");

  freeGpuBuffers();
  instanceModel(world, model, material);
}

QString ModelInstanceBody::getModelName() const
{
  return modelName;
}

void ModelInstanceBody::setModelName(const QString &newModelName)
{
  if(modelName == newModelName) return;

  modelName = newModelName;
  reenableNotNeeded.clear();
}

QString ModelInstanceBody::getMaterialName() const
{
  return materialName;
}

void ModelInstanceBody::setMaterialName(const QString &newMaterialName)
{
  if(materialName == newMaterialName) return;

  materialName = newMaterialName;
  reenableNotNeeded.clear();
}

btTransform ModelInstanceBody::getWorldTrsf() const
{
  const auto tr = Body::getWorldTrsf();

  const_cast<ModelInstanceBody*>(this)->setVisualTransformation(
        { tr.getOrigin().x(), tr.getOrigin().y(), tr.getOrigin().z() },

        { tr.getRotation().w(),
          tr.getRotation().x(), tr.getRotation().y(), tr.getRotation().z() }
        );

  return tr;
}

void ModelInstanceBody::writeGpuBufs()
{
  instance.updateLocation(positionDirty, rotationDirty);
  positionDirty = rotationDirty = false;
}

void ModelInstanceBody::freeGpuBuffers()
{
  if(!instance.meshInstances) return;

  instance = ModelInstance();
}

void ModelInstanceBody::instanceModel(World &world, KawaiiModel3D *model, KawaiiMaterial *material)
{
  instance = world.res().instanceModel(model, material);
  if(!btShape || lastModel != model)
    {
      QObject::disconnect(onModelGeometryChanged);
      lastModel = model;

      auto setBtShapeFunc = [this] {
          btShape = getCollisionShape(lastModel);
          if(!onShapeChangedQueued)
            {
              onShapeChangedQueued = true;
              QMetaObject::invokeMethod(lastModel, [this] {
                  onShapeChanged();
                  onShapeChangedQueued = false;
                }, Qt::QueuedConnection);
            }
        };
      btShape = getCollisionShape(lastModel);
      onShapeChanged();
      onModelGeometryChanged = QObject::connect(lastModel, &KawaiiModel3D::geometryChanged, setBtShapeFunc);
    }
}



QDataStream& operator<<(QDataStream &st, const ModelInstanceBody &obj)
{
  return st << static_cast<const Body&>(obj) << obj.getModelName() << obj.getMaterialName();
}

QDataStream& operator>>(QDataStream &st, ModelInstanceBody &obj)
{
  st >> static_cast<Body&>(obj);

  QString str;
  st >> str;
  obj.setModelName(str);

  st >> str;
  obj.setMaterialName(str);

  return st;
}
