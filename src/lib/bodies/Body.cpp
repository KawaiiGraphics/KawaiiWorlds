#include "Body.hpp"

#include <QQuaternion>
#include <QMetaProperty>

Body::Body(std::string_view adapterId):
  sib_utils::Serializable(adapterId, adapters)
{
  overlapingPairCacheValid.test_and_set();
  reenableNotNeeded.test_and_set();
}

Body::~Body()
{
  btCollisionObj.reset();
  if(oldBtCollisionObj)
    oldBtCollisionObj.reset();
}

void Body::setTransformation(const QVector3D &pos, const QQuaternion &rotation)
{
  if(Q_LIKELY(btCollisionObj))
    {
      btTransform tr;
      tr.setIdentity();
      tr.setOrigin(btVector3(pos.x(), pos.y(), pos.z()));
      tr.setRotation(btQuaternion(
                       rotation.x(), rotation.y(), rotation.z(),
                       rotation.scalar()));
      btCollisionObj->setWorldTransform(tr);
    }
  setVisualTransformation(pos, rotation);
}

void Body::getTransformation(QVector3D &pos, QQuaternion &rotation)
{
  const auto tr = getWorldTrsf();
  pos = { tr.getOrigin().x(), tr.getOrigin().y(), tr.getOrigin().z() };

  rotation = { tr.getRotation().w(),
               tr.getRotation().x(), tr.getRotation().y(), tr.getRotation().z() };
}

bool Body::setMotionState([[maybe_unused]] btMotionState *motionState)
{
  return false;
}

void Body::setVelocity(const QVector3D &vel)
{
  if(Q_LIKELY(btCollisionObj))
    btCollisionObj->setInterpolationLinearVelocity({vel.x(), vel.y(), vel.z()});
}

void Body::setAngularVelocity(const QVector3D &angularVel)
{
  if(Q_LIKELY(btCollisionObj))
    btCollisionObj->setInterpolationAngularVelocity({angularVel.x(), angularVel.y(), angularVel.z()});
}

void Body::getVelocity(QVector3D &vel, QVector3D &angularVel)
{
  if(Q_LIKELY(btCollisionObj))
    {
      const auto btVel = btCollisionObj->getInterpolationLinearVelocity();
      vel = {btVel.x(), btVel.y(), btVel.z()};

      const auto btAngularVel = btCollisionObj->getInterpolationAngularVelocity();
      angularVel = {btAngularVel.x(), btAngularVel.y(), btAngularVel.z()};
    } else
    {
      vel = angularVel = {};
    }
}

btTransform Body::getWorldTrsf() const
{
  if(Q_LIKELY(btCollisionObj))
    return btCollisionObj->getWorldTransform();
  else
    {
      btTransform btTrsf;
      btTrsf.setIdentity();
      return btTrsf;
    }
}

bool Body::reenableIfNeeds(World &world)
{
  if(!reenableNotNeeded.test_and_set())
    {
      reenable(world);
      return true;
    } else
    return false;
}

void Body::handleCollisionObjChange(const std::function<void (btCollisionObject *, btCollisionObject *)> &func)
{
  if(Q_UNLIKELY(!func)) return;

  if(!collisionObjNotChanged.test_and_set())
    {
      func(btCollisionObj.get(), oldBtCollisionObj.get());
      oldBtCollisionObj.reset();
    }
}

void Body::markCollisionObjUpdated()
{
  if(!collisionObjNotChanged.test_and_set())
    oldBtCollisionObj.reset();
}

void Body::handleOverlapingPairCacheInvalidated(const std::function<void ()> &func)
{
  if(Q_UNLIKELY(!func)) return;

  if(!overlapingPairCacheValid.test_and_set())
    func();
}

std::shared_ptr<btCollisionShape> Body::getCollisionShape(KawaiiModel3D *model) const
{
  if(collisionShapeCreator)
    return collisionShapeCreator->createCollisionShape(model);
  else
    return nullptr;
}

void Body::removeCollisionShapeCreator()
{
  collisionShapeCreator.reset();
}

void Body::setCollisionShapeCreatorType(const QString &collisionShapeCreatorType)
{
  const auto typeUtf8 = collisionShapeCreatorType.toUtf8();
  const std::string_view typeStdUtf8 = {typeUtf8.constData(), static_cast<size_t>(typeUtf8.size())};

  if(collisionShapeCreator && collisionShapeCreator->getSerializableType() == typeStdUtf8)
    return;

  collisionShapeCreator = CollisionShapeCreator::create(typeStdUtf8);
}

QString Body::collisionShapeCreatorType() const
{
  if(!collisionShapeCreator)
    return QString();

  const auto typeStdUtf8 = collisionShapeCreator->getSerializableType();
  return QString::fromUtf8(typeStdUtf8.data(), typeStdUtf8.size());
}

bool Body::getAabb(const btTransform &t, btVector3 &aabbMin, btVector3 &aabbMax) const
{
  if(!btCollisionObj || !btCollisionObj->getCollisionShape()) return false;

  auto btShape = btCollisionObj->getCollisionShape();
  if(btShape->getUserIndex() == 1)
    {
      if(auto *p = dynamic_cast<btPolyhedralConvexAabbCachingShape*>(btShape); p)
        {
          p->recalcLocalAabb();
          p->setUserIndex(-1);
        }
    }

  btCollisionObj->getCollisionShape()->getAabb(t, aabbMin, aabbMax);
  return true;
}

// #include "ModelInstanceBody.hpp"

std::unique_ptr<Body> Body::create(const QVariantMap &data)
{
  if(data.empty())
    return nullptr;

  auto body = Body::create(data.value(QStringLiteral("type")).toString());
  if(body)
    for(auto i = data.cbegin(); i != data.cend(); ++i)
      {
        auto keyUtf8 = i.key().toUtf8();
        body->setProperty(keyUtf8, i.value());
      }
  return body;
}

void Body::setProperty(const QByteArray &propName, const QVariant &value)
{
  auto &bodyMetaObj = metaObject();
  int propertyIndex = bodyMetaObj.indexOfProperty(propName.constData());
  if(Q_LIKELY(propertyIndex > -1))
    {
      void *actualGadget = this;
      int downcastIndex = bodyMetaObj.indexOfMethod("downcast(Body*)");
      if(downcastIndex > -1)
        bodyMetaObj.method(downcastIndex).invokeOnGadget(this, Q_RETURN_ARG(void*, actualGadget), Q_ARG(Body*, this));

      auto property = bodyMetaObj.property(propertyIndex);
      property.writeOnGadget(actualGadget, value);
    }
}

QVariant Body::getProperty(const QByteArray &propName)
{
  auto &bodyMetaObj = metaObject();
  int propertyIndex = bodyMetaObj.indexOfProperty(propName.constData());
  if(Q_LIKELY(propertyIndex > -1))
    {
      void *actualGadget = this;
      if(int downcastIndex = bodyMetaObj.indexOfMethod("downcast(Body*)"); downcastIndex > -1)
        bodyMetaObj.method(downcastIndex).invokeOnGadget(this, Q_RETURN_ARG(void*, actualGadget), Q_ARG(Body*, this));

      auto property = bodyMetaObj.property(propertyIndex);
      return property.readOnGadget(actualGadget);
    }
  return QVariant();
}

void Body::setBtCollisionObject(std::unique_ptr<btCollisionObject> &&newBtCollisionObject)
{
  oldBtCollisionObj = std::move(btCollisionObj);
  btCollisionObj = std::move(newBtCollisionObject);
  if(btCollisionObj && btCollisionObj->getCollisionShape())
    btCollisionObj->setAnisotropicFriction(btCollisionObj->getCollisionShape()->getAnisotropicRollingFrictionDirection(),
                                           btCollisionObject::CF_ANISOTROPIC_ROLLING_FRICTION);

  collisionObjNotChanged.clear();
}


QDataStream& operator<<(QDataStream &st, const Body &obj)
{
  if(Q_LIKELY(obj.hasCollisionShapeCreator()))
    obj.collisionShapeCreatorRef().saveTo(st);
  else
    sib_utils::Serializable::stubSaveTo(st);

  return st;
}

QDataStream& operator>>(QDataStream &st, Body &obj)
{
  obj.collisionShapeCreator = CollisionShapeCreator::loadFrom(st);
  return st;
}
