#include "RigidModelBody.hpp"
#include "ConvexHullCreator.hpp"

#include <BulletDynamics/Dynamics/btRigidBody.h>

RigidModelBody::RigidModelBody():
  Body(reg.adapterId),
  ModelInstanceBody(),
  AbstractRigidBody()
{
  setCollisionShapeCreator<OptimizedConvexHullCreator>();
}

RigidModelBody::~RigidModelBody()
{ }

void RigidModelBody::onShapeChanged()
{
  AbstractRigidBody::onShapeChanged(getBtShape());
}


KAWAIIWORLDS_SHARED_EXPORT QDataStream& operator<<(QDataStream &st, const RigidModelBody &obj)
{
  return st << static_cast<const ModelInstanceBody&>(obj);
}

KAWAIIWORLDS_SHARED_EXPORT QDataStream& operator>>(QDataStream &st, RigidModelBody &obj)
{
  return st >> static_cast<ModelInstanceBody&>(obj);
}
