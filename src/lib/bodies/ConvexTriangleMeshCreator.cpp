#include "ConvexTriangleMeshCreator.hpp"

#include <BulletCollision/CollisionShapes/btTriangleIndexVertexArray.h>
#include <BulletCollision/CollisionShapes/btConvexTriangleMeshShape.h>
#include <Kawaii3D/Geometry/KawaiiMesh3D.hpp>

#include <QVariant>

Q_DECLARE_METATYPE(std::weak_ptr<btConvexTriangleMeshShape>)

namespace {
  class ModelConvexMeshShape: public btConvexTriangleMeshShape
  {
    QMetaObject::Connection onModelTopologyChanged;
    QMetaObject::Connection onModelGeometryChanged;
  public:
    ModelConvexMeshShape(KawaiiModel3D *model, btStridingMeshInterface *meshInterface, bool calcAabb = true):
      btConvexTriangleMeshShape(meshInterface, calcAabb)
    {
      onModelTopologyChanged = QObject::connect(model, &KawaiiModel3D::topologyChanged, model, [this] { setUserIndex(1); });
      onModelGeometryChanged = QObject::connect(model, &KawaiiModel3D::geometryChanged, model, [this] { setUserIndex(1); });
    }

    ~ModelConvexMeshShape()
    {
      QObject::disconnect(onModelTopologyChanged);
      QObject::disconnect(onModelGeometryChanged);
    }
  };
}

ConvexTriangleMeshCreator::ConvexTriangleMeshCreator():
  CollisionShapeCreator(reg.adapterId)
{ }

std::shared_ptr<btCollisionShape> ConvexTriangleMeshCreator::createCollisionShape(KawaiiModel3D *model) const
{
  auto btModel = model->property("btConvexMesh").value<std::weak_ptr<btConvexTriangleMeshShape>>().lock();
  if(!btModel)
    {
      btTriangleIndexVertexArray *btIndexVertexArray = new btTriangleIndexVertexArray;
      model->forallMeshes([btIndexVertexArray](KawaiiMesh3D &mesh) {
        btIndexedMesh btMesh;
        btMesh.m_numTriangles = static_cast<int>(mesh.trianglesCount());
        btMesh.m_triangleIndexBase = reinterpret_cast<const unsigned char*>(mesh.getRawTriangles().data());
        btMesh.m_triangleIndexStride = sizeof(KawaiiTriangle3D);

        btMesh.m_numVertices = static_cast<int>(mesh.vertexCount());
        btMesh.m_vertexBase = reinterpret_cast<const unsigned char*>(mesh.getVertices().data());
        btMesh.m_vertexStride = sizeof(KawaiiPoint3D);

        btMesh.m_indexType = PHY_INTEGER;
        btMesh.m_vertexType = PHY_FLOAT;
        btIndexVertexArray->addIndexedMesh(btMesh, btMesh.m_indexType);
      });

      btModel = std::make_shared<ModelConvexMeshShape>(model, btIndexVertexArray);
      model->setProperty("btConvexMesh", QVariant::fromValue<std::weak_ptr<btConvexTriangleMeshShape>>(btModel));

      btModel->setLocalScaling({1,1,1});
      btModel->setMargin(0.03);
      btModel->setUserIndex(1);
    }
  return btModel;
}

