#include <Kawaii3D/KawaiiDataUnit.hpp>

#include "ConvexHullCreator.hpp"
#include "ConvexTriangleMeshCreator.hpp"
#include "GImpactTriangleMeshCreator.hpp"
#include "RigidModelBody.hpp"
#include "ActorBody.hpp"

KAWAII_COMMON_IMPLEMENT(Body::adapters);

Body::AdapterRegistrator<ModelInstanceBody> ModelInstanceBody::reg("model_instance_body");
Body::AdapterRegistrator<RigidModelBody> RigidModelBody::reg("rigid_model_body");
Body::AdapterRegistrator<ActorBody> ActorBody::reg("actor_body");

KAWAII_COMMON_IMPLEMENT(CollisionShapeCreator::adapters);

CollisionShapeCreator::AdapterRegistrator<ConvexHullCreator> ConvexHullCreator::reg("convex_hull_creator");
CollisionShapeCreator::AdapterRegistrator<OptimizedConvexHullCreator> OptimizedConvexHullCreator::reg("optimized_convex_hull_creator");
CollisionShapeCreator::AdapterRegistrator<ConvexTriangleMeshCreator> ConvexTriangleMeshCreator::reg("convex_triangle_mesh_creator");
CollisionShapeCreator::AdapterRegistrator<GimpactTriangleMeshCreator> GimpactTriangleMeshCreator::reg("g_impact_mesh_creator");
