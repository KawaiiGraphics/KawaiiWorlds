#ifndef MODELINSTANCEBODY_HPP
#define MODELINSTANCEBODY_HPP

#include <Kawaii3D/Geometry/KawaiiMeshInstance.hpp>
#include <Kawaii3D/Geometry/KawaiiModel3D.hpp>
#include <Kawaii3D/KawaiiGpuBuf.hpp>

#include <QQuaternion>
#include <QVector3D>
#include <QVector4D>

#include "Body.hpp"
#include "ModelInstance.hpp"

class KAWAIIWORLDS_SHARED_EXPORT ModelInstanceBody: public virtual Body
{
  Q_GADGET

  Q_PROPERTY(QString model READ getModelName WRITE setModelName)
  Q_PROPERTY(QString material READ getMaterialName WRITE setMaterialName)

  static AdapterRegistrator<ModelInstanceBody> reg;

public:
  ModelInstanceBody();
  ~ModelInstanceBody();

  QString getModelName() const;
  void setModelName(const QString &newModelName);

  QString getMaterialName() const;
  void setMaterialName(const QString &newMaterialName);

  inline const QMetaObject& metaObject() const override { return staticMetaObject; }

  Q_INVOKABLE inline void *downcast(Body *body)
  { return dynamic_cast<ModelInstanceBody*>(body); }

  // Body interface
  btTransform getWorldTrsf() const override;
  void setVisualTransformation(const QVector3D &pos, const QQuaternion &rotation) override;

  void enable(World &world) override;
  void disable() override;
  void reenable(World &world) override;


protected:
  inline btCollisionShape* getBtShape() const
  { return btShape.get(); }

  inline virtual void onShapeChanged() {}



  // IMPLEMENT
private:
  QString modelName;
  QString materialName;

  KawaiiModel3D *lastModel;
  QMetaObject::Connection onModelGeometryChanged;
  ModelInstance instance;

  std::shared_ptr<btCollisionShape> btShape;

  bool positionDirty;
  bool rotationDirty;

  bool onShapeChangedQueued;

  void writeGpuBufs();

  void freeGpuBuffers();

  void instanceModel(World &world, KawaiiModel3D *model, KawaiiMaterial *material);
};


KAWAIIWORLDS_SHARED_EXPORT QDataStream& operator<<(QDataStream &st, const ModelInstanceBody &obj);
KAWAIIWORLDS_SHARED_EXPORT QDataStream& operator>>(QDataStream &st, ModelInstanceBody &obj);

#endif // MODELINSTANCEBODY_HPP
