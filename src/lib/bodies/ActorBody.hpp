#ifndef ACTORBODY_HPP
#define ACTORBODY_HPP

#include <Kawaii3D/Geometry/KawaiiSkeletalAnimation.hpp>
#include <Kawaii3D/Geometry/KawaiiMeshInstance.hpp>
#include <Kawaii3D/Geometry/KawaiiModel3D.hpp>
#include <Kawaii3D/KawaiiGpuBuf.hpp>

#include <QQuaternion>
#include <QVector3D>
#include <QVector4D>
#include <QTimer>

#include "AbstractRigidBody.hpp"
#include "ModelInstance.hpp"

class KAWAIIWORLDS_SHARED_EXPORT ActorBody: public AbstractRigidBody
{
  Q_GADGET

  Q_PROPERTY(QString model READ getModelName WRITE setModelName FINAL)
  Q_PROPERTY(QString material READ getMaterialName WRITE setMaterialName FINAL)
  Q_PROPERTY(QString animationName READ getAnimationName WRITE setAnimationName FINAL)

  Q_PROPERTY(float animationPositionSec READ getAnimationPositionSec WRITE setAnimationPositionSec FINAL)
  Q_PROPERTY(int animationTimerIntervalMs READ getAnimationTimerIntervalMs WRITE setAnimationTimerIntervalMs FINAL)
  Q_PROPERTY(float animationAccelerator READ getAnimationAccelerator WRITE setAnimationAccelerator FINAL)
  Q_PROPERTY(bool animateMoveX READ getAnimateMoveX WRITE setAnimateMoveX FINAL)
  Q_PROPERTY(bool animateMoveY READ getAnimateMoveY WRITE setAnimateMoveY FINAL)
  Q_PROPERTY(bool animateMoveZ READ getAnimateMoveZ WRITE setAnimateMoveZ FINAL)
  Q_PROPERTY(bool animateRotationX READ getAnimateRotationX WRITE setAnimateRotationX FINAL)
  Q_PROPERTY(bool animateRotationY READ getAnimateRotationY WRITE setAnimateRotationY FINAL)
  Q_PROPERTY(bool animateRotationZ READ getAnimateRotationZ WRITE setAnimateRotationZ FINAL)
  Q_PROPERTY(QString animationDriverStr READ getAnimationDriverStr WRITE setAnimationDriverStr FINAL)

  static AdapterRegistrator<ActorBody> reg;

protected:
  ActorBody(std::string_view adapterId);

public:
  enum class AnimationDriver: uint8_t
  {
    None = 0,
    Timer,
    OnMove
  };

  ActorBody();
  ~ActorBody();

  QString getModelName() const;
  void setModelName(const QString &newModelName);

  QString getMaterialName() const;
  void setMaterialName(const QString &newMaterialName);

  const QString &getAnimationName() const;
  void setAnimationName(const QString &newAnimationName);

  float getAnimationPositionSec() const;
  void setAnimationPositionSec(float sec);

  int getAnimationTimerIntervalMs() const;
  void setAnimationTimerIntervalMs(int ms);

  float getAnimationAccelerator() const;
  void setAnimationAccelerator(float newAnimationAccelerator);

  AnimationDriver getAnimationDriver() const;
  void setAnimationDriver(AnimationDriver newAnimationDriver);

  const QString &getAnimationDriverStr() const;
  void setAnimationDriverStr(const QString &newAnimationDriver);

  bool getAnimateMoveX() const;
  void setAnimateMoveX(bool newAnimateMoveX);

  bool getAnimateMoveY() const;
  void setAnimateMoveY(bool newAnimateMoveY);

  bool getAnimateMoveZ() const;
  void setAnimateMoveZ(bool newAnimateMoveZ);

  bool getAnimateRotationX() const;
  void setAnimateRotationX(bool newAnimateRotationX);

  bool getAnimateRotationY() const;
  void setAnimateRotationY(bool newAnimateRotationY);

  bool getAnimateRotationZ() const;
  void setAnimateRotationZ(bool newAnimateRotationZ);

  inline const QMetaObject& metaObject() const override { return staticMetaObject; }

  Q_INVOKABLE inline void *downcast(Body *body)
  { return dynamic_cast<ActorBody*>(body); }

  // Body interface
  void setVisualTransformation(const QVector3D &pos, const QQuaternion &rotation) override;

  void enable(World &world) override;
  void disable() override;
  void reenable(World &world) override;

protected:
  inline btCollisionShape* getBtShape() const
  { return btShape.get(); }



  // IMPLEMENT
private:
  QString modelName;
  QString materialName;
  QString animationName;

  std::unique_ptr<KawaiiModel3D> model;
  QPointer<KawaiiModel3D> lastModel;
  QVector3D modelSize;
  QMetaObject::Connection onModelGeometryChanged;
  ModelInstance instance;

  std::shared_ptr<btCollisionShape> btShape;

  std::unique_ptr<KawaiiSkeletalAnimation::Instance> animationInstance;
  std::unique_ptr<QTimer> animationTimer;
  std::chrono::steady_clock::time_point animationT0;
  std::chrono::steady_clock::time_point lastAnimationT;
  float animationPositionSec;
  float animationLastPos;
  int animationTimerIntervalMs;
  float animationAccelerator;
  AnimationDriver animationDriver;
  bool animateMoveX;
  bool animateMoveY;
  bool animateMoveZ;
  bool animateRotationX;
  bool animateRotationY;
  bool animateRotationZ;

  bool positionDirty;
  bool rotationDirty;

  bool onShapeChangedQueued;

  void createAnimationTimer();
  void startAnimationTimer();

  void writeGpuBufs();

  void freeGpuBuffers();

  void instanceModel(World &world, KawaiiModel3D *model, KawaiiMaterial *material);
};


KAWAIIWORLDS_SHARED_EXPORT QDataStream& operator<<(QDataStream &st, const ActorBody &obj);
KAWAIIWORLDS_SHARED_EXPORT QDataStream& operator>>(QDataStream &st, ActorBody &obj);

#endif // ACTORBODY_HPP
