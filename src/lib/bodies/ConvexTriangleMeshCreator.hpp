#ifndef CONVEXTRIANGLEMESHCREATOR_HPP
#define CONVEXTRIANGLEMESHCREATOR_HPP

#include "CollisionShapeCreator.hpp"

class ConvexTriangleMeshCreator : public CollisionShapeCreator
{
  static AdapterRegistrator<ConvexTriangleMeshCreator> reg;

public:
  ConvexTriangleMeshCreator();

  // CollisionShapeCreator interface
  std::shared_ptr<btCollisionShape> createCollisionShape(KawaiiModel3D *model) const override;
};


KAWAIIWORLDS_SHARED_EXPORT inline QDataStream& operator<<(QDataStream &st, const ConvexTriangleMeshCreator&) { return st; }
KAWAIIWORLDS_SHARED_EXPORT inline QDataStream& operator>>(QDataStream &st, ConvexTriangleMeshCreator&) { return st; }

#endif // CONVEXTRIANGLEMESHCREATOR_HPP
