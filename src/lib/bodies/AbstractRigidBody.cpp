#include "AbstractRigidBody.hpp"

#include <QQuaternion>
#include <BulletDynamics/Dynamics/btRigidBody.h>

AbstractRigidBody::AbstractRigidBody():
  btRigid(nullptr)
{ }

AbstractRigidBody::~AbstractRigidBody()
{ }

bool AbstractRigidBody::rigidBodyCreated() const
{
  return btRigid;
}

void AbstractRigidBody::setMass(float newMass)
{
  if(btRigid)
    {
      btVector3 localInertia(newMass, newMass, newMass);
      if(btRigid->getCollisionShape())
        btRigid->getCollisionShape()->calculateLocalInertia(newMass, localInertia);
      btRigid->setMassProps(newMass, localInertia);
    }
}

bool AbstractRigidBody::setMotionState(btMotionState *motionState)
{
  if(Q_LIKELY(btRigid))
    {
      btRigid->setMotionState(motionState);
      return true;
    } else
    return false;
}

void AbstractRigidBody::setAngularVelocity(const QVector3D &angularVel)
{
  if(Q_LIKELY(btRigid))
    btRigid->setAngularVelocity({angularVel.x(), angularVel.y(), angularVel.z()});
  else
    Body::setAngularVelocity(angularVel);
}

void AbstractRigidBody::setVelocity(const QVector3D &vel)
{
  if(Q_LIKELY(btRigid))
    btRigid->setLinearVelocity({vel.x(), vel.y(), vel.z()});
  else
    Body::setVelocity(vel);
}

void AbstractRigidBody::getVelocity(QVector3D &vel, QVector3D &angularVel)
{
  if(Q_LIKELY(btRigid))
    {
      const auto btVel = btRigid->getLinearVelocity();
      vel = {btVel.x(), btVel.y(), btVel.z()};

      const auto btAngularVel = btRigid->getAngularVelocity();
      angularVel = {btAngularVel.x(), btAngularVel.y(), btAngularVel.z()};
    }
  else
    Body::getVelocity(vel, angularVel);
}

void AbstractRigidBody::onShapeChanged(btCollisionShape *newShape)
{
  btVector3 localInertia(0, 0, 0);
  if(!btRigid)
    {
      btRigidBody::btRigidBodyConstructionInfo rbInfo(1.0, nullptr, newShape, localInertia);
      btRigid = new btRigidBody(rbInfo);
      btRigid->setSleepingThresholds(0.1, 0.1);
      setBtCollisionObject(std::unique_ptr<btRigidBody>(btRigid));
    } else
    {
      if(btRigid->getCollisionShape() != newShape)
        btRigid->setCollisionShape(newShape);

      if(Q_LIKELY(btRigid->getCollisionShape()))
        {
          const btScalar mass = btRigid->getMass();
          btRigid->getCollisionShape()->calculateLocalInertia(mass, localInertia);
          btRigid->setMassProps(mass, localInertia);
        }

      overlapingPairCacheValid.clear();
    }
  if(Q_LIKELY(newShape))
    btRigid->setAnisotropicFriction(newShape->getAnisotropicRollingFrictionDirection(),
                                    btCollisionObject::CF_ANISOTROPIC_ROLLING_FRICTION);
}
