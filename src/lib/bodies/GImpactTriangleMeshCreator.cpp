#include "GImpactTriangleMeshCreator.hpp"

#include <BulletCollision/CollisionShapes/btTriangleIndexVertexArray.h>
#include <BulletCollision/Gimpact/btGImpactShape.h>
#include <Kawaii3D/Geometry/KawaiiMesh3D.hpp>

#include <QVariant>

Q_DECLARE_METATYPE(std::weak_ptr<btGImpactMeshShape>)

namespace {
  class ModelGImpactMeshShape: public btGImpactMeshShape
  {
    QMetaObject::Connection onModelTopologyChanged;
    QMetaObject::Connection onModelGeometryChanged;
  public:
    ModelGImpactMeshShape(KawaiiModel3D *model, btStridingMeshInterface *meshInterface):
      btGImpactMeshShape(meshInterface)
    {
      onModelTopologyChanged = QObject::connect(model, &KawaiiModel3D::topologyChanged, model, [this] { postUpdate(); });
      onModelGeometryChanged = QObject::connect(model, &KawaiiModel3D::geometryChanged, model, [this] { postUpdate(); });
    }

    ~ModelGImpactMeshShape()
    {
      QObject::disconnect(onModelTopologyChanged);
      QObject::disconnect(onModelGeometryChanged);
    }
  };
}

GimpactTriangleMeshCreator::GimpactTriangleMeshCreator():
  CollisionShapeCreator(reg.adapterId)
{ }

std::shared_ptr<btCollisionShape> GimpactTriangleMeshCreator::createCollisionShape(KawaiiModel3D *model) const
{
  auto btModel = model->property("btGImpactMesh").value<std::weak_ptr<btGImpactMeshShape>>().lock();
  if(!btModel)
    {
      btTriangleIndexVertexArray *btIndexVertexArray = new btTriangleIndexVertexArray;
      model->forallMeshes([btIndexVertexArray](KawaiiMesh3D &mesh) {
        btIndexedMesh btMesh;
        btMesh.m_numTriangles = static_cast<int>(mesh.trianglesCount());
        btMesh.m_triangleIndexBase = reinterpret_cast<const unsigned char*>(mesh.getRawTriangles().data());
        btMesh.m_triangleIndexStride = sizeof(KawaiiTriangle3D);

        btMesh.m_numVertices = static_cast<int>(mesh.vertexCount());
        btMesh.m_vertexBase = reinterpret_cast<const unsigned char*>(mesh.getVertices().data());
        btMesh.m_vertexStride = sizeof(KawaiiPoint3D);

        btMesh.m_indexType = PHY_INTEGER;
        btMesh.m_vertexType = PHY_FLOAT;
        btIndexVertexArray->addIndexedMesh(btMesh, btMesh.m_indexType);
      });

      btModel = std::make_shared<ModelGImpactMeshShape>(model, btIndexVertexArray);
      model->setProperty("btGImpactMesh", QVariant::fromValue<std::weak_ptr<btGImpactMeshShape>>(btModel));

      btModel->setLocalScaling({1,1,1});
      btModel->setMargin(0.03);
      btModel->postUpdate();
    }
  return btModel;
}

