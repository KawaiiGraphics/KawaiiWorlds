#ifndef CONVEXHULLCREATOR_HPP
#define CONVEXHULLCREATOR_HPP

#include "CollisionShapeCreator.hpp"

class KAWAIIWORLDS_SHARED_EXPORT ConvexHullCreator: public CollisionShapeCreator
{
  static AdapterRegistrator<ConvexHullCreator> reg;

public:
  ConvexHullCreator();

  // CollisionShapeCreator interface
  std::shared_ptr<btCollisionShape> createCollisionShape(KawaiiModel3D *model) const override;
};


KAWAIIWORLDS_SHARED_EXPORT inline QDataStream& operator<<(QDataStream &st, const ConvexHullCreator&) { return st; }
KAWAIIWORLDS_SHARED_EXPORT inline QDataStream& operator>>(QDataStream &st, ConvexHullCreator&) { return st; }



class KAWAIIWORLDS_SHARED_EXPORT OptimizedConvexHullCreator: public CollisionShapeCreator
{
  static AdapterRegistrator<OptimizedConvexHullCreator> reg;

public:
  OptimizedConvexHullCreator();

  // CollisionShapeCreator interface
  std::shared_ptr<btCollisionShape> createCollisionShape(KawaiiModel3D *model) const override;
};


KAWAIIWORLDS_SHARED_EXPORT inline QDataStream& operator<<(QDataStream &st, const OptimizedConvexHullCreator&) { return st; }
KAWAIIWORLDS_SHARED_EXPORT inline QDataStream& operator>>(QDataStream &st, OptimizedConvexHullCreator&) { return st; }

#endif // CONVEXHULLCREATOR_HPP
