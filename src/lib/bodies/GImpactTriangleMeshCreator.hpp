#ifndef GIMPACTTRIANGLEMESHCREATOR_HPP
#define GIMPACTTRIANGLEMESHCREATOR_HPP

#include "CollisionShapeCreator.hpp"

class GimpactTriangleMeshCreator : public CollisionShapeCreator
{
  static AdapterRegistrator<GimpactTriangleMeshCreator> reg;

public:
  GimpactTriangleMeshCreator();

  // CollisionShapeCreator interface
  std::shared_ptr<btCollisionShape> createCollisionShape(KawaiiModel3D *model) const override;
};


KAWAIIWORLDS_SHARED_EXPORT inline QDataStream& operator<<(QDataStream &st, const GimpactTriangleMeshCreator&) { return st; }
KAWAIIWORLDS_SHARED_EXPORT inline QDataStream& operator>>(QDataStream &st, GimpactTriangleMeshCreator&) { return st; }

#endif // GIMPACTTRIANGLEMESHCREATOR_HPP
