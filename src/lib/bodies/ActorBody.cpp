#include "ActorBody.hpp"
#include "ConvexHullCreator.hpp"
#include "World.hpp"
#include <QCoreApplication>

ActorBody::ActorBody():
  ActorBody(reg.adapterId)
{ }

ActorBody::ActorBody(std::string_view adapterId):
  Body(adapterId),
  lastModel(nullptr),
  modelSize(0.5, 0.5, 0.5),
  animationPositionSec(0),
  animationLastPos(0.),
  animationTimerIntervalMs(10),
  animationAccelerator(1.),
  animationDriver(AnimationDriver::None),
  animateMoveX(true),
  animateMoveY(true),
  animateMoveZ(true),
  animateRotationX(false),
  animateRotationY(true),
  animateRotationZ(false),
  positionDirty(true),
  rotationDirty(true),
  onShapeChangedQueued(false)
{
  setCollisionShapeCreator<ConvexHullCreator>();
}

ActorBody::~ActorBody()
{
  if(lastModel)
    while(onShapeChangedQueued)
      QCoreApplication::sendPostedEvents(lastModel);
  freeGpuBuffers();
  btShape.reset();
  QObject::disconnect(onModelGeometryChanged);
}

void ActorBody::enable(World &world)
{
  reenableNotNeeded.test_and_set();

  if(instance.meshInstances)
    return reenable(world);

  auto *origModel = world.res().getModel(modelName);
  if(!origModel)
    throw std::runtime_error("ActorBody::enable: model not exists");
  model.reset(static_cast<KawaiiModel3D*>(origModel->clone(true).release()));
  model->updateParent(&world);

  auto *material = world.res().getMaterial(materialName);
  if(!material)
    throw std::invalid_argument("ActorBody::enable: material not exists");

  instanceModel(world, model.get(), material);
}

void ActorBody::disable()
{
  freeGpuBuffers();
}

void ActorBody::reenable(World &world)
{
  if(!model)
    {
      auto *origModel = world.res().getModel(modelName);
      if(!origModel)
        throw std::runtime_error("ActorBody::enable: model not exists");
      model.reset(static_cast<KawaiiModel3D*>(origModel->clone(true).release()));
    }

  auto *material = world.res().getMaterial(materialName);
  if(!material)
    throw std::invalid_argument("ActorBody::enable: material not exists");

  freeGpuBuffers();
  instanceModel(world, model.get(), material);
}

void ActorBody::createAnimationTimer()
{
  if(!animationTimer)
    {
      animationTimer = std::make_unique<QTimer>();
      QObject::connect(animationTimer.get(), &QTimer::timeout, [this] {
          const auto t = std::chrono::steady_clock::now();
          const float elapsedTimeSec = static_cast<double>(std::chrono::duration_cast<std::chrono::milliseconds>(t-animationT0).count()) / 1000.0f;
          setAnimationPositionSec(elapsedTimeSec);
        });
    }
  animationTimer->setInterval(animationTimerIntervalMs);
}

void ActorBody::startAnimationTimer()
{
  if(!animationTimer->isActive())
    {
      animationT0 = std::chrono::steady_clock::now();
      animationTimer->start();
    }
}

QString ActorBody::getModelName() const
{
  return modelName;
}

void ActorBody::setModelName(const QString &newModelName)
{
  if(modelName == newModelName) return;

  modelName = newModelName;
  reenableNotNeeded.clear();
}

QString ActorBody::getMaterialName() const
{
  return materialName;
}

void ActorBody::setMaterialName(const QString &newMaterialName)
{
  if(materialName == newMaterialName) return;

  materialName = newMaterialName;
  reenableNotNeeded.clear();
}

const QString& ActorBody::getAnimationName() const
{
  return animationName;
}

void ActorBody::setAnimationName(const QString &newAnimationName)
{
  if(newAnimationName == animationName)
    return;

  animationName = newAnimationName;
  reenableNotNeeded.clear();
}

float ActorBody::getAnimationPositionSec() const
{
  return animationPositionSec;
}

void ActorBody::setAnimationPositionSec(float sec)
{
  if (qFuzzyCompare(animationPositionSec, sec))
    return;
  animationPositionSec = sec;

  const auto t = std::chrono::steady_clock::now();
  const float elapsedTimeSec = static_cast<double>(std::chrono::duration_cast<std::chrono::milliseconds>(t-lastAnimationT).count()) / 1000.0f;

  if(std::abs(animationPositionSec - animationLastPos) >= 0.01 && elapsedTimeSec >= 0.01)
    if(Q_LIKELY(animationInstance))
      {
        animationInstance->tick(animationPositionSec);
        animationLastPos = animationPositionSec;
        lastAnimationT = t;
      }
}

int ActorBody::getAnimationTimerIntervalMs() const
{
  return animationTimerIntervalMs;
}

void ActorBody::setAnimationTimerIntervalMs(int ms)
{
  if(ms == animationTimerIntervalMs)
    return;

  animationTimerIntervalMs = ms;
  createAnimationTimer();

  if(animationDriver == AnimationDriver::Timer)
    startAnimationTimer();
}

float ActorBody::getAnimationAccelerator() const
{
  return animationAccelerator;
}

void ActorBody::setAnimationAccelerator(float newAnimationAccelerator)
{
  if (qFuzzyCompare(animationAccelerator, newAnimationAccelerator))
    return;
  animationAccelerator = newAnimationAccelerator;
}

ActorBody::AnimationDriver ActorBody::getAnimationDriver() const
{
  return animationDriver;
}

void ActorBody::setAnimationDriver(AnimationDriver newAnimationDriver)
{
  if(newAnimationDriver == animationDriver)
    return;

  animationDriver = newAnimationDriver;
  if(animationDriver == AnimationDriver::Timer)
    {
      createAnimationTimer();
      startAnimationTimer();
    }
}

const QString &ActorBody::getAnimationDriverStr() const
{
  static const std::unordered_map<AnimationDriver, QString> driverNames = {
    { AnimationDriver::None, QStringLiteral("None") },
    { AnimationDriver::Timer, QStringLiteral("Timer") },
    { AnimationDriver::OnMove, QStringLiteral("OnMove") },
  };

  if(auto el = driverNames.find(animationDriver); Q_LIKELY(el != driverNames.end()))
    return el->second;
  else
    {
      static const QString nullStr;
      Q_UNREACHABLE_RETURN(nullStr);
    }
}

void ActorBody::setAnimationDriverStr(const QString &newAnimationDriver)
{
  static const QHash<QString, AnimationDriver> animationDrivers = {
    { QStringLiteral("None"), AnimationDriver::None },
    { QStringLiteral("Timer"), AnimationDriver::Timer },
    { QStringLiteral("OnMove"), AnimationDriver::OnMove },
  };

  if(auto el = animationDrivers.find(newAnimationDriver); el != animationDrivers.end())
    setAnimationDriver(el.value());
  else
    {
      qWarning().noquote() << "ActorBody::setAnimationDriverStr: unknown animation driver \"" << newAnimationDriver << "\""
                           << ";\tChoose one of {\"None\", \"Timer\", \"OnMove\"}";
      setAnimationDriver(AnimationDriver::None);
    }
}

bool ActorBody::getAnimateMoveX() const
{
  return animateMoveX;
}

void ActorBody::setAnimateMoveX(bool newAnimateMoveX)
{
  animateMoveX = newAnimateMoveX;
}

bool ActorBody::getAnimateMoveY() const
{
  return animateMoveY;
}

void ActorBody::setAnimateMoveY(bool newAnimateMoveY)
{
  animateMoveY = newAnimateMoveY;
}

bool ActorBody::getAnimateMoveZ() const
{
  return animateMoveZ;
}

void ActorBody::setAnimateMoveZ(bool newAnimateMoveZ)
{
  animateMoveZ = newAnimateMoveZ;
}

bool ActorBody::getAnimateRotationX() const
{
  return animateRotationX;
}

void ActorBody::setAnimateRotationX(bool newAnimateRotationX)
{
  animateRotationX = newAnimateRotationX;
}

bool ActorBody::getAnimateRotationY() const
{
  return animateRotationY;
}

void ActorBody::setAnimateRotationY(bool newAnimateRotationY)
{
  animateRotationY = newAnimateRotationY;
}

bool ActorBody::getAnimateRotationZ() const
{
  return animateRotationZ;
}

void ActorBody::setAnimateRotationZ(bool newAnimateRotationZ)
{
  animateRotationZ = newAnimateRotationZ;
}

void ActorBody::setVisualTransformation(const QVector3D &pos, const QQuaternion &rotation)
{
  QVector3D distance, rotationDistance;
  if(!qFuzzyCompare(pos, instance.position().toVector3D()))
    {
      switch(animationDriver)
        {
        case AnimationDriver::None:
        case AnimationDriver::Timer:
          break;

        case AnimationDriver::OnMove:
          distance = pos - instance.position().toVector3D();
          if(!animateMoveX)
            distance.setX(0);
          if(!animateMoveY)
            distance.setY(0);
          if(!animateMoveZ)
            distance.setZ(0);
          break;
        }

      instance.position() = QVector4D(pos, 0);
      positionDirty = true;
    }
  if(!qFuzzyCompare(rotation, instance.rotation()))
    {
      switch(animationDriver)
        {
        case AnimationDriver::None:
        case AnimationDriver::Timer:
          break;

        case AnimationDriver::OnMove:
          rotationDistance = rotation.toEulerAngles() - instance.rotation().toEulerAngles();
          if(!animateRotationX)
            rotationDistance.setX(0);
          else
            rotationDistance.setX(rotationDistance.x() * std::max(modelSize.y(), modelSize.z()));

          if(!animateRotationY)
            rotationDistance.setY(0);
          else
            rotationDistance.setY(rotationDistance.y() * std::max(modelSize.x(), modelSize.z()));

          if(!animateRotationZ)
            rotationDistance.setZ(0);
          else
            rotationDistance.setZ(rotationDistance.z() * std::max(modelSize.y(), modelSize.z()));

          rotationDistance *= M_PI / 180.;
          break;
        }

      instance.rotation() = rotation;
      rotationDirty = true;
    }

  if(!distance.isNull() || !rotationDistance.isNull())
    setAnimationPositionSec(getAnimationPositionSec() + getAnimationAccelerator() * (distance.length() + rotationDistance.length()));
  writeGpuBufs();
}

void ActorBody::writeGpuBufs()
{
  instance.updateLocation(positionDirty, rotationDirty);
  positionDirty = rotationDirty = false;
}

void ActorBody::freeGpuBuffers()
{
  if(!instance.meshInstances) return;

  animationTimer.reset();
  animationInstance.reset();
  instance = ModelInstance();
}

void ActorBody::instanceModel(World &world, KawaiiModel3D *model, KawaiiMaterial *material)
{
  instance = world.res().instanceModel(model, material);
  if(!btShape || lastModel != model)
    {
      QObject::disconnect(onModelGeometryChanged);
      lastModel = model;

      auto setBtShapeFunc = [this] {
          btShape = getCollisionShape(lastModel);
          if(!onShapeChangedQueued)
            {
              onShapeChangedQueued = true;
              QMetaObject::invokeMethod(lastModel, [this] {
                  onShapeChanged(btShape.get());
                  btTransform trans;
                  trans.setIdentity();
                  btVector3 min, max;
                  btShape->getAabb(trans, min, max);

                  modelSize = 0.5 * QVector3D(max.x() - min.x(),
                                              max.y() - min.y(),
                                              max.z() - min.z());
                  onShapeChangedQueued = false;
                }, Qt::QueuedConnection);
            }
        };
      btShape = getCollisionShape(lastModel);
      onShapeChanged(btShape.get());
      modelSize = 0.5*lastModel->computeSize();
      onModelGeometryChanged = QObject::connect(lastModel, &KawaiiModel3D::geometryChanged, setBtShapeFunc);
    }

  lastAnimationT = std::chrono::steady_clock::now();

  animationInstance = world.res().getSkeletalAnimation(model, animationName);
  if(Q_LIKELY(animationInstance))
    animationInstance->tick(animationPositionSec);
}


QDataStream& operator<<(QDataStream &st, const ActorBody &obj)
{
  return st << static_cast<const Body&>(obj) << obj.getModelName() << obj.getMaterialName()
            << obj.getAnimationName() << obj.getAnimationPositionSec()
            << obj.getAnimationTimerIntervalMs() << obj.getAnimationAccelerator()
            << static_cast<quint8>(obj.getAnimationDriver());
}

QDataStream& operator>>(QDataStream &st, ActorBody &obj)
{
  st >> static_cast<Body&>(obj);

  QString str;
  st >> str;
  obj.setModelName(str);

  st >> str;
  obj.setMaterialName(str);

  st >> str;
  obj.setAnimationName(str);

  float valf;
  st >> valf;
  obj.setAnimationPositionSec(valf);

  int val;
  st >> val;
  obj.setAnimationTimerIntervalMs(val);

  st >> valf;
  obj.setAnimationAccelerator(valf);

  quint8 valu8;
  st >> valu8;
  obj.setAnimationDriver(static_cast<ActorBody::AnimationDriver>(valu8));

  return st;
}
