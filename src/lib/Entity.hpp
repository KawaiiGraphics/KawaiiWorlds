#ifndef ENTITY_HPP
#define ENTITY_HPP

#include <QVector3D>
#include <QQuaternion>
#include <unordered_set>
#include <glm/mat4x4.hpp>

#include <Kawaii3D/KawaiiDataUnit.hpp>
#include <BulletDynamics/ConstraintSolver/btTypedConstraint.h>

#include "bodies/Body.hpp"

class World;
class Location;
class Landscape;
class LandscapeInfo;

class KAWAIIWORLDS_SHARED_EXPORT Entity: public sib_utils::TreeNode
{
  Q_OBJECT

  Q_PROPERTY(QQuaternion rotation    READ getRotation    WRITE setRotation    NOTIFY rotationChanged FINAL)
  Q_PROPERTY(QVector3D   eulerAngles READ getEulerAngles WRITE setEulerAngles NOTIFY rotationChanged FINAL)
  Q_PROPERTY(QVector3D   position    READ getPosition    WRITE setPosition    NOTIFY positionChanged FINAL)

  Q_PROPERTY(QVector3D velocity   READ getVelocity   WRITE setVelocity   NOTIFY velocityChanged FINAL)
  Q_PROPERTY(QVector3D angularVel READ getAngularVel WRITE setAngularVel NOTIFY angularVelChanged FINAL)

  Q_PROPERTY(float mass READ getMass WRITE setMass NOTIFY massChanged FINAL)
  Q_PROPERTY(float restitution READ getRestitution WRITE setRestitution NOTIFY restitutionChanged FINAL)
  Q_PROPERTY(float friction READ getFriction WRITE setFriction NOTIFY frictionChanged FINAL)
  Q_PROPERTY(float frictionRolling READ getFrictionRolling WRITE setFrictionRolling NOTIFY frictionRollingChanged FINAL)
  Q_PROPERTY(float frictionSpinning READ getFrictionSpinning WRITE setFrictionSpinning NOTIFY frictionSpinningChanged FINAL)

  Q_PROPERTY(float lookingForwardFactorX READ getLookingForwardFactorX WRITE setLookingForwardFactorX NOTIFY lookingForwardFactorXChanged FINAL)
  Q_PROPERTY(float lookingForwardFactorY READ getLookingForwardFactorY WRITE setLookingForwardFactorY NOTIFY lookingForwardFactorYChanged FINAL)
  Q_PROPERTY(float lookingForwardFactorZ READ getLookingForwardFactorZ WRITE setLookingForwardFactorZ NOTIFY lookingForwardFactorZChanged FINAL)

  Q_PROPERTY(bool clickOpacity READ getClickOpacity WRITE setClickOpacity NOTIFY clickOpacityChanged FINAL)
  Q_PROPERTY(bool lookForward  READ getLookForward  WRITE setLookForward  NOTIFY lookForwardChanged  FINAL)
  Q_PROPERTY(bool hasContacts  READ hasContacts NOTIFY hasContactsChanged FINAL)

  Q_PROPERTY(Body *body READ getBody WRITE setBody NOTIFY bodyChanged FINAL)

  Entity(const Entity&) = delete;
  Entity& operator=(const Entity&) = delete;

public:
  struct Constraint
  {
    std::function<std::unique_ptr<btTypedConstraint> (btCollisionObject *)> creator;
    std::unique_ptr<btTypedConstraint> current;
  };

  Entity();
  ~Entity();

  const QQuaternion &getRotation() const;
  void setRotation(const QQuaternion &newRotation);
  void changeRotation(const std::function<bool(QQuaternion&)> &func);

  const QVector3D &getEulerAngles() const;
  void setEulerAngles(const QVector3D &newEulerAngles);
  void changeEulerAngles(const std::function<bool(QVector3D&)> &func);

  const QVector3D &getPosition() const;
  void setPosition(const QVector3D &newPosition);
  void changePosition(const std::function<bool(QVector3D&)> &func);

  const QVector3D &getVelocity() const;
  void setVelocity(const QVector3D &newVelocity, bool writeBtObj = true);

  const QVector3D &getAngularVel() const;
  void setAngularVel(const QVector3D &newAngularVel, bool writeBtObj = true);

  const QString &getMetadata(const QString &key) const;
  void setMetadata(const QString &key, const QString &value);
  void setMetadata(const QHash<QString, QString> &value);

  bool getLookForward() const;
  void setLookForward(bool newLookForward);

  float getLookingForwardFactorX() const;
  void setLookingForwardFactorX(float newLookingForwardFactorX);

  float getLookingForwardFactorY() const;
  void setLookingForwardFactorY(float newLookingForwardFactorY);

  float getLookingForwardFactorZ() const;
  void setLookingForwardFactorZ(float newLookingForwardFactorZ);

  bool getClickOpacity() const;
  void setClickOpacity(bool newClickOpacity);

  World *getWorld() const;
  Location *getLocation() const;

  Body *getBody() const;
  void setBody(Body *newBody);

  float getMass() const;
  void setMass(float newMass);

  float getRestitution() const;
  void setRestitution(float newRestitution);

  float getFriction() const;
  void setFriction(float newFriction);

  float getFrictionRolling() const;
  void setFrictionRolling(float newFrictionRolling);

  float getFrictionSpinning() const;
  void setFrictionSpinning(float newFrictionSpinning);

  void push(const QVector3D &force);

  std::pair<QVector3D, QVector3D> getBaseBBox() const;
  std::pair<QVector3D, QVector3D> getBBox() const;

  std::pair<QVector3D, QVector3D> getAABB(const glm::mat4 &viewProjMat) const;

  /// Returns true if a new collision registered, false otherwise
  bool addContact(Entity *withEntity);

  bool hasContact(Entity *withEntity) const;
  bool hasContacts() const;

  void readBulletObj();
  void writeBulletObj();

  void onBodyBtObjChange();

  void allignToLand(Landscape *landscape);
  void allignToLand(LandscapeInfo *landscapeInfo);

  float distanceToLand(Landscape *landscape);
  float distanceToLand(LandscapeInfo *landscapeInfo);

  QObject* getScriptWrapper() const;
  void changeScriptWrapper(const std::function<void(QObject *&)> &func);

  std::unique_ptr<QMutexLocker<QRecursiveMutex>> lock() const;

  /// Returns true if the Entity can interact with mouse / pointer.
  /// (signals pointerPress or pointerReleaseor or pointerDblClicked have been connected to a handler)
  bool hasPointerInteractions() const;

  void setConstraint(const std::string &name, Constraint &&constraint);
  void removeConstraint(const std::string &name);
  const std::unordered_map<std::string, Constraint> &getConstraints() const;

signals:
  void tick(Entity *e, float secElapsed);
  void contact(Entity *e0, Entity *e1);
  void contactLost(Entity *e0, Entity *e1);

  void pointerPress(Entity *e);
  void pointerRelease(Entity *e);
  void pointerDblClicked(Entity *e);

  void aboutToBeDestroyed(Entity *e);

  void rotationChanged();
  void positionChanged();

  void velocityChanged();
  void angularVelChanged();

  void bodyIsAboutToBeChanged();
  void bodyChanged();

  void worldChanged();
  void locationChanged();

  void massChanged();
  void restitutionChanged();
  void frictionChanged();
  void frictionRollingChanged();
  void frictionSpinningChanged();

  void lookingForwardFactorXChanged();
  void lookingForwardFactorYChanged();
  void lookingForwardFactorZChanged();

  void lookForwardChanged();
  void clickOpacityChanged();

  void hasContactsChanged();

  void constraintAdded(btTypedConstraint *constraint);
  void constraintRemoved(btTypedConstraint *constraint);



  // IMPLEMENT
private:
  std::unique_ptr<QRecursiveMutex> mutex;

  QQuaternion rotation;
  QVector3D position;
  QVector3D eulerAngles;

  QVector3D velocity;
  QVector3D angularVel;

  QHash<QString, QString> metadata;

  std::unordered_set<Entity*> contacts;
  std::unordered_set<Entity*> lastTickContacts;

  Body *body;
  std::unique_ptr<btMotionState> motionState;

  World *world;
  Location *location;

  QObject *scriptWrapper;

  std::unordered_map<std::string, Constraint> constraints;

  float mass;
  float restitution;
  float friction;
  float frictionRolling;
  float frictionSpinning;

  float lookingForwardFactorX;
  float lookingForwardFactorY;
  float lookingForwardFactorZ;

  bool trIsDirty;

  bool lookForward;
  bool clickOpacity;

  bool recentBulletTick;


  static void internalTick(Entity *e, float secElapsed);

  void updateQuaternionToAngles();
  void updateAnglesToQuaternion();
  void setupLookForwardAngularFactor();

  void onParentChanged();
  void setWorld(World *newWorld);
  void setLocation(Location *newLocation);
};


KAWAIIWORLDS_SHARED_EXPORT QDataStream& operator<<(QDataStream &st, const Entity &obj);
KAWAIIWORLDS_SHARED_EXPORT QDataStream& operator>>(QDataStream &st, Entity &obj);

#endif // ENTITY_HPP
