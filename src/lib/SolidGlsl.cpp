#include "SolidGlsl.hpp"
#include "GlobalConfig.hpp"

SolidGlsl::SolidGlsl(const ShaderEffectSrc &materialEffectSrc)
{
  auto setShaderParams = [&materialEffectSrc] (KawaiiShader *module) {
      for(auto i = materialEffectSrc.shaderParams.cbegin(); i != materialEffectSrc.shaderParams.cend(); ++i)
        module->setParameter(i.key(), i.value());
    };
  auto *module = createShaderModule(KawaiiShaderType::Vertex, materialEffectSrc.vertexSrc);
  setShaderParams(module);
  createShaderModule(KawaiiShaderType::Vertex, QFile(":/shaders/solid/main.vs.glsl"))->setParameter("MAX_PACK_INSTANCES", QString::number(global_config::maxPackInstances));

  module = createShaderModule(KawaiiShaderType::Fragment, materialEffectSrc.fragmentSrc);
  setShaderParams(module);
  if(!materialEffectSrc.illuminationName.isNull())
    createShaderModule(KawaiiShaderType::Fragment, QFile(":/shaders/solid/main.fs.glsl"))->setParameter("ILLUMINATION_NAME", materialEffectSrc.illuminationName);
  else
    createShaderModule(KawaiiShaderType::Fragment, QFile(":/shaders/solid/main_no_illumination.fs.glsl"));
  setCullMode(materialEffectSrc.cullMode);
}
