#include "ModelInstance.hpp"
#include <QQuaternion>
#include <QVector4D>

namespace {
  struct ObjectsBufLayout
  {
    QQuaternion q;
    QVector4D pos;
  };
}

ModelInstance::ModelInstance():
  meshInstances(nullptr),
  modelInstances(nullptr),
  allModelInstances(nullptr),
  objectsSSBO(nullptr),
  instanceIndex(0),
  objectIndex(0)
{ }

ModelInstance::ModelInstance(const QList<KawaiiMeshInstance*> &meshInstances, QList<ModelInstance*> &modelInstances,
                             KawaiiGpuBuf *objectsSSBO, QList<ModelInstance *> &allModelInstances):
  meshInstances(&meshInstances),
  modelInstances(&modelInstances),
  allModelInstances(&allModelInstances),
  objectsSSBO(objectsSSBO),
  instanceIndex(meshInstances.empty()? 0: meshInstances.back()->getInstanceCount()),
  objectIndex(allModelInstances.size())
{
  for(auto *i: meshInstances)
    if(Q_LIKELY(i))
      i->setInstanceCount(instanceIndex + 1);

  writeObjectIndexToUbo();
  if(Q_LIKELY(objectsSSBO))
    {
      ObjectsBufLayout objectsBufLayout = {QQuaternion(), QVector4D(0,0,0,1)};
      objectsSSBO->append(&objectsBufLayout, sizeof(objectsBufLayout));
    }
  modelInstances.push_back(this);
  allModelInstances.push_back(this);
}

ModelInstance::ModelInstance(ModelInstance &&orig):
  meshInstances(orig.meshInstances),
  modelInstances(orig.modelInstances),
  allModelInstances(orig.allModelInstances),
  objectsSSBO(orig.objectsSSBO),
  instanceIndex(orig.instanceIndex),
  objectIndex(orig.objectIndex)
{
  if(Q_LIKELY(modelInstances))
    if(auto el = std::find(modelInstances->begin(), modelInstances->end(), &orig);
       Q_LIKELY(el != modelInstances->end()))
      *el = this;

  if(Q_LIKELY(allModelInstances))
    if(auto el = std::find(allModelInstances->begin(), allModelInstances->end(), &orig);
       Q_LIKELY(el != allModelInstances->end()))
      *el = this;

  orig.meshInstances = nullptr;
  orig.modelInstances = nullptr;
  orig.objectsSSBO = nullptr;
  orig.instanceIndex = 0;
  orig.objectIndex = 0;
}

ModelInstance &ModelInstance::operator=(ModelInstance &&orig)
{
  destroy();
  meshInstances = orig.meshInstances;
  modelInstances = orig.modelInstances;
  allModelInstances = orig.allModelInstances;
  objectsSSBO = orig.objectsSSBO;
  instanceIndex = orig.instanceIndex;
  objectIndex = orig.objectIndex;

  if(Q_LIKELY(modelInstances))
    if(auto el = std::find(modelInstances->begin(), modelInstances->end(), &orig);
       Q_LIKELY(el != modelInstances->end()))
      *el = this;

  if(Q_LIKELY(allModelInstances))
    if(auto el = std::find(allModelInstances->begin(), allModelInstances->end(), &orig);
       Q_LIKELY(el != allModelInstances->end()))
      *el = this;

  orig.meshInstances = nullptr;
  orig.modelInstances = nullptr;
  orig.objectsSSBO = nullptr;
  orig.instanceIndex = 0;
  orig.objectIndex = 0;

  return *this;
}

ModelInstance::~ModelInstance()
{
  destroy();
}

size_t ModelInstance::getInstanceIndex() const
{
  return instanceIndex;
}

size_t ModelInstance::getObjectIndex() const
{
  return objectIndex;
}

QQuaternion &ModelInstance::rotation() const
{
  static QQuaternion q;
  if(Q_LIKELY(objectsSSBO))
    return static_cast<ObjectsBufLayout*>(objectsSSBO->getData())[objectIndex].q;
  else
    return q;
}

QVector4D &ModelInstance::position() const
{
  static QVector4D pos;
  if(Q_LIKELY(objectsSSBO))
    return static_cast<ObjectsBufLayout*>(objectsSSBO->getData())[objectIndex].pos;
  else
    return pos;
}

KawaiiGpuBuf *ModelInstance::getObjectsSSBO() const
{
  return objectsSSBO;
}

void ModelInstance::updateLocation(bool posDirty, bool rotationDirty)
{
  size_t offset = sizeof(ObjectsBufLayout),
      size = 0;

  if(rotationDirty)
    {
      offset = std::min<size_t>(offset, offsetof(ObjectsBufLayout, q));
      size += sizeof(QQuaternion);
    }

  if(posDirty)
    {
      offset = std::min<size_t>(offset, offsetof(ObjectsBufLayout, pos));
      size += sizeof(QVector3D);
    }

  if(size > 0)
    {
      if(Q_LIKELY(getObjectsSSBO()))
        emit getObjectsSSBO()->dataChanged(offset + getObjectIndex() * sizeof(ObjectsBufLayout), size);
    }
}

void ModelInstance::writeObjectIndexToUbo()
{
  if(Q_LIKELY(meshInstances))
    for(auto *i: *meshInstances)
      if(Q_LIKELY(i))
        if(i->getUniforms())
          {
            auto *dstBuf = static_cast<int*>(i->getUniforms()->getData()) + instanceIndex;
            *dstBuf = objectIndex;
            i->getUniforms()->dataChanged(sizeof(int) * instanceIndex,
                                          sizeof(int));
          }
}

namespace {
  void cutFragment(KawaiiGpuBuf &buf, size_t offset, size_t size)
  {
    std::byte *p = static_cast<std::byte*>(buf.getData()) + offset;
    std::memmove(p, p + size, buf.getDataSize() - (offset + size));
    emit buf.dataChanged(offset, buf.getDataSize() - offset);
  }
}

void ModelInstance::destroy()
{
  if(!meshInstances)
    return;

  if(Q_LIKELY(modelInstances))
    {
      if(auto el = std::find(modelInstances->begin(), modelInstances->end(), this);
         Q_LIKELY(el != modelInstances->end()))
        modelInstances->erase(el);

      for(auto &i: *modelInstances)
        if(i->instanceIndex > instanceIndex)
          --i->instanceIndex;
    }

  for(auto *meshInstance: *meshInstances)
    if(Q_LIKELY(meshInstance))
      {
        meshInstance->setInstanceCount(meshInstance->getInstanceCount() - 1);
        if(meshInstance->getUniforms())
          cutFragment(*meshInstance->getUniforms(), sizeof(int) * instanceIndex, sizeof(int));
      }

  if(Q_LIKELY(allModelInstances))
    {
      if(auto el = std::find(allModelInstances->begin(), allModelInstances->end(), this);
         Q_LIKELY(el != allModelInstances->end()))
        allModelInstances->erase(el);

      for(auto &i: *allModelInstances)
        if(i->objectIndex > objectIndex)
          {
            --i->objectIndex;
            i->writeObjectIndexToUbo();
          }
    }
  if(Q_LIKELY(objectsSSBO))
    objectsSSBO->erase(sizeof(ObjectsBufLayout) * objectIndex, sizeof(ObjectsBufLayout));
}
