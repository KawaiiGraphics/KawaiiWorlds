#ifndef RESOURCEPACKJSONLOAD_HPP
#define RESOURCEPACKJSONLOAD_HPP

#include "ResourcePack.hpp"
#include <QJsonObject>
#include <QJsonArray>
#include <functional>
#include <QHash>
#include <QDir>

class KAWAIIWORLDS_SHARED_EXPORT ResourcePackJsonLoader
{
public:
  QDir rootDir;
  QHash<QString, std::function<void(const QJsonValue &val, std::vector<std::byte> &content, KawaiiGpuBuf *buf, size_t &byteIndex)>> customUboElementLoaders;
  QHash<QString, std::function<KawaiiImageSource*(const QJsonObject &val, KawaiiRenderpass *rp)>> customImgSrcLoaders;

  void loadJson(const QJsonObject &json, ResourcePack &res) const;

  void loadBufferElement(const QJsonValue &element, const ResourcePack &res, KawaiiGpuBuf *buf, std::vector<std::byte> &bufferContent, size_t &readingIndex) const;

private:
  KawaiiGpuBuf *findGpuBuf(const QString &str, const ResourcePack &res) const;
  KawaiiGpuBuf *readGpuBuf(const QJsonValue &ubo_json, ResourcePack &res) const;
  void loadBaseTextureProperties(const QJsonObject &obj, KawaiiTexture *tex) const;
  bool readRenderpass(const QJsonObject &rp_json, KawaiiRenderpass &rp) const;

  template<typename T>
  void loadMaterial(const QJsonObject &obj, ResourcePack &res) const
  {
    KawaiiGpuBuf *buf = nullptr;
    if(obj.contains(QLatin1String("buffer")))
      if(auto bufJs = obj.value(QLatin1String("buffer")); bufJs.isArray() || bufJs.isString())
        buf = readGpuBuf(bufJs, res);

    auto &material = res.createMaterial<T>(
          obj.value(QLatin1String("name")).toString(),
          obj.value(QLatin1String("shader")).toString(),
          buf);
    if(buf && !buf->parent())
      buf->updateParent(&material);
  }
};


#endif // RESOURCEPACKJSONLOAD_HPP
