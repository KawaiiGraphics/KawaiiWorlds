#include "IlluminationModel.hpp"
#include "World.hpp"

IlluminationModelBase::IlluminationModelBase(std::string_view adapterId):
  sib_utils::Serializable(adapterId, adapters),
  jsWrapper(nullptr)
{
}

void IlluminationModelBase::init(World &w)
{
  world = &w;

  createLampArrays(*illumination);
  illumination->createUBO(uboBindingPoint+1);
  if(!illumination->tryEnable())
    throw std::runtime_error("can not enable illumination");
}

void IlluminationModelBase::init(World &w, const QString &name, uint32_t uboBindingPoint)
{
  this->name = name;
  illumination = w.createChild_uptr<KawaiiIllumination>(name);
  this->uboBindingPoint = uboBindingPoint;
  init(w);
}

void IlluminationModelBase::addDirLight()
{
  illumination->getLampsLayout().addDirLight();
}

void IlluminationModelBase::addDotLight()
{
  illumination->getLampsLayout().addDotLight();
}

void IlluminationModelBase::addSpotLight()
{
  illumination->getLampsLayout().addSpotLight();
}

void IlluminationModelBase::removeDirLight(size_t i)
{
  illumination->getLampsLayout().removeDirLight(i);
}

void IlluminationModelBase::removeDotLight(size_t i)
{
  illumination->getLampsLayout().removeDotLight(i);
}

void IlluminationModelBase::removeSpotLight(size_t i)
{
  illumination->getLampsLayout().removeSpotLight(i);
}

size_t IlluminationModelBase::getDirLightCount() const
{
  return illumination->getLampsLayout().getDirLightCount();
}

size_t IlluminationModelBase::getDotLightCount() const
{
  return illumination->getLampsLayout().getDotLightCount();
}

size_t IlluminationModelBase::getSpotLightCount() const
{
  return illumination->getLampsLayout().getSpotLightCount();
}

KawaiiShadowMapper *IlluminationModelBase::createShadowMapper() const
{
  if(!world->res().hasShadowrender_scene())
    world->res().setShadowrender_scene(world->createChild_uptr<KawaiiScene>());

  return getLampsLayout().createShadowMapper(&world->res().getShadowrender_scene());
}

KawaiiShadowMapper *IlluminationModelBase::getShadowMapper() const
{
  return getLampsLayout().getShadowMapper();
}

KawaiiLampsLayoutAbstract &IlluminationModelBase::getLampsLayout() const
{
  return illumination->getLampsLayout();
}


QDataStream& operator<<(QDataStream &st, const IlluminationModelBase &obj)
{
  return st << obj.getName() << obj.getUboBindingPoint();
}

QDataStream& operator>>(QDataStream &st, IlluminationModelBase &obj)
{
  return st >> obj.name >> obj.uboBindingPoint;
}
