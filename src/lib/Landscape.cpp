#include "Landscape.hpp"
#include <BulletCollision/CollisionShapes/btHeightfieldTerrainShape.h>

#include <Kawaii3D/Geometry/KawaiiModel3D.hpp>
#include <Kawaii3D/KawaiiMaterial.hpp>

#include <QRadialGradient>
#include <QVector2D>
#include <QPainter>
#include <cmath>

namespace {
  void buildLand(const QVector3D &size, float step, KawaiiMesh3D &out)
  {
    const QVector2D c(size.x() * 0.5, size.z() * 0.5);
    for(float x = 0.0; x <= size.x(); x += step)
      for(float y = 0.0; y <= size.z(); y += step)
        out.addVertex(QVector4D(x - c.x(), 0, y - c.y(), 1),
                      QVector3D(0,1,0),
                      QVector3D(x / size.x(), y / size.z(), 0));

    const size_t x_stepCount = std::ceil(size.x() / step)+1,
        y_stepCount = std::ceil(size.z() / step)+1;

    auto getVertIndex = [&y_stepCount, &out] (size_t xn, size_t yn) {
        uint32_t result = yn + xn * y_stepCount;
        Q_ASSERT(result < out.vertexCount());
        return result;
      };

    for(size_t i = 0; i < x_stepCount-1; ++i)
      for(size_t j = 0; j < y_stepCount-1; ++j)
        {
          const uint32_t a = getVertIndex(i+1, j+1),
              b = getVertIndex(i+1, j),
              c = getVertIndex(i, j),
              d = getVertIndex(i, j+1);

          out.addTriangle(a, b, c);
          out.addTriangle(d, a, c);
        }
  }

  float mix(float a, float b, float x)
  {
    return x * b + a * (1.0 - x);
  }
}

Landscape::Landscape():
  jsWrapper(nullptr),
  meshInstance(nullptr),
  restitution(0),
  friction(0.5),
  frictionRolling(0.33),
  frictionSpinning(0.125),
  heightMapDirty(false)
{
}

Landscape::~Landscape()
{
  if(jsWrapper)
    delete jsWrapper;
  mesh.reset();
  model.reset();
}

void Landscape::setHeightMap(KawaiiImage *heightMap)
{
  if(heights != heightMap)
    {
      heights = heightMap;
      initUniforms();
    }
}

void Landscape::build(const QVector3D &pos, const QVector3D &size, float step)
{
  sz = size;
  this->pos = pos;
  this->step = step;

  model = std::make_unique<KawaiiModel3D>();
  mesh = model->createChild_uptr<KawaiiMesh3D>();
  buildLand(sz, step, *mesh);
  model->updateParent(this);

  initUniforms();
}

const std::vector<std::unique_ptr<btRigidBody>>& Landscape::createRigidBodies(const QVector2D &chunkSize)
{
  disconnect(updateBtHeightfieldData);
  createHeightfieldData(chunkSize);
  updateBtHeightfieldData = connect(heights.data(), &KawaiiImage::imageUpdated,
                                    this, [this, chunkSize] { createHeightfieldData(chunkSize); });

  // btTriangleIndexVertexArray *btIndexVertexArray = new btTriangleIndexVertexArray;
  // model->forallMeshes([btIndexVertexArray](KawaiiMesh3D &mesh) {
  //   btIndexedMesh btMesh;
  //   btMesh.m_numTriangles = static_cast<int>(mesh.trianglesCount());
  //   btMesh.m_triangleIndexBase = reinterpret_cast<const unsigned char*>(mesh.getRawTriangles().data());
  //   btMesh.m_triangleIndexStride = sizeof(KawaiiTriangle3D);

  //   btMesh.m_numVertices = static_cast<int>(mesh.vertexCount());
  //   btMesh.m_vertexBase = reinterpret_cast<const unsigned char*>(mesh.getVertices().data());
  //   btMesh.m_vertexStride = sizeof(KawaiiPoint3D);

  //   btMesh.m_indexType = PHY_INTEGER;
  //   btMesh.m_vertexType = PHY_FLOAT;
  //   btIndexVertexArray->addIndexedMesh(btMesh, btMesh.m_indexType);
  // });

  // btShape = std::make_unique<btBvhTriangleMeshShape>(btIndexVertexArray, true);
  // static_cast<btBvhTriangleMeshShape&>(*btShape).buildOptimizedBvh();

  if(btShapes.empty())
    {
      btShapes.reserve(heightfieldData.size());
      for(auto &heightfield: heightfieldData)
        btShapes.push_back(std::make_unique<btHeightfieldTerrainShape>(heightfield.szX, heightfield.szZ, heightfield.data.data(),
                                                                       heightfield.minHeight, heightfield.maxHeight, 1, false));
    }

  if(rigidBodies.empty())
    {
      float chunkSzX = std::floor(chunkSize.x()),
          chunkSzZ = std::floor(chunkSize.y());

      size_t sz_x = std::ceil(sz.x() / chunkSzX);
      size_t sz_z = std::ceil(sz.z() / chunkSzZ);

      for(size_t i = 0; i < sz_x; ++i)
        for(size_t j = 0; j < sz_z; ++j)
          {
            btTransform tr;
            tr.setIdentity();

            auto& btShape = btShapes[i*sz_z + j];
            btVector3 v0, v1;
            btShape->getAabb(tr, v0, v1);
            btVector3 vSz = v1-v0;

            auto &heightfield = heightfieldData[i*sz_z + j];
            btVector3 origin(pos.x() - 0.5*sz.x() + i * chunkSzX + 0.5*vSz.x(),
                             pos.y() + 0.5*vSz.y() + heightfield.minHeight,
                             pos.z() - 0.5*sz.z() + j * chunkSzZ + 0.5*vSz.z());
            tr.setOrigin(origin);

            btShape->getAabb(tr, v0, v1);

            btVector3 localInertia(0, 0, 0);
            if(Q_LIKELY(btShape))
              btShape->calculateLocalInertia(0, localInertia);
            btRigidBody::btRigidBodyConstructionInfo rbInfo(0.0, nullptr, btShape.get(), localInertia);
            rbInfo.m_startWorldTransform = tr;
            rigidBodies.push_back(std::make_unique<btRigidBody>(rbInfo));
            rigidBodies.back()->setAnisotropicFriction(btShape->getAnisotropicRollingFrictionDirection(),
                                                       btCollisionObject::CF_ANISOTROPIC_ROLLING_FRICTION);

            rigidBodies.back()->setRestitution(restitution);
            rigidBodies.back()->setFriction(friction);
            rigidBodies.back()->setRollingFriction(frictionRolling);
            rigidBodies.back()->setSpinningFriction(frictionSpinning);
            rigidBodies.back()->setActivationState(DISABLE_SIMULATION);
          }
    }

  for(auto i = rigidBodies.cbegin(); i < rigidBodies.cend(); ++i)
    for(auto j = i+1; i < rigidBodies.cend(); ++i)
      {
        (*i)->setIgnoreCollisionCheck(j->get(), true);
        (*j)->setIgnoreCollisionCheck(i->get(), true);
      }

  return rigidBodies;
}

const std::vector<std::unique_ptr<btRigidBody>> &Landscape::getRigidBodies() const
{
  return rigidBodies;
}

void Landscape::removeRigidBodies()
{
  rigidBodies.clear();
  btShapes.clear();
  heightfieldData.clear();
  disconnect(updateBtHeightfieldData);
}

float Landscape::getHeight(const QVector2D &pos) const
{
  if(!heights
     || std::abs(pos.x()) > 0.5*sz.x()
     || std::abs(pos.y()) > 0.5*sz.z())
    return this->pos.y();

  QVector2D texCoord = pos / QVector2D(sz.x(), sz.z()); // [-0.5; 0.5]
  texCoord += QVector2D(0.5, 0.5);

  if(Q_UNLIKELY(
       texCoord.x() < 0 || texCoord.x() >= 1
       || texCoord.y() < 0 || texCoord.y() >= 1)) {
      return this->pos.y();
    }

  QVector2D pixelCoord(texCoord.x() * (*heights)->width(),
                       texCoord.y() * (*heights)->height());

  QVector2D v0(std::trunc(pixelCoord.x()), std::trunc(pixelCoord.y()));
  if((pixelCoord - v0).lengthSquared() < 0.001)
    return convertHeight(qRed((*heights)->pixel(v0.x(), v0.y())));

  QVector2D v1(std::min<int>(v0.x() + 1, (*heights)->width() - 1),
               std::min<int>(v0.y() + 1, (*heights)->height() - 1)),
      delta = pixelCoord - v0;

  float q00 = qRed((*heights)->pixel(v0.x(), v0.y())),
      q01 = qRed((*heights)->pixel(v0.x(), v1.y())),
      q11 = qRed((*heights)->pixel(v1.x(), v1.y())),
      q10 = qRed((*heights)->pixel(v1.x(), v0.y()));

  float q0 = mix(q00, q01, delta.y()),
      q1 = mix(q10, q11, delta.y());

  return convertHeight(mix(q0, q1, delta.x()));
}

float Landscape::getHeight(const QVector2D &pos0, const QVector2D &pos1) const
{
  int maxH = std::numeric_limits<int>::min();
  bool computed = forallHeightmapPixel(pos0, pos1, [&maxH] (const QRgb *rgb) {
      if(int val = qRed(*rgb); val > maxH)
        maxH = val;
    });

  if(computed)
    return convertHeight(maxH);
  else
    return pos.y();
}

float Landscape::getSlope(const QVector2D &pos0, const QVector2D &pos1) const
{
  int maxDelta = std::numeric_limits<int>::min();

  const QRgb *begin = heights?
        reinterpret_cast<const QRgb*>((*heights)->constBits()):
        nullptr;

  const QRgb *end = heights?
        reinterpret_cast<const QRgb*>((*heights)->constBits() + (*heights)->sizeInBytes()):
        nullptr;

  enum class Direction {
    X,
    Z,
    None
  };

  Direction dir = Direction::None;

  bool computed = forallHeightmapPixel(pos0, pos1, [&maxDelta, &dir, begin, end, this] (const QRgb *rgb) {
      const uchar *ch = reinterpret_cast<const uchar*>(rgb);
      const QRgb *top = reinterpret_cast<const QRgb*>(ch - (*heights)->bytesPerLine());
      const QRgb *bottom = reinterpret_cast<const QRgb*>(ch + (*heights)->bytesPerLine());
      const QRgb *left = rgb-1;
      const QRgb *right = rgb+1;
      int val = qRed(*rgb);

      if(top >= begin)
        if(int delta = std::abs(val - qRed(*top)); delta > maxDelta)
          {
            maxDelta = delta;
            dir = Direction::Z;
          }

      if(bottom < end)
        if(int delta = std::abs(val - qRed(*bottom)); delta > maxDelta)
          {
            maxDelta = delta;
            dir = Direction::Z;
          }

      if(left >= begin)
        if(int delta = std::abs(val - qRed(*left)); delta > maxDelta)
          {
            maxDelta = delta;
            dir = Direction::X;
          }

      if(right < end)
        if(int delta = std::abs(val - qRed(*right)); delta > maxDelta)
          {
            maxDelta = delta;
            dir = Direction::X;
          }
    });

  if(computed && maxDelta > 0)
    {
      const float k = sz.y() / 255.0;
      const float deltaY = k * static_cast<float>(maxDelta);
      float deltaX = deltaY;
      switch (dir)
        {
        case Direction::X:
          deltaX = sz.x() / static_cast<float>((*heights)->width());
          break;

        case Direction::Z:
          deltaX = sz.z() / static_cast<float>((*heights)->height());
          break;

        case Direction::None:
          deltaX = 1;
          break;
        }

      return deltaY / deltaX;
    } else
    return 0;
}

void Landscape::bump(const QVector2D &pos, float val, float radius, float hardness, float pressure)
{
  QPointF texCoord(pos.x() / sz.x(),
                   pos.y() / sz.z());
  texCoord += QPointF(0.5, 0.5);

  const float k = 255.0 / sz.y();
  val = std::round(val * k) - pos.y();

  QColor pressureColor = qRgb(val, val, val);
  pressureColor.setAlphaF(std::round(pressure));
  QColor alphaColor = pressureColor;
  alphaColor.setAlpha(0);

  QRadialGradient radialGradient;
  radialGradient.setColorAt(0, pressureColor);
  radialGradient.setColorAt(1, alphaColor);
  radialGradient.setColorAt(hardness / 100.0, pressureColor);

  heights->changeImage([&radialGradient, &texCoord, radius, this] (QImage &img) {
      QPainter p(&img);
      const float w = static_cast<float>(p.window().width()),
          h = static_cast<float>(p.window().height());
      texCoord.setX(texCoord.x() * w);
      texCoord.setY(texCoord.y() * h);
      p.setBrush(radialGradient);
      p.drawEllipse(texCoord, radius * w / sz.x(), radius * h / sz.z());
      return false;
    });
  heightMapDirty = true;
}

void Landscape::setMaterial(KawaiiMaterial &m)
{
  if(material == &m)
    return;

  material = &m;
  if(meshInstance)
    meshInstance->setMaterial(material.data());
}

bool Landscape::flushHeightMap()
{
  if(heightMapDirty)
    {
      heightMapDirty = false;
      emit heights->imageUpdated();
      return true;
    } else
    return false;
}

void Landscape::unload(LandscapeInfo &out)
{
  out.size = sz;
  out.pos = pos;
  if(Q_LIKELY(material))
    out.material = material->objectName();
  if(Q_LIKELY(heights))
    out.heightMapTexture = heights->objectName();
  out.metadata = std::move(metadata);
  out.step = step;
  out.restitution = restitution;
  out.friction = friction;
  out.frictionRolling = frictionRolling;
  out.frictionSpinning = frictionSpinning;
}

bool Landscape::isEq(const LandscapeInfo &val) const
{
  return step == val.step
      && restitution == val.restitution
      && friction == val.friction
      && frictionRolling == val.frictionRolling
      && frictionSpinning == val.frictionSpinning
      && sz == val.size
      && pos == val.pos
      && material->objectName() == val.material
      && heights->objectName() == val.heightMapTexture
      && metadata == val.metadata;
}

float Landscape::getRestitution() const
{
  return restitution;
}

void Landscape::setRestitution(float newRestitution)
{
  if (qFuzzyCompare(restitution, newRestitution))
    return;
  restitution = newRestitution;
  for(auto &rigidBody: rigidBodies)
    rigidBody->setRestitution(restitution);
  emit restitutionChanged();
}

float Landscape::getFriction() const
{
  return friction;
}

void Landscape::setFriction(float newFriction)
{
  if (qFuzzyCompare(friction, newFriction))
    return;
  friction = newFriction;
  for(auto &rigidBody: rigidBodies)
    rigidBody->setFriction(friction);
  emit frictionChanged();
}

float Landscape::getFrictionRolling() const
{
  return frictionRolling;
}

void Landscape::setFrictionRolling(float newFrictionRolling)
{
  if (qFuzzyCompare(frictionRolling, newFrictionRolling))
    return;
  frictionRolling = newFrictionRolling;
  for(auto &rigidBody: rigidBodies)
    rigidBody->setRollingFriction(frictionRolling);
  emit frictionRollingChanged();
}

float Landscape::getFrictionSpinning() const
{
  return frictionSpinning;
}

void Landscape::setFrictionSpinning(float newFrictionSpinning)
{
  if (qFuzzyCompare(frictionSpinning, newFrictionSpinning))
    return;
  frictionSpinning = newFrictionSpinning;
  for(auto &rigidBody: rigidBodies)
    rigidBody->setSpinningFriction(frictionSpinning);
  emit frictionSpinningChanged();
}

void Landscape::initUniforms()
{
  if(!mesh) return;

  if(!meshInstance)
    meshInstance = mesh->createChild<KawaiiMeshInstance>();

  const Uniforms buf = {QVector4D(sz, 0), QVector4D(pos, 0)};
  if(!meshInstance->getUniforms())
    {
      auto uniforms = mesh->createChild<KawaiiGpuBuf>(&buf, sizeof(buf));
      uniforms->bindTexture(QStringLiteral("heights"), heights.data());
      meshInstance->setUniforms(uniforms);
    } else
    {
      auto uniforms = meshInstance->getUniforms();
      uniforms->setData(&buf, sizeof(buf));
      uniforms->bindTexture(QStringLiteral("heights"), heights.data());
    }
  if(material)
    meshInstance->setMaterial(material.data());
}

float Landscape::convertHeight(float val) const
{
  const float k = sz.y() / 255.0;
  return pos.y() + k * val;
}

void Landscape::checkHeightmapFmt() const
{
  switch ((*heights)->format())
    {
    case QImage::Format_RGB32:
    case QImage::Format_ARGB32:
    case QImage::Format_ARGB32_Premultiplied:
      break;

    default:
      qDebug("KawaiiWorlds::Landscape: converting heightmap to Format_ARGB32");
      heights->setImage((*heights)->convertToFormat(QImage::Format_ARGB32));
      break;
    }
}

bool Landscape::forallHeightmapPixel(const QVector2D &pos0, const QVector2D &pos1, const std::function<void (const QRgb *)> &func) const
{
  if(!func
     || !heights
     || std::abs(pos0.x()) >= sz.x()
     || std::abs(pos0.y()) >= sz.z()
     || std::abs(pos1.x()) >= sz.x()
     || std::abs(pos1.y()) >= sz.z())
    return false;

  QVector2D texCoord0 = pos0 / QVector2D(sz.x(), sz.z()),
      texCoord1 = pos1 / QVector2D(sz.x(), sz.z());

  texCoord0 += QVector2D(0.5, 0.5);
  texCoord1 += QVector2D(0.5, 0.5);

  if(texCoord0.x() < 0)
    texCoord0.setX(0);
  if(texCoord0.y() < 0)
    texCoord0.setY(0);

  if(texCoord1.x() > 1)
    texCoord1.setX(1);
  if(texCoord1.y() > 1)
    texCoord1.setY(1);

  QVector2D pixelCoord0(std::floor(texCoord0.x() * (*heights)->width()),
                        std::floor(texCoord0.y() * (*heights)->height()));

  QVector2D pixelCoord1(std::ceil(texCoord1.x() * (*heights)->width()),
                        std::ceil(texCoord1.y() * (*heights)->height()));

  checkHeightmapFmt();

  for(int y = pixelCoord0.y(); y < pixelCoord1.y(); ++y)
    {
      Q_ASSERT(y >= 0 && y < (*heights)->height());
      const uchar *s = (*heights)->constScanLine(y);
      for(int x = pixelCoord0.x(); x < pixelCoord1.x(); ++x)
        {
          Q_ASSERT(x >= 0 && x < (*heights)->width());
          func(reinterpret_cast<const QRgb*>(s) + x);
        }
    }
  return true;
}

void Landscape::createHeightfieldData(const QVector2D &chunkSize)
{
  // model->forallMeshes([this](KawaiiMesh3D &mesh) {
  //   mesh.forallVerticesP([this](KawaiiPoint3D &vert) {
  //     float h = getHeight({vert.positionRef().x(), vert.positionRef().z()});
  //     vert.positionRef().setY(pos.y() + sz.y() * h);
  //   });
  // });
  auto generateHeightfieldChunk = [this](int x0, int x1, int z0, int z1,
      std::vector<float> &out, float &minHeight, float &maxHeight) {
      for(int y = z0; y <= z1; ++y)
        for(int x = x0; x <= x1; ++x)
          {
            const float h = getHeight(QVector2D((x - 0.5*sz.x()),
                                                (y - 0.5*sz.z()))) - pos.y();
            if(h > maxHeight)
              maxHeight = h;
            if(h < minHeight)
              minHeight = h;
            out.push_back(h);
          }
    };

  heightfieldData.clear();

  int chunkSzX = std::floor(chunkSize.x());
  int chunkSzZ = std::floor(chunkSize.y());

  size_t sz_x = std::ceil(sz.x() / chunkSzX);
  size_t sz_z = std::ceil(sz.z() / chunkSzZ);

  heightfieldData.reserve(sz_x * sz_z);
  for(size_t i = 0; i < sz_x; ++i)
    for(size_t j = 0; j < sz_z; ++j)
      {
        heightfieldData.emplace_back();
        const int x0 = i*chunkSzX,
            x1 = std::min<int>((i+1)*chunkSzX, sz.x()),
            z0 = j*chunkSzZ,
            z1 = std::min<int>((j+1)*chunkSzZ, sz.z());

        heightfieldData.back().szX = x1 - x0 + 1;
        heightfieldData.back().szZ = z1 - z0 + 1;
        heightfieldData.back().maxHeight = 0;
        heightfieldData.back().minHeight = sz.y();
        generateHeightfieldChunk(x0, x1, z0, z1, heightfieldData.back().data,
                                 heightfieldData.back().minHeight, heightfieldData.back().maxHeight);
      }
}
