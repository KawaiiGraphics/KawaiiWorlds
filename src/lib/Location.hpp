#ifndef LOCATION_HPP
#define LOCATION_HPP

#include "LandscapeInfo.hpp"
#include "Entity.hpp"

#include <sib_utils/NamedQObjectIndex.hpp>
#include <Kawaii3D/KawaiiDataUnit.hpp>
#include <list>
#include <QHash>

class KAWAIIWORLDS_SHARED_EXPORT Location: public KawaiiDataUnit
{
  Q_OBJECT
  KAWAII_UNIT_DEF(Location);
  Q_PROPERTY(const QString MementoType MEMBER nodeType DESIGNABLE false CONSTANT);

  friend KAWAIIWORLDS_SHARED_EXPORT QDataStream& operator<<(QDataStream &st, const Location &obj);
  friend KAWAIIWORLDS_SHARED_EXPORT QDataStream& operator>>(QDataStream &st, Location &obj);

public:
  Location();
  ~Location();

  Entity *createEntity(Body *body, const QString &id, float mass = 1);
  void registerEntity(Entity *e);
  void forallEntities(const std::function<void(Entity&)> &func);
  void forallEntitiesConst(const std::function<void(const Entity&)> &func) const;
  Entity *findEntity(const QString &id);

  LandscapeInfo *addLandscapeInfo(const QString &material, const QString &heightMapTexture, const QVector3D &size, bool createCollisionShape = false);
  void removeLandscapeInfo(LandscapeInfo &l);
  void forallLandscapes(const std::function<void(LandscapeInfo&)> &func);
  LandscapeInfo *findLandscapeInfo(const QString &id);

  void clear();

  QObject* getScriptWrapper() const;
  void changeScriptWrapper(const std::function<void(QObject *&)> &func);

signals:
  void entityAdded(Entity *e);
  void entityRemoved(Entity *e);

  void landscapeAdded(LandscapeInfo *info);
  void landscapeRemoved(LandscapeInfo *info);

  void aboutToBeDestroyed();



  //implement
private:
  std::list<Entity*> objects;
  std::list<LandscapeInfo> landscapes;
  QObject *scriptWrapper;

  std::unique_ptr<QReadWriteLock> objectsM;
  std::unique_ptr<QReadWriteLock> landscapesM;
  std::unique_ptr<QReadWriteLock> scriptWrapperM;

  sib_utils::NamedTypedQObjectIndex<Entity> indexedObjects;

  void removeEntity(std::list<Entity*>::iterator el);
};


KAWAIIWORLDS_SHARED_EXPORT QDataStream& operator<<(QDataStream &st, const Location &obj);
KAWAIIWORLDS_SHARED_EXPORT QDataStream& operator>>(QDataStream &st, Location &obj);

#endif // LOCATION_HPP
