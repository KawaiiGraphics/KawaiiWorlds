#ifndef ILLUMINATIONMODEL_HPP
#define ILLUMINATIONMODEL_HPP

#include <memory>
#include <cstdlib>
#include <cstring>
#include <QPointer>
#include <functional>
#include <QDataStream>
#include <sib_utils/Serializable.hpp>

#include <Kawaii3D/Illumination/KawaiiLamp.hpp>
#include <Kawaii3D/Illumination/KawaiiIllumination.hpp>
#include <Kawaii3D/Illumination/KawaiiShadowMapper.hpp>

#include <Kawaii3D/Illumination/KawaiiPBR.hpp>
#include <Kawaii3D/Illumination/KawaiiPhong.hpp>
#include <Kawaii3D/Illumination/KawaiiCel.hpp>

#include "KawaiiWorlds_global.hpp"

class World;

class KAWAIIWORLDS_SHARED_EXPORT IlluminationModelBase: public sib_utils::Serializable
{
  friend KAWAIIWORLDS_SHARED_EXPORT QDataStream& operator>>(QDataStream &st, IlluminationModelBase &obj);

public:
  QPointer<QObject> jsWrapper;

  IlluminationModelBase(std::string_view adapterId);
  virtual ~IlluminationModelBase() = default;

  void init(World &w);
  void init(World &w, const QString &name, uint32_t uboBindingPoint);

  inline const QString &getName() const
  { return name; }

  inline uint32_t getUboBindingPoint() const
  { return uboBindingPoint; }

  void addDirLight();
  void addDotLight();
  void addSpotLight();

  void removeDirLight(size_t i);
  void removeDotLight(size_t i);
  void removeSpotLight(size_t i);

  size_t getDirLightCount() const;
  size_t getDotLightCount() const;
  size_t getSpotLightCount() const;

  inline static std::unique_ptr<IlluminationModelBase> loadFrom(QDataStream &st, World &w)
  {
    auto ptr = sib_utils::Serializable::loadFrom(st, adapters);
    auto result = std::unique_ptr<IlluminationModelBase>(static_cast<IlluminationModelBase*>(ptr.release()));
    result->init(w);
    return result;
  }

  KawaiiShadowMapper* createShadowMapper() const;
  KawaiiShadowMapper* getShadowMapper() const;

protected:
  inline static std::unordered_map<std::string_view, ByteAdapter> adapters = {};

  template<class T> using AdapterRegistrator = sib_utils::Serializable::AdapterRegistrator<T, adapters>;


  KawaiiLampsLayoutAbstract &getLampsLayout() const;


private:
  QString name;
  std::unique_ptr<KawaiiIllumination> illumination;
  World *world;
  uint32_t uboBindingPoint;

  virtual void createLampArrays(KawaiiIllumination &illumination) = 0;
};

KAWAIIWORLDS_SHARED_EXPORT QDataStream& operator<<(QDataStream &st, const IlluminationModelBase &obj);
KAWAIIWORLDS_SHARED_EXPORT QDataStream& operator>>(QDataStream &st, IlluminationModelBase &obj);



template<typename T>
class IlluminationModel: public IlluminationModelBase
{
public:
  using IlluminationStructs = KawaiiIlluminationStructs<T>;
  using DirLight = typename IlluminationStructs::DirLight;
  using DotLight = typename IlluminationStructs::Lamp;
  using SpotLight = typename IlluminationStructs::Projector;

  IlluminationModel():
    IlluminationModelBase(reg.adapterId)
  { }

  IlluminationModel(World &w, const QString &name, size_t i):
    IlluminationModelBase(reg.adapterId)
  {
    init(w, name, i);
  }

  const DirLight& getDirLight(size_t i) const
  {
    return static_cast<DirLight&>(getLampsLayout().getDirLight(i));
  }

  const DotLight& getDotLight(size_t i) const
  {
    return static_cast<DotLight&>(getLampsLayout().getDotLight(i));
  }

  const SpotLight& getSpotLight(size_t i) const
  {
    return static_cast<SpotLight&>(getLampsLayout().getSpotLight(i));
  }

  void changeDirLight(size_t i, const std::function<bool(DirLight&)> &func)
  {
    auto &l = static_cast<DirLight&>(getLampsLayout().getDirLight(i));
    if(func && func(l))
      getLampsLayout().updateDirLight(i);
  }

  void changeDotLight(size_t i, const std::function<bool(DotLight&)> &func)
  {
    auto &l = static_cast<DotLight&>(getLampsLayout().getDotLight(i));
    if(func && func(l))
      getLampsLayout().updateDotLight(i);
  }

  void changeSpotLight(size_t i, const std::function<bool(SpotLight&)> &func)
  {
    auto &l = static_cast<SpotLight&>(getLampsLayout().getSpotLight(i));
    if(func && func(l))
      getLampsLayout().updateSpotLight(i);
  }

private:
  static AdapterRegistrator<IlluminationModel<T>> reg;

  void createLampArrays(KawaiiIllumination &illumination) override
  {
    illumination.createLampArrays<T>();
  }
};

template<typename T>
QDataStream& operator<<(QDataStream &st, const IlluminationModel<T> &obj)
{
  st << static_cast<const IlluminationModelBase&>(obj)
     << static_cast<quint64>(obj.getDirLightCount())
     << static_cast<quint64>(obj.getDotLightCount())
     << static_cast<quint64>(obj.getSpotLightCount());

  for(size_t i = 0; i < obj.getDirLightCount(); ++i)
    {
      const auto &l = obj.getDirLight(i);
      st << static_cast<const T&>(l) << static_cast<const KawaiiDirLight&>(l);
    }
  for(size_t i = 0; i < obj.getDotLightCount(); ++i)
    {
      const auto &l = obj.getDotLight(i);
      st << static_cast<const T&>(l) << static_cast<const KawaiiLamp&>(l);
    }
  for(size_t i = 0; i < obj.getSpotLightCount(); ++i)
    {
      const auto &l = obj.getSpotLight(i);
      st << static_cast<const T&>(l) << static_cast<const KawaiiProjector&>(l);
    }
  return st;
}

template<typename T>
QDataStream& operator>>(QDataStream &st, IlluminationModel<T> &obj)
{
  st >> static_cast<IlluminationModelBase&>(obj);

  quint64 dirLights,
      dotLights,
      spotLights;

  const quint64 currentDirLightCount = obj.getDirLightCount(),
      currentDotLightCount = obj.getDotLightCount(),
      currentSpotLightCount = obj.getSpotLightCount();

  st >> dirLights >> dotLights >> spotLights;
  const qint64 dirLightsDelta = dirLights - currentDirLightCount;
  if(dirLightsDelta > 0)
    {
      for(qint64 i = 0; i < dirLightsDelta; ++i)
        obj.addDirLight();
    }
  else if(dirLightsDelta < 0)
    for(qint64 i = 0; i > dirLightsDelta; --i)
      obj.removeDirLight(0);
  for(size_t i = 0; i < dirLights; ++i)
    {
      obj.changeDirLight(i, [&st](typename IlluminationModel<T>::DirLight &l) {
          st >> static_cast<T&>(l) >> static_cast<KawaiiDirLight&>(l);
          return true;
        });
    }

  const qint64 dotLightsDelta = dotLights - currentDotLightCount;
  if(dotLightsDelta > 0)
    {
      for(qint64 i = 0; i < dotLightsDelta; ++i)
        obj.addDotLight();
    }
  else if(dotLightsDelta < 0)
    for(qint64 i = 0; i > dotLightsDelta; --i)
      obj.removeDotLight(0);
  for(size_t i = 0; i < dotLights; ++i)
    {
      obj.changeDotLight(i, [&st](typename IlluminationModel<T>::DotLight &l) {
          st >> static_cast<T&>(l) >> static_cast<KawaiiLamp&>(l);
          return true;
        });
    }

  const qint64 spotLightsDelta = spotLights - currentSpotLightCount;
  if(spotLightsDelta > 0)
    {
      for(qint64 i = 0; i < spotLightsDelta; ++i)
        obj.addSpotLight();
    }
  else if(spotLightsDelta < 0)
    for(qint64 i = 0; i > spotLightsDelta; --i)
      obj.removeSpotLight(0);
  for(size_t i = 0; i < spotLights; ++i)
    {
      obj.changeSpotLight(i, [&st](typename IlluminationModel<T>::SpotLight &l) {
          st >> static_cast<T&>(l) >> static_cast<KawaiiProjector&>(l);
          return true;
        });
    }
  return st;
}


template<> inline IlluminationModelBase::AdapterRegistrator<IlluminationModel<KawaiiPhongLight>>
IlluminationModel<KawaiiPhongLight>::reg("phong_illumination_model");

template<> inline IlluminationModelBase::AdapterRegistrator<IlluminationModel<KawaiiCelLight>>
IlluminationModel<KawaiiCelLight>::reg("cel_illumination_model");

template<> inline IlluminationModelBase::AdapterRegistrator<IlluminationModel<KawaiiPbrLight>>
IlluminationModel<KawaiiPbrLight>::reg("pbr_illumination_model");

#endif // ILLUMINATIONMODEL_HPP
