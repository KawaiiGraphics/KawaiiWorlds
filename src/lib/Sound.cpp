#include "Sound.hpp"
#include <random>
#include <QDir>
#include <sib_utils/ioReadAll.hpp>

KAWAII_COMMON_IMPLEMENT(Sound::reg);

namespace {
  std::default_random_engine dev_random(std::chrono::system_clock::now().time_since_epoch().count());
}

Sound::Sound(const QString &fName):
  url(QUrl::fromLocalFile(fName)),
  playRequested(false),
  loopEnabled(true)
{
  player.setAudioOutput(&audioOut);
  player.setSource(url);
  player.setLoops(QMediaPlayer::Infinite);

  connect(&player, &QMediaPlayer::mediaStatusChanged, [this] (QMediaPlayer::MediaStatus status) {
    if(status == QMediaPlayer::MediaStatus::EndOfMedia)
      {
        if(!loopEnabled)
          emit finished(this);
      } else if(playRequested)
      switch(status)
        {
        case QMediaPlayer::LoadedMedia:
        case QMediaPlayer::BufferedMedia:
        case QMediaPlayer::BufferingMedia:
          playRequested = false;
          player.play();
          if(audioOut.volume() != requestedVolume)
            audioOut.setVolume(requestedVolume);
          break;
        default: break;
        }
  });

  connect(&player, &QMediaPlayer::seekableChanged, [this] (bool s) {
      if(s && pendingPosition) {
          seek(*pendingPosition);
          pendingPosition.reset();
        }
    });
}

Sound::~Sound()
{
  stop();
  player.setSource({});
}

void Sound::play(const std::chrono::milliseconds &position, int volume)
{
  setPlaybackPos(position.count());

  playRequest(volume);
  if(audioOut.volume() != volume)
    audioOut.setVolume(volume);
}

void Sound::play(Sound::random_pos_t, int volume)
{
  if(positionsDistribution)
    setPlaybackPos((*positionsDistribution)(dev_random));
  else {
      auto playerDurationChanged = [this] (uint64_t duration) {
        positionsDistribution = std::make_unique<std::uniform_int_distribution<uint64_t>>(0, duration);
        setPlaybackPos((*positionsDistribution)(dev_random));
        QObject::disconnect(onPlayerDurationChanged);
      };

      onPlayerDurationChanged = connect(&player, &QMediaPlayer::durationChanged, playerDurationChanged);
    }

  playRequest(volume);
}

void Sound::pause()
{
  playRequested = false;
  player.pause();
}

void Sound::stop()
{
  loopEnabled = false;
  playRequested = false;
  player.setLoops(1);
  player.stop();
}

void Sound::setAutoRepeat(bool val)
{
  loopEnabled = val;
  if(loopEnabled)
    player.setLoops(QMediaPlayer::Infinite);
  else
    player.setLoops(1);
}

void Sound::writeBinary(sib_utils::memento::Memento::DataMutator &mem) const
{
  const QString fName = url.toLocalFile();
  mem.setData("url", url.toString());

  if(extractedMemento)
    mem.setData(QStringLiteral("blob"), extractedMemento->readAll());
  else
    mem.setData(QStringLiteral("blob"), sib_utils::ioReadAll(QFile(fName), QFile::ReadOnly));
}

void Sound::writeText(sib_utils::memento::Memento::DataMutator &mem) const
{
  writeBinary(mem);
}

void Sound::read(sib_utils::memento::Memento::DataReader &mem)
{
  url = QUrl();

  QString urlStr; bool ok;
  mem.get(QStringLiteral("url"), urlStr, ok);

  mem.read(QStringLiteral("blob"), [this, &urlStr] (const QByteArray &blob) {
      extractedMemento = std::make_unique<QBuffer>();
      extractedMemento->setData(blob);
      extractedMemento->open(QBuffer::ReadOnly);

      url = QUrl(urlStr);
    });

  positionsDistribution = nullptr;
  player.setSourceDevice(extractedMemento.get(), url);
}

void Sound::playRequest(int volume)
{
  switch(player.mediaStatus())
    {
    case QMediaPlayer::NoMedia:
    case QMediaPlayer::InvalidMedia:
      player.setSource(url);
      [[fallthrough]];
    case QMediaPlayer::LoadingMedia:
    case QMediaPlayer::StalledMedia:
      playRequested = true;
      requestedVolume = volume;
      break;

    case QMediaPlayer::LoadedMedia:
    case QMediaPlayer::BufferingMedia:
    case QMediaPlayer::BufferedMedia:
    case QMediaPlayer::EndOfMedia:
      player.play();
      if(audioOut.volume() != volume)
        audioOut.setVolume(volume);
      playRequested = false;
      break;
    }
}

void Sound::setPlaybackPos(uint64_t pos)
{
  seek(pos);
  if(!player.isSeekable())
    pendingPosition = pos;
}

void Sound::seek(uint64_t pos)
{
  const auto state = player.playbackState();
  player.setPosition(pos);
  switch(state)
    {
    case QMediaPlayer::PlayingState:
      player.play();
      break;
    case QMediaPlayer::PausedState:
      player.pause();
      break;
    case QMediaPlayer::StoppedState:
      player.stop();
      break;
    }
}

Sound *Sound::createFromMemento(sib_utils::memento::Memento::DataReader &)
{
  return new Sound;
}
