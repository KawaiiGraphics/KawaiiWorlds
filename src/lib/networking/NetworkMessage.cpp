#include "NetworkMessage.hpp"
#include <sib_utils/refine_type.hpp>
#include <iostream>

NetworkMessage::NetworkMessage():
  sz(0),
  type(NetworkMessageType::InitialState),
  state(State::Init)
{ }

NetworkMessage::NetworkMessage(QIODevice &io):
  state(State::Init)
{
  readFrom(io);
}

NetworkMessage::NetworkMessage(NetworkMessageType type, QByteArray &&bytes):
  bytes(std::move(bytes)),
  sz(this->bytes.size()),
  type(type),
  state(State::Compleate)
{
}

NetworkMessage::NetworkMessage(NetworkMessageType type, const QByteArray &bytes):
  bytes(bytes),
  sz(bytes.size()),
  type(type),
  state(State::Compleate)
{
}

NetworkMessage::NetworkMessage(NetworkMessage &&origin):
  bytes(std::move(origin.bytes)),
  sz(origin.sz),
  type(origin.type),
  state(origin.state)
{
  origin.state = State::Init;
  origin.sz = 0;
}

NetworkMessage::NetworkMessage(const NetworkMessage &origin):
  bytes(origin.bytes),
  sz(origin.sz),
  type(origin.type),
  state(origin.state)
{
}

NetworkMessage &NetworkMessage::operator=(NetworkMessage &&origin)
{
  bytes = std::move(origin.bytes);
  sz = origin.sz;
  type = origin.type;
  state = origin.state;

  origin.state = State::Init;
  origin.sz = 0;

  return *this;
}

NetworkMessage &NetworkMessage::operator=(const NetworkMessage &origin)
{
  bytes = origin.bytes;
  sz = origin.sz;
  type = origin.type;
  state = origin.state;
  return *this;
}

void NetworkMessage::send(QIODevice &io) const
{
  Q_ASSERT(isCompleated());

  if(io.write(reinterpret_cast<const char*>(&type), sizeof(type)) != sizeof(type))
    {
      std::cerr << "Error while writing message type" << std::endl;
      return;
    }

  if(io.write(reinterpret_cast<const char*>(&sz), sizeof(sz)) != sizeof(sz))
    {
      std::cerr << "Error while writing bytes size" << std::endl;
      return;
    }

  if(io.write(bytes) != bytes.size())
    {
      std::cerr << "Error while writing bytes" << std::endl;
      return;
    }
}

void NetworkMessage::readFrom(QIODevice &io)
{
  auto readVal = [&io] (auto &val) {
      using T = sib_utils::refined_type<decltype(val)>;
      constexpr const size_t sz = sizeof(T);
      if(static_cast<size_t>(io.bytesAvailable()) >= sz)
        {
          QByteArray buf = io.read(sz);
          val = *reinterpret_cast<T*>(buf.data());
          return true;
        } else
        return false;
    };

  while(io.bytesAvailable())
    {
      switch(state)
        {
        case State::Init:
        case State::ReadingType:
          if(readVal(type))
            state = State::ReadingSize;
          else
            state = State::ReadingType;
          break;

        case State::ReadingSize:
          if(readVal(sz))
            state = State::ReadingData;
          break;

        case State::ReadingData:
          if(sz > static_cast<size_t>(bytes.size()))
            bytes.append(io.read(sz - bytes.size()));

          if(sz == static_cast<size_t>(bytes.size()))
            state = State::Compleate;
          break;

        case State::Compleate:
          return;
        }
    }
}

NetworkMessage::State NetworkMessage::currentState() const
{
  return state;
}

bool NetworkMessage::isCompleated() const
{
  return state == State::Compleate;
}

const QByteArray &NetworkMessage::getData() const
{
  return bytes;
}

NetworkMessageType NetworkMessage::getType() const
{
  return type;
}

void NetworkMessage::parseInputMessages(std::vector<InputEventsPack> &&events, World &world)
{
  for(auto &i: events)
    if(world.getInputCount() > i.index)
      world.getInput(i.index).pushMessages(std::move(i.data));
    else
      std::cerr << "NetworkMessage::parseInputMessages: WARNING: input #" << i.index << " not found!" << std::endl;
}
