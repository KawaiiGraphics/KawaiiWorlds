#ifndef NETWORKSERVER_HPP
#define NETWORKSERVER_HPP

#include "../World.hpp"
#include "NetworkMessage.hpp"
#include <QTcpServer>
#include <functional>
#include <QByteArray>
#include <QThreadPool>

class KAWAIIWORLDS_SHARED_EXPORT NetworkServer: public QObject
{
  Q_OBJECT

public:
  NetworkServer(World &world);
  ~NetworkServer();

  QThreadPool threadPool;
  NetworkMessage customHello;

  void broadcastMessage(const NetworkMessage &msg);
  void broadcastMessage(const NetworkMessage &msg, QTcpSocket &exclude);

  QTcpSocket* connectionFor(const QHostAddress &address) const;

  QHostAddress serverAddress() const;

  uint64_t getHeartbeatInterval() const;
  void setHeartbeatInterval(uint64_t msec);

  int16_t getWorldSyncInterval() const;
  void setWorldSyncInterval(int16_t ticks);

  int16_t getEntitiesSyncInterval() const;
  void setEntitiesSyncInterval(int16_t ticks);

public slots:
  void start(const QHostAddress &address = QHostAddress::Any, quint16 port = 0);
  void stop();

signals:
  void clientConnected(QTcpSocket &client);
  void clientDisconnected(QTcpSocket &client);
  void customMessageReceived(QTcpSocket &client, const QByteArray &message);



  //IMPLEMENT
private:
  class HeartbeatThread;

  struct ClientConnection {
    QTcpSocket *socket;
    NetworkMessage pendingMessage;

    inline bool operator==(const ClientConnection &other) const
    { return socket == other.socket; }
  };

  QTcpServer tcp;
  World &world;


  std::list<ClientConnection> clients;
  std::unique_ptr<HeartbeatThread> heartbeatThread;
  std::unique_ptr<QReadWriteLock> intervalsLock;
  uint64_t heartbeatInterval;
  int16_t entitiesSyncInterval;
  int16_t worldSyncInterval;


  NetworkMessage initialMsg();

  void connectClient();

  void processInputMessages(const std::list<ClientConnection>::iterator &connection);

  void processClientMessage(const std::list<ClientConnection>::iterator &connection);

  void disconnectClient(const std::list<ClientConnection>::iterator &el);
};

#endif // NETWORKSERVER_HPP
