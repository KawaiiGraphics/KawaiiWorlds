#ifndef HEARTBEATSTRUCTS_HPP
#define HEARTBEATSTRUCTS_HPP

#include <QQuaternion>
#include <QDataStream>
#include <QVector3D>
#include <QString>
#include <vector>

#include "KawaiiWorlds_global.hpp"

struct EntityState
{
  QQuaternion rotation;
  QVector3D angular_vel;

  QVector3D pos;
  QVector3D vel;

  QString id;
};

KAWAIIWORLDS_SHARED_EXPORT QDataStream& operator<<(QDataStream &st, const EntityState &obj);
KAWAIIWORLDS_SHARED_EXPORT QDataStream& operator>>(QDataStream &st, EntityState &obj);


struct InputEventsPack
{
  quint64 index;
  std::vector<QString> data;
};

KAWAIIWORLDS_SHARED_EXPORT QDataStream& operator<<(QDataStream &st, const InputEventsPack &obj);
KAWAIIWORLDS_SHARED_EXPORT QDataStream& operator>>(QDataStream &st, InputEventsPack &obj);

struct ServerHeartbeat
{
  std::vector<EntityState> entities;
  std::vector<InputEventsPack> inputEvents;
  QByteArray worldState;

  bool empty() const;
};

KAWAIIWORLDS_SHARED_EXPORT QDataStream& operator<<(QDataStream &st, const ServerHeartbeat &obj);
KAWAIIWORLDS_SHARED_EXPORT QDataStream& operator>>(QDataStream &st, ServerHeartbeat &obj);

struct ClientHeartbeat
{
  std::vector<InputEventsPack> inputEvents;
};

KAWAIIWORLDS_SHARED_EXPORT QDataStream& operator<<(QDataStream &st, const ClientHeartbeat &obj);
KAWAIIWORLDS_SHARED_EXPORT QDataStream& operator>>(QDataStream &st, ClientHeartbeat &obj);

#endif // HEARTBEATSTRUCTS_HPP
