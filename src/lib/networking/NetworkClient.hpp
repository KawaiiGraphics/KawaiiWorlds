#ifndef NETWORKCLIENT_HPP
#define NETWORKCLIENT_HPP

#include "../World.hpp"
#include "NetworkMessage.hpp"
#include <QHostAddress>
#include <QTcpSocket>

class KAWAIIWORLDS_SHARED_EXPORT NetworkClient: public QObject
{
  Q_OBJECT

public:
  NetworkClient(World &world);
  ~NetworkClient();

  QHostAddress clientAddress() const;
  QHostAddress serverAddress() const;

  uint64_t getHeartbeatInterval() const;
  void setHeartbeatInterval(uint64_t msec);

  const QByteArray &receiveHello();

public slots:
  void start(const QHostAddress &serverAddres, quint16 serverPort);
  void stop();

signals:
  void clientConnected(const QHostAddress &client);
  void clientDisconnected(const QHostAddress &client);

  void customMessageReceived(const QByteArray &message);
  void customHelloReceived(const QByteArray &message);



  //IMPLEMENT
private:
  class HeartbeatThread;

  World &world;

  QTcpSocket socket;
  NetworkMessage msg;

  std::unique_ptr<HeartbeatThread> heartbeatThread;
  std::unique_ptr<QReadWriteLock> intervalsLock;
  QByteArray receivedHello;
  uint64_t heartbeatInterval;
  bool isInEventLoop;


  void parseInitialState();
  void parseInputEvents();
  void parseServerHeartbeat();
  void parseClientConnected();
  void parseClientDisconnected();
  void parseHello();
  void parseCustom();

  void readMsg();
};

#endif // NETWORKCLIENT_HPP
