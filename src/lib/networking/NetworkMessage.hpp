#ifndef NETWORKMESSAGE_HPP
#define NETWORKMESSAGE_HPP

#include "NetworkMessageType.hpp"
#include "HeartbeatStructs.hpp"
#include <QByteArray>
#include <QIODevice>
#include "../World.hpp"

class KAWAIIWORLDS_SHARED_EXPORT NetworkMessage
{
public:
  enum class State: uint8_t
  {
    Init = 0,
    ReadingType,
    ReadingSize,
    ReadingData,
    Compleate
  };

  NetworkMessage();
  NetworkMessage(QIODevice &io);
  NetworkMessage(NetworkMessageType type, QByteArray &&bytes);
  NetworkMessage(NetworkMessageType type, const QByteArray &bytes);

  NetworkMessage(NetworkMessage &&origin);
  NetworkMessage(const NetworkMessage &origin);
  NetworkMessage& operator=(NetworkMessage &&origin);
  NetworkMessage& operator=(const NetworkMessage &origin);

  void send(QIODevice &io) const;
  void readFrom(QIODevice &io);

  State currentState() const;
  bool isCompleated() const;

  const QByteArray& getData() const;
  NetworkMessageType getType() const;

  static void parseInputMessages(std::vector<InputEventsPack> &&events, World &world);



  //IMPLEMENT
private:
  QByteArray bytes;
  uint64_t sz;
  NetworkMessageType type;
  State state;
};

#endif // NETWORKMESSAGE_HPP
