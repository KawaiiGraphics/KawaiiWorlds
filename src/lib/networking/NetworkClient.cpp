#include "NetworkClient.hpp"
#include <sib_utils/Memento/BlobLoader.hpp>
#include <QEventLoop>
#include <iostream>

class NetworkClient::HeartbeatThread: public QThread
{
  NetworkClient &clnt;
public:
  HeartbeatThread(NetworkClient &clnt):
    QThread(&clnt),
    clnt(clnt)
  { }

  // QThread interface
protected:
  void run() override
  {
    while(clnt.socket.state() != QAbstractSocket::ClosingState
          && clnt.socket.state() != QAbstractSocket::UnconnectedState)
      {
        ClientHeartbeat heartbeat;

        quint64 index = 0;
        clnt.world.forallInputs([&heartbeat, &index] (Input &input) {
            if(input.active)
              {
                input.messageLogging = true;

                auto msg_arr = input.pullMessages();
                if(!msg_arr.empty())
                  heartbeat.inputEvents.push_back(InputEventsPack {
                                                    .index = index,
                                                    .data = std::move(msg_arr)
                                                  });
              }
            ++index;
          });
        if(!heartbeat.inputEvents.empty())
          {
            QByteArray bytes;
            {
              QDataStream st(&bytes, QIODevice::WriteOnly);
              st << heartbeat;
            }
            bytes = qCompress(bytes, 9);
            NetworkMessage msg(NetworkMessageType::InputMessages, bytes);
            QMetaObject::invokeMethod(&clnt, [this, msg] { msg.send(clnt.socket); });
          }
        msleep(clnt.getHeartbeatInterval());
      }
  }
};



NetworkClient::NetworkClient(World &world):
  world(world),
  intervalsLock(std::make_unique<QReadWriteLock>()),
  heartbeatInterval(10),
  isInEventLoop(false)
{
  connect(&socket, &QIODevice::readyRead, this, &NetworkClient::readMsg);
}

NetworkClient::~NetworkClient()
{
  stop();
}

QHostAddress NetworkClient::clientAddress() const
{
  return socket.localAddress();
}

QHostAddress NetworkClient::serverAddress() const
{
  return socket.peerAddress();
}

uint64_t NetworkClient::getHeartbeatInterval() const
{
  QReadLocker l(intervalsLock.get());
  return heartbeatInterval;
}

void NetworkClient::setHeartbeatInterval(uint64_t msec)
{
  QWriteLocker l(intervalsLock.get());
  heartbeatInterval = msec;
}

const QByteArray &NetworkClient::receiveHello()
{
  if(receivedHello.isEmpty())
    {
      QEventLoop eventLoop;
      connect(this, &NetworkClient::customHelloReceived, this, [this, &eventLoop] {
          isInEventLoop = false;
          eventLoop.quit();
        });
      isInEventLoop = true;
      eventLoop.exec();
      if(socket.bytesAvailable())
        QMetaObject::invokeMethod(this, &NetworkClient::readMsg, Qt::QueuedConnection);
    }
  return receivedHello;
}

void NetworkClient::start(const QHostAddress &serverAddres, quint16 serverPort)
{
  if(socket.peerAddress() != serverAddres || socket.peerPort() != serverPort)
    {
      stop();
      socket.connectToHost(serverAddres, serverPort);

      world.forallInputs([] (Input &input) { input.messageLogging = true; });
      heartbeatThread = std::make_unique<HeartbeatThread>(*this);
      heartbeatThread->start(QThread::IdlePriority);
    }
}

void NetworkClient::stop()
{
  if(socket.isOpen())
    socket.close();
  if(heartbeatThread)
    {
      heartbeatThread->wait();
      heartbeatThread.reset();
    }
  world.forallInputs([] (Input &input) { input.messageLogging = false; });
}

void NetworkClient::parseInitialState()
{
  const QByteArray unpacked = qUncompress(msg.getData());
  char *resMementoBytes;
  qint64 resMementoLength;
  QDataStream st(unpacked);
  st.readBytes(resMementoBytes, resMementoLength);
  world.res().reset(QByteArray::fromRawData(resMementoBytes, resMementoLength));
  delete[] resMementoBytes;
  world.load(st);
}

void NetworkClient::parseInputEvents()
{
  const QByteArray unpacked = qUncompress(msg.getData());
  QDataStream st(unpacked);
  ClientHeartbeat inputData;
  st >> inputData;
  NetworkMessage::parseInputMessages(std::move(inputData.inputEvents), world);
}

void NetworkClient::parseServerHeartbeat()
{
  const QByteArray unpacked = qUncompress(msg.getData());
  QDataStream st(unpacked);
  ServerHeartbeat heartbeat;
  st >> heartbeat;
  if(!heartbeat.worldState.isEmpty())
    {
      QDataStream st(heartbeat.worldState);
      world.load(st);
    } else
    {
      NetworkMessage::parseInputMessages(std::move(heartbeat.inputEvents), world);
      for(const auto &i: heartbeat.entities)
        {
          auto *e = world.getEntity(i.id);
          if(e)
            {
              e->setPosition(i.pos);
              e->setRotation(i.rotation);
              e->setVelocity(i.vel);
              e->setAngularVel(i.angular_vel);
            } else
            std::cerr << "NetworkClient::parseServerHeartbeat: No entity with ID " << i.id.toStdString() << std::endl;
        }
    }
}

void NetworkClient::parseClientConnected()
{
  QString addrStr = QString::fromUtf8(msg.getData());
  emit clientConnected(QHostAddress(addrStr));
}

void NetworkClient::parseClientDisconnected()
{
  QString addrStr = QString::fromUtf8(msg.getData());
  emit clientDisconnected(QHostAddress(addrStr));
}

void NetworkClient::parseCustom()
{
  emit customMessageReceived(msg.getData());
}

void NetworkClient::parseHello()
{
  if(!receivedHello.isEmpty())
    std::cerr << "NetworkClient::parseHello: WARNING: Multiple Hello messages received" << std::endl;
  receivedHello = msg.getData();
  emit customHelloReceived(receivedHello);
}

void NetworkClient::readMsg()
{
  bool wasInEventLoop = isInEventLoop;
  while(socket.bytesAvailable() && wasInEventLoop == isInEventLoop)
    {
      msg.readFrom(socket);
      if(msg.isCompleated())
        {
          switch (msg.getType())
            {
            case NetworkMessageType::InitialState:
              parseInitialState();
              break;

            case NetworkMessageType::InputMessages:
              parseInputEvents();
              break;

            case NetworkMessageType::ServerHeartbeat:
              parseServerHeartbeat();
              break;

            case NetworkMessageType::ClientConnected:
              parseClientConnected();
              break;

            case NetworkMessageType::ClientDisconnected:
              parseClientDisconnected();
              break;

            case NetworkMessageType::CustomHello:
              parseHello();
              break;

            case NetworkMessageType::Custom:
              parseCustom();
            }
          msg = {};
        }
    }
}
