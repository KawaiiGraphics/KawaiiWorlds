#ifndef NETWORKMESSAGETYPE_HPP
#define NETWORKMESSAGETYPE_HPP

#include <cinttypes>

enum class NetworkMessageType: uint8_t
{
  InitialState = 0,
  InputMessages = 1,
  ServerHeartbeat = 2,
  ClientConnected = 3,
  ClientDisconnected = 4,
  CustomHello = 5,
  Custom = 6
};

#endif // NETWORKMESSAGETYPE_HPP
