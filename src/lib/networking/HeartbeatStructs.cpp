#include "HeartbeatStructs.hpp"
#include <sib_utils/qDataStreamExpansion.hpp>
#include <cstring>

KAWAIIWORLDS_SHARED_EXPORT QDataStream &operator<<(QDataStream &st, const EntityState &obj)
{
  return st << obj.rotation << obj.angular_vel
            << obj.pos << obj.vel
            << obj.id;
}

KAWAIIWORLDS_SHARED_EXPORT QDataStream &operator>>(QDataStream &st, EntityState &obj)
{
  return st >> obj.rotation >> obj.angular_vel
            >> obj.pos >> obj.vel
            >> obj.id;
}

KAWAIIWORLDS_SHARED_EXPORT QDataStream &operator<<(QDataStream &st, const InputEventsPack &obj)
{
  return st << obj.index << obj.data;
}

KAWAIIWORLDS_SHARED_EXPORT QDataStream &operator>>(QDataStream &st, InputEventsPack &obj)
{
  return st >> obj.index >> obj.data;
}

KAWAIIWORLDS_SHARED_EXPORT QDataStream &operator<<(QDataStream &st, const ServerHeartbeat &obj)
{
  st.writeBytes(obj.worldState.data(), obj.worldState.size());
  return st << obj.entities << obj.inputEvents;
}

KAWAIIWORLDS_SHARED_EXPORT QDataStream &operator>>(QDataStream &st, ServerHeartbeat &obj)
{
  char *buf;
  qint64 bufLength;
  st.readBytes(buf, bufLength);
  obj.worldState.resize(bufLength);
  std::memcpy(obj.worldState.data(), buf, bufLength);
  delete [] buf;

  return st >> obj.entities >> obj.inputEvents;
}

KAWAIIWORLDS_SHARED_EXPORT QDataStream &operator<<(QDataStream &st, const ClientHeartbeat &obj)
{
  return st << obj.inputEvents;
}

KAWAIIWORLDS_SHARED_EXPORT QDataStream &operator>>(QDataStream &st, ClientHeartbeat &obj)
{
  return st >> obj.inputEvents;
}

bool ServerHeartbeat::empty() const
{
  return entities.empty()
      && inputEvents.empty()
      && worldState.isEmpty();
}
