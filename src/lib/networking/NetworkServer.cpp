#include "NetworkServer.hpp"
#include "NetworkMessage.hpp"
#include "HeartbeatStructs.hpp"
#include <sib_utils/Memento/BlobSaver.hpp>
#include <QtConcurrent>
#include <QTcpSocket>
#include <QBuffer>

class NetworkServer::HeartbeatThread: public QThread
{
  NetworkServer &srv;
  int16_t i0;
  int16_t i1;

public:
  HeartbeatThread(NetworkServer &srv):
    QThread(&srv),
    srv(srv),
    i0(0),
    i1(0)
  {}

  // QThread interface
protected:
  void run() override final
  {
    while(srv.tcp.isListening())
      {
        tick();
        msleep(srv.getHeartbeatInterval());
      }
  }

private:
  void getWorldState(ServerHeartbeat &heartbeat)
  {
    QDataStream st(&heartbeat.worldState, QIODevice::WriteOnly);
    srv.world.save(st);
    srv.world.forallInputs([] (Input &input) {
      if(input.active && input.messageLogging)
        input.pullMessages();
    });
  }

  void doRegularTick(ServerHeartbeat &heartbeat)
  {
    const auto entitiesSyncInt = srv.getEntitiesSyncInterval();
    if(i0 == entitiesSyncInt)
      {
        srv.world.forallEntities([&heartbeat] (Entity &e) {
          if(!e.objectName().isNull())
            heartbeat.entities.push_back(EntityState {
                                           .rotation = e.getRotation(),
                                           .angular_vel = e.getAngularVel(),
                                           .pos = e.getPosition(),
                                           .vel = e.getVelocity(),
                                           .id = e.objectName()
                                         });
        });
        i0 = 0;
      }
    else if(entitiesSyncInt > 0)
      ++i0;

    quint64 index = 0;
    srv.world.forallInputs([&heartbeat, &index] (Input &input) {
        if(input.active)
          {
            input.messageLogging = true;

            auto msg_arr = input.pullMessages();
            if(!msg_arr.empty())
              heartbeat.inputEvents.push_back(InputEventsPack {
                                                .index = index,
                                                .data = std::move(msg_arr)
                                              });
          }
        ++index;
      });
  }

  void tick()
  {
    if(!srv.clients.empty())
      {
        ServerHeartbeat heartbeat;
        const auto worldSyncInt = srv.getWorldSyncInterval();
        if(Q_UNLIKELY(i1 == worldSyncInt))
          {
            getWorldState(heartbeat);
            i1 = 0;
          } else
          {
            doRegularTick(heartbeat);
            if(worldSyncInt > 0)
              ++i1;
          }

        if(!heartbeat.empty())
          {
            QByteArray bytes;
            {
              QDataStream st(&bytes, QIODevice::WriteOnly);
              st << heartbeat;
            }
            bytes = qCompress(bytes, 9);
            NetworkMessage msg(NetworkMessageType::ServerHeartbeat, bytes);
            QMetaObject::invokeMethod(&srv, [this, msg] { srv.broadcastMessage(msg); });
          }
      }
  }
};

NetworkServer::NetworkServer(World &world):
  world(world),
  intervalsLock(std::make_unique<QReadWriteLock>()),
  heartbeatInterval(10),
  entitiesSyncInterval(15),
  worldSyncInterval(1500)
{
  connect(&tcp, &QTcpServer::newConnection, this, &NetworkServer::connectClient);
}

NetworkServer::~NetworkServer()
{
  clients.clear();
  stop();
}

void NetworkServer::broadcastMessage(const NetworkMessage &msg)
{
  for(const auto &connection: clients)
    msg.send(*connection.socket);
  //    threadPool.start([&connection, msg] { msg.send(*connection.socket); });
}

void NetworkServer::broadcastMessage(const NetworkMessage &msg, QTcpSocket &exclude)
{
  for(const auto &connection: clients)
    if(connection.socket != &exclude)
      msg.send(*connection.socket);
  //      threadPool.start([&connection, msg] { msg.send(*connection.socket); });
}

QTcpSocket *NetworkServer::connectionFor(const QHostAddress &address) const
{
  for(const auto &connection: clients)
    if(connection.socket->peerAddress().isEqual(address, QHostAddress::TolerantConversion))
      return connection.socket;

  return nullptr;
}

QHostAddress NetworkServer::serverAddress() const
{
  return tcp.serverAddress();
}

uint64_t NetworkServer::getHeartbeatInterval() const
{
  QReadLocker l(intervalsLock.get());
  return heartbeatInterval;
}

void NetworkServer::setHeartbeatInterval(uint64_t msec)
{
  QWriteLocker l(intervalsLock.get());
  heartbeatInterval = msec;
}

int16_t NetworkServer::getWorldSyncInterval() const
{
  QReadLocker l(intervalsLock.get());
  return worldSyncInterval;
}

void NetworkServer::setWorldSyncInterval(int16_t ticks)
{
  QWriteLocker l(intervalsLock.get());
  worldSyncInterval = ticks;
}

int16_t NetworkServer::getEntitiesSyncInterval() const
{
  QReadLocker l(intervalsLock.get());
  return entitiesSyncInterval;
}

void NetworkServer::setEntitiesSyncInterval(int16_t ticks)
{
  QWriteLocker l(intervalsLock.get());
  entitiesSyncInterval = ticks;
}

void NetworkServer::start(const QHostAddress &address, quint16 port)
{
  stop();
  world.forallInputs([] (Input &input) { input.messageLogging = true; });
  tcp.listen(address, port);
  heartbeatThread = std::make_unique<HeartbeatThread>(*this);
  heartbeatThread->start(QThread::IdlePriority);
}

void NetworkServer::stop()
{
  if(tcp.isListening())
    tcp.close();
  if(heartbeatThread)
    {
      heartbeatThread->wait();
      heartbeatThread.reset();
    }
  world.forallInputs([] (Input &input) { input.messageLogging = false; });
}

NetworkMessage NetworkServer::initialMsg()
{
  QByteArray content;
  const auto &resMementoBytes = world.res().serialize();
  {
    QDataStream st(&content, QIODevice::WriteOnly);
    st.writeBytes(resMementoBytes.data(), resMementoBytes.size());
    world.save(st);
  }
  return NetworkMessage(NetworkMessageType::InitialState, qCompress(content, 9));
}

void NetworkServer::connectClient()
{
  while(tcp.hasPendingConnections())
    {
      auto connection = tcp.nextPendingConnection();

      initialMsg().send(*connection);

      auto el = clients.insert(clients.end(), {.socket = connection});
      connect(connection, &QIODevice::readyRead, std::bind(&NetworkServer::processClientMessage, this, el));
      connect(connection, &QAbstractSocket::disconnected, std::bind(&NetworkServer::disconnectClient, this, el));
      emit clientConnected(*connection);
      if(customHello.isCompleated())
        customHello.send(*connection);

      for(const auto &i: clients)
        NetworkMessage(NetworkMessageType::ClientConnected, i.socket->peerAddress().toString().toUtf8()).send(*connection);

      NetworkMessage connectedMsg(NetworkMessageType::ClientConnected, connection->peerAddress().toString().toUtf8());
      broadcastMessage(connectedMsg, *connection);
    }
}

void NetworkServer::processInputMessages(const std::list<NetworkServer::ClientConnection>::iterator &connection)
{
  const QByteArray unpacked = qUncompress(connection->pendingMessage.getData());
  QDataStream st(unpacked);
  ClientHeartbeat inputData;
  st >> inputData;
  NetworkMessage::parseInputMessages(std::move(inputData.inputEvents), world);
  broadcastMessage(NetworkMessage(NetworkMessageType::InputMessages, connection->pendingMessage.getData()), *connection->socket);
}

void NetworkServer::processClientMessage(const std::list<NetworkServer::ClientConnection>::iterator &connection)
{
  auto &msg = connection->pendingMessage;
  while(connection->socket->bytesAvailable())
    {
      msg.readFrom(*connection->socket);
      if(msg.isCompleated())
        {
          switch(msg.getType())
            {
            case NetworkMessageType::InputMessages:
              processInputMessages(connection);
              break;

            case NetworkMessageType::Custom:
              emit customMessageReceived(*connection->socket, msg.getData());
              break;

            default:
              qCritical().noquote() << "KawaiiWorlds::NetworkServer::processClientMessage: client("
                                    << connection->socket->peerAddress()
                                    << ") Clients can only send input messages or custom messages!";
              break;
            }
          msg = {};
        }
    }
}

void NetworkServer::disconnectClient(const std::list<NetworkServer::ClientConnection>::iterator &el)
{
  disconnect(el->socket, nullptr, nullptr, nullptr);
  emit clientDisconnected(*el->socket);
  NetworkMessage connectedMsg(NetworkMessageType::ClientDisconnected, el->socket->peerAddress().toString().toUtf8());
  broadcastMessage(connectedMsg, *el->socket);

  el->socket->deleteLater();
  clients.erase(el);
}
