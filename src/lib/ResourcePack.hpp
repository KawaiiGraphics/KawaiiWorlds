#ifndef RESOURCEPACK_HPP
#define RESOURCEPACK_HPP

#include <Kawaii3D/Textures/KawaiiImage.hpp>
#include <Kawaii3D/Textures/KawaiiCubemap.hpp>
#include <Kawaii3D/Textures/KawaiiEnvMap.hpp>
#include <Kawaii3D/Textures/KawaiiFramebuffer.hpp>
#include <Kawaii3D/Shaders/KawaiiShader.hpp>
#include <Kawaii3D/Geometry/KawaiiModel3D.hpp>
#include <Kawaii3D/Geometry/KawaiiSkeletalAnimation.hpp>
#include <Kawaii3D/Shaders/KawaiiScene.hpp>

#include <sib_utils/PairHash.hpp>
#include <QReadWriteLock>
#include <unordered_map>
#include <QThreadPool>

#include "KawaiiWorlds_global.hpp"
#include "ShaderEffectSrc.hpp"
#include "ModelInstance.hpp"
#include "Sound.hpp"

class KAWAIIWORLDS_SHARED_EXPORT ResourcePack: public KawaiiDataUnit
{
  Q_OBJECT
  KAWAII_UNIT_DEF(ResourcePack);
  Q_PROPERTY(const QString MementoType MEMBER nodeType DESIGNABLE false CONSTANT);

public:
  enum class InternalShaderEffect
  {
    DiffuseTexturePbr,
    DiffuseTexturePhong,
    DiffuseTextureCel,
    Blur4x4,
    SSAO,
    GaussianBlur,
    IrradianceConvolution,
    GammaAndExposure,
    BrightnessAndContrast,
    ColorCorrection
  };

  ResourcePack();
  ~ResourcePack();

  void waitTasks();

  float getGamma() const;
  void setGamma(float gamma);

  float getExposure() const;
  void setExposure(float exposure);

  float getBrightness() const;
  void setBrightness(float brightness);

  float getContrast() const;
  void setContrast(float contrast);

  KawaiiImage *getTexture2D(const QString &name) const;
  KawaiiCubemap *getTextureCube(const QString &name) const;
  const ShaderEffectSrc &getShader(const QString &name) const;
  KawaiiModel3D *getModel(const QString &name) const;
  KawaiiMaterial *getMaterial(const QString &name) const;
  Sound *getSound(const QString &name) const;

  KawaiiScene *addScene(const QString &name);
  KawaiiScene *getScene(const QString &name) const;
  KawaiiScene *getPrimaryScene() const;

  std::pair<QFuture<bool>, KawaiiImage *> loadTexture2D(const QString &assetStr, const QString &resName);
  std::pair<QFuture<bool>, KawaiiCubemap *> loadCubemap(const QString &assetStr, const QString &resName);
  std::pair<QFuture<bool>, KawaiiModel3D *> loadModel(const QString &assetStr, const QString &resName);
  QFuture<bool> loadAnimation(const QString &assetStr, const QString &targetModelName);
  std::unique_ptr<KawaiiSkeletalAnimation::Instance> getSkeletalAnimation(const QString &modelName, QAnyStringView animationName) const;
  std::unique_ptr<KawaiiSkeletalAnimation::Instance> getSkeletalAnimation(KawaiiModel3D *model, QAnyStringView animationName) const;

  KawaiiImage* getNullTexture2D();
  KawaiiImage* getMonochromeTexture2D(const QColor &color);

  KawaiiCamera *getCamera(const QString &name) const;

  ModelInstance instanceModel(KawaiiModel3D *model, KawaiiMaterial *material);

  void addShaderEffect(const QString &name, const ShaderEffectSrc &effect);
  void loadInternalShaderEffect(InternalShaderEffect effect, ShaderEffectSrc &effectParams);

  void startSkeletalAnimation(const QString &model, QAnyStringView animationName);
  void stopSkeletalAnimation(const QString &model);

  template<typename T>
  KawaiiMaterial& createMaterial(const QString &materialName, const QString &shaderName, KawaiiGpuBuf *ubo = nullptr)
  {
    if(!getPrimaryScene())
      throw std::runtime_error("ResourcePack::createMaterial: default scene has not been set");

    if(getMaterial(materialName))
      throw std::invalid_argument("ResourcePack::createMaterial: material with this name already exists");

    T *prog = getShaderProg<T>(getShader(shaderName), shaderName);

    auto result = new KawaiiMaterial;
    if(ubo)
      ubo->updateParent(result);
    result->setObjectName(materialName);
    result->setUniforms(ubo);
    result->updateParent(this);

    setProgramToMaterial(getPrimaryScene(), result, prog);
    return *result;
  }

  template<typename T>
  void addMaterialToScene(KawaiiScene *scene, const QString &materialName, const QString &shaderName)
  {
    if(!scene)
      throw std::invalid_argument("ResourcePack::addMaterialToScene: scene not found");
    if(Q_UNLIKELY(scene->parent() != this))
      throw std::invalid_argument("ResourcePack::addMaterialToScene: scene not assigned with resource pack");

    auto *material = getMaterial(materialName);
    if(!material)
      throw std::invalid_argument("ResourcePack::addMaterialToScene: material not found");

    T *prog = getShaderProg<T>(getShader(shaderName), shaderName);
    setProgramToMaterial(scene, material, prog);
  }

  template<typename T>
  void addMaterialToScene(const QString &secondarySceneName, const QString &materialName, const ShaderEffectSrc &shader)
  {
    addMaterialToScene<T>(getScene(secondarySceneName), materialName, shader);
  }

  KawaiiFramebuffer* createFbo(const QString &fboName, const QString &colorTexture, KawaiiCamera *cam);
  KawaiiFramebuffer* createFbo(const QString &fboName, KawaiiTexture *colorTexture, KawaiiRenderpass *renderpass);
  KawaiiEnvMap* createEnvMap(const QString &envMapName, const QString &colorTexture, const glm::vec3 &pos);

  inline QThreadPool &getAssetLoadPool()
  { return assetLoadPool; }

  void clear();

  const QByteArray &serialize();
  void reset(const QByteArray &serializedBytes);

  void updateSerializedBytes();

  KawaiiScene& getShadowrender_scene() const;
  bool hasShadowrender_scene() const;
  void setShadowrender_scene(std::unique_ptr<KawaiiScene> &&value);

  void setMainRenderpass(KawaiiRenderpass *renderpass);
  KawaiiRenderpass *getMainRenderpass() const;

  template<typename FuncT>
  void forall(FuncT &&func) const
  {
    QReadLocker l(resourcesM.get());
    invokeForallInHash(func, textures);
    invokeForallInHash(func, cubemaps);
    invokeForallInHash(func, models);
    invokeForallInHash(func, materials);
    invokeForallInHash(func, sounds);
    invokeForallInHash(func, fbo);
    invokeForallInHash(func, envMaps);
    invokeForallInHash(func, scenes);
    invokeForObj(func, primaryScene);
    invokeForallInHash(func, cameras);
    invokeForObj(func, mainRp);
  }

  // QObject interface
protected:
  void childEvent(QChildEvent *event) override;

  // TreeNode interface
protected:
  void writeBinary(sib_utils::memento::Memento::DataMutator &mem) const override;
  void writeText(sib_utils::memento::Memento::DataMutator &mem) const override;
  void read(sib_utils::memento::Memento::DataReader &mem) override;



  //IMPLEMENT
private:
  class MaterialReplicator;
  class ChildObserver;
  struct Gamma {
    float exposure;
    float gamma;
    float brightness;
    float contrast;
  };

  // A chunk of the mesh instances for a model
  struct MeshInstancesChunk {
    QList<KawaiiMeshInstance*> meshInstances;
    QList<ModelInstance*> modelInstances;
  };

  QHash<QString, KawaiiImage*> textures;
  QHash<QString, KawaiiCubemap*> cubemaps;
  QHash<QString, KawaiiModel3D*> models;
  QHash<QString, KawaiiMaterial*> materials;
  QHash<QString, Sound*> sounds;
  QHash<QString, ShaderEffectSrc> shaders;
  QHash<QString, KawaiiFramebuffer*> fbo;
  QHash<QString, KawaiiEnvMap*> envMaps;

  QHash<QString, KawaiiScene*> scenes;
  KawaiiScene *primaryScene;
  std::unique_ptr<KawaiiScene> shadowrender_scene;

  QHash<QString, KawaiiCamera*> cameras;

  KawaiiGpuBuf *gamma_buf;
  KawaiiGpuBuf *objectLocations_buf;
  std::unique_ptr<QReadWriteLock> resourcesM;
  std::unique_ptr<QRecursiveMutex> meshInstancesM;

  std::unordered_map<QObject*, QMetaObject::Connection> onChildRenamedConnections;

  std::unordered_map<KawaiiModel3D const*, QList<KawaiiMeshInstance*>> virginMeshInstances;

  using PairHash = sib_utils::PairHash<KawaiiModel3D*, KawaiiMaterial*>;
  std::unordered_map<std::pair<KawaiiModel3D*, KawaiiMaterial*>, QList<MeshInstancesChunk>, PairHash> meshInstances;

  QList<ModelInstance*> allModelInstances;

  QThreadPool assetLoadPool;

  QByteArray serialized;

  std::unique_ptr<ChildObserver> childObserver;

  KawaiiRenderpass *mainRp;


  void onChildAdded(QObject *child, const QString &childName);
  void onChildRemoved(QObject *child);

  void checkModel(KawaiiModel3D *model);
  void checkModels();

  template<typename T>
  static void removeFrom(QHash<QString, T*> &hash, T *el)
  {
    for(auto i = hash.begin(); i != hash.end(); )
      {
        if(i.value() == el)
          i = hash.erase(i);
        else
          ++i;
      }
  }

  template<typename T>
  static void onElementRenamed(QHash<QString, T*> &hash, QObject *el, const QString &newName)
  {
    T *t_el = static_cast<T*>(el);

    removeFrom(hash, t_el);
    hash[newName] = t_el;
  }

  KawaiiProgram* createShadowRenderProgram(KawaiiProgram *orig) const;

  template<typename T>
  T* getShaderProg(const ShaderEffectSrc &shader, const QString &preferredProgName)
  {
    QStringList shaderData({shader.vertexSrc, shader.fragmentSrc, shader.illuminationName});
    for(auto i = shader.shaderParams.cbegin(); i!= shader.shaderParams.cend(); ++i)
      shaderData << i.key() << i.value();

    const QVariant shaderDataVariant = QVariant(shaderData);
    const QVariant cullModeVariant = QVariant::fromValue(static_cast<quint8>(shader.cullMode));

    for(auto *i: getPrimaryScene()->findChildren<T*>(QString(), Qt::FindDirectChildrenOnly))
      if(i->property("cull_mode") == cullModeVariant &&
         shader.shadowModeMatches(i->property("suppress_shadows").toBool()) &&
         i->property("shader_data") == shaderDataVariant)
        {
          return i;
        }
    auto prog = getPrimaryScene()->createChild<T>(shader);
    prog->setObjectName(preferredProgName);
    prog->setProperty("shader_data", shaderDataVariant);
    prog->setProperty("cull_mode", cullModeVariant);
    if(shader.shadowMode != ShaderEffectSrc::ShadowMode::Default)
      prog->setProperty("suppress_shadows", (shader.shadowMode == ShaderEffectSrc::ShadowMode::Suppress));
    return prog;
  }

  void setProgramToMaterial(KawaiiScene *sc, KawaiiMaterial *material, KawaiiProgram *prog);

  template<typename FuncT, typename T>
  static void invokeForallInHash(FuncT &&func, const QHash<QString, T*> &h)
  {
    if constexpr(std::is_invocable_v<FuncT, const QString&, T*>)
    {
      for(auto i = h.begin(); i != h.end(); ++i)
        std::invoke(std::forward<FuncT>(func), i.key(), i.value());
    }
  }

  template<typename FuncT, typename T>
  static void invokeForObj(FuncT &&func, T *object)
  {
    if constexpr(std::is_invocable_v<FuncT, const QString&, T*>)
    {
      std::invoke(std::forward<FuncT>(func), object->objectName(), object);
    }
  }
};

#endif // RESOURCEPACK_HPP
