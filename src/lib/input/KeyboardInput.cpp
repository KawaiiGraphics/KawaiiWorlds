#include "KeyboardInput.hpp"
#include "World.hpp"

#include <QKeyEvent>
#include <iostream>

KeyboardInput::KeyboardInput()
{
  onTick = std::bind(&KeyboardInput::tick, this, std::placeholders::_1);
}

KeyboardInput::~KeyboardInput()
{
  auto _eventFilters = std::move(eventFilters);
  for(auto *i: _eventFilters)
    delete i;
}

void KeyboardInput::bindKey(int keyCode, KeyBinding &&binding)
{
  if(binding.onTick || binding.onPressed || binding.onReleased)
    {
      auto el = codeKeyBindings.find(keyCode);
      if(el == codeKeyBindings.end())
        el = codeKeyBindings.insert(keyCode, {
                                      .onTick = std::move(binding.onTick),
                                      .onPressed = std::move(binding.onPressed),
                                      .onReleased = std::move(binding.onReleased),
                                      .active = activeKeys.end()
                                    });
      else {
          el->onTick = std::move(binding.onTick);
          el->onPressed = std::move(binding.onPressed);
          el->onReleased = std::move(binding.onReleased);

          if(el->active != activeKeys.end())
            *el->active = el->onTick;
        }
    } else
    {
      auto el = codeKeyBindings.find(keyCode);
      if(el != codeKeyBindings.end())
        {
          if(el->active != activeKeys.end())
            activeKeys.erase(el->active);
          codeKeyBindings.erase(el);
        }
    }
}

void KeyboardInput::bindKey(const QString &keyText, KeyBinding &&binding)
{
  if(binding.onTick || binding.onPressed || binding.onReleased)
    {
      auto el = textKeyBindings.find(keyText);
      if(el == textKeyBindings.end())
        el = textKeyBindings.insert(keyText, {
                                      .onTick = std::move(binding.onTick),
                                      .onPressed = std::move(binding.onPressed),
                                      .onReleased = std::move(binding.onReleased),
                                      .active = activeKeys.end()
                                    });
      else {
          el->onTick = std::move(binding.onTick);
          el->onPressed = std::move(binding.onPressed);
          el->onReleased = std::move(binding.onReleased);

          if(el->active != activeKeys.end())
            *el->active = el->onTick;
        }
    } else
    {
      auto el = textKeyBindings.find(keyText);
      if(el != textKeyBindings.end())
        {
          if(el->active != activeKeys.end())
            activeKeys.erase(el->active);
          textKeyBindings.erase(el);
        }
    }
}

void KeyboardInput::install(World &world, KawaiiSurface &sfc)
{
  class EventFilter: public QObject
  {
  public:
    KeyboardInput &obj;
    QWindow &wnd;
    World &world;

    EventFilter(KeyboardInput &obj, World &world, QWindow &wnd):
      obj(obj),
      wnd(wnd),
      world(world)
    {
      connect(&wnd, &QObject::destroyed, this, &QObject::deleteLater);
    }

    ~EventFilter()
    {
      if(auto el = std::find(obj.eventFilters.begin(), obj.eventFilters.end(), this);
         el != obj.eventFilters.end())
        obj.eventFilters.erase(el);
    }

    // QObject interface
  public:
    bool eventFilter([[maybe_unused]] QObject *watched, QEvent *event) override
    {
      if(watched == &wnd)
        {
          if(obj.active)
            {
              auto keyEvent = static_cast<QKeyEvent*>(event);
              switch(event->type())
                {
                case QEvent::KeyPress:
                  if(!keyEvent->isAutoRepeat())
                    {
                      obj.addKey(keyEvent->key());
                      obj.addKey(keyEvent->text());
                    }
                  break;

                case QEvent::KeyRelease:
                  if(!keyEvent->isAutoRepeat())
                    {
                      obj.delKey(keyEvent->key());
                      obj.delKey(keyEvent->text());
                    }
                  break;

                default: break;
                }
            }
        }

      return false;
    }
  };

  lastAgeSec = world.getAgeSeconds();

  auto *filter = new EventFilter(*this, world, sfc.getWindow());
  eventFilters.push_back(filter);
  sfc.getWindow().installEventFilter(filter);
  world.installEventFilter(filter);
}

void KeyboardInput::pushMessages(std::vector<QString> &&msg)
{
  static const QLatin1String addCodeLiteral("add_code:");
  static const QLatin1String addTextLiteral("add_text:");
  static const QLatin1String delCodeLiteral("del_code:");
  static const QLatin1String delTextLiteral("del_text:");

  for(const QString &str: msg)
    {
      if(str.startsWith(addCodeLiteral))
        {
          int code = QStringView(str.constData() + addCodeLiteral.size(),
                                 str.size() - addCodeLiteral.size()).toInt();
          addKeyEvent(code);
        } else if(str.startsWith(addTextLiteral))
        {
          const auto text = QStringView(str.constData() + addTextLiteral.size(),
                                        str.size() - addTextLiteral.size());
          addKeyEvent(text.toString());
        } else if(str.startsWith(delCodeLiteral))
        {
          int code = QStringView(str.constData() + delCodeLiteral.size(),
                                 str.size() - delCodeLiteral.size()).toInt();
          delKeyEvent(code);
        } else if(str.startsWith(delTextLiteral))
        {
          const auto text = QStringView(str.constData() + delTextLiteral.size(),
                                        str.size() - delTextLiteral.size());
          delKeyEvent(text.toString());
        } else
        std::cerr << "KeyboardInput: Unknown event " << str.toStdString() << std::endl;
    }
}

void KeyboardInput::addKey(int key)
{
  if(addKeyEvent(key) && messageLogging)
    {
      QMutexLocker l(&messagesMutex);
      messages.push_back(QStringLiteral("add_code:%1").arg(key));
    }
}

bool KeyboardInput::addKeyEvent(int key)
{
  auto el = codeKeyBindings.find(key);
  if(el != codeKeyBindings.end())
    {
      if(el->active == activeKeys.end())
        {
          el->active = activeKeys.insert(activeKeys.end(), el->onTick);
          if(el->onPressed)
            el->onPressed();
          return true;
        }
    }
  return false;
}

void KeyboardInput::addKey(const QString &text)
{
  if(addKeyEvent(text) && messageLogging)
    {
      QMutexLocker l(&messagesMutex);
      messages.push_back(QStringLiteral("add_text:%1").arg(text));
    }
}

bool KeyboardInput::addKeyEvent(const QString &text)
{
  auto el = textKeyBindings.find(text);
  if(el != textKeyBindings.end())
    {
      if(el->active == activeKeys.end())
        {
          el->active = activeKeys.insert(activeKeys.end(), el->onTick);
          if(el->onPressed)
            el->onPressed();
          return true;
        }
    }
  return false;
}

void KeyboardInput::delKey(int key)
{
  if(delKeyEvent(key) && messageLogging)
    {
      QMutexLocker l(&messagesMutex);
      messages.push_back(QStringLiteral("del_code:%1").arg(key));
    }
}

bool KeyboardInput::delKeyEvent(int key)
{
  auto el = codeKeyBindings.find(key);
  if(el != codeKeyBindings.end())
    {
      if(el->active != activeKeys.end())
        {
          activeKeys.erase(el->active);
          el->active = activeKeys.end();
          if(el->onReleased)
            el->onReleased();
          return true;
        }
    }
  return false;
}

void KeyboardInput::delKey(const QString &text)
{
  if(delKeyEvent(text) && messageLogging)
    {
      QMutexLocker l(&messagesMutex);
      messages.push_back(QStringLiteral("del_text:%1").arg(text));
    }
}

bool KeyboardInput::delKeyEvent(const QString &text)
{
  auto el = textKeyBindings.find(text);
  if(el != textKeyBindings.end())
    {
      if(el->active != activeKeys.end())
        {
          activeKeys.erase(el->active);
          el->active = activeKeys.end();
          if(el->onReleased)
            el->onReleased();
          return true;
        }
    }
  return false;
}

void KeyboardInput::tick(float sec_elapsed)
{
  for(const auto &func: activeKeys)
    if(func)
      func(sec_elapsed);
  if(postTick)
    postTick(sec_elapsed);
}
