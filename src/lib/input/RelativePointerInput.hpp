#ifndef RELATIVEPOINTERINPUT_HPP
#define RELATIVEPOINTERINPUT_HPP

#include "Input.hpp"
#include "KawaiiWorlds_global.hpp"
#include <functional>
#include <QObject>
#include <QPoint>

class KAWAIIWORLDS_SHARED_EXPORT RelativePointerInput: public Input
{
public:
  std::function<void(const QPoint &)> func;

  RelativePointerInput();
  ~RelativePointerInput();

  // Input interface
public:
  void install(World &world, KawaiiSurface &sfc) override;
  void pushMessages(std::vector<QString> &&msg) override;



  //IMPLEMENT
private:
  std::vector<QObject*> eventFilters;
  std::list<std::function<void()>> onDestroyedFuncs;

  void event(const QPoint &delta);
};

#endif // RELATIVEPOINTERINPUT_HPP
