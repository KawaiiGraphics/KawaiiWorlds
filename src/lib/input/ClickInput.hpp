#ifndef CLICKINPUT_HPP
#define CLICKINPUT_HPP

#include "Input.hpp"
#include "KawaiiWorlds_global.hpp"

#include <QObject>

class KAWAIIWORLDS_SHARED_EXPORT ClickInput: public Input
{
public:
  ClickInput();
  ~ClickInput();

  // Input interface
public:
  void install(World &world, KawaiiSurface &sfc) override;
  void pushMessages(std::vector<QString> &&msg) override;



  //IMPLEMENT
private:
  std::vector<QObject*> eventFilters;
};

#endif // CLICKINPUT_HPP
