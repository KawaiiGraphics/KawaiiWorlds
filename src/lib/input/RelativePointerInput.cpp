#include "RelativePointerInput.hpp"

#include <Kawaii3D/Surfaces/KawaiiSurface.hpp>
#include <QMouseEvent>

#if defined(Q_OS_LINUX) && !defined(Q_OS_ANDROID)
# include <qpa/qplatformwindow.h>
# include <qpa/qplatformnativeinterface.h>
# include <wayland-client.h>
# include <wl_relative-pointer-unstable-v1.h>
# include <wl_pointer-constraints-unstable-v1.h>
# include <QGuiApplication>
# include <cstring>
#endif


RelativePointerInput::RelativePointerInput()
{
}

RelativePointerInput::~RelativePointerInput()
{
  auto _eventFilters = std::move(eventFilters);
  for(auto *i: _eventFilters)
    delete i;

  for(const auto &func: onDestroyedFuncs)
    func();
}

void RelativePointerInput::install([[maybe_unused]] World &world, KawaiiSurface &sfc)
{
#if defined(Q_OS_LINUX) && !defined(Q_OS_ANDROID)
  if(!qgetenv("WAYLAND_DISPLAY").isEmpty())
    {
      class WaylandGlobals
      {
        struct ListenerInfo {
          RelativePointerInput *obj;
          decltype(RelativePointerInput::onDestroyedFuncs)::iterator onDestroyedFuncsIter;
          QWindow *wnd;
          zwp_confined_pointer_v1 *confinedPointer;
          wl_region *confinedInRegion;
          int x;
          int y;
        };
        std::list<ListenerInfo> listeners;

        wl_compositor *compositor;
        wl_display *display;
        wl_seat *seat;
        wl_pointer *pointer;
        zwp_relative_pointer_manager_v1 *relativePointerManager;
        zwp_relative_pointer_v1 *relativePointer;
        zwp_pointer_constraints_v1 *pointerConstraints;

        uint32_t compositorName;
        uint32_t relativePointerManagerName;
        uint32_t pointerConstraintsName;

        static void way_relative_motion(void *data,
                                        [[maybe_unused]] zwp_relative_pointer_v1 *zwp_relative_pointer_v1,
                                        [[maybe_unused]] uint32_t utime_hi,
                                        [[maybe_unused]] uint32_t utime_lo,
                                        [[maybe_unused]] wl_fixed_t dx,
                                        [[maybe_unused]] wl_fixed_t dy,
                                        wl_fixed_t dx_unaccel,
                                        wl_fixed_t dy_unaccel)
        {
          auto obj = static_cast<WaylandGlobals*>(data);
          for(auto &listener: obj->listeners)
            {
              if(listener.obj->active)
                {
                  QCursor cursor;
                  cursor.setShape(Qt::CursorShape::BlankCursor);
                  listener.wnd->setCursor(cursor);
                  listener.obj->event(QPoint(dx_unaccel / 128, dy_unaccel / 128));
                  obj->try_confine_pointer(listener);
                } else
                {
                  if(listener.confinedPointer)
                    {
                      zwp_confined_pointer_v1_destroy(listener.confinedPointer);
                      listener.confinedPointer = nullptr;
                      listener.x = listener.y = -1;
                    }
                  listener.wnd->unsetCursor();
                }
            }
        }

        void initRelativePointer()
        {
          if(!relativePointer && relativePointerManager && pointer)
            {
              relativePointer = zwp_relative_pointer_manager_v1_get_relative_pointer(relativePointerManager, pointer);

              if(relativePointer)
                {
                  static const zwp_relative_pointer_v1_listener listener = {
                    .relative_motion = way_relative_motion,
                  };

                  zwp_relative_pointer_v1_add_listener(relativePointer, &listener, this);
                }
            }
        }

        static void way_registry_handle_global(void *data, wl_registry *registry, uint32_t name, const char *interface, uint32_t version)
        {
//          printf("global interface \'%s\', name %d, version %d\n", interface, name, version);

          auto obj = static_cast<WaylandGlobals*>(data);
          if(!obj->relativePointerManager && std::strcmp(interface, "zwp_relative_pointer_manager_v1") == 0)
            {
              obj->relativePointerManagerName = name;
              obj->relativePointerManager = static_cast<zwp_relative_pointer_manager_v1*>(wl_registry_bind(registry, name, &zwp_relative_pointer_manager_v1_interface, version));
              obj->initRelativePointer();
            }
          else if(!obj->pointerConstraints && std::strcmp(interface, "zwp_pointer_constraints_v1") == 0)
            {
              obj->pointerConstraints = static_cast<zwp_pointer_constraints_v1*>(wl_registry_bind(registry, name, &zwp_pointer_constraints_v1_interface, version));
              obj->pointerConstraintsName = name;
            }
          else if(!obj->compositor && std::strcmp(interface, "wl_compositor") == 0)
            {
              obj->compositor = static_cast<wl_compositor*>(wl_registry_bind(registry, name, &wl_compositor_interface, version));
              obj->compositorName = name;
            }

          for(auto &listener: obj->listeners)
            if(listener.obj->active)
              obj->try_confine_pointer(listener);
        }

        static void way_registry_handle_global_remove(void *data, wl_registry*, uint32_t name)
        {
          auto obj = static_cast<WaylandGlobals*>(data);

          if(obj->relativePointerManager && obj->relativePointerManagerName == name)
            obj->relativePointerManager = nullptr;
          else if(obj->pointerConstraints && obj->pointerConstraintsName == name)
            obj->pointerConstraints = nullptr;
          else if(obj->compositor && obj->compositorName == name)
            obj->pointerConstraints = nullptr;
        }

        static void onPointerConfined(void*, zwp_confined_pointer_v1*) {}

        static void onPointerUnconfined(void *data, zwp_confined_pointer_v1*)
        {
          auto listener = static_cast<ListenerInfo*>(data);
          if(Q_UNLIKELY(!listener)) return;
          listener->confinedPointer = nullptr;
          listener->x = listener->y = -1;
        }

      public:
        WaylandGlobals():
          compositor(nullptr),
          display(nullptr),
          seat(nullptr),
          pointer(nullptr),
          relativePointerManager(nullptr),
          relativePointer(nullptr),
          pointerConstraints(nullptr),
          compositorName(0),
          relativePointerManagerName(0),
          pointerConstraintsName(0)
        {
          auto *native = QGuiApplication::platformNativeInterface();
          display = static_cast<wl_display*>(native->nativeResourceForWindow("display", nullptr));
          seat = static_cast<wl_seat*>(native->nativeResourceForIntegration("wl_seat"));
          if(seat)
            pointer = wl_seat_get_pointer(seat);
          wl_registry *registry = wl_display_get_registry(display);

          static const wl_registry_listener registry_listener = {
            .global = way_registry_handle_global,
            .global_remove = way_registry_handle_global_remove,
          };
          wl_registry_add_listener(registry, &registry_listener, this);
        }

        ~WaylandGlobals()
        {
          for(const auto &listener: listeners)
            listener.obj->onDestroyedFuncs.erase(listener.onDestroyedFuncsIter);
        }

        void add_listener(RelativePointerInput *listener, QWindow *wnd)
        {
          auto el = listeners.insert(listeners.end(), { .obj = listener,
                                                        .wnd = wnd,
                                                        .confinedPointer = nullptr,
                                                        .confinedInRegion = nullptr,
                                                        .x = -1, .y = -1
                                     });
          auto onListenerDestroyed = [el, this] {
              if(el->confinedPointer)
                zwp_confined_pointer_v1_destroy(el->confinedPointer);
              if(el->confinedInRegion)
                wl_region_destroy(el->confinedInRegion);
              listeners.erase(el);
            };

          el->onDestroyedFuncsIter = listener->onDestroyedFuncs.insert(listener->onDestroyedFuncs.end(), onListenerDestroyed);
          try_confine_pointer(*el);
        }

        void try_confine_pointer(ListenerInfo &listener)
        {
          int x = listener.wnd->width() / 2,
              y = listener.wnd->height() / 2;
          if(x != listener.x || y != listener.y)
            {
              if(!listener.confinedInRegion)
                listener.confinedInRegion = compositor? wl_compositor_create_region(compositor): nullptr;
              else
                wl_region_subtract(listener.confinedInRegion,
                                   listener.x, listener.y, 1, 1);
              if(Q_LIKELY(listener.confinedInRegion))
                {
                  wl_region_add(listener.confinedInRegion, x, y, 1, 1);
                  listener.x = x;
                  listener.y = y;
                }
            }

          if(listener.confinedPointer)
            {
              if(listener.confinedInRegion)
                zwp_confined_pointer_v1_set_region(listener.confinedPointer, listener.confinedInRegion);
              return;
            }

          auto *native = QGuiApplication::platformNativeInterface();
          wl_surface *wlSfc = static_cast<wl_surface*>(native->nativeResourceForWindow("surface", listener.wnd));
          if(wlSfc && pointer && pointerConstraints)
            {
              listener.confinedPointer = zwp_pointer_constraints_v1_confine_pointer(pointerConstraints, wlSfc, pointer, listener.confinedInRegion, ZWP_POINTER_CONSTRAINTS_V1_LIFETIME_ONESHOT);
              if(Q_LIKELY(listener.confinedPointer))
                {
                  static const zwp_confined_pointer_v1_listener unlockListener = {
                    .confined = onPointerConfined,
                    .unconfined = onPointerUnconfined
                  };
                  zwp_confined_pointer_v1_add_listener(listener.confinedPointer, &unlockListener, &listener);
                }
            }
        }
      };

      static WaylandGlobals waylandGlob;
      waylandGlob.add_listener(this, &sfc.getWindow());
      return;
    }
#endif

  class EventFilter: public QObject
  {
  public:
    RelativePointerInput &obj;
    QWindow &wnd;
    bool mouseCatched;

    void markMouseDetached()
    { mouseCatched = false; deleteLater(); }

    EventFilter(RelativePointerInput &obj, QWindow &wnd):
      obj(obj),
      wnd(wnd),
      mouseCatched(false)
    {
      connect(&wnd, &QObject::destroyed, this, &EventFilter::markMouseDetached);
    }

    ~EventFilter()
    {
      if(mouseCatched)
        wnd.unsetCursor();
      if(auto el = std::find(obj.eventFilters.begin(), obj.eventFilters.end(), this);
         el != obj.eventFilters.end())
        obj.eventFilters.erase(el);
    }

    // QObject interface
  public:
    bool eventFilter([[maybe_unused]] QObject *watched, QEvent *event) override
    {
      if(event->type() != QEvent::MouseMove) return false;

      if(!obj.active)
        {
          wnd.unsetCursor();
          return false;
        }

      Q_ASSERT(watched == &wnd);

      QPoint wndCenter(wnd.x() + wnd.size().width() / 2, wnd.y() + wnd.size().height() / 2);
      if(mouseCatched)
        {
          const QPoint delta = wnd.cursor().pos() - wndCenter;
          if(!delta.isNull())
            obj.event(delta);
        } else
        mouseCatched = true;


      QCursor cursor;
      cursor.setShape(Qt::CursorShape::BlankCursor);
      cursor.setPos(wndCenter);
      wnd.setCursor(cursor);

      return false;
    }
  };

  auto *filter = new EventFilter(*this, sfc.getWindow());
  eventFilters.push_back(filter);
  sfc.getWindow().installEventFilter(filter);
}

void RelativePointerInput::pushMessages(std::vector<QString> &&msg)
{
  if(func)
    for(const auto &str: msg)
      {
        const auto terms = QStringView(str).split(';');
        if(terms.size() > 1)
          func(QPoint(terms[0].toInt(), terms[1].toInt()));
      }
}

void RelativePointerInput::event(const QPoint &delta)
{
  if(func)
    func(delta);
  if(messageLogging)
    {
      QMutexLocker l(&messagesMutex);
      messages.push_back(QStringLiteral("%1;%2").arg(delta.x()).arg(delta.y()));
    }
}
