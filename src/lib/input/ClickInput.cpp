#include "ClickInput.hpp"
#include "World.hpp"

#include <QMouseEvent>

ClickInput::ClickInput()
{

}

ClickInput::~ClickInput()
{
  auto _eventFilters = std::move(eventFilters);
  for(auto *i: _eventFilters)
    delete i;
}

void ClickInput::install(World &world, KawaiiSurface &sfc)
{
  class EventFilter: public QObject
  {
    ClickInput &obj;
    World &world;
    KawaiiSurface &sfc;
  public:
    explicit EventFilter(ClickInput &obj, World &world, KawaiiSurface &sfc):
      obj(obj),
      world(world),
      sfc(sfc)
    {
      connect(&sfc.getWindow(), &QObject::destroyed, this, &QObject::deleteLater);
    }

    ~EventFilter()
    {
      if(auto el = std::find(obj.eventFilters.begin(), obj.eventFilters.end(), this);
         el != obj.eventFilters.end())
        obj.eventFilters.erase(el);
    }

    KawaiiSurface::Tile* findTile(const QPointF &windowPos)
    {
      for(size_t i = 0; i < sfc.tileCount(); ++i)
        if(QRectF(sfc.getTile(i).getResultRect()).contains(windowPos))
          return &sfc.getTile(i);
      return nullptr;
    }

    QPointF getScreenSpacePoint(const QPointF &windowPoint, const QRectF &resultRect)
    {
      return QPointF((windowPoint.x() - resultRect.x()) / resultRect.width(),
                     (windowPoint.y() - resultRect.y()) / resultRect.height());
    }

    bool eventFilter([[maybe_unused]] QObject *watched, QEvent *event) override
    {
      if(!obj.active) return false;

      Q_ASSERT(watched == &sfc.getWindow());

      QMouseEvent *mouseEvent = static_cast<QMouseEvent*>(event);
      KawaiiSurface::Tile *tile;

      switch(event->type())
        {
        case QEvent::MouseButtonPress:
          tile = findTile(mouseEvent->scenePosition());
          if(tile)
            world.onPointerPress(getScreenSpacePoint(mouseEvent->scenePosition(), tile->getResultRect()));
          break;

        case QEvent::MouseButtonRelease:
          tile = findTile(mouseEvent->scenePosition());
          if(tile)
            world.onPointerRelease(getScreenSpacePoint(mouseEvent->scenePosition(), tile->getResultRect()));
          break;

        case QEvent::MouseButtonDblClick:
          tile = findTile(mouseEvent->scenePosition());
          if(tile)
            world.onPointerDoubleClick(getScreenSpacePoint(mouseEvent->scenePosition(), tile->getResultRect()));
          break;

        default: break;
        }
      return false;
    }
  };

  auto *filter = new EventFilter(*this, world, sfc);
  eventFilters.push_back(filter);
  sfc.getWindow().installEventFilter(filter);
}

void ClickInput::pushMessages([[maybe_unused]] std::vector<QString> &&msg)
{
  // ignore
}
