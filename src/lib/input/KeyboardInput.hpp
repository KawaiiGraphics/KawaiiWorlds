#ifndef KEYBOARDINPUT_HPP
#define KEYBOARDINPUT_HPP

#include "Input.hpp"
#include "KawaiiWorlds_global.hpp"

#include <functional>
#include <QObject>
#include <QHash>

class KAWAIIWORLDS_SHARED_EXPORT KeyboardInput: public Input
{
public:
  std::function<void(float)> postTick;

  struct KeyBinding
  {
    std::function<void(float)> onTick;
    std::function<void()> onPressed;
    std::function<void()> onReleased;
  };

  KeyboardInput();
  ~KeyboardInput();

  void bindKey(int keyCode, KeyBinding &&binding);
  void bindKey(const QString &keyText, KeyBinding &&binding);

  // Input interface
public:
  void install(World &world, KawaiiSurface &sfc) override;
  void pushMessages(std::vector<QString> &&msg) override;



  //IMPLEMENT
private:
  std::vector<QObject*> eventFilters;

  struct KeyBinding_implement
  {
    std::function<void(float)> onTick;
    std::function<void()> onPressed;
    std::function<void()> onReleased;
    std::list<std::function<void(float)>>::iterator active;
  };

  QHash<QString, KeyBinding_implement> textKeyBindings;
  QHash<int, KeyBinding_implement> codeKeyBindings;

  std::list<std::function<void(float)>> activeKeys;

  float lastAgeSec;

  void addKey(int key);
  bool addKeyEvent(int key);

  void addKey(const QString &text);
  bool addKeyEvent(const QString &text);

  void delKey(int key);
  bool delKeyEvent(int key);

  void delKey(const QString &text);
  bool delKeyEvent(const QString &text);

  void tick(float sec_elapsed);
};

#endif // KEYBOARDINPUT_HPP
