#ifndef INPUT_HPP
#define INPUT_HPP

#include <functional>
#include <QString>
#include <QMutex>
#include <vector>

class World;
class KawaiiSurface;

class Input
{
public:
  std::function<void(float sec_elapsed)> onTick;
  bool messageLogging;
  bool active;

  inline Input():
    messageLogging(false),
    active(true)
  {}

  virtual ~Input() = default;

  virtual void install(World &world, KawaiiSurface &sfc) = 0;
  virtual void pushMessages(std::vector<QString> &&msg) = 0;

  inline std::vector<QString> pullMessages() {
    std::vector<QString> result;
    {
      QMutexLocker l(&messagesMutex);
      result = std::move(messages);
    }
    return result;
  }

protected:
  std::vector<QString> messages;
  QMutex messagesMutex;
};

#endif // INPUT_HPP
