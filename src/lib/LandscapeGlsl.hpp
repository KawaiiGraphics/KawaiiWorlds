#ifndef LANDSCAPEGLSL_HPP
#define LANDSCAPEGLSL_HPP

#include <Kawaii3D/Shaders/KawaiiProgram.hpp>
#include <Kawaii3D/Shaders/KawaiiScene.hpp>
#include "ShaderEffectSrc.hpp"

class LandscapeGlsl : public KawaiiProgram
{
  Q_OBJECT
public:
  LandscapeGlsl(const ShaderEffectSrc &materialEffectSrc);
};

#endif // LANDSCAPEGLSL_HPP
