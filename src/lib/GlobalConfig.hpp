#ifndef GLOBALCONFIG_HPP
#define GLOBALCONFIG_HPP

#include <cinttypes>
#include <cstddef>

namespace global_config
{
  extern const uint16_t maxPackInstances;
  extern const size_t instancePackSz;
}

#endif // GLOBALCONFIG_HPP
