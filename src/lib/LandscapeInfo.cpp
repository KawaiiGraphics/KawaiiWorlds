#include "LandscapeInfo.hpp"
#include <QDataStream>

KAWAIIWORLDS_SHARED_EXPORT QDataStream &operator<<(QDataStream &st, const LandscapeInfo &obj)
{
  return st << obj.id << obj.size << obj.pos
            << obj.material << obj.heightMapTexture
            << obj.metadata << obj.chunkSize << obj.step
            << obj.restitution
            << obj.friction << obj.frictionRolling << obj.frictionSpinning
            << obj.createCollisionShape;
}

KAWAIIWORLDS_SHARED_EXPORT QDataStream &operator>>(QDataStream &st, LandscapeInfo &obj)
{
  return st >> obj.id >> obj.size >> obj.pos
            >> obj.material >> obj.heightMapTexture
            >> obj.metadata >> obj.chunkSize >> obj.step
            >> obj.restitution
            >> obj.friction >> obj.frictionRolling >> obj.frictionSpinning
            >> obj.createCollisionShape;
}
