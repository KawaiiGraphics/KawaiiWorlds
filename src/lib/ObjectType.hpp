#ifndef OBJECTTYPE_HPP
#define OBJECTTYPE_HPP

#include <cinttypes>

enum class ObjectType: uint8_t
{
  None   = 0, //No physics at all
  Spirit = 1, //Affected by gravity, viscosity and world barrier, but not by collisions
  Wall   = 2, //Can not be moved by collisions
  Solid  = 3,
  Liquid = 4,
  Gas    = 5
};

#endif // OBJECTTYPE_HPP
