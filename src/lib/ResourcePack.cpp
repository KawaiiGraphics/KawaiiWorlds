#include "ResourcePack.hpp"
#include "GlobalConfig.hpp"
#include <Kawaii3D/KawaiiBufBinding.hpp>
#include <Kawaii3D/AssetZygote/KawaiiMaterialReplicator.hpp>
#include <Kawaii3D/AssetZygote/KawaiiSubsceneZygote.hpp>
#include <Kawaii3D/Renderpass/KawaiiSceneLayer.hpp>
#include <sib_utils/Memento/BlobLoader.hpp>
#include <sib_utils/Memento/BlobSaver.hpp>
#include <sib_utils/ioReadAll.hpp>
#include <QtConcurrent>
#include <QChildEvent>
#include <cstring>

KAWAII_COMMON_IMPLEMENT(ResourcePack::reg);

const uint16_t global_config::maxPackInstances = 32;
const size_t global_config::instancePackSz = sizeof(int) * global_config::maxPackInstances;

class ResourcePack::MaterialReplicator: public KawaiiMaterialReplicator
{
  ResourcePack &target;

public:
  MaterialReplicator(ResourcePack &target):
    target(target)
  {}

  // KawaiiMaterialReplicator interface
private:
  KawaiiTexture *replicateTexture(KawaiiTextureRole texRole, size_t texId) override
  {
    if(texRole != KawaiiTextureRole::Diffuse || texId != 0)
      return nullptr;

    if(auto fName = getTextureParam<QString>(KawaiiTextureParamRole::FileName); fName.has_value())
      {
        const int N = fName->size() - fName->lastIndexOf('/') - 1;
        QString texName = fName->right(N);

        auto texture = target.getTexture2D(texName);
        if(!texture)
          {
            if(QImage img; img.load(*fName))
              {
                texture = new KawaiiImage;
                texture->setObjectName(texName);
                texture->setImage(std::move(img));
                texture->moveToThread(target.thread());
                texture->updateParent(&target);
              }
          }
        return texture;
      }

    return target.getNullTexture2D();
  }

  KawaiiMaterial *replicateMaterial() override
  {
    return nullptr;
  }

  KawaiiGpuBuf *replicateModelBuffer() override
  {
    auto diffuse = getTexture(KawaiiTextureRole::Diffuse, 0);
    if(!diffuse)
      {
        if(auto v4 = getParam<QVector4D>(KawaiiRenderParamRole::ColorDiffuse); v4.has_value())
          diffuse = target.getMonochromeTexture2D(QColor(v4->x() * 255.0, v4->y() * 255.0, v4->z() * 255.0, v4->w() * 255.0) );
        else
          if(auto v3 = getParam<QVector3D>(KawaiiRenderParamRole::ColorDiffuse); v3.has_value())
            diffuse = target.getMonochromeTexture2D(QColor(v3->x() * 255.0, v3->y() * 255.0, v3->z() * 255.0) );

        if(!diffuse)
          return nullptr;
      }

    KawaiiGpuBuf *result = new KawaiiGpuBuf;
    result->bindTexture(QStringLiteral("diffuse_tex"), diffuse);
    return result;
  }
};

ResourcePack::ResourcePack():
  primaryScene(nullptr),
  resourcesM(std::make_unique<QReadWriteLock>(QReadWriteLock::Recursive)),
  meshInstancesM(std::make_unique<QRecursiveMutex>()),
  childObserver(std::make_unique<ChildObserver>(this)),
  mainRp(nullptr)
{
  createChild<KawaiiScene>();

  Gamma gammaData = {
    .exposure = 0.75,
    .gamma = 2.6,
    .brightness = 0.0,
    .contrast = 1.0
  };

  gamma_buf = createChild<KawaiiGpuBuf>(&gammaData, sizeof(gammaData));
  gamma_buf->createChild<KawaiiBufBinding>()->setBindingPoint(1);

  objectLocations_buf = createChild<KawaiiGpuBuf>(nullptr, 0);
  auto objectsLocationsBinding = objectLocations_buf->createChild<KawaiiBufBinding>();
  objectsLocationsBinding->setTarget(KawaiiBufferTarget::SSBO);
  objectsLocationsBinding->setBindingPoint(1);

  auto excludedChildren = property("ExcludedChildren").value<QList<QVariant>>();
  excludedChildren.push_back(QVariant::fromValue<QObject*>(gamma_buf));
  excludedChildren.push_back(QVariant::fromValue<QObject*>(objectLocations_buf));
  setProperty("ExcludedChildren", excludedChildren);
}

ResourcePack::~ResourcePack()
{
  for(auto &i: onChildRenamedConnections)
    disconnect(i.second);

  childObserver.reset();
//  scene.release();
}

void ResourcePack::waitTasks()
{
  assetLoadPool.waitForDone();
  QCoreApplication::sendPostedEvents();
}

float ResourcePack::getGamma() const
{
  return static_cast<Gamma*>(gamma_buf->getData())->gamma;
}

void ResourcePack::setGamma(float gamma)
{
  static_cast<Gamma*>(gamma_buf->getData())->gamma = gamma;
  emit gamma_buf->dataChanged(offsetof(Gamma, gamma), sizeof(float));
}

float ResourcePack::getExposure() const
{
  return static_cast<Gamma*>(gamma_buf->getData())->exposure;
}

void ResourcePack::setExposure(float exposure)
{
  static_cast<Gamma*>(gamma_buf->getData())->exposure = exposure;
  emit gamma_buf->dataChanged(offsetof(Gamma, exposure), sizeof(float));
}

float ResourcePack::getBrightness() const
{
  return static_cast<Gamma*>(gamma_buf->getData())->brightness;
}

void ResourcePack::setBrightness(float brightness)
{
  static_cast<Gamma*>(gamma_buf->getData())->brightness = brightness;
  emit gamma_buf->dataChanged(offsetof(Gamma, brightness), sizeof(float));
}

float ResourcePack::getContrast() const
{
  return static_cast<Gamma*>(gamma_buf->getData())->contrast;
}

void ResourcePack::setContrast(float contrast)
{
  static_cast<Gamma*>(gamma_buf->getData())->contrast = contrast;
  emit gamma_buf->dataChanged(offsetof(Gamma, contrast), sizeof(float));
}

KawaiiImage *ResourcePack::getTexture2D(const QString &name) const
{
  QReadLocker l(resourcesM.get());
  return textures.value(name, nullptr);
}

KawaiiCubemap *ResourcePack::getTextureCube(const QString &name) const
{
  QReadLocker l(resourcesM.get());
  return cubemaps.value(name, nullptr);
}

const ShaderEffectSrc &ResourcePack::getShader(const QString &name) const
{
  QReadLocker l(resourcesM.get());
  auto el = shaders.find(name);
  if(el == shaders.end())
    {
      static const ShaderEffectSrc emptyShader {};
      return emptyShader;
    } else
    return el.value();
}

KawaiiModel3D *ResourcePack::getModel(const QString &name) const
{
  QReadLocker l(resourcesM.get());
  return models.value(name, nullptr);
}

KawaiiMaterial *ResourcePack::getMaterial(const QString &name) const
{
  QReadLocker l(resourcesM.get());
  return materials.value(name, nullptr);
}

Sound *ResourcePack::getSound(const QString &name) const
{
  QReadLocker l(resourcesM.get());
  return sounds.value(name, nullptr);
}

KawaiiScene *ResourcePack::addScene(const QString &name)
{
  QWriteLocker l(resourcesM.get());
  if(scenes.contains(name))
    throw std::invalid_argument("ResourcePack::addScene: scene with this name already exists");
  auto *scene = new KawaiiScene;
  scene->setObjectName(name);
  scene->setDefaultProgram(nullptr);
  scene->updateParent(this);
  return scene;
}

KawaiiScene *ResourcePack::getScene(const QString &name) const
{
  QReadLocker l(resourcesM.get());
  return scenes.value(name, nullptr);
}

KawaiiScene *ResourcePack::getPrimaryScene() const
{
  QReadLocker l(resourcesM.get());
  return primaryScene;
}

std::pair<QFuture<bool>, KawaiiImage *> ResourcePack::loadTexture2D(const QString &assetStr, const QString &resName)
{
  auto *img = new KawaiiImage;
  img->setObjectName(resName);
  img->updateParent(this);

  auto task = img->loadAssetP(assetStr, &assetLoadPool);
  return { task, img };
}

std::pair<QFuture<bool>, KawaiiCubemap *> ResourcePack::loadCubemap(const QString &assetStr, const QString &resName)
{
  auto *cubemap = new KawaiiCubemap;
  cubemap->setObjectName(resName);
  cubemap->updateParent(this);

  auto task = cubemap->loadAssetP(assetStr, &assetLoadPool);
  return { task, cubemap };
}

std::pair<QFuture<bool>, KawaiiModel3D *> ResourcePack::loadModel(const QString &assetStr, const QString &resName)
{
  auto model = createChild<KawaiiModel3D>();
  model->setObjectName(resName);

  auto task = QtConcurrent::run(&assetLoadPool,
        [model, assetStr, this]() {
      bool loaded = model->loadAsset(assetStr);
      if(!loaded) return false;

      model->setCenter(QVector3D(0,0,0));
      QMetaObject::invokeMethod(model, [model, this] {
          if(auto subsc = model->findChild<KawaiiSubsceneZygote*>(); subsc)
            {
              MaterialReplicator r(*this);
              subsc->replicate(*this, r);
              delete subsc;
            }

          QMutexLocker l(meshInstancesM.get());
          auto &meshInstances = virginMeshInstances[model];
          model->forallMeshes([&meshInstances] (KawaiiMesh3D &mesh) {
            auto inst = mesh.findChild<KawaiiMeshInstance*>(QString(), Qt::FindDirectChildrenOnly);
            if(!inst)
              inst = mesh.createChild<KawaiiMeshInstance>();
            inst->setInstanceCount(0);
            meshInstances.push_back(inst);
          });
        });

      return true;
    });

  return { task, model };
}

QFuture<bool> ResourcePack::loadAnimation(const QString &assetStr, const QString &targetModelName)
{
  auto target = getModel(targetModelName);
  if(!target)
    throw std::invalid_argument("ResourcePack::loadAnimation: target model not exists");

  auto model = new KawaiiModel3D;
  model->setObjectName(targetModelName + "_Anim");

  auto task = QtConcurrent::run(&assetLoadPool,
        [model, assetStr, target]() {
      bool loaded = model->loadAsset(assetStr);
      if(!loaded)
        {
          delete model;
          return false;
        }

      for(auto *i: model->findChildren<KawaiiSkeletalAnimation*>())
        i->updateParent(target);
      delete model;
      return true;
    });

  return task;
}

std::unique_ptr<KawaiiSkeletalAnimation::Instance> ResourcePack::getSkeletalAnimation(const QString &modelName, QAnyStringView animationName) const
{
  auto model = getModel(modelName);
  if(!model)
    throw std::invalid_argument("ResourcePack::getAnimation: model not exists");

  return getSkeletalAnimation(model, animationName);
}

std::unique_ptr<KawaiiSkeletalAnimation::Instance> ResourcePack::getSkeletalAnimation(KawaiiModel3D *model, QAnyStringView animationName) const
{
  auto *anim = model->findChild<KawaiiSkeletalAnimation*>(animationName);
  if(!anim)
    return nullptr;

  model->getSkeleton()->stopAnimation();
  return std::make_unique<KawaiiSkeletalAnimation::Instance>(*model->getSkeleton(), *anim);
}

namespace {
  KawaiiMeshInstance* cloneInstance(KawaiiMeshInstance *i, KawaiiMaterial *material)
  {
    auto parent = static_cast<sib_utils::TreeNode*>(i->parent());
    auto instance = parent->createChild<KawaiiMeshInstance>(material);
    KawaiiGpuBuf *modelUbo = instance->createChild<KawaiiGpuBuf>(nullptr, global_config::instancePackSz);
    instance->setUniforms(modelUbo);
    instance->setInstanceCount(0);

    if(i->getUniforms())
      i->getUniforms()->forallTextureBindings([modelUbo] (const QString &bindingName, KawaiiTexture *tex) {
          modelUbo->bindTexture(bindingName, tex);
        });

    auto excludedChildren = parent->property("ExcludedChildren").value<QList<QVariant>>();
    excludedChildren.push_back(QVariant::fromValue<QObject*>(instance));
    parent->setProperty("ExcludedChildren", excludedChildren);

    return instance;
  }

  QList<KawaiiMeshInstance*> cloneInstances(const QList<KawaiiMeshInstance*> &lst, KawaiiMaterial *material)
  {
    QList<KawaiiMeshInstance*> result;
    result.reserve(lst.size());
    for(auto *i: lst)
      result.push_back(cloneInstance(i, material));
    return result;
  }
}

KawaiiImage *ResourcePack::getNullTexture2D()
{
  auto texture = getTexture2D(QStringLiteral("//NULL"));
  if(!texture)
    {
      QImage img(1,1, QImage::Format_RGBA8888);
      img.setPixel(0,0, 0);

      texture = new KawaiiImage;
      texture->setObjectName(QStringLiteral("//NULL"));
      texture->setImage(std::move(img));
      texture->updateParent(this);
    }
  return texture;
}

KawaiiImage *ResourcePack::getMonochromeTexture2D(const QColor &color)
{
  const QString texName = QStringLiteral("//COLOR_%1").arg(color.name(QColor::NameFormat::HexArgb));

  auto texture = getTexture2D(texName);
  if(!texture)
    {
      QImage img(1,1, QImage::Format_RGBA8888);
      img.setPixelColor(0,0, color);

      texture = new KawaiiImage;
      texture->setObjectName(texName);
      texture->setImage(std::move(img));
      texture->updateParent(this);
    }
  return texture;
}

KawaiiCamera *ResourcePack::getCamera(const QString &name) const
{
  return cameras.value(name, nullptr);
}

ModelInstance ResourcePack::instanceModel(KawaiiModel3D *model, KawaiiMaterial *material)
{
  QMutexLocker l(meshInstancesM.get());

  if(material->parent() != this)
    throw std::invalid_argument("ResourcePack::instanceModel: material must be direct child of the resource pack");

  auto el = meshInstances.find({model, material});
  if(el == meshInstances.end())
    el = meshInstances.insert({{model, material}, {}}).first;

  if(!el->second.empty())
    {
      for(auto &i: el->second)
        {
          if(Q_UNLIKELY(i.meshInstances.empty()))
            return ModelInstance();
          if(i.meshInstances.front()->getInstanceCount() < static_cast<size_t>(global_config::maxPackInstances - 1))
            return ModelInstance(i.meshInstances, i.modelInstances, objectLocations_buf, allModelInstances);
        }
    }

  auto virginMeshesEl = virginMeshInstances.find(model);
  if(virginMeshesEl == virginMeshInstances.end())
    {
      checkModel(model);
      virginMeshesEl = virginMeshInstances.find(model);
      Q_ASSERT(virginMeshesEl != virginMeshInstances.end());
    }

  el->second.push_back({ cloneInstances(virginMeshesEl->second, material), {} });
  return ModelInstance(el->second.back().meshInstances, el->second.back().modelInstances, objectLocations_buf, allModelInstances);
}

void ResourcePack::addShaderEffect(const QString &name, const ShaderEffectSrc &effect)
{
  QWriteLocker l(resourcesM.get());
  if(shaders.contains(name))
    throw std::invalid_argument("ResourcePack::addShaderEffect: shader effect with the given name exists");
  shaders[name] = effect;
}

void ResourcePack::loadInternalShaderEffect(InternalShaderEffect effect, ShaderEffectSrc &effectParams)
{
  QString vertexFName, fragmentFName;
  switch (effect)
    {
    case InternalShaderEffect::DiffuseTexturePbr:
      vertexFName = QStringLiteral(":/shaders/materials/diffuseTexture.vs.glsl");
      fragmentFName = QStringLiteral(":/shaders/materials/diffuseTexturePbr.fs.glsl");
      break;

    case InternalShaderEffect::DiffuseTexturePhong:
      vertexFName = QStringLiteral(":/shaders/materials/diffuseTexture.vs.glsl");
      fragmentFName = QStringLiteral(":/shaders/materials/diffuseTexturePhong.fs.glsl");
      break;

    case InternalShaderEffect::DiffuseTextureCel:
      vertexFName = QStringLiteral(":/shaders/materials/diffuseTexture.vs.glsl");
      fragmentFName = QStringLiteral(":/shaders/materials/diffuseTextureCel.fs.glsl");
      break;

    case InternalShaderEffect::Blur4x4:
      vertexFName = QString();
      fragmentFName = QStringLiteral(":/shaders/effect/blur4x4.fs.glsl");
      effectParams.cullMode = KawaiiProgram::CullMode::CullBack;
      break;

    case InternalShaderEffect::SSAO:
      vertexFName = QString();
      fragmentFName = QStringLiteral(":/shaders/effect/ssao.fs.glsl");
      effectParams.cullMode = KawaiiProgram::CullMode::CullBack;
      break;

    case InternalShaderEffect::GaussianBlur:
      vertexFName = QString();
      fragmentFName = QStringLiteral(":/shaders/effect/gaussian_blur.fs.glsl");
      effectParams.cullMode = KawaiiProgram::CullMode::CullBack;
      break;

    case InternalShaderEffect::IrradianceConvolution:
      vertexFName = QString();
      fragmentFName = QStringLiteral(":/shaders/effect/irradiance_convolution.fs.glsl");
      effectParams.cullMode = KawaiiProgram::CullMode::CullBack;
      break;

    case InternalShaderEffect::GammaAndExposure:
      vertexFName = QString();
      fragmentFName = QStringLiteral(":/shaders/effect/gamma_exposure.fs.glsl");
      effectParams.cullMode = KawaiiProgram::CullMode::CullBack;
      break;

    case InternalShaderEffect::BrightnessAndContrast:
      vertexFName = QString();
      fragmentFName = QStringLiteral(":/shaders/effect/brightness_contrast.fs.glsl");
      effectParams.cullMode = KawaiiProgram::CullMode::CullBack;
      break;

    case InternalShaderEffect::ColorCorrection:
      vertexFName = QString();
      fragmentFName = QStringLiteral(":/shaders/effect/color_correction.fs.glsl");
      effectParams.cullMode = KawaiiProgram::CullMode::CullBack;
      break;
    }

  effectParams.vertexSrc = !vertexFName.isNull()? sib_utils::ioReadAll(QFile(vertexFName)): vertexFName;
  effectParams.fragmentSrc = sib_utils::ioReadAll(QFile(fragmentFName));
}

void ResourcePack::startSkeletalAnimation(const QString &model, QAnyStringView animationName)
{
  auto model_ptr = getModel(model);
  if(!model_ptr)
    throw std::invalid_argument("ResourcePack::startSkeletalAnimation: model not found");

  auto animation = model_ptr->findChild<KawaiiSkeletalAnimation*>(animationName);
  if(animation)
    if(auto sk = model_ptr->getSkeleton(); sk)
      sk->setAnimation(*animation);
}

void ResourcePack::stopSkeletalAnimation(const QString &model)
{
  auto model_ptr = getModel(model);
  if(!model_ptr)
    throw std::invalid_argument("ResourcePack::stopSkeletalAnimation: model not found");

  if(auto sk = model_ptr->getSkeleton(); sk)
    sk->stopAnimation();
}

KawaiiFramebuffer *ResourcePack::createFbo(const QString &fboName, const QString &colorTexture, KawaiiCamera *cam)
{
  auto colorTex = getTexture2D(colorTexture);
  if(!colorTex)
    throw std::invalid_argument("ResourcePack::createFbo: color texture not found");

  KawaiiRenderpass *rp = createChild<KawaiiRenderpass>();
  rp->setObjectName(fboName + QStringLiteral(".renderpass"));
  rp->addGBuf(KawaiiRenderpass::GBuffer { .format = KawaiiRenderpass::InternalImgFormat::RGBA8_Unorm, .needClear = true, .needStore = true });
  rp->addGBuf(KawaiiRenderpass::GBuffer { .format = KawaiiRenderpass::InternalImgFormat::Depth16, .needClear = true, .needStore = false });
  KawaiiSceneLayer *layer = rp->createChild<KawaiiSceneLayer>();
  layer->setCamera(cam);
  rp->addSubpass(KawaiiRenderpass::Subpass { .imgSrc = layer, .outputGBufs = {0}, .depthGBuf = 1 } );
  try {
    return createFbo(fboName, colorTex, rp);
  } catch(...)
  {
    delete rp;
    throw;
  }
}

KawaiiFramebuffer *ResourcePack::createFbo(const QString &fboName, KawaiiTexture *colorTexture, KawaiiRenderpass *renderpass)
{
  auto framebuffer = new KawaiiFramebuffer(glm::uvec2(colorTexture->getResolution().at(0), colorTexture->getResolution().at(1)));
  framebuffer->setRenderpass(renderpass);
  framebuffer->setObjectName(fboName);
  switch(colorTexture->getType())
    {
    case KawaiiTextureType::Texture1D:
    case KawaiiTextureType::TextureBuffer:
      delete framebuffer;
      framebuffer = nullptr;
      qWarning("ResourcePack::createFbo: 1D textures are not allowed in fbo!");
      break;
    case KawaiiTextureType::Texture1D_Array:
    case KawaiiTextureType::Texture2D:
    case KawaiiTextureType::TextureRectangle:
    case KawaiiTextureType::Texture2D_Multisample:
      framebuffer->attachTexture(0, colorTexture);
      break;
    default:
      for(uint64_t i = 0; i < colorTexture->getResolution().at(2); ++i)
        framebuffer->attachTexture(i, colorTexture, i);
      break;
    }
  framebuffer->updateParent(this);
  return framebuffer;
}

KawaiiEnvMap *ResourcePack::createEnvMap(const QString &envMapName, const QString &colorTexture, const glm::vec3 &pos)
{
  auto colorTex = getTextureCube(colorTexture);
  if(!colorTex)
    throw std::invalid_argument("ResourcePack::createEnvMap: color texture not found");

  int w = 0, h = 0;
  for(uint16_t i = 0 ; i < 6; ++i)
    {
      if(w < colorTex->getImage(i).width())
        w = colorTex->getImage(i).width();

      if(h < colorTex->getImage(i).height())
        h = colorTex->getImage(i).height();
    }

  auto envMap = new KawaiiEnvMap(glm::uvec2(w, h));
  envMap->setPosition(pos);
  envMap->setObjectName(envMapName);
  envMap->updateParent(getPrimaryScene());
  envMap->attachTexture(KawaiiEnvMap::AttachmentMode::Color, colorTex);
  return envMap;
}

void ResourcePack::clear()
{
  forall([](QObject* obj) { obj->deleteLater(); });
  QCoreApplication::sendPostedEvents();
  shaders.clear();
  serialized.clear();
}

const QByteArray &ResourcePack::serialize()
{
  if(serialized.isEmpty())
    {
      std::unique_ptr<sib_utils::memento::Memento> rootMemento;
      rootMemento.reset(KawaiiDataUnit::getMementoFactory().saveObjectTreeBinary(*this));
      sib_utils::memento::BlobSaver().save(serialized, *rootMemento);
      serialized = qCompress(serialized, 9);
    }
  return serialized;
}

void ResourcePack::reset(const QByteArray &serializedBytes)
{
  clear();
  serialized = qUncompress(serializedBytes);
  if(!serialized.isEmpty())
    {
      std::unique_ptr<sib_utils::memento::Memento> rootMemento;
      rootMemento.reset(sib_utils::memento::BlobLoader().load(serialized));
      KawaiiDataUnit::getMementoFactory().loadChildren(*rootMemento, this);
      checkModels();
    }
}

void ResourcePack::updateSerializedBytes()
{
  serialized.clear();
}

void ResourcePack::childEvent(QChildEvent *event)
{
  switch(event->type())
    {
    case QEvent::ChildAdded:
      onChildAdded(event->child(), event->child()->objectName());
      break;

    case QEvent::ChildRemoved:
      onChildRemoved(event->child());
      break;

    default: break;
    }
}

void ResourcePack::writeBinary(sib_utils::memento::Memento::DataMutator &mem) const
{
  KawaiiDataUnit::writeBinary(mem);
  mem.setLink(QStringLiteral("MainRenderpass"), mainRp);
  mem.setLink(QStringLiteral("PrimaryScene"), primaryScene);
}

void ResourcePack::writeText(sib_utils::memento::Memento::DataMutator &mem) const
{
  KawaiiDataUnit::writeText(mem);
  mem.setLink(QStringLiteral("PrimaryScene"), primaryScene);
}

void ResourcePack::read(sib_utils::memento::Memento::DataReader &mem)
{
  KawaiiDataUnit::read(mem);
  QMetaObject::invokeMethod(this, std::bind(&ResourcePack::checkModels, this), Qt::QueuedConnection);
  mem.read(QStringLiteral("MainRenderpass"), [this] (sib_utils::TreeNode *node) {
    setMainRenderpass(qobject_cast<KawaiiRenderpass*>(node));
  })
  .read(QStringLiteral("PrimaryScene"), [this] (sib_utils::TreeNode *node) {
    primaryScene = qobject_cast<KawaiiScene*>(node);
  });
}

KawaiiScene &ResourcePack::getShadowrender_scene() const
{
  return *shadowrender_scene;
}

bool ResourcePack::hasShadowrender_scene() const
{
  return shadowrender_scene.get();
}

void ResourcePack::setShadowrender_scene(std::unique_ptr<KawaiiScene> &&value)
{
  shadowrender_scene = std::move(value);
  auto enableShadows = [this] (KawaiiScene *scene) {
    if(scene->property("suppress_shadows").toBool()) return;

    const auto &sceneHash = scene->getScene();
    for(auto i = sceneHash.begin(); i != sceneHash.end(); ++i)
      {
        if(i->shaderProg && !i->shaderProg->property("suppress_shadows").toBool())
          shadowrender_scene->setProgramToMaterial(const_cast<KawaiiMaterial*>(i.key()), createShadowRenderProgram(i->shaderProg));
        else
          shadowrender_scene->setProgramToMaterial(const_cast<KawaiiMaterial*>(i.key()), nullptr);
      }
  };
  enableShadows(getPrimaryScene());
  for(auto scene: scenes)
    enableShadows(scene);
}

void ResourcePack::setMainRenderpass(KawaiiRenderpass *renderpass)
{
  if(mainRp != renderpass)
    {
      mainRp = renderpass;
      updateSerializedBytes();
    }
}

KawaiiRenderpass *ResourcePack::getMainRenderpass() const
{
  return mainRp;
}

class ResourcePack::ChildObserver: public QObject
{
  ResourcePack *res;

public:
  inline explicit ChildObserver(ResourcePack *res):
    QObject(res),
    res(res)
  {
    res->installEventFilter(this);
  }

  // QObject interface
public:
  bool eventFilter(QObject *obj, QEvent *event) override final
  {
    auto *childEvent = static_cast<QChildEvent*>(event);
    if(obj == res)
      {
        if(event->type() == QEvent::ChildAdded)
          {
            if(auto scene = qobject_cast<KawaiiScene*>(childEvent->child()); scene)
              {
                for(auto *ch: scene->children())
                  res->onChildAdded(ch, ch->objectName());

                scene->installEventFilter(this);
              }
          }
        else if(event->type() == QEvent::ChildRemoved)
          if(auto scene = qobject_cast<KawaiiScene*>(childEvent->child()); scene)
            {
              for(auto *ch: scene->children())
                res->onChildRemoved(ch);

              scene->removeEventFilter(this);
            }
      } else
      {
        if(event->type() == QEvent::ChildAdded)
          res->onChildAdded(childEvent->child(), childEvent->child()->objectName());

        else if(event->type() == QEvent::ChildRemoved)
          res->onChildRemoved(childEvent->child());
      }
    return false;
  }
};

void ResourcePack::onChildAdded(QObject *child, const QString &childName)
{
  QWriteLocker l(resourcesM.get());

  if(auto img = qobject_cast<KawaiiImage*>(child); img)
    {
      textures[childName] = img;
      onChildRenamedConnections[img] = connect(img, &QObject::objectNameChanged, std::bind(&ResourcePack::onElementRenamed<KawaiiImage>, std::ref(textures), img, std::placeholders::_1));
      connect(child, &QObject::objectNameChanged, this, &ResourcePack::updateSerializedBytes);
      connect(img, &KawaiiImage::imageUpdated, this, &ResourcePack::updateSerializedBytes);
      connect(img, &KawaiiImage::magFilterChanged, this, &ResourcePack::updateSerializedBytes);
      connect(img, &KawaiiImage::minFilterChanged, this, &ResourcePack::updateSerializedBytes);
      connect(img, &KawaiiImage::wrapModeRChanged, this, &ResourcePack::updateSerializedBytes);
      connect(img, &KawaiiImage::wrapModeSChanged, this, &ResourcePack::updateSerializedBytes);
      connect(img, &KawaiiImage::wrapModeTChanged, this, &ResourcePack::updateSerializedBytes);
      updateSerializedBytes();
    } else if(auto framebuffer = qobject_cast<KawaiiFramebuffer*>(child); framebuffer)
    {
      fbo[childName] = framebuffer;
      onChildRenamedConnections[framebuffer] = connect(framebuffer, &QObject::objectNameChanged, std::bind(&ResourcePack::onElementRenamed<KawaiiFramebuffer>, std::ref(fbo), framebuffer, std::placeholders::_1));
      connect(child, &QObject::objectNameChanged, this, &ResourcePack::updateSerializedBytes);
      connect(framebuffer, &KawaiiFramebuffer::renderpassChanged, this, &ResourcePack::updateSerializedBytes);
      connect(framebuffer, &KawaiiFramebuffer::sizeChanged, this, &ResourcePack::updateSerializedBytes);
      connect(framebuffer, &KawaiiFramebuffer::textureDetached, this, &ResourcePack::updateSerializedBytes);
      connect(framebuffer, &KawaiiFramebuffer::textureAttached, this, &ResourcePack::updateSerializedBytes);
      updateSerializedBytes();
    } else if(auto envmap = qobject_cast<KawaiiEnvMap*>(child); envmap)
    {
      envMaps[childName] = envmap;
      onChildRenamedConnections[envmap] = connect(envmap, &QObject::objectNameChanged, std::bind(&ResourcePack::onElementRenamed<KawaiiEnvMap>, std::ref(envMaps), envmap, std::placeholders::_1));
      connect(child, &QObject::objectNameChanged, this, &ResourcePack::updateSerializedBytes);
      connect(envmap, &KawaiiEnvMap::positionChanged, this, &ResourcePack::updateSerializedBytes);
      connect(envmap, &KawaiiEnvMap::resolutionChanged, this, &ResourcePack::updateSerializedBytes);
      connect(envmap, &KawaiiEnvMap::sceneChanged, this, &ResourcePack::updateSerializedBytes);
      connect(envmap, &KawaiiEnvMap::textureDetached, this, &ResourcePack::updateSerializedBytes);
      connect(envmap, &KawaiiEnvMap::textureAttached, this, &ResourcePack::updateSerializedBytes);
      updateSerializedBytes();
    } else if(auto cubemap = qobject_cast<KawaiiCubemap*>(child); cubemap)
    {
      cubemaps[childName] = cubemap;
      onChildRenamedConnections[cubemap] = connect(cubemap, &QObject::objectNameChanged, std::bind(&ResourcePack::onElementRenamed<KawaiiCubemap>, std::ref(cubemaps), cubemap, std::placeholders::_1));
      connect(child, &QObject::objectNameChanged, this, &ResourcePack::updateSerializedBytes);
      connect(cubemap, &KawaiiCubemap::imageUpdated, this, &ResourcePack::updateSerializedBytes);
      connect(cubemap, &KawaiiCubemap::magFilterChanged, this, &ResourcePack::updateSerializedBytes);
      connect(cubemap, &KawaiiCubemap::minFilterChanged, this, &ResourcePack::updateSerializedBytes);
      connect(cubemap, &KawaiiCubemap::wrapModeRChanged, this, &ResourcePack::updateSerializedBytes);
      connect(cubemap, &KawaiiCubemap::wrapModeSChanged, this, &ResourcePack::updateSerializedBytes);
      connect(cubemap, &KawaiiCubemap::wrapModeTChanged, this, &ResourcePack::updateSerializedBytes);
      updateSerializedBytes();
    } else if(auto model = qobject_cast<KawaiiModel3D*>(child); model)
    {
      models[childName] = model;
      onChildRenamedConnections[model] = connect(model, &QObject::objectNameChanged, std::bind(&ResourcePack::onElementRenamed<KawaiiModel3D>, std::ref(models), model, std::placeholders::_1));
      checkModel(model);
      connect(child, &QObject::objectNameChanged, this, &ResourcePack::updateSerializedBytes);
      connect(model, &KawaiiModel3D::meshAdded, this, &ResourcePack::updateSerializedBytes);
      connect(model, &KawaiiModel3D::meshRemoved, this, &ResourcePack::updateSerializedBytes);
      model->forallMeshes([this](KawaiiMesh3D &mesh) {
        connect(&mesh, &KawaiiMesh3D::indicesUpdated, this, &ResourcePack::updateSerializedBytes);
        connect(&mesh, &KawaiiMesh3D::triangleAdded, this, &ResourcePack::updateSerializedBytes);
        connect(&mesh, &KawaiiMesh3D::trianglesRemoved, this, &ResourcePack::updateSerializedBytes);
        connect(&mesh, &KawaiiMesh3D::vertexAdded, this, &ResourcePack::updateSerializedBytes);
        connect(&mesh, &KawaiiMesh3D::verticesRemoved, this, &ResourcePack::updateSerializedBytes);
        connect(&mesh, &KawaiiMesh3D::verticesUpdated, this, &ResourcePack::updateSerializedBytes);
      });

      updateSerializedBytes();
    } else if(auto material = qobject_cast<KawaiiMaterial*>(child); material)
    {
      materials[childName] = material;
      onChildRenamedConnections[material] = connect(material, &QObject::objectNameChanged, std::bind(&ResourcePack::onElementRenamed<KawaiiMaterial>, std::ref(materials), material, std::placeholders::_1));
      connect(child, &QObject::objectNameChanged, this, &ResourcePack::updateSerializedBytes);
      connect(material, &KawaiiMaterial::uniformsChanged, this, &ResourcePack::updateSerializedBytes);
      updateSerializedBytes();
    } else if(auto sound = qobject_cast<Sound*>(child); sound)
    {
      sounds[childName] = sound;
      onChildRenamedConnections[sound] = connect(sound, &QObject::objectNameChanged, std::bind(&ResourcePack::onElementRenamed<Sound>, std::ref(sounds), sound, std::placeholders::_1));
      connect(child, &QObject::objectNameChanged, this, &ResourcePack::updateSerializedBytes);
      updateSerializedBytes();
    } else if(auto scene = qobject_cast<KawaiiScene*>(child); scene)
    {
      if(!primaryScene)
        primaryScene = scene;
      else
        {
          scenes[childName] = scene;
          onChildRenamedConnections[scene] = connect(scene, &QObject::objectNameChanged, std::bind(&ResourcePack::onElementRenamed<KawaiiScene>, std::ref(scenes), scene, std::placeholders::_1));
        }
      connect(child, &QObject::objectNameChanged, this, &ResourcePack::updateSerializedBytes);
      updateSerializedBytes();
    } else if(auto camera = qobject_cast<KawaiiCamera*>(child); camera)
    {
      cameras[childName] = camera;
      onChildRenamedConnections[camera] = connect(camera, &QObject::objectNameChanged, std::bind(&ResourcePack::onElementRenamed<KawaiiCamera>, std::ref(cameras), camera, std::placeholders::_1));
      connect(child, &QObject::objectNameChanged, this, &ResourcePack::updateSerializedBytes);
      updateSerializedBytes();
    }
}

void ResourcePack::onChildRemoved(QObject *child)
{
  QWriteLocker l(resourcesM.get());
  if(auto scene = qobject_cast<KawaiiScene*>(child); scene)
    {
      removeFrom(scenes, scene);
      if(scene == primaryScene)
        {
          if(!scenes.empty())
            primaryScene = *scenes.cbegin();
          else
            primaryScene = nullptr;
        }
    } else if(auto img = qobject_cast<KawaiiImage*>(child); img)
    {
      disconnect(img, &KawaiiImage::imageUpdated, this, &ResourcePack::updateSerializedBytes);
      disconnect(img, &KawaiiImage::magFilterChanged, this, &ResourcePack::updateSerializedBytes);
      disconnect(img, &KawaiiImage::minFilterChanged, this, &ResourcePack::updateSerializedBytes);
      disconnect(img, &KawaiiImage::wrapModeRChanged, this, &ResourcePack::updateSerializedBytes);
      disconnect(img, &KawaiiImage::wrapModeSChanged, this, &ResourcePack::updateSerializedBytes);
      disconnect(img, &KawaiiImage::wrapModeTChanged, this, &ResourcePack::updateSerializedBytes);
      removeFrom(textures, img);
    } else if(auto cubemap = qobject_cast<KawaiiCubemap*>(child); cubemap)
    {
      removeFrom(cubemaps, cubemap);
    } else if(auto framebuffer = qobject_cast<KawaiiFramebuffer*>(child); framebuffer)
    {
      removeFrom(fbo, framebuffer);
    } else if(auto envmap = qobject_cast<KawaiiEnvMap*>(child); envmap)
    {
      removeFrom(envMaps, envmap);
    } else if(auto model = qobject_cast<KawaiiModel3D*>(child); model)
    {
      removeFrom(models, model);
    } else if(auto material = qobject_cast<KawaiiMaterial*>(child); material)
    {
      removeFrom(materials, material);
    } else if(child == mainRp)
    {
      mainRp = nullptr;
      updateSerializedBytes();
    }

  auto el = onChildRenamedConnections.find(child);
  if(el != onChildRenamedConnections.end())
    {
      disconnect(el->second);
      onChildRenamedConnections.erase(el);
      disconnect(child, &QObject::objectNameChanged, this, &ResourcePack::updateSerializedBytes);
      updateSerializedBytes();
    }
}

void ResourcePack::checkModel(KawaiiModel3D *model)
{
  QMutexLocker l(meshInstancesM.get());
  auto &instances = virginMeshInstances[model];
  model->forallMeshes([this, &instances] (KawaiiMesh3D &mesh) {
      auto inst = mesh.findChild<KawaiiMeshInstance*>(QString(), Qt::FindDirectChildrenOnly);
      if(!inst)
        inst = mesh.createChild<KawaiiMeshInstance>();
      inst->setInstanceCount(0);
      if(!instances.contains(inst))
        {
          instances.push_back(inst);
          updateSerializedBytes();
        }
    });
}

void ResourcePack::checkModels()
{
  for(auto *model: models)
    {
      if(model->getSkeleton())
        model->enableSkeleton();

      checkModel(model);
    }
}

KawaiiProgram *ResourcePack::createShadowRenderProgram(KawaiiProgram *orig) const
{
  if(auto ptr = orig->property("shadow_render").toULongLong(); ptr)
    {
      return reinterpret_cast<KawaiiProgram*>(ptr);
    }
  else if(shadowrender_scene)
    {
      KawaiiProgram *p = shadowrender_scene->createChild<KawaiiProgram>();
      auto cloneShader = [p] (KawaiiShader *sh) {
        if(!sh->isGloballyLoadedModule() && sh->getType() != KawaiiShaderType::Fragment)
          {
            auto clone = sh->clone();
            clone->updateParent(p);
            clone->setProperty("orig", reinterpret_cast<qulonglong>(sh));
          }
      };
      orig->forallModules(cloneShader);
      connect(orig, &KawaiiProgram::shaderRegistered, cloneShader);
      connect(orig, &KawaiiProgram::shaderUnregistered, [p] (KawaiiShader *sh) {
        for(auto *sh1: p->children())
          if(sh1->property("orig").toULongLong() == reinterpret_cast<qulonglong>(sh))
            sh1->deleteLater();
      });
      connect(orig, &KawaiiProgram::destroyed, p, &QObject::deleteLater);

      static const QByteArray frag_main_src = sib_utils::ioReadAll(QFile(QStringLiteral(":/shaders/main_shadow.fs.glsl")));
      p->createShaderModule(KawaiiShaderType::Fragment, frag_main_src);
      orig->setProperty("shadow_render", reinterpret_cast<qulonglong>(p));
      return p;
    }
  else
    return nullptr;
}

void ResourcePack::setProgramToMaterial(KawaiiScene *sc, KawaiiMaterial *material, KawaiiProgram *prog)
{
  sc->setProgramToMaterial(material, prog);
  if(shadowrender_scene)
    {
      if(prog && !prog->property("suppress_shadows").toBool() && !sc->property("suppress_shadows").toBool())
        shadowrender_scene->setProgramToMaterial(material, createShadowRenderProgram(prog));
      else
        shadowrender_scene->setProgramToMaterial(material, nullptr);
    }
}
