#ifndef SKYBOXGLSL_HPP
#define SKYBOXGLSL_HPP

#include "KawaiiWorlds_global.hpp"
#include <Kawaii3D/Shaders/KawaiiProgram.hpp>
#include <Kawaii3D/Shaders/KawaiiScene.hpp>
#include "ShaderEffectSrc.hpp"

class KAWAIIWORLDS_SHARED_EXPORT SkyboxGlsl : public KawaiiProgram
{
  Q_OBJECT

  Q_PROPERTY(bool suppress_shadows MEMBER suppressShadows DESIGNABLE false SCRIPTABLE false CONSTANT);
  inline static constexpr bool suppressShadows = true;
public:
  SkyboxGlsl(const ShaderEffectSrc &materialEffectSrc);
};

#endif // SKYBOXGLSL_HPP
