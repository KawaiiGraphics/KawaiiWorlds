#include "BarierClockwork.hpp"
#include "FrictionClockwork.hpp"
#include "GravityAccelerationClockwork.hpp"
#include "HookesLawClockwork.hpp"
#include "SolidizerClockwork.hpp"
#include "ViscosityClockwork.hpp"

KAWAII_COMMON_IMPLEMENT(Clockwork::adapters);

Clockwork::AdapterRegistrator<BarierClockwork> BarierClockwork::reg("barier");
Clockwork::AdapterRegistrator<FrictionClockwork> FrictionClockwork::reg("friction");
Clockwork::AdapterRegistrator<GravityAccelerationClockwork> GravityAccelerationClockwork::reg("gravity_acceleration");
Clockwork::AdapterRegistrator<HookesLawClockwork> HookesLawClockwork::reg("hookes_law");
Clockwork::AdapterRegistrator<SolidizerClockwork> SolidizerClockwork::reg("solidizer");
Clockwork::AdapterRegistrator<ViscosityClockwork> ViscosityClockwork::reg("viscosity");


#include "Gearbox.hpp"
#include "GearboxForRigid.hpp"

KAWAII_COMMON_IMPLEMENT(Gearbox::adapters);

Gearbox::AdapterRegistrator<GearboxForRigid> GearboxForRigid::reg("gearbox_for_rigid");
