#ifndef CLOCKWORK_HPP
#define CLOCKWORK_HPP

#include <sib_utils/Serializable.hpp>
#include <functional>
#include <QVector3D>

#include "Entity.hpp"

struct CollisionInfo {
  QVector3D normal;
  QVector3D overlap;

  Entity *a;
  Entity *b;

  float friction;
};

class KAWAIIWORLDS_SHARED_EXPORT Clockwork: public sib_utils::Serializable
{
public:
  Clockwork(std::string_view adapterId);
  virtual ~Clockwork() = default;

  /// Handle single object
  virtual void applySingle(Entity *a, float sec_elapsed) = 0;

  /// Handle pair of objects
  virtual void applyPair(Entity *a, Entity *b, float sec_elapsed) = 0;

  /// Handle collision
  virtual void applyCollision(const CollisionInfo &collision, float sec_elapsed) = 0;



  inline static std::unique_ptr<Clockwork> loadFrom(QDataStream &st)
  {
    auto res = sib_utils::Serializable::loadFrom(st, adapters);
    return std::unique_ptr<Clockwork>(static_cast<Clockwork*>(res.release()));
  }

protected:
  static std::unordered_map<std::string_view, ByteAdapter> adapters;

  template<class T> using AdapterRegistrator = sib_utils::Serializable::AdapterRegistrator<T, adapters>;
};

class KAWAIIWORLDS_SHARED_EXPORT ClockworkSingle: public Clockwork
{
public:
  ClockworkSingle(std::string_view adapterId): Clockwork(adapterId) {};

  inline void applyPair(Entity*, Entity*, [[maybe_unused]] float sec_elapsed) override final {}
  inline void applyCollision(const CollisionInfo&, [[maybe_unused]] float sec_elapsed) override final {}
};

class KAWAIIWORLDS_SHARED_EXPORT ClockworkPair: public Clockwork
{
public:
  ClockworkPair(std::string_view adapterId): Clockwork(adapterId) {};

  inline void applySingle(Entity*, [[maybe_unused]] float sec_elapsed) override final {}
  inline void applyCollision(const CollisionInfo&, [[maybe_unused]] float sec_elapsed) override final {}
};

class KAWAIIWORLDS_SHARED_EXPORT ClockworkCollision: public Clockwork
{
public:
  ClockworkCollision(std::string_view adapterId): Clockwork(adapterId) {};

  inline void applySingle(Entity *, [[maybe_unused]] float sec_elapsed) override final {}
  inline void applyPair(Entity *, Entity *, [[maybe_unused]] float sec_elapsed) override final {}
};

#endif // CLOCKWORK_HPP
