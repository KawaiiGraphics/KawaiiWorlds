#ifndef VISCOSITYCLOCKWORK_HPP
#define VISCOSITYCLOCKWORK_HPP

#include "Clockwork.hpp"

class KAWAIIWORLDS_SHARED_EXPORT ViscosityClockwork : public ClockworkSingle
{
  static AdapterRegistrator<ViscosityClockwork> reg;
  float velFactor;

public:
  ViscosityClockwork(float viscosityFactor = 0.05);

  void setViscosityFactor(float factor);
  float getViscosityFactor() const;

  // ClockworkSingle interface
public:
  void applySingle(Entity *a, float sec_elapsed) override;
};

KAWAIIWORLDS_SHARED_EXPORT QDataStream& operator<<(QDataStream &st, const ViscosityClockwork &obj);
KAWAIIWORLDS_SHARED_EXPORT QDataStream& operator>>(QDataStream &st, ViscosityClockwork &obj);

#endif // VISCOSITYCLOCKWORK_HPP
