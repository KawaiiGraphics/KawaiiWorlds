#ifndef GEARBOXFORRIGID_HPP
#define GEARBOXFORRIGID_HPP

#include "Gearbox.hpp"

class KAWAIIWORLDS_SHARED_EXPORT GearboxForRigid : public Gearbox
{
  static Gearbox::AdapterRegistrator<GearboxForRigid> reg;
public:
  GearboxForRigid();
  GearboxForRigid(World &w);

  ~GearboxForRigid();

  void addCollisionObject(btCollisionObject &obj) override;
  void removeCollisionObject(btCollisionObject &obj) override;



  // IMPLEMENT
private:
  std::unique_ptr<btCollisionConfiguration> collisionConfig;
  std::unique_ptr<btDispatcher> dispatcher;
  std::unique_ptr<btOverlappingPairCache> overlappingPairCache;
  std::unique_ptr<btBroadphaseInterface> broadphase;
  std::unique_ptr<btConstraintSolver> solver;
  btDiscreteDynamicsWorld *discreteDynamicsWorld;


  void createBtCollisionWorld();
};


KAWAIIWORLDS_SHARED_EXPORT QDataStream& operator<<(QDataStream &st, const GearboxForRigid &obj);
KAWAIIWORLDS_SHARED_EXPORT QDataStream& operator>>(QDataStream &st, GearboxForRigid &obj);

#endif // GEARBOXFORRIGID_HPP
