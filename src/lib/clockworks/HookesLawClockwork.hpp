#ifndef HOOKESLAWCLOCKWORK_HPP
#define HOOKESLAWCLOCKWORK_HPP

#include "Clockwork.hpp"

class KAWAIIWORLDS_SHARED_EXPORT HookesLawClockwork : public ClockworkCollision
{
  static AdapterRegistrator<HookesLawClockwork> reg;
public:
  HookesLawClockwork();

  // ClockworkCollision interface
public:
  void applyCollision(const CollisionInfo &collision, float sec_elapsed) override;

private:
  void collideSolid(Entity &a, Entity &b, const QVector3D &overlap, const QVector3D &collision_N, float sec_elapsed);
  void collideSolidAndWall(Entity &solid, Entity &wall, const QVector3D &overlap, const QVector3D &collision_N, float sec_elapsed);
  void collideWall(Entity &a, Entity &b, const QVector3D &overlap, const QVector3D &collision_N, float sec_elapsed);

  void collideEntityAndNull(Entity &a, const QVector3D &overlap, const QVector3D &collision_N, float sec_elapsed);
  void collideSolidAndNull(Entity &a, const QVector3D &overlap, const QVector3D &collision_N, float sec_elapsed);
  void collideWallAndNull(Entity &a, const QVector3D &overlap, const QVector3D &collision_N, float sec_elapsed);
};

inline QDataStream& operator<<(QDataStream &st, const HookesLawClockwork&) { return st; }
inline QDataStream& operator>>(QDataStream &st, HookesLawClockwork&) { return st; }

#endif // HOOKESLAWCLOCKWORK_HPP
