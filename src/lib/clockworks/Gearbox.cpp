#include "Gearbox.hpp"
#include "World.hpp"
#include "bodies/ConvexHullCreator.hpp"

#include <BulletCollision/Gimpact/btGImpactCollisionAlgorithm.h>
#include <BulletCollision/Gimpact/btGImpactShape.h>
#include <QCoreApplication>
#include <QtConcurrent>
#include <array>

Gearbox::Gearbox(std::string_view adapterId):
  sib_utils::Serializable(adapterId, adapters),
  world(nullptr),
  bulletFixedTimeStepSec(1.0/100.0),
  useBulletSimulation(false),
  bulletStepsInterpolation(false),
  detectCollisions(true),
  gImpactEnabled(false),
  processingTick(false)
{ }

Gearbox::Gearbox(std::string_view adapterId, World &world):
  sib_utils::Serializable(adapterId, adapters),
  world(&world),
  bulletFixedTimeStepSec(1.0/100.0),
  useBulletSimulation(false),
  bulletStepsInterpolation(false),
  detectCollisions(true),
  gImpactEnabled(false),
  processingTick(false)
{ }

size_t Gearbox::addClockwork(std::unique_ptr<Clockwork> &&clockwork)
{
  if(Q_LIKELY(clockwork))
    {
      clockworks.push_back(std::move(clockwork));
      return clockworks.size() - 1;
    } else
    return std::numeric_limits<size_t>::max();
}

void Gearbox::swapClockworks(size_t i1, size_t i2)
{
  if(Q_LIKELY(i1 != i2))
    std::swap(clockworks.at(i1), clockworks.at(i2));
}

Clockwork &Gearbox::getClockwork(size_t i) const
{
  return *clockworks.at(i);
}

void Gearbox::forallClockworks(const std::function<void(Clockwork&)> &func) const
{
  if(Q_LIKELY(func))
    for(const auto &i: clockworks)
      func(*i);
}

void Gearbox::removeClockwork(size_t i)
{
  if(i < clockworks.size())
    clockworks.erase(clockworks.cbegin() + i);
}

namespace {
  template<typename T>
  struct FilteredRayResultCallback: T
  {
    FilteredRayResultCallback(const btVector3& rayFromWorld, const btVector3& rayToWorld,
                              const std::function<bool (Entity&)> &filterFunc):
      T(rayFromWorld, rayToWorld),
      filterFunc(filterFunc)
    {
    }

    const std::function<bool (Entity&)> &filterFunc;

    bool needsCollision(btBroadphaseProxy* proxy0) const override
    {
      Entity *e = nullptr;
      if(auto el = static_cast<btCollisionObject*>(proxy0->m_clientObject)->getUserPointer(); Q_LIKELY(el))
        e = static_cast<Entity*>(el);

      if(e && filterFunc(*e))
        return T::needsCollision(proxy0);
      else
        return false;
    }
  };
}

std::unordered_set<Entity*> Gearbox::allEntitiesAt(const QPointF &screenPos, const KawaiiSceneLayer &sceneLayer)
{
  if(Q_UNLIKELY(!collisionWorld))
    return {};

  auto rayRes = rayTest<btCollisionWorld::AllHitsRayResultCallback>(screenPos, sceneLayer);

  std::unordered_set<Entity*> result;
  result.reserve(rayRes.m_collisionObjects.size());
  for(int i = 0; i < rayRes.m_collisionObjects.size(); ++i)
    {
      if(auto e = getEntity(rayRes.m_collisionObjects[i]); Q_LIKELY(e))
        result.insert(e);
      else
        qWarning("Gearbox::allEntitiesAt: Unknown collision object!");
    }
  return result;
}

std::unordered_set<Entity *> Gearbox::allEntitiesAt(const QPointF &screenPos, const KawaiiSceneLayer &sceneLayer, const std::function<bool (Entity &)> &func)
{
  if(Q_UNLIKELY(!func)) return allEntitiesAt(screenPos, sceneLayer);

  if(Q_UNLIKELY(!collisionWorld)) return {};

  using AllHitsFilteredRayResultCallback = FilteredRayResultCallback<btCollisionWorld::AllHitsRayResultCallback>;

  auto rayRes = rayTest<AllHitsFilteredRayResultCallback>(screenPos, sceneLayer, func);

  std::unordered_set<Entity*> result;
  result.reserve(rayRes.m_collisionObjects.size());
  for(int i = 0; i < rayRes.m_collisionObjects.size(); ++i)
    {
      if(auto e = getEntity(rayRes.m_collisionObjects[i]); Q_LIKELY(e))
        result.insert(e);
      else
        qWarning("Gearbox::allEntitiesAt: Unknown collision object!");
    }
  return result;
}

Entity *Gearbox::nearestEntityAt(const QPointF &screenPos, const KawaiiSceneLayer &sceneLayer)
{
  if(Q_UNLIKELY(!collisionWorld)) return nullptr;

  auto rayRes = rayTest<btCollisionWorld::ClosestRayResultCallback>(screenPos, sceneLayer);
  if(!rayRes.hasHit())
    return nullptr;

  if(auto e = getEntity(rayRes.m_collisionObject); Q_LIKELY(e))
    return e;
  else {
      qWarning("Gearbox::nearestEntityAt: Unknown collision object!");
      return nullptr;
    }
}

Entity *Gearbox::nearestEntityAt(const QPointF &screenPos, const KawaiiSceneLayer &sceneLayer, const std::function<bool (Entity&)> &func)
{
  if(Q_UNLIKELY(!func)) return nearestEntityAt(screenPos, sceneLayer);

  if(Q_UNLIKELY(!collisionWorld)) return nullptr;

  using ClosestFilteredRayResultCallback = FilteredRayResultCallback<btCollisionWorld::ClosestRayResultCallback>;

  auto rayRes = rayTest<ClosestFilteredRayResultCallback>(screenPos, sceneLayer, func);
  if(!rayRes.hasHit())
    return nullptr;

  if(auto e = getEntity(rayRes.m_collisionObject); Q_LIKELY(e))
    return e;
  else {
      qWarning("Gearbox::nearestEntityAt: Unknown collision object!");
      return nullptr;
    }
}

void Gearbox::tick(float secElapsed)
{
  auto t0 = std::chrono::steady_clock::now();

  while(processingTick)
    {
      QCoreApplication::instance()->processEvents();
      QThread::msleep(2);
    }

  validate();

  // entities -> bullet
  world->forallEntitiesP([](Entity &e) {
    e.writeBulletObj();
  });

  if(Q_UNLIKELY(!collisionWorld)) return;

  for(int i = 0; i < collisionWorld->getNumCollisionObjects(); ++i)
    if(auto gImpactShape = dynamic_cast<btGImpactShapeInterface*>(collisionWorld->getCollisionObjectArray()[i]->getCollisionShape()); gImpactShape)
      {
        if(!gImpactEnabled)
          if(auto dispatcher = dynamic_cast<btCollisionDispatcher*>(collisionWorld->getDispatcher()); Q_LIKELY(dispatcher))
            {
              btGImpactCollisionAlgorithm::registerAlgorithm(dispatcher);
              gImpactEnabled = true;
            }
        gImpactShape->updateBound();
      }

  // Bullet step simulation
  if(useBulletSimulation)
    {
      bulletTickTask = QtConcurrent::run([this, secElapsed, t0] {
          processingTick = true;
          if(Q_LIKELY(collisionWorld))
            {
              auto t1 = std::chrono::steady_clock::now();
              float sec_elapsed = secElapsed + 0.001 * std::chrono::duration_cast<std::chrono::milliseconds>(t1 - t0).count();
              const int bulletTickCount = collisionWorld->stepSimulation(sec_elapsed, 4, bulletFixedTimeStepSec) > 0;
              if(Q_LIKELY(!bulletTickTask.isCanceled()))
                QMetaObject::invokeMethod(this, &Gearbox::postTickActions, Qt::QueuedConnection, sec_elapsed, bulletTickCount > 0);
            }
        });
    } else
    postTickActions(secElapsed, false);
}

void Gearbox::postTickActions(float secElapsed, bool bulletTick)
{
  if(bulletTick || (useBulletSimulation && !bulletStepsInterpolation))
    {
      // bullet -> entities
      world->forallEntitiesP([](Entity &e) {
        e.readBulletObj();
      });
    }
  if(!bulletTick && detectCollisions)
    {
      collisionWorld->performDiscreteCollisionDetection();
      btTickCallback(collisionWorld.get(), 0);
    }

  // Run clockworks
  for(auto &clockwork: clockworks)
    {
      world->forallCollisions([&clockwork, secElapsed](const CollisionInfo &collision) {
          clockwork->applyCollision(collision, secElapsed);
        });
      world->forallEntityPairs([&clockwork, secElapsed](Entity &a, Entity &b) {
          clockwork->applyPair(&a, &b, secElapsed);
        });
      world->forallEntities([&clockwork, secElapsed](Entity &e) {
          clockwork->applySingle(&e, secElapsed);
      });
    }

  // Run Entity::tick for each entity
  world->forallEntitiesP([secElapsed](Entity &e) {
    e.tick(&e, secElapsed);
  });

  processingTick = false;
}

void Gearbox::addConstraint(btTypedConstraint *constraint)
{
  if(Q_LIKELY(collisionWorld))
    collisionWorld->addConstraint(constraint);
}

void Gearbox::removeConstraint(btTypedConstraint *constraint)
{
  if(Q_LIKELY(collisionWorld))
    collisionWorld->removeConstraint(constraint);
}

void Gearbox::init(std::unique_ptr<btDynamicsWorld> &&newCollisionWorld)
{
  collisionWorld = std::move(newCollisionWorld);
  if(collisionWorld)
    {
      collisionWorld->setInternalTickCallback(btTickCallback, this);
      collisionWorld->setForceUpdateAllAabbs(false);
    }
  if(world)
    init();
}

void Gearbox::init()
{
  world->forallEntities([this](Entity &e) {
    connectEntity(&e);
  });
  connect(world, &World::entityActiveted, this, &Gearbox::connectEntity, Qt::UniqueConnection);
  connect(world, &World::entityDeactiveted, this, &Gearbox::disconnectEntity, Qt::UniqueConnection);
}

void Gearbox::destroyCollisionWorld()
{
  bulletTickTask.cancel();
  bulletTickTask.waitForFinished();
  collisionWorld.reset();
}

bool Gearbox::getProcessingTick() const
{
  return processingTick;
}

void Gearbox::debugRayTest(const glm::vec4 &rayFrom, const glm::vec4 &rayTo)
{
  if(debugRayTestModelInstances.size() != 60)
    {
      auto model = world->res().getModel(QStringLiteral("ray_test"));
      if(!model) return;

      auto material = world->res().getMaterial(QStringLiteral("ray_test"));
      if(!material) return;

      debugRayTestModelInstances.resize(60);
      for(auto &body: debugRayTestModelInstances)
        {
          body = std::make_unique<ModelInstanceBody>();
          body->setModelName(QStringLiteral("ray_test"));
          body->setMaterialName(QStringLiteral("ray_test"));
          body->enable(*world);
        }
    }

  const auto delta = (rayTo - rayFrom) / 60.0f;
  const QVector3D qVecDelta(delta.x, delta.y, delta.z);
  const QVector3D qRayFrom(rayFrom.x, rayFrom.y, rayFrom.z);

  for(size_t i = 0; i < debugRayTestModelInstances.size(); ++i)
    {
      auto &body = debugRayTestModelInstances[i];
      body->setTransformation(qVecDelta * (i+1) + qRayFrom, {});
    }
}

Entity *Gearbox::getEntity(const btCollisionObject *btObj)
{
  return static_cast<Entity*>(btObj->getUserPointer());
}

float Gearbox::getBulletFixedTimeStepSec() const
{
  return bulletFixedTimeStepSec;
}

void Gearbox::setBulletFixedTimeStepSec(float newBulletFixedTimeStepSec)
{
  if (qFuzzyCompare(bulletFixedTimeStepSec, newBulletFixedTimeStepSec))
    return;
  bulletFixedTimeStepSec = newBulletFixedTimeStepSec;
  emit bulletFixedTimeStepSecChanged();
}


bool Gearbox::getUseBulletSimulation() const
{
  return useBulletSimulation;
}

void Gearbox::setUseBulletSimulation(bool newUseBulletSimulation)
{
  if(useBulletSimulation != newUseBulletSimulation)
    {
      useBulletSimulation = newUseBulletSimulation;
      useBulletSimulationChanged();
    }
}

bool Gearbox::getBulletStepsInterpolation() const
{
  return bulletStepsInterpolation;
}

void Gearbox::setBulletStepsInterpolation(bool newBulletStepsInterpolation)
{
  if (bulletStepsInterpolation == newBulletStepsInterpolation)
    return;
  bulletStepsInterpolation = newBulletStepsInterpolation;
  emit bulletStepsInterpolationChanged();
}

bool Gearbox::getDetectCollisions() const
{
  return detectCollisions;
}

void Gearbox::setDetectCollisions(bool newDetectCollisions)
{
  if (detectCollisions == newDetectCollisions)
    return;
  detectCollisions = newDetectCollisions;
  emit detectCollisionsChanged();
}

QVector3D Gearbox::getBulletGravity() const
{
  if(Q_LIKELY(collisionWorld))
    {
      const auto g = collisionWorld->getGravity();
      return {g.x(), g.y(), g.z()};
    } else
    return QVector3D();
}

void Gearbox::setBulletGravity(const QVariant &newBulletGravity)
{
  if(Q_UNLIKELY(!collisionWorld))
    return;

  auto doSetBulletGravity = [this] (const std::array<float, 3> &val) {
      const auto g = collisionWorld->getGravity();

      if (qFuzzyCompare(g.x(), val[0])
          && qFuzzyCompare(g.y(), val[1])
          && qFuzzyCompare(g.z(), val[2]))
        return;

      collisionWorld->setGravity({val[0], val[1], val[2]});
      emit bulletGravityChanged();
    };

  if(newBulletGravity.canConvert<QVector3D>())
    {
      const auto vec3 = newBulletGravity.value<QVector3D>();
      doSetBulletGravity({vec3.x(), vec3.y(), vec3.z()});
    }
  else if(newBulletGravity.canConvert<QVariantList>())
    {
      doSetBulletGravity({
                           newBulletGravity.toList().at(0).toFloat(),
                           newBulletGravity.toList().at(1).toFloat(),
                           newBulletGravity.toList().at(2).toFloat(),
                         });
    }
}

bool Gearbox::canPlace(btCollisionObject &btObject, const QVector3D &pos) const
{
  btTransform btOldTrsf = btObject.getWorldTransform(),
      btTrsf;
  btTrsf.setIdentity();
  btTrsf.setOrigin({pos.x(), pos.y(), pos.z()});

  btObject.setWorldTransform(btTrsf);

  struct ContactResultCallback: btCollisionWorld::ContactResultCallback
  {
    btCollisionObject *btObj;
    bool hasCollision = false;

    btScalar addSingleResult(btManifoldPoint&,
                             const btCollisionObjectWrapper *objA, int, int,
                             const btCollisionObjectWrapper *objB, int, int) override
    {
      if(Q_LIKELY(objA->getCollisionObject() == btObj || objB->getCollisionObject() == btObj))
        hasCollision = true;
      return 1;
    }
  };

  ContactResultCallback contactResult;
  contactResult.btObj = &btObject;
  collisionWorld->contactTest(&btObject, contactResult);
  btObject.setWorldTransform(btOldTrsf);

  return !(contactResult.hasCollision);
}

bool Gearbox::canPlace(KawaiiModel3D *model, const QVector3D &pos) const
{
  auto btShape = ConvexHullCreator().createCollisionShape(model);
  btCollisionObject collisionObj;
  collisionObj.setCollisionShape(btShape.get());
  return canPlace(collisionObj, pos);
}

void Gearbox::addCollisionObject(btCollisionObject &obj)
{
  if(Q_LIKELY(collisionWorld))
    collisionWorld->addCollisionObject(&obj);
}

void Gearbox::removeCollisionObject(btCollisionObject &obj)
{
  if(Q_LIKELY(collisionWorld))
    collisionWorld->removeCollisionObject(&obj);
}

void Gearbox::saveClockworks(QDataStream &st) const
{
  st << static_cast<quint64>(clockworks.size());
  for(const auto &i: clockworks)
    i->saveTo(st);
}

void Gearbox::loadClockworks(QDataStream &st)
{
  quint64 sz;
  st >> sz;
  for(quint64 i = 0; i < sz; ++i)
    clockworks.push_back(Clockwork::loadFrom(st));
}

void Gearbox::loadClockworksStub(QDataStream &st)
{
  quint64 sz;
  st >> sz;
  for(quint64 i = 0; i < sz; ++i)
    static_cast<void>(Clockwork::loadFrom(st));
}

void Gearbox::attachEntityBody(Entity *e)
{
  if(Q_LIKELY(e->getBody() && e->getBody()->hasCollisionObject()))
    {
      addCollisionObject(e->getBody()->getBtCollisionObject());
      e->getBody()->markCollisionObjUpdated();
      e->onBodyBtObjChange();
    }
}

void Gearbox::detachEntityBody(Entity *e)
{
  if(Q_LIKELY(e->getBody() && e->getBody()->hasCollisionObject()))
    removeCollisionObject(e->getBody()->getBtCollisionObject());
}

void Gearbox::connectEntity(Entity *e)
{
  attachEntityBody(e);
  connect(e, &Entity::bodyIsAboutToBeChanged, this, [this, e] { detachEntityBody(e); });
  connect(e, &Entity::bodyChanged, this, [this, e] { attachEntityBody(e); });
  connect(e, &Entity::constraintAdded, this, &Gearbox::addConstraint);
  connect(e, &Entity::constraintRemoved, this, &Gearbox::removeConstraint);
}

void Gearbox::disconnectEntity(Entity *e)
{
  detachEntityBody(e);
  disconnect(e, &Entity::bodyIsAboutToBeChanged, this, nullptr);
  disconnect(e, &Entity::bodyChanged, this, nullptr);
  disconnect(e, &Entity::constraintAdded, this, &Gearbox::addConstraint);
  disconnect(e, &Entity::constraintRemoved, this, &Gearbox::removeConstraint);
}

namespace {
  QVector3D qvec3(const btVector3 &btvec3)
  { return { btvec3.x(), btvec3.y(), btvec3.z() }; }

  QVector3D abs(const QVector3D &vec)
  { return { std::abs(vec.x()), std::abs(vec.y()), std::abs(vec.z()) }; }
}

bool Gearbox::handleCollision(const btCollisionObject *a, const btCollisionObject *b, const btVector3 &normal, btScalar combinedFriction)
{
  auto entity0 = getEntity(a);
  if(!entity0) return false;

  auto entity1 = getEntity(b);
  if(!entity1) return false;

  const auto bbox_a = entity0->getBBox();
  const auto bbox_b = entity1->getBBox();

  const QVector3D n = 0.5*(bbox_b.first + bbox_b.second - bbox_a.first - bbox_a.second);

  QVector3D a_extent = 0.5*(bbox_a.second - bbox_a.first),
      b_extent = 0.5*(bbox_b.second - bbox_b.first);

  QVector3D overlap = a_extent + b_extent - abs(n);

  world->addCollision(CollisionInfo {
                            .normal = qvec3(normal),
                            .overlap = overlap,
                            .a = entity0,
                            .b = entity1,
                            .friction = combinedFriction
                          });
  return true;
}

void Gearbox::btTickCallback(btDynamicsWorld *dynamicsWorld, btScalar timeStep)
{
  static_cast<void>(timeStep);

  auto gb = static_cast<Gearbox*>(dynamicsWorld->getWorldUserInfo());
  if(!gb->detectCollisions) return;

  int numManifolds = dynamicsWorld->getDispatcher()->getNumManifolds();
  for (int i = 0; i < numManifolds; ++i)
    {
      btPersistentManifold *contactManifold = dynamicsWorld->getDispatcher()->getManifoldByIndexInternal(i);
      auto *objA = contactManifold->getBody0();
      auto *objB = contactManifold->getBody1();

      if(contactManifold->getNumContacts() > 0)
        {
          const auto &cp = contactManifold->getContactPoint(0);
          gb->handleCollision(objA, objB, cp.m_normalWorldOnB, cp.m_combinedFriction);
        }
    }
}

void Gearbox::validate()
{
  world->forallEntities([this](Entity &e) {
    auto l = e.lock();
    auto body = e.getBody();
    if(Q_LIKELY(body))
      {
        body->handleCollisionObjChange([this, &e] (btCollisionObject *newBtObj, btCollisionObject *oldBtObj) {
            if(oldBtObj)
              removeCollisionObject(*oldBtObj);
            e.onBodyBtObjChange();
            if(newBtObj)
              addCollisionObject(*newBtObj);
          });

        if(Q_LIKELY(body->hasCollisionObject()))
          {
            btCollisionObject *collisionObj = &body->getBtCollisionObject();
            body->handleOverlapingPairCacheInvalidated([collisionObj, this] {
                collisionWorld->getBroadphase()->getOverlappingPairCache()->cleanProxyFromPairs(
                      collisionObj->getBroadphaseHandle(),
                      collisionWorld->getDispatcher()
                      );
              });
          }

        if(body->reenableIfNeeds(*world))
          body->setVisualTransformation(e.getPosition(), e.getRotation());
      }
  });
}


KAWAIIWORLDS_SHARED_EXPORT QDataStream& operator<<(QDataStream &st, const Gearbox &obj)
{
  obj.saveClockworks(st);
  return st << obj.getUseBulletSimulation() << obj.getBulletFixedTimeStepSec() << obj.getBulletGravity();
}

KAWAIIWORLDS_SHARED_EXPORT QDataStream& operator>>(QDataStream &st, Gearbox &obj)
{
  obj.loadClockworks(st);
  bool bulletSimulation;
  st >> bulletSimulation;
  obj.setUseBulletSimulation(bulletSimulation);

  float bulletFixedTimeStepSec;
  st >> bulletFixedTimeStepSec;
  obj.setBulletFixedTimeStepSec(bulletFixedTimeStepSec);

  QVector3D bulletGravity;
  st >> bulletGravity;
  obj.setBulletGravity(bulletGravity);
  return st;
}
