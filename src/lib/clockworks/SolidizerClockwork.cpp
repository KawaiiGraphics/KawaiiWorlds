#include "SolidizerClockwork.hpp"

SolidizerClockwork::SolidizerClockwork():
  ClockworkCollision(reg.adapterId)
{
}

namespace {
  void solidize(const QVector3D &N, QVector3D &vel)
  {
    if(N.x() < 0)
      {
        if(vel.x() > 0)
          vel.setX(0);
      } else if(N.x() > 0)
      {
        if(vel.x() < 0)
          vel.setX(0);
      }

    if(N.y() < 0)
      {
        if(vel.y() > 0)
          vel.setY(0);
      } else if(N.y() > 0)
      {
        if(vel.y() < 0)
          vel.setY(0);
      }

    if(N.z() < 0)
      {
        if(vel.z() > 0)
          vel.setZ(0);
      } else if(N.z() > 0)
      {
        if(vel.z() < 0)
          vel.setZ(0);
      }
  }
}

void SolidizerClockwork::applyCollision(const CollisionInfo &collision, float /*sec_elapsed*/)
{
  if(collision.a)
    {
      QVector3D vel = collision.a->getVelocity();
      solidize(-collision.normal, vel);
      collision.a->setVelocity(vel);
    }
  if(collision.b)
    {
      QVector3D vel = collision.b->getVelocity();
      solidize(collision.normal, vel);
      collision.b->setVelocity(vel);
    }
}
