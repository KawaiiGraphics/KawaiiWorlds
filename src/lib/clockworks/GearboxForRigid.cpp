#include "GearboxForRigid.hpp"
//#include <Bullet3OpenCL/BroadphaseCollision/

GearboxForRigid::GearboxForRigid():
  Gearbox(reg.adapterId)
{
  createBtCollisionWorld();
}

GearboxForRigid::GearboxForRigid(World &w):
  Gearbox(reg.adapterId, w)
{
  createBtCollisionWorld();
}

GearboxForRigid::~GearboxForRigid()
{
  destroyCollisionWorld();
  solver.reset();
  broadphase.reset();
  overlappingPairCache.reset();
  dispatcher.reset();
  collisionConfig.reset();
}

void GearboxForRigid::addCollisionObject(btCollisionObject &obj)
{
  if(Q_LIKELY(discreteDynamicsWorld))
    {
      btRigidBody* rigidBody = btRigidBody::upcast(&obj);
      if(rigidBody)
        return discreteDynamicsWorld->addRigidBody(rigidBody);
    }
  Gearbox::addCollisionObject(obj);
}

void GearboxForRigid::removeCollisionObject(btCollisionObject &obj)
{
  if(Q_LIKELY(discreteDynamicsWorld))
    {
      btRigidBody* rigidBody = btRigidBody::upcast(&obj);
      if(rigidBody)
        {
          for(int i = (rigidBody->getNumConstraintRefs() - 1); i >= 0; --i)
            discreteDynamicsWorld->removeConstraint(rigidBody->getConstraintRef(i));
          discreteDynamicsWorld->removeRigidBody(rigidBody);
        }
    }
  Gearbox::removeCollisionObject(obj);
}

void GearboxForRigid::createBtCollisionWorld()
{
  collisionConfig = std::make_unique<btDefaultCollisionConfiguration>();
  dispatcher = std::make_unique<btCollisionDispatcher>(collisionConfig.get());
  overlappingPairCache = std::make_unique<btHashedOverlappingPairCache>();
  broadphase = std::make_unique<btDbvtBroadphase>(overlappingPairCache.get());
  solver = std::make_unique<btSequentialImpulseConstraintSolver>();

  discreteDynamicsWorld = new btDiscreteDynamicsWorld(dispatcher.get(), broadphase.get(), solver.get(), collisionConfig.get());
  btContactSolverInfo& info = discreteDynamicsWorld->getSolverInfo();
  info.m_numIterations = 4;
  discreteDynamicsWorld->setLatencyMotionStateInterpolation(true);

  init(std::unique_ptr<btDiscreteDynamicsWorld>(discreteDynamicsWorld));
}


KAWAIIWORLDS_SHARED_EXPORT QDataStream& operator<<(QDataStream &st, const GearboxForRigid &obj)
{
  return st << static_cast<const Gearbox&>(obj);
}

KAWAIIWORLDS_SHARED_EXPORT QDataStream& operator>>(QDataStream &st, GearboxForRigid &obj)
{
  return st >> static_cast<Gearbox&>(obj);
}
