#include "ViscosityClockwork.hpp"
#include <cmath>

ViscosityClockwork::ViscosityClockwork(float viscosityFactor):
  ClockworkSingle(reg.adapterId),
  velFactor(1.0 - viscosityFactor)
{
}

void ViscosityClockwork::setViscosityFactor(float factor)
{
  velFactor = 1.0 - std::min<float>(factor, 1.0);
}

float ViscosityClockwork::getViscosityFactor() const
{
  return 1.0 - velFactor;
}

void ViscosityClockwork::applySingle(Entity *a, float sec_elapsed)
{
  a->setVelocity(a->getVelocity() * std::pow(velFactor, sec_elapsed));
}

KAWAIIWORLDS_SHARED_EXPORT QDataStream &operator<<(QDataStream &st, const ViscosityClockwork &obj)
{
  return st << obj.getViscosityFactor();
}

KAWAIIWORLDS_SHARED_EXPORT QDataStream &operator>>(QDataStream &st, ViscosityClockwork &obj)
{
  float f;
  st >> f;
  obj.setViscosityFactor(f);
  return st;
}
