#include "HookesLawClockwork.hpp"

HookesLawClockwork::HookesLawClockwork():
  ClockworkCollision(reg.adapterId)
{
}

void HookesLawClockwork::applyCollision(const CollisionInfo &collision, float sec_elapsed)
{
  // ignore non-entities
  if(!collision.a && !collision.b) return;

  if(!collision.a)
    return collideEntityAndNull(*collision.b, collision.overlap, collision.normal, sec_elapsed);

  if(!collision.b)
    return collideEntityAndNull(*collision.a, collision.overlap, collision.normal, sec_elapsed);

  if(qFuzzyIsNull(collision.a->getMass()))
    {
      if(qFuzzyIsNull(collision.b->getMass()))
        collideWall(*collision.a, *collision.b, collision.overlap, collision.normal, sec_elapsed);
      else
        collideSolidAndWall(*collision.b, *collision.a, collision.overlap, collision.normal, sec_elapsed);
    } else
    {
      if(qFuzzyIsNull(collision.b->getMass()))
        collideSolidAndWall(*collision.a, *collision.b, collision.overlap, collision.normal, sec_elapsed);
      else
        collideSolid(*collision.a, *collision.b, collision.overlap, collision.normal, sec_elapsed);
    }
}

void HookesLawClockwork::collideSolid(Entity &a, Entity &b, const QVector3D &overlap, const QVector3D &collision_N,
                                      float sec_elapsed)
{
  if(collision_N.isNull()) return;

  const auto rv = b.getVelocity() - a.getVelocity();

  const auto velAlongNormal = QVector3D::dotProduct(rv, collision_N);

  if(velAlongNormal > 0) return;

//  const QVector3D k = 2.0 * (a.getPulse() + b.getPulse()) / (a.getMass() + b.getMass());

  float e = std::min<float>(a.getRestitution(), b.getRestitution());

  float j = (1.0 + e) * (velAlongNormal - 2.0 * std::abs(QVector3D::dotProduct(overlap, collision_N)));
  j /= (1.0 / a.getMass()) + (1.0 / b.getMass());

  QVector3D force = j * collision_N * sec_elapsed;

//  a.setVelocity(k - a.getVelocity());
//  b.setVelocity(k - b.getVelocity());

  a.push(force);
  b.push(-force);
}

void HookesLawClockwork::collideSolidAndWall(Entity &solid, Entity &wall, const QVector3D &overlap, const QVector3D &collision_N,
                                             float sec_elapsed)
{
  const auto rv = wall.getVelocity() - solid.getVelocity();
  const auto velAlongNormal = QVector3D::dotProduct(rv, collision_N);

  if(velAlongNormal > 0) return;

  float e = std::min<float>(solid.getRestitution(), wall.getRestitution());

  float j = (1.0 + e) * (velAlongNormal - 2.0 * std::abs(QVector3D::dotProduct(overlap, collision_N)));
//  float j = (1 + e) * (velAlongNormal - overlap.length());
  j /= 1.0 / solid.getMass();

  QVector3D force = j * collision_N * sec_elapsed;
  solid.push(force);
}

void HookesLawClockwork::collideWall(Entity &a, Entity &b, const QVector3D &overlap, const QVector3D &collision_N,
                                     float sec_elapsed)
{
  if(collision_N.isNull()) return;

  const auto rv = b.getVelocity() - a.getVelocity();

  const auto velAlongNormal = QVector3D::dotProduct(rv, collision_N);

  if(velAlongNormal > 0) return;

  float e = std::min<float>(a.getRestitution(), b.getRestitution());

  float j = (1.0 + e) * (velAlongNormal - 2.0 * std::abs(QVector3D::dotProduct(overlap, collision_N)));

  QVector3D force = j * collision_N * sec_elapsed;

  a.setVelocity(a.getVelocity() + force);
  b.setVelocity(b.getVelocity() - force);
}

void HookesLawClockwork::collideEntityAndNull(Entity &a, const QVector3D &overlap, const QVector3D &collision_N, float sec_elapsed)
{
  if(qFuzzyIsNull(a.getMass()))
    collideWallAndNull(a, overlap, collision_N, sec_elapsed);
  else
    collideSolidAndNull(a, overlap, collision_N, sec_elapsed);
}

void HookesLawClockwork::collideSolidAndNull(Entity &a, const QVector3D &overlap, const QVector3D &collision_N, float sec_elapsed)
{
  const auto rv = -a.getVelocity();
  const auto velAlongNormal = QVector3D::dotProduct(rv, collision_N);

  if(velAlongNormal > 0) return;

  float e = a.getRestitution();

  float j = (1.0 + e) * (velAlongNormal - 2.0 * std::abs(QVector3D::dotProduct(overlap, collision_N)));
  j /= 1.0 / a.getMass();

  QVector3D force = j * collision_N * sec_elapsed;
  a.push(force);
}

void HookesLawClockwork::collideWallAndNull(Entity &a, const QVector3D &overlap, const QVector3D &collision_N, float sec_elapsed)
{
  if(collision_N.isNull()) return;

  const auto rv = -a.getVelocity();

  const auto velAlongNormal = QVector3D::dotProduct(rv, collision_N);

  if(velAlongNormal > 0) return;

  float e = a.getRestitution();

  float j = (1.0 + e) * (velAlongNormal - 2.0 * std::abs(QVector3D::dotProduct(overlap, collision_N)));

  QVector3D force = j * collision_N * sec_elapsed;

  a.setVelocity(a.getVelocity() + force);
}
