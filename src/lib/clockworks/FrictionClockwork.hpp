#ifndef FRICTIONCLOCKWORK_HPP
#define FRICTIONCLOCKWORK_HPP

#include "Clockwork.hpp"

class KAWAIIWORLDS_SHARED_EXPORT FrictionClockwork : public ClockworkCollision
{
  static AdapterRegistrator<FrictionClockwork> reg;
public:
  FrictionClockwork();

  // Clockwork interface
public:
  void applyCollision(const CollisionInfo &collision, float sec_elapsed) override;
};

inline QDataStream& operator<<(QDataStream &st, const FrictionClockwork&) { return st; }
inline QDataStream& operator>>(QDataStream &st, FrictionClockwork&) { return st; }

#endif // FRICTIONCLOCKWORK_HPP
