#ifndef BARIERCLOCKWORK_HPP
#define BARIERCLOCKWORK_HPP

#include "Clockwork.hpp"
#include <optional>

class KAWAIIWORLDS_SHARED_EXPORT BarierClockwork : public ClockworkSingle
{
  static AdapterRegistrator<BarierClockwork> reg;
public:
  BarierClockwork();

  std::optional<float> min_x = {};
  std::optional<float> max_x = {};

  std::optional<float> min_y = {};
  std::optional<float> max_y = {};

  std::optional<float> min_z = {};
  std::optional<float> max_z = {};

  std::optional<float> frictionFactor = {};

  // ClockworkSingle interface
public:
  void applySingle(Entity *a, float sec_elapsed) override;
};

KAWAIIWORLDS_SHARED_EXPORT QDataStream& operator<<(QDataStream &st, const BarierClockwork &obj);
KAWAIIWORLDS_SHARED_EXPORT QDataStream& operator>>(QDataStream &st, BarierClockwork &obj);

#endif // BARIERCLOCKWORK_HPP
