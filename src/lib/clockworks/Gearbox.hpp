#ifndef GEARBOX_HPP
#define GEARBOX_HPP

#include "bodies/ModelInstanceBody.hpp"
#include "Clockwork.hpp"
#include "Entity.hpp"

#include <Kawaii3D/Renderpass/KawaiiSceneLayer.hpp>
#include <Kawaii3D/Geometry/KawaiiModel3D.hpp>
#include <sib_utils/Serializable.hpp>
#include <btBulletDynamicsCommon.h>
#include <vector>

class World;

class KAWAIIWORLDS_SHARED_EXPORT Gearbox: public QObject, public sib_utils::Serializable
{
  Q_OBJECT

  Q_PROPERTY(float     bulletFixedTimeStepSec   READ getBulletFixedTimeStepSec   WRITE setBulletFixedTimeStepSec   NOTIFY bulletFixedTimeStepSecChanged FINAL)
  Q_PROPERTY(bool      useBulletSimulation      READ getUseBulletSimulation      WRITE setUseBulletSimulation      NOTIFY useBulletSimulationChanged FINAL)
  Q_PROPERTY(bool      bulletStepsInterpolation READ getBulletStepsInterpolation WRITE setBulletStepsInterpolation NOTIFY bulletStepsInterpolationChanged FINAL)
  Q_PROPERTY(bool      detectCollisions         READ getDetectCollisions         WRITE setDetectCollisions         NOTIFY detectCollisionsChanged FINAL)
  Q_PROPERTY(QVariant  bulletGravity            READ getBulletGravity            WRITE setBulletGravity            NOTIFY bulletGravityChanged FINAL)

public:
  Gearbox(std::string_view adapterId);
  Gearbox(std::string_view adapterId, World &world);
  virtual ~Gearbox() = default;

  size_t addClockwork(std::unique_ptr<Clockwork> &&clockwork);
  void swapClockworks(size_t i1, size_t i2);
  Clockwork& getClockwork(size_t i) const;
  void forallClockworks(const std::function<void(Clockwork&)> &func) const;
  void removeClockwork(size_t i);

  std::unordered_set<Entity*> allEntitiesAt(const QPointF &screenPos, const KawaiiSceneLayer &sceneLayer);
  std::unordered_set<Entity*> allEntitiesAt(const QPointF &screenPos, const KawaiiSceneLayer &sceneLayer, const std::function<bool (Entity &)> &func);
  Entity *nearestEntityAt(const QPointF &screenPos, const KawaiiSceneLayer &sceneLayer);
  Entity *nearestEntityAt(const QPointF &screenPos, const KawaiiSceneLayer &sceneLayer, const std::function<bool (Entity &)> &func);

  void tick(float secElapsed);

  float getBulletFixedTimeStepSec() const;
  void setBulletFixedTimeStepSec(float newBulletFixedTimeStepSec);

  bool getUseBulletSimulation() const;
  void setUseBulletSimulation(bool newUseBulletSimulation);

  bool getBulletStepsInterpolation() const;
  void setBulletStepsInterpolation(bool newBulletStepsInterpolation);

  bool getDetectCollisions() const;
  void setDetectCollisions(bool newDetectCollisions);

  QVector3D getBulletGravity() const;
  void setBulletGravity(const QVariant &newBulletGravity);

  bool canPlace(btCollisionObject &btObject, const QVector3D &pos) const;
  bool canPlace(KawaiiModel3D *model, const QVector3D &pos) const;

  virtual void addCollisionObject(btCollisionObject &obj);
  virtual void removeCollisionObject(btCollisionObject &obj);

  bool getProcessingTick() const;

  void saveClockworks(QDataStream &st) const;
  void loadClockworks(QDataStream &st);

  static void loadClockworksStub(QDataStream &st);


  inline static std::unique_ptr<Gearbox> loadFrom(QDataStream &st, World &w)
  {
    auto res = sib_utils::Serializable::loadFrom(st, adapters);
    auto gearboxPtr = std::unique_ptr<Gearbox>(static_cast<Gearbox*>(res.release()));
    gearboxPtr->world = &w;
    gearboxPtr->init();
    return gearboxPtr;
  }

  inline static std::unique_ptr<Gearbox> create(std::string_view type, World &w)
  {
    auto el = adapters.find(type);
    if(el == adapters.end())
      return std::unique_ptr<Gearbox>();

    auto result = el->second.creator();
    auto gearboxPtr = std::unique_ptr<Gearbox>(static_cast<Gearbox*>(result.release()));
    gearboxPtr->world = &w;
    gearboxPtr->init();
    return std::unique_ptr<Gearbox>(static_cast<Gearbox*>(gearboxPtr.release()));
  }

  inline static std::unique_ptr<Gearbox> create(const QString &type, World &w)
  {
    const auto typeUtf8 = type.toUtf8();
    return create({typeUtf8.constData(), static_cast<size_t>(typeUtf8.size())}, w);
  }

signals:
  void useBulletSimulationChanged();
  void bulletFixedTimeStepSecChanged();
  void bulletGravityChanged();
  void bulletStepsInterpolationChanged();

  void detectCollisionsChanged();


protected:
  static std::unordered_map<std::string_view, ByteAdapter> adapters;

  template<class T> using AdapterRegistrator = sib_utils::Serializable::AdapterRegistrator<T, adapters>;

  /// Should be called in the constructor of derived classes
  void init(std::unique_ptr<btDynamicsWorld> &&newCollisionWorld);

  void init();

  void destroyCollisionWorld();



  // IMPLEMENT
private:
  World *world;
  std::unique_ptr<btDynamicsWorld> collisionWorld;

  std::vector<std::unique_ptr<Clockwork>> clockworks;

  std::vector<std::unique_ptr<ModelInstanceBody>> debugRayTestModelInstances;

  QFuture<void> bulletTickTask;

  float bulletFixedTimeStepSec;

  bool useBulletSimulation;
  bool bulletStepsInterpolation;
  bool detectCollisions;
  bool gImpactEnabled;

  bool processingTick;

  void debugRayTest(const glm::vec4 &rayFrom, const glm::vec4 &rayTo);

  template<typename T, typename... ArgsT>
  T rayTest(glm::vec4 &&rayFrom, glm::vec4 &&rayTo, ArgsT&&... args)
  {
    rayFrom /= rayFrom.w;
    rayTo /= rayTo.w;

    btVector3 btRayFrom (rayFrom.x, rayFrom.y, rayFrom.z),
        btRayTo (rayTo.x, rayTo.y, rayTo.z);
    T rayResult(btRayFrom, btRayTo, std::forward<ArgsT>(args)...);
    debugRayTest(rayFrom, rayTo);
    validate();
    collisionWorld->updateAabbs();
    collisionWorld->computeOverlappingPairs();
    collisionWorld->rayTest(btRayFrom, btRayTo, rayResult);
    return rayResult;
  }

  template<typename T, typename... ArgsT>
  T rayTest(const QPointF &screenPos, const KawaiiSceneLayer &sceneLayer, ArgsT&&... args)
  {
    // See also https://halogenica.net/software/ray-casting-and-picking-using-bullet-physics/

    glm::vec4 rayStart_NDC(
          2.0*screenPos.x()-1.0,
          -2.0*screenPos.y()+1.0,
          -1.0,
          1.0f
          );

    glm::vec4 rayEnd_NDC(
          2.0*screenPos.x()-1.0,
          -2.0*screenPos.y()+1.0,
          0.0,
          1.0f
          );

    glm::mat4 mat = glm::inverse(sceneLayer.getViewProjectionMatrix()) * sceneLayer.getClipCorrection();
    glm::vec4 rayStart_world = mat * rayStart_NDC;
    rayStart_world /= rayStart_world.w;

    glm::vec4 rayEnd_world = mat * rayEnd_NDC;
    rayEnd_world /= rayEnd_world.w;

    glm::vec4 rayDir_world = glm::normalize(rayEnd_world - rayStart_world);

    const float farClip = sceneLayer.getProjectionPtr()->getFarClip();
    rayEnd_world = rayStart_world + rayDir_world * farClip;
    rayEnd_world.w = 1;

    return rayTest<T, ArgsT...>(std::move(rayStart_world), std::move(rayEnd_world), std::forward<ArgsT>(args)...);
  }

  static Entity* getEntity(const btCollisionObject *btObj);

  void attachEntityBody(Entity *e);
  void detachEntityBody(Entity *e);
  void connectEntity(Entity *e);
  void disconnectEntity(Entity *e);

  bool handleCollision(const btCollisionObject *a, const btCollisionObject *b, const btVector3 &normal, btScalar combinedFriction);
  static void btTickCallback(btDynamicsWorld *dynamicsWorld, btScalar timeStep);

  void validate();

  void postTickActions(float secElapsed, bool bulletTick);

  void addConstraint(btTypedConstraint *constraint);
  void removeConstraint(btTypedConstraint *constraint);
};


KAWAIIWORLDS_SHARED_EXPORT QDataStream& operator<<(QDataStream &st, const Gearbox &obj);
KAWAIIWORLDS_SHARED_EXPORT QDataStream& operator>>(QDataStream &st, Gearbox &obj);

#endif // GEARBOX_HPP
