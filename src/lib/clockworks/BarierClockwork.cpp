#include "BarierClockwork.hpp"
#include "World.hpp"
#include <sib_utils/qDataStreamExpansion.hpp>

BarierClockwork::BarierClockwork():
  ClockworkSingle(reg.adapterId)
{
}

namespace {
  bool operator >=(float a, const std::optional<float> &b)
  {
    return b.has_value() && a >= *b;
  }

  bool operator <=(float a, const std::optional<float> &b)
  {
    return b.has_value() && a <= *b;
  }

  QVector3D abs(const QVector3D &x)
  { return QVector3D(std::abs(x.x()), std::abs(x.y()), std::abs(x.z())); }
}

void BarierClockwork::applySingle(Entity *a, [[maybe_unused]] float sec_elapsed)
{
  if(!a->getBody()) return;

  auto [min, max] = a->getAABB(glm::mat4(1.0));

  const float frictionFactor_v = frictionFactor.value_or(0);
  QVector3D delta;

  World *w = a->getWorld();
  if(max.x() >= max_x)
    {
      QVector3D overlap(*max_x - max.x(), 0, 0);
      delta += overlap;

      const CollisionInfo collision = {
        .normal = QVector3D(1, 0, 0),
        .overlap = abs(overlap),
        .a = a,
        .b = nullptr,
        .friction = w->mixFrictionFactor(a->getFriction(), frictionFactor_v)
      };
      w->addCollision(collision);
    }
  if(min.x() <= min_x)
    {
      QVector3D overlap(*min_x - min.x(), 0, 0);
      delta += overlap;

      const CollisionInfo collision = {
        .normal = QVector3D(-1, 0, 0),
        .overlap = abs(overlap),
        .a = a,
        .b = nullptr,
        .friction = w->mixFrictionFactor(a->getFriction(), frictionFactor_v)
      };
      w->addCollision(collision);
    }

  if(max.y() >= max_y)
    {
      QVector3D overlap(0, *max_y - max.y(), 0);
      delta += overlap;

      const CollisionInfo collision = {
        .normal = QVector3D(0, 1, 0),
        .overlap = abs(overlap),
        .a = a,
        .b = nullptr,
        .friction = w->mixFrictionFactor(a->getFriction(), frictionFactor_v)
      };
      w->addCollision(collision);
    }
  if(min.y() <= min_y)
    {
      QVector3D overlap(0, *min_y - min.y(), 0);
      delta += overlap;

      const CollisionInfo collision = {
        .normal = QVector3D(0, -1, 0),
        .overlap = abs(overlap),
        .a = a,
        .b = nullptr,
        .friction = w->mixFrictionFactor(a->getFriction(), frictionFactor_v)
      };
      w->addCollision(collision);
    }

  if(max.z() >= max_z)
    {
      QVector3D overlap(0, 0, *max_z - max.z());
      delta += overlap;

      const CollisionInfo collision = {
        .normal = QVector3D(0, 0, 1),
        .overlap = abs(overlap),
        .a = a,
        .b = nullptr,
        .friction = w->mixFrictionFactor(a->getFriction(), frictionFactor_v)
      };
      w->addCollision(collision);
    }
  if(min.z() <= min_z)
    {
      QVector3D overlap(0, 0, *min_z - min.z());
      delta += overlap;

      const CollisionInfo collision = {
        .normal = QVector3D(0, 0, -1),
        .overlap = abs(overlap),
        .a = a,
        .b = nullptr,
        .friction = w->mixFrictionFactor(a->getFriction(), frictionFactor_v)
      };
      w->addCollision(collision);
    }
  a->setPosition(a->getPosition() + delta);
}

KAWAIIWORLDS_SHARED_EXPORT QDataStream &operator<<(QDataStream &st, const BarierClockwork &obj)
{
  return st
      << obj.min_x
      << obj.max_x
      << obj.min_y
      << obj.max_y
      << obj.min_z
      << obj.max_z
      << obj.frictionFactor
         ;
}

KAWAIIWORLDS_SHARED_EXPORT QDataStream &operator>>(QDataStream &st, BarierClockwork &obj)
{
  return st
      >> obj.min_x
      >> obj.max_x
      >> obj.min_y
      >> obj.max_y
      >> obj.min_z
      >> obj.max_z
      >> obj.frictionFactor
         ;
}
