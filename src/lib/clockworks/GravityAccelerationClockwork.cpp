#include "GravityAccelerationClockwork.hpp"

GravityAccelerationClockwork::GravityAccelerationClockwork(const QVector3D &acceleration):
  ClockworkSingle(reg.adapterId),
  acceleration(acceleration)
{
}

void GravityAccelerationClockwork::applySingle(Entity *a, float sec_elapsed)
{
  const QVector3D gravityEffect = acceleration * sec_elapsed;
  a->setVelocity(a->getVelocity() + gravityEffect);
}

KAWAIIWORLDS_SHARED_EXPORT QDataStream &operator<<(QDataStream &st, const GravityAccelerationClockwork &obj)
{
  return st << obj.acceleration;
}

KAWAIIWORLDS_SHARED_EXPORT QDataStream &operator>>(QDataStream &st, GravityAccelerationClockwork &obj)
{
  return st >> obj.acceleration;
}
