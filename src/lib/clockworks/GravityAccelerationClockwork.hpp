#ifndef GRAVITYACCELERATIONCLOCKWORK_HPP
#define GRAVITYACCELERATIONCLOCKWORK_HPP

#include "Clockwork.hpp"
#include <QVector3D>

class KAWAIIWORLDS_SHARED_EXPORT GravityAccelerationClockwork : public ClockworkSingle
{
  static AdapterRegistrator<GravityAccelerationClockwork> reg;
public:
  QVector3D acceleration;

  GravityAccelerationClockwork(const QVector3D &acceleration = QVector3D(0, -9.8, 0));

  // ClockworkSingle interface
public:
  void applySingle(Entity *a, float sec_elapsed) override;
};

KAWAIIWORLDS_SHARED_EXPORT QDataStream& operator<<(QDataStream &st, const GravityAccelerationClockwork &obj);
KAWAIIWORLDS_SHARED_EXPORT QDataStream& operator>>(QDataStream &st, GravityAccelerationClockwork &obj);

#endif // GRAVITYACCELERATIONCLOCKWORK_HPP
