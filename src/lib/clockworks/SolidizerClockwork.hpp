#ifndef SOLIDIZERCLOCKWORK_HPP
#define SOLIDIZERCLOCKWORK_HPP

#include "Clockwork.hpp"

class KAWAIIWORLDS_SHARED_EXPORT SolidizerClockwork : public ClockworkCollision
{
  static AdapterRegistrator<SolidizerClockwork> reg;
public:
  SolidizerClockwork();

  // Clockwork interface
public:
  void applyCollision(const CollisionInfo &collision, float sec_elapsed) override;
};

inline QDataStream& operator<<(QDataStream &st, const SolidizerClockwork&) { return st; }
inline QDataStream& operator>>(QDataStream &st, SolidizerClockwork&) { return st; }

#endif // SOLIDIZERCLOCKWORK_HPP
