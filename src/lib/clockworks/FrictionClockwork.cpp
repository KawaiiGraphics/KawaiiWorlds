#include "FrictionClockwork.hpp"

FrictionClockwork::FrictionClockwork():
  ClockworkCollision(reg.adapterId)
{
}

namespace {
  template<typename T>
  T sign(T x)
  {
    if(x == 0)
      return 0;
    else if(x>0)
      return static_cast<T>(1);
    else
      return static_cast<T>(-1);
  }
}

void FrictionClockwork::applyCollision(const CollisionInfo &collision, float sec_elapsed)
{
  if(!collision.a && collision.b)
    return applyCollision({.normal = collision.normal, .overlap = collision.overlap, .a = collision.b, .b = collision.a, .friction = collision.friction}, sec_elapsed);

  if(!collision.a && !collision.a->getBody()) return;

  QVector3D rel_vel = -collision.a->getVelocity();
  float denum = 1.0 / collision.a->getMass();
  float restitution = collision.a->getRestitution();
  if(collision.b && collision.b->getBody())
    {
      rel_vel += collision.b->getVelocity();
      denum += 1.0 / collision.b->getMass();
      if(restitution > collision.b->getRestitution())
        restitution = collision.b->getRestitution();
    }

  const auto velAlongNormal = QVector3D::dotProduct(rel_vel, -collision.normal);

  float j = (1.0 + restitution) * (velAlongNormal - 2.0 * std::abs(QVector3D::dotProduct(collision.overlap, -collision.normal)));
  j /= denum;

  QVector3D tangent = rel_vel + velAlongNormal * collision.normal;
  tangent.normalize();

  float jt = -QVector3D::dotProduct(rel_vel, tangent);
  jt /= denum;

  const float j_mu = j * collision.friction;
  QVector3D force;
  if(std::abs(jt) < std::abs(j_mu))
    force = -jt * tangent;
  else
    force = -sign(j)*sign(jt)* j_mu * tangent;

  collision.a->push(force);
  if(collision.b)
    collision.b->push(-force);
}
