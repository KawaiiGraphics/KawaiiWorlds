#ifndef SOLIDGLSL_HPP
#define SOLIDGLSL_HPP

#include "KawaiiWorlds_global.hpp"
#include <Kawaii3D/Shaders/KawaiiProgram.hpp>
#include "ShaderEffectSrc.hpp"

class KAWAIIWORLDS_SHARED_EXPORT SolidGlsl : public KawaiiProgram
{
  Q_OBJECT
public:
  SolidGlsl(const ShaderEffectSrc &materialEffectSrc);
};

#endif // SOLIDGLSL_HPP
