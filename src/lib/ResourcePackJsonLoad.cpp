#include "ResourcePackJsonLoad.hpp"
#include "SolidGlsl.hpp"
#include "SkyboxGlsl.hpp"
#include "LandscapeGlsl.hpp"
#include "EffectGlsl.hpp"

#include <sib_utils/Memento/BlobLoader.hpp>
#include <sib_utils/jsonLoaders.hpp>
#include <sib_utils/ioReadAll.hpp>
#include <QtConcurrent>
#include <QQuaternion>
#include <QJsonArray>
#include <QVector3D>
#include <iostream>

#include <Kawaii3D/Renderpass/KawaiiSceneLayer.hpp>
#include <Kawaii3D/Renderpass/KawaiiShaderEffect.hpp>
#include <Kawaii3D/Renderpass/KawaiiTexLayer.hpp>
#include <Kawaii3D/KawaiiPerspective.hpp>
#include <Kawaii3D/KawaiiOrtho.hpp>
#include <Kawaii3D/KawaiiRoot.hpp>

void ResourcePackJsonLoader::loadJson(const QJsonObject &json, ResourcePack &res) const
{
  using namespace sib_utils::json;

  if(json.contains(QLatin1String("memento")))
    {
      std::unique_ptr<sib_utils::memento::Memento> rootMemento;
      rootMemento.reset(sib_utils::memento::BlobLoader().load(QFile(rootDir.absoluteFilePath(json.value("memento").toString()))));
      KawaiiDataUnit::getMementoFactory().loadChildren(*rootMemento, &res);
    }
  for(const auto &i: json.value(QLatin1String("textures")).toArray())
    {
      const auto obj = i.toObject();
      auto tex = res.loadTexture2D(rootDir.absoluteFilePath(obj.value(QLatin1String("path")).toString()), obj.value(QLatin1String("name")).toString()).second;
      loadBaseTextureProperties(obj, tex);
    }
  for(const auto &i: json.value("cubemaps").toArray())
    {
      const auto obj = i.toObject();
      if(obj.contains(QLatin1String("path")))
        {
          auto cubemap = res.loadCubemap(rootDir.absoluteFilePath(obj.value(QLatin1String("path")).toString()),
                                         obj.value(QLatin1String("name")).toString()).second;
          loadBaseTextureProperties(obj, cubemap);
          continue;
        }
      KawaiiCubemap *cubemap = res.createChild<KawaiiCubemap>();
      cubemap->setObjectName(obj.value(QLatin1String("name")).toString());
      loadBaseTextureProperties(obj, cubemap);
      if(obj.contains(QLatin1String("resolution")))
        {
          const glm::vec2 sz = readVec2(obj.value(QLatin1String("resolution")), glm::vec2(128,128));
          for(uint16_t i = 0; i < 6; ++i)
            cubemap->setImage(i, QImage(sz.x, sz.y, QImage::Format_RGBA8888));
          continue;
        }

      struct Task {
        QString fName;
        uint16_t sideIndex;
      };
      QVector<Task> cubemapLoader = {
        {
          .fName = rootDir.absoluteFilePath(obj.value(QLatin1String("right_path")).toString()),
          .sideIndex = KawaiiCubemap::PositiveX
        },
        {
          .fName = rootDir.absoluteFilePath(obj.value(QLatin1String("left_path")).toString()),
          .sideIndex = KawaiiCubemap::NegativeX
        },
        {
          .fName = rootDir.absoluteFilePath(obj.value(QLatin1String("front_path")).toString()),
          .sideIndex = KawaiiCubemap::PositiveZ
        },
        {
          .fName = rootDir.absoluteFilePath(obj.value(QLatin1String("back_path")).toString()),
          .sideIndex = KawaiiCubemap::NegativeZ
        },
        {
          .fName = rootDir.absoluteFilePath(obj.value(QLatin1String("top_path")).toString()),
          .sideIndex = KawaiiCubemap::PositiveY
        },
        {
          .fName = rootDir.absoluteFilePath(obj.value(QLatin1String("bottom_path")).toString()),
          .sideIndex = KawaiiCubemap::NegativeY
        }
      };
      class LoadCubemapRunnable: public QRunnable
      {
      public:
        KawaiiCubemap *cubemap;
        const QVector<Task> cubemapLoader;

        LoadCubemapRunnable(KawaiiCubemap *cubemap, QVector<Task> &&cubemapLoader):
          cubemap(cubemap),
          cubemapLoader(std::move(cubemapLoader))
        {
          setAutoDelete(true);
        }

        void run() override
        {
          QtConcurrent::map(cubemapLoader, [this] (const Task &t) {
              cubemap->changeImage(t.sideIndex, [&t] (QImage &img) { return img.load(t.fName); });
            }).waitForFinished();
        }
      };

      res.getAssetLoadPool().start(new LoadCubemapRunnable(cubemap, std::move(cubemapLoader)));
    }
  for(const auto &i: json.value("models").toArray())
    {
      const auto obj = i.toObject();
      const auto modelName = obj.value(QLatin1String("name")).toString();
      auto loadTask = res.loadModel(rootDir.absoluteFilePath(obj.value(QLatin1String("path")).toString()), modelName);
      if(obj.contains(QLatin1String("scale")))
        {
          loadTask.first.waitForFinished();
          const glm::vec3 factor = readVec3(obj.value(QLatin1String("scale")), glm::vec3(1,1,1));
          loadTask.second->scale(QVector3D(factor.x, factor.y, factor.z));
        } else if(obj.contains(QLatin1String("resize_to")))
        {
          loadTask.first.waitForFinished();
          const glm::vec3 sz = readVec3(obj.value(QLatin1String("resize_to")), glm::vec3(1,1,1));
          loadTask.second->setSize(QVector3D(sz.x, sz.y, sz.z));
        } else if(obj.contains(QLatin1String("set_major_side")))
        {
          loadTask.first.waitForFinished();
          loadTask.second->setMajorSide(obj.value(QLatin1String("set_major_side")).toDouble(1.0));
        }
      if(obj.contains(QLatin1String("rotate")))
        {
          loadTask.first.waitForFinished();
          const glm::vec3 angles = readVec3(obj.value(QLatin1String("rotate")), glm::vec3(0,0,0));
          loadTask.second->rotate(QQuaternion::fromEulerAngles(angles.x, angles.y, angles.z));
        }
      if(obj.contains(QLatin1String("additional_animations"))
         && obj.value(QLatin1String("additional_animations")).isArray())
        {
          for(const auto &addAnimJson: obj.value(QLatin1String("additional_animations")).toArray())
            res.loadAnimation(rootDir.absoluteFilePath(addAnimJson.toString()), modelName);
        }
    }
  for(const auto &i: json.value("music").toArray())
    {
      const auto obj = i.toObject();
      auto snd = new Sound(rootDir.absoluteFilePath(obj.value(QLatin1String("path")).toString()));
      snd->setObjectName(obj.value(QLatin1String("name")).toString());
      snd->updateParent(&res);
    }

  for(const auto &i: json.value("shaders").toArray())
    {
      const auto obj = i.toObject();
      ShaderEffectSrc effect;
      if(obj.contains("illumination_name"))
        effect.illuminationName = obj.value("illumination_name").toString();

      const QString name = obj.value(QLatin1String("name")).toString();
      if(auto jsonParams = obj.value(QLatin1String("parameters")); jsonParams.isObject())
        for(const auto &j: jsonParams.toObject().keys())
          effect.shaderParams[j] = jsonParams.toObject().value(j).toString();

      static const QHash<QString, KawaiiProgram::CullMode> knownCullModes = {
        {QStringLiteral("cull_none"), KawaiiProgram::CullMode::CullNone},
        {QStringLiteral("cull_back"), KawaiiProgram::CullMode::CullBack},
        {QStringLiteral("cull_front"), KawaiiProgram::CullMode::CullFront},
        {QStringLiteral("cull_all"), KawaiiProgram::CullMode::CullAll},
      };
      effect.cullMode = knownCullModes.value(obj.value(QLatin1String("cull")).toString(), KawaiiProgram::CullMode::CullNone);
      if(!obj.contains(QLatin1String("suppress_shadows")))
        effect.shadowMode = ShaderEffectSrc::ShadowMode::Default;
      else
        effect.shadowMode = obj.value(QLatin1String("suppress_shadows")).toBool()?
              ShaderEffectSrc::ShadowMode::Suppress:
              ShaderEffectSrc::ShadowMode::DontSuppress;

      if(obj.contains("internal"))
        {
          const QString internalName = obj.value("internal").toString().toLower();
          const static QHash<QString, ResourcePack::InternalShaderEffect> knownInternalEffects = {
            {QStringLiteral("diffuse_texture_pbr"), ResourcePack::InternalShaderEffect::DiffuseTexturePbr},
            {QStringLiteral("diffuse_texture_phong"), ResourcePack::InternalShaderEffect::DiffuseTexturePhong},
            {QStringLiteral("diffuse_texture_cel"), ResourcePack::InternalShaderEffect::DiffuseTextureCel},
            {QStringLiteral("blur_4x4"), ResourcePack::InternalShaderEffect::Blur4x4},
            {QStringLiteral("ssao"), ResourcePack::InternalShaderEffect::SSAO},
            {QStringLiteral("gaussian_blur"), ResourcePack::InternalShaderEffect::GaussianBlur},
            {QStringLiteral("irradiance_convolution"), ResourcePack::InternalShaderEffect::IrradianceConvolution},
            {QStringLiteral("gamma_and_exposure"), ResourcePack::InternalShaderEffect::GammaAndExposure},
            {QStringLiteral("brightness_and_contrast"), ResourcePack::InternalShaderEffect::BrightnessAndContrast},
            {QStringLiteral("color_correction"), ResourcePack::InternalShaderEffect::ColorCorrection},
          };

          if(auto el = knownInternalEffects.find(internalName); el != knownInternalEffects.end())
            res.loadInternalShaderEffect(el.value(), effect);
          else
            std::cerr << "Unknown internal shader effect \"" << internalName.toStdString() << "\"" << std::endl;
        } else
        {
          if(obj.contains("fragment_src"))
            effect.fragmentSrc = obj.value("fragment_src").toString();
          else if(obj.contains("fragment_path"))
            effect.fragmentSrc = sib_utils::ioReadAll(QFile(rootDir.absoluteFilePath(obj.value("fragment_path").toString())));

          if(obj.contains("vertex_src"))
            effect.vertexSrc = obj.value("vertex_src").toString();
          else if(obj.contains("vertex_path"))
            effect.vertexSrc = sib_utils::ioReadAll(QFile(rootDir.absoluteFilePath(obj.value("vertex_path").toString())));
        }
      res.addShaderEffect(name, effect);
    }

  for(const auto &i: json.value("framebuffers").toArray())
    {
      const auto obj = i.toObject();
      KawaiiCamera *cam = res.getCamera(obj.value(QLatin1String("camera")).toString());
      if(cam)
        res.createFbo(obj.value(QLatin1String("name")).toString(), obj.value(QLatin1String("color_texture")).toString(), cam);
      else
        {
          KawaiiRenderpass *rp = res.createChild<KawaiiRenderpass>();
          bool readed = readRenderpass(obj.value(QLatin1String("renderpass")).toObject(), *rp);
          if(readed)
            {
              KawaiiTexture *tex = nullptr;
              if(auto colorTexJson = obj.value(QLatin1String("color_texture")); colorTexJson.isString())
                tex = res.getTexture2D(colorTexJson.toString());
              else if(auto colorTexJson = obj.value(QLatin1String("color_cubemap")); colorTexJson.isString())
                tex = res.getTextureCube(colorTexJson.toString());
              if(tex)
                {
                  res.waitTasks();
                  auto *fbo = res.createFbo(obj.value(QLatin1String("name")).toString(), tex, rp);
                  if(rp->objectName().isEmpty() || rp->objectName().isNull())
                    rp->setObjectName(fbo->objectName() + QStringLiteral(".renderpass"));
                } else
                delete rp;
            } else
            delete rp;
        }
    }
  for(const auto &i: json.value("envmaps").toArray())
    {
      const auto obj = i.toObject();
      auto envMap = res.createEnvMap(obj.value(QLatin1String("name")).toString(),
                                     obj.value(QLatin1String("color_texture")).toString(),
                                     readVec3(obj.value(QLatin1String("position")), glm::vec3(0,0,0)));

      if(obj.contains(QLatin1String("near_clip")))
        envMap->setNearClip(obj.value(QLatin1String("near_clip")).toDouble());
      if(obj.contains(QLatin1String("far_clip")))
        envMap->setNearClip(obj.value(QLatin1String("far_clip")).toDouble());
    }

  for(const auto &i: json.value(QLatin1String("solid_materials")).toArray())
    loadMaterial<SolidGlsl>(i.toObject(), res);

  for(const auto &i: json.value(QLatin1String("skybox_materials")).toArray())
    loadMaterial<SkyboxGlsl>(i.toObject(), res);

  for(const auto &i: json.value(QLatin1String("landscape_materials")).toArray())
    loadMaterial<LandscapeGlsl>(i.toObject(), res);

  for(const auto &i: json.value(QLatin1String("secondary_scenes")).toArray())
    {
      const auto sceneJson = i.toObject();
      auto *scene = res.addScene(sceneJson.value(QLatin1String("name")).toString());
      if(sceneJson.contains(QLatin1String("suppress_shadows")))
        scene->setProperty("suppress_shadows", sceneJson.value(QLatin1String("suppress_shadows")).toBool());

      for(const auto &i: sceneJson.value(QLatin1String("solid_materials")).toArray())
        {
          const auto obj = i.toObject();
          res.addMaterialToScene<SolidGlsl>(scene,
                                            obj.value(QLatin1String("name")).toString(),
                                            obj.value("shader").toString());
        }

      for(const auto &i: sceneJson.value(QLatin1String("skybox_materials")).toArray())
        {
          const auto obj = i.toObject();
          res.addMaterialToScene<SkyboxGlsl>(scene,
                                             obj.value(QLatin1String("name")).toString(),
                                             obj.value("shader").toString());
        }

      for(const auto &i: sceneJson.value(QLatin1String("landscape_materials")).toArray())
        {
          const auto obj = i.toObject();
          res.addMaterialToScene<LandscapeGlsl>(scene,
                                                obj.value(QLatin1String("name")).toString(),
                                                obj.value("shader").toString());
        }
    }

  KawaiiRenderpass *rp = res.createChild<KawaiiRenderpass>();
  bool readed = readRenderpass(json.value(QLatin1String("renderpass")).toObject(), *rp);
  if(readed)
    {
      if(rp->objectName().isEmpty() || rp->objectName().isNull())
        rp->setObjectName(QStringLiteral("MainRenderpass"));
      res.setMainRenderpass(rp);
    } else
    delete rp;
  res.waitTasks();
}

void ResourcePackJsonLoader::loadBufferElement(const QJsonValue &j, const ResourcePack &res, KawaiiGpuBuf *buf, std::vector<std::byte> &content, size_t &index) const
{
  using namespace sib_utils::json;

  const QString valType = j.toObject().value(QLatin1String("type")).toString().toLower();
  const QJsonValue val = j.toObject().value(QLatin1String("value"));

  if(valType == QLatin1String("float"))
    {
      content.resize(content.size() + sizeof(float));
      *reinterpret_cast<float*>(&content[index]) = val.toDouble(0);
      index += sizeof(float);
    } else if(valType == QLatin1String("vec2"))
    {
      content.resize(content.size() + sizeof(glm::vec3));
      *reinterpret_cast<glm::vec2*>(&content[index]) = readVec2(val, glm::vec2());
      index += sizeof(glm::vec2);
    } else if(valType == QLatin1String("vec3"))
    {
      content.resize(content.size() + sizeof(glm::vec4));
      *reinterpret_cast<glm::vec4*>(&content[index]) = glm::vec4(readVec3(val, glm::vec3()), 0.0);
      index += sizeof(glm::vec4);
    } else if(valType == QLatin1String("vec4"))
    {
      content.resize(content.size() + sizeof(glm::vec4));
      *reinterpret_cast<glm::vec4*>(&content[index]) = readVec4(val, glm::vec4());
      index += sizeof(glm::vec4);
    } else if(valType == QLatin1String("mat4"))
    {
      content.resize(content.size() + sizeof(glm::mat4));
      *reinterpret_cast<glm::mat4*>(&content[index]) = readMat4(val, glm::mat4());
      index += sizeof(glm::mat4);
    } else if(valType == QLatin1String("texture2d"))
    {
      const auto binding = val.toObject();

      const QString name = binding.value(QLatin1String("name")).toString(QString());
      const QString texture = binding.value(QLatin1String("texture")).toString(name);

      if(!name.isNull())
        buf->bindTexture(name, res.getTexture2D(texture));
    } else if(valType == QLatin1String("texturecube"))
    {
      const auto binding = val.toObject();

      const QString name = binding.value(QLatin1String("name")).toString(QString());
      const QString texture = binding.value(QLatin1String("texture")).toString(name);

      if(!name.isNull())
        buf->bindTexture(name, res.getTextureCube(texture));
    } else if (valType == QLatin1String("alias"))
    {
      const auto aliasJson = val.toObject();
      const quint64 srcOffset = aliasJson.value(QLatin1String("src_offset")).toDouble(0);
      const quint64 dstOffset = index;
      const quint64 size = aliasJson.value(QLatin1String("size")).toDouble(0);
      const QString srcBufStr = aliasJson.value(QLatin1String("src")).toString(QString());
      KawaiiGpuBuf *srcBuf = findGpuBuf(srcBufStr, res);
      if(Q_UNLIKELY(!srcBuf))
        {
          qWarning("KawaiiWorlds::ResourcePackJsonLoader::loadBufferElement: partial GPU buffer alias can not be created: src buffer (\"%s\") not found!", srcBufStr.toLocal8Bit().constData());
          return;
        }
      if(Q_UNLIKELY(size == 0))
        {
          qWarning("KawaiiWorlds::ResourcePackJsonLoader::loadBufferElement: partial GPU buffer alias can not be created: alias size must be greater than 0!");
          return;
        }
      content.resize(content.size() + size);
      buf->addAlias(srcBuf, srcOffset, dstOffset, size);
      index += size;
    } else if(auto el = customUboElementLoaders.find(valType); el != customUboElementLoaders.end())
    {
      if(el.value())
        el.value()(val, content, buf, index);
    }
}

KawaiiGpuBuf *ResourcePackJsonLoader::findGpuBuf(const QString &str, const ResourcePack &res) const
{
  const auto terms = QStringView(str).split('.');
  if(terms.front() == QLatin1String("renderpass"))
    {
      if(terms.size() < 3)
        {
          qWarning("KawaiiWorlds::ResourcePackJsonLoader::findGpuBuf: For \"renderpass\" domain the renderpass name and the layer number should be specified!");
          return nullptr;
        }

      auto *rp = res.findChild<KawaiiRenderpass*>(terms[1].toString(), Qt::FindDirectChildrenOnly);
      if(!rp)
        {
          qWarning("KawaiiWorlds::ResourcePackJsonLoader::findGpuBuf: Renderpass named \"%s\" not found!", terms[1].toLocal8Bit().constData());
          return nullptr;
        }
      const qulonglong subpassIndex = terms[2].toULong();
      auto *layer = rp->getSubpass(subpassIndex).imgSrc;
      if(auto *effect = dynamic_cast<KawaiiShaderEffect*>(layer); effect)
        {
          if(terms.size() < 4)
            return effect->getSurfaceUBO();

          if(terms[3].toString() == QLatin1String("surface"))
            return effect->getSurfaceUBO();
          else if(terms[3].toString() == QLatin1String("camera"))
            return const_cast<KawaiiGpuBuf*>(effect->getCamera()->getUniforms());
          else
            {
              qWarning("KawaiiWorlds::ResourcePackJsonLoader::findGpuBuf: Unknown GPU buffer (KawaiiShaderEffect::%s)!", terms[4].toLocal8Bit().constData());
              return nullptr;
            }
        }
      else if(auto *effect = dynamic_cast<KawaiiSceneLayer*>(layer); effect)
        {
          if(terms.size() < 4)
            return effect->getSurfaceUBO();

          if(terms[3].toString() == QLatin1String("surface"))
            return effect->getSurfaceUBO();
          else if(terms[3].toString() == QLatin1String("camera"))
            return const_cast<KawaiiGpuBuf*>(effect->getCamera()->getUniforms());
          else
            {
              qWarning("KawaiiWorlds::ResourcePackJsonLoader::findGpuBuf: Unknown GPU buffer (KawaiiSceneLayer::%s)!", terms[4].toLocal8Bit().constData());
              return nullptr;
            }
        }
      else
        {
          qWarning("KawaiiWorlds::ResourcePackJsonLoader::findGpuBuf: layer #%lld of renderpass \"%s\" has no GPU buffers", subpassIndex, terms[1].toLocal8Bit().data());
          return nullptr;
        }
    }
  else if(terms.front() == QLatin1String("material"))
    {
      if(terms.size() < 2)
        {
          qWarning("KawaiiWorlds::ResourcePackJsonLoader::findGpuBuf: For \"material\" domain the material name should be specified!");
          return nullptr;
        }
      auto *material = res.findChild<KawaiiMaterial*>(terms[1].toString(), Qt::FindDirectChildrenOnly);
      if(!material)
        {
          qWarning("KawaiiWorlds::ResourcePackJsonLoader::findGpuBuf: Material named \"%s\" not found!", terms[1].toLocal8Bit().constData());
          return nullptr;
        }
      return material->getUniforms();
    }
  else if(terms.front() == QLatin1String("camera"))
    {
      if(terms.size() < 2)
        {
          qWarning("KawaiiWorlds::ResourcePackJsonLoader::findGpuBuf: For \"camera\" domain the camera name should be specified!");
          return nullptr;
        }
      auto cam = res.getCamera(terms[1].toString());
      if(!cam)
        {
          qWarning("KawaiiWorlds::ResourcePackJsonLoader::findGpuBuf: Camera named \"%s\" not found!", terms[1].toLocal8Bit().constData());
          return nullptr;
        }
      return const_cast<KawaiiGpuBuf*>(cam->getUniforms());
    } else
    {
      qWarning("KawaiiWorlds::ResourcePackJsonLoader::findGpuBuf: Unknown domain \"%s\"", terms.front().toLocal8Bit().constData());
      return nullptr;
    }
}

KawaiiGpuBuf *ResourcePackJsonLoader::readGpuBuf(const QJsonValue &ubo_json, ResourcePack &res) const
{
  if(ubo_json.isArray())
    {
      std::vector<std::byte> content;
      size_t index = 0;
      auto buf = new KawaiiGpuBuf();
      for(const auto &j: ubo_json.toArray())
        if(j.isObject())
          loadBufferElement(j, res, buf, content, index);

      buf->setData(content.data(), content.size());
      return buf;
    }
  else if(ubo_json.isString())
    {
      static const auto aliasPrefix = QLatin1String("alias:");
      QString str = ubo_json.toString().trimmed();
      if(str.startsWith(aliasPrefix))
        {
          str.remove(0, aliasPrefix.size());
          return findGpuBuf(str, res);
        }
    }
  return nullptr;
}

void ResourcePackJsonLoader::loadBaseTextureProperties(const QJsonObject &obj, KawaiiTexture *tex) const
{
  const static QHash<QString, KawaiiTextureFilter> knownTextureFilters = {
    { QStringLiteral("linear"), KawaiiTextureFilter::Linear },
    { QStringLiteral("nearest"), KawaiiTextureFilter::Nearest },
    { QStringLiteral("linear_mipmap_linear"), KawaiiTextureFilter::LinearMipmapLinear },
    { QStringLiteral("linear_mipmap_nearest"), KawaiiTextureFilter::LinearMipmapNearest },
    { QStringLiteral("nearest_mipmap_linear"), KawaiiTextureFilter::NearestMipmapLinear },
    { QStringLiteral("nearest_mipmap_nearest"), KawaiiTextureFilter::NearestMipmapNearest },
  };

  const static QHash<QString, KawaiiTextureWrapMode> knownWrapModes = {
    { QStringLiteral("repeat"), KawaiiTextureWrapMode::Repeat },
    { QStringLiteral("clamp_to_edge"), KawaiiTextureWrapMode::ClampToEdge },
    { QStringLiteral("clamp_to_border"), KawaiiTextureWrapMode::ClampToBorder },
    { QStringLiteral("mirrored_repeat"), KawaiiTextureWrapMode::MirroredRepeat },
    { QStringLiteral("mirrored_clamp_to_edge"), KawaiiTextureWrapMode::MirroredClampToEdge },
  };

  tex->setMagFilter(knownTextureFilters.value(obj.value(QLatin1String("mag_filter")).toString().toLower(), tex->getMagFilter()));
  tex->setMinFilter(knownTextureFilters.value(obj.value(QLatin1String("min_filter")).toString().toLower(), tex->getMinFilter()));
  tex->setWrapModeS(knownWrapModes.value(obj.value(QLatin1String("wrap_s")).toString().toLower(), tex->getWrapModeS()));
  tex->setWrapModeT(knownWrapModes.value(obj.value(QLatin1String("wrap_t")).toString().toLower(), tex->getWrapModeT()));
  tex->setWrapModeR(knownWrapModes.value(obj.value(QLatin1String("wrap_r")).toString().toLower(), tex->getWrapModeR()));
}

bool ResourcePackJsonLoader::readRenderpass(const QJsonObject &rp_json, KawaiiRenderpass &rp) const
{
  if(rp_json.contains(QLatin1String("name")))
    rp.setObjectName(rp_json.value(QLatin1String("name")).toString());

  static const QHash<QString, KawaiiRenderpass::InternalImgFormat> knownImgFormats = {
    { QStringLiteral("rgba8"), KawaiiRenderpass::InternalImgFormat::RGBA8_Unorm },
    { QStringLiteral("rgba8_snorm"), KawaiiRenderpass::InternalImgFormat::RGBA8_Snorm },
    { QStringLiteral("rg8"), KawaiiRenderpass::InternalImgFormat::RG8_Unorm },
    { QStringLiteral("rg8_snorm"), KawaiiRenderpass::InternalImgFormat::RG8_Snorm },
    { QStringLiteral("r8"), KawaiiRenderpass::InternalImgFormat::R8_Unorm },
    { QStringLiteral("r8_snorm"), KawaiiRenderpass::InternalImgFormat::R8_Snorm },
    { QStringLiteral("rgba32f"), KawaiiRenderpass::InternalImgFormat::RGBA32_F },
    { QStringLiteral("rgba32i"), KawaiiRenderpass::InternalImgFormat::RGBA32_SInt },
    { QStringLiteral("rgba32ui"), KawaiiRenderpass::InternalImgFormat::RGBA32_UInt },
    { QStringLiteral("rg32f"), KawaiiRenderpass::InternalImgFormat::RG32_F },
    { QStringLiteral("rg32i"), KawaiiRenderpass::InternalImgFormat::RG32_SInt },
    { QStringLiteral("rg32ui"), KawaiiRenderpass::InternalImgFormat::RG32_UInt },
    { QStringLiteral("r32f"), KawaiiRenderpass::InternalImgFormat::R32_F },
    { QStringLiteral("r32i"), KawaiiRenderpass::InternalImgFormat::R32_SInt },
    { QStringLiteral("r32ui"), KawaiiRenderpass::InternalImgFormat::R32_UInt },
    { QStringLiteral("rgba16f"), KawaiiRenderpass::InternalImgFormat::RGBA16_F },
    { QStringLiteral("rgba16i"), KawaiiRenderpass::InternalImgFormat::RGBA16_SInt },
    { QStringLiteral("rgba16ui"), KawaiiRenderpass::InternalImgFormat::RGBA16_UInt },
    { QStringLiteral("rg16f"), KawaiiRenderpass::InternalImgFormat::RG16_F },
    { QStringLiteral("rg16i"), KawaiiRenderpass::InternalImgFormat::RG16_SInt },
    { QStringLiteral("rg16ui"), KawaiiRenderpass::InternalImgFormat::RG16_UInt },
    { QStringLiteral("depth16"), KawaiiRenderpass::InternalImgFormat::Depth16 },
    { QStringLiteral("depth32"), KawaiiRenderpass::InternalImgFormat::Depth32 },
  };

  if(!rp_json.contains(QLatin1String("layers")))
    return false;

  if(rp_json.contains(QLatin1String("gbuffers")))
    if(auto val = rp_json.value(QLatin1String("gbuffers")); val.isArray())
      {
        for(const auto &i: val.toArray())
          if(i.isObject())
            {
              const auto obj = i.toObject();
              const KawaiiRenderpass::GBuffer val = {
                .format = knownImgFormats.value(obj.value(QLatin1String("format")).toString(), KawaiiRenderpass::InternalImgFormat::RGBA8_Unorm),
                .needClear = obj.value(QLatin1String("clear")).toBool(true),
                .needStore = obj.value(QLatin1String("store")).toBool(false),
              };
              rp.addGBuf(val);
            }
      }

  if(rp.gBufCount() == 0)
    {
      rp.addGBuf(KawaiiRenderpass::GBuffer { .format = KawaiiRenderpass::InternalImgFormat::RGBA8_Unorm, .needClear = true, .needStore = true });
      rp.addGBuf(KawaiiRenderpass::GBuffer { .format = KawaiiRenderpass::InternalImgFormat::Depth16, .needClear = true, .needStore = false });
    }

  ResourcePack *res = KawaiiRoot::getRoot<ResourcePack>(&rp);
  if(auto val = rp_json.value(QLatin1String("layers")); val.isArray())
    {
      for(const auto &i: val.toArray())
        if(i.isObject())
          {
            const auto obj = i.toObject();
            const QString type = obj.value(QLatin1String("type")).toString().toLower();
            KawaiiImageSource *imgSrc = nullptr;
            if(type == QStringLiteral("scene"))
              {
                auto *layer = rp.createChild<KawaiiSceneLayer>();
                const auto cam = obj.value(QLatin1String("camera")).toString();
                KawaiiCamera *camera;
                if(!obj.contains(QLatin1String("scene")))
                  camera = res->getCamera(cam);
                else
                  {
                    const auto sceneName = obj.value(QLatin1String("scene")).toString();
                    auto *scene = res->getScene(sceneName);
                    if(!scene)
                      {
                        camera = nullptr;
                        qWarning("KawaiiWorlds::ResourcePackJsonLoader::readRenderpass: Read scene layer: scene \"%s\" not found!", sceneName.toLocal8Bit().constData());
                      } else
                      {
                        camera = scene->findChild<KawaiiCamera*>(rp.objectName() + QStringLiteral(".") + cam, Qt::FindDirectChildrenOnly);
                        if(!camera)
                          {
                            auto *origCam = res->getCamera(cam);
                            if(origCam)
                              {
                                camera = scene->createChild<KawaiiCamera>();
                                camera->setObjectName(cam);
                                camera->setViewMat(origCam->getViewMat());
                                QObject::connect(origCam, &KawaiiCamera::viewMatUpdated, camera, [camera, origCam] {
                                    if(camera->getViewMat() != origCam->getViewMat())
                                      camera->setViewMat(origCam->getViewMat());
                                  });
                                QObject::connect(camera, &KawaiiCamera::viewMatUpdated, origCam, [camera, origCam] {
                                    if(camera->getViewMat() != origCam->getViewMat())
                                      origCam->setViewMat(camera->getViewMat());
                                  });
                              }
                          }
                      }
                  }
                if(!camera)
                  qWarning("KawaiiWorlds::ResourcePackJsonLoader::readRenderpass: Read scene layer: camera \"%s\" is not found!", cam.toLocal8Bit().constData());
                else if(obj.value(QLatin1String("clickable")).toBool(true))
                  layer->setProperty("clickable", true);
                layer->setCamera(camera);

                if(obj.contains(QLatin1String("projection")))
                  if(auto projJson = obj.value(QLatin1String("projection")); projJson.isObject())
                    {
                      const auto projJsonObj = projJson.toObject();
                      const QString projType = projJsonObj.value(QLatin1String("type")).toString().toLower();
                      const double nearClip = projJsonObj.value(QLatin1String("near_clip")).toDouble(0.1);
                      const double farClip = projJsonObj.value(QLatin1String("far_clip")).toDouble(100);
                      if(projType == QStringLiteral("perspective"))
                        {
                          const double fovDegrees = projJsonObj.value(QLatin1String("fov")).toDouble(45);
                          layer->createProjection<KawaiiPerspective>(fovDegrees * M_PI / 180.0, nearClip, farClip);
                        }
                      else if(projType == QStringLiteral("ortho"))
                        {
                          const double scale = projJsonObj.value(QLatin1String("scale")).toDouble(1);
                          layer->createProjection<KawaiiOrtho>(scale, nearClip, farClip);
                        }
                    }
                imgSrc = layer;
              } else if(type == QStringLiteral("shader_effect"))
              {
                const auto &shader = res->getShader(obj.value(QLatin1String("shader")).toString());
                const QStringList shaders({shader.vertexSrc, shader.fragmentSrc});
                const QVariant shaderEffectVariant = QVariant(shaders);
                EffectGlsl *prog = nullptr;
                for(auto *p: res->getPrimaryScene()->findChildren<EffectGlsl*>(QString(), Qt::FindDirectChildrenOnly))
                  if(p->property("shader_effect") == shaderEffectVariant
                     && p->property("illumination_name") == shader.illuminationName)
                    {
                      prog = p;
                      break;
                    }
                if(!prog)
                  {
                    prog = res->getPrimaryScene()->createChild<EffectGlsl>(shader);
                    prog->setProperty("shader_effect", shaderEffectVariant);
                    prog->setProperty("illumination_name", shader.illuminationName);
                  }

                auto *effect = rp.createChild<KawaiiShaderEffect>(prog);
                if(auto uboJson = obj.value(QLatin1String("surface_ubo")); uboJson.isArray() || uboJson.isString())
                  {
                    auto *ubo = readGpuBuf(uboJson, *res);
                    if(ubo && !ubo->parent())
                      ubo->updateParent(effect);
                    effect->setSurfaceUBO(ubo);
                  }
                if(auto cam = obj.value(QLatin1String("camera")); cam.isString())
                  {
                    auto *camera = res->getCamera(cam.toString());
                    if(!camera)
                      qWarning("KawaiiWorlds::ResourcePackJsonLoader::readRenderpass: Read shader effect layer: camera \"%s\" is not found!", cam.toString().toLocal8Bit().constData());
                    effect->setCamera(camera);
                  }

                imgSrc = effect;
              } else if(type == QStringLiteral("texture"))
              {
                KawaiiTexture *tex = nullptr;
                if(auto texNameJson = obj.value(QLatin1String("texture")); texNameJson.isString())
                  {
                    tex = res->getTexture2D(texNameJson.toString());
                  }
                else if(auto cubemapNameJson = obj.value(QLatin1String("cubemap")); cubemapNameJson.isString())
                  {
                    tex = res->getTextureCube(cubemapNameJson.toString());
                  }
                if(tex)
                  {
                    auto texLayer = rp.createChild<KawaiiTexLayer>(tex);
                    texLayer->setKeepAspectRatio(obj.value(QLatin1String("keep_aspect_ratio")).toBool(false));
                    imgSrc = texLayer;
                  }
              } else if(auto el = customImgSrcLoaders.find(type); el != customImgSrcLoaders.cend()) {
                imgSrc = (*el)(obj, &rp);
              }
            if(!imgSrc)
              continue;

            if(auto name = obj.value(QLatin1String("name")); name.isString())
              imgSrc->setObjectName(name.toString());

            if(auto blend = obj.value(QLatin1String("alpha_blend")); blend.isBool())
              imgSrc->setBlendEnabled(blend.toBool());

            if(auto test = obj.value(QLatin1String("depth_test")); test.isBool())
              imgSrc->setDepthTest(test.toBool());

            if(auto write = obj.value(QLatin1String("depth_write")); write.isBool())
              imgSrc->setDepthWrite(write.toBool());

            if(auto depthF = obj.value(QLatin1String("depth_func")); depthF.isString())
              {
                const static QHash<QString, KawaiiImageSource::DepthFunc> knownDepthFuncs = {
                  { QStringLiteral("never"), KawaiiImageSource::DepthFunc::Never },
                  { QStringLiteral("less"), KawaiiImageSource::DepthFunc::Less },
                  { QStringLiteral("less_or_equal"), KawaiiImageSource::DepthFunc::LessEqual },
                  { QStringLiteral("equal"), KawaiiImageSource::DepthFunc::Equal },
                  { QStringLiteral("greater"), KawaiiImageSource::DepthFunc::Greater },
                  { QStringLiteral("greater_or_equal"), KawaiiImageSource::DepthFunc::GreaterEqual },
                  { QStringLiteral("always"), KawaiiImageSource::DepthFunc::Always }
                };
                if(auto el = knownDepthFuncs.find(depthF.toString()); el != knownDepthFuncs.cend())
                  imgSrc->setDepthTestFunc(el.value());
              }
            const static QHash<QString, KawaiiImageSource::BlendFactor> knownBlendFactors = {
              { QStringLiteral("zero"), KawaiiImageSource::BlendFactor::FactorZero },
              { QStringLiteral("0"), KawaiiImageSource::BlendFactor::FactorZero },
              { QStringLiteral("one"), KawaiiImageSource::BlendFactor::FactorOne },
              { QStringLiteral("1"), KawaiiImageSource::BlendFactor::FactorOne },
              { QStringLiteral("src_alpha"), KawaiiImageSource::BlendFactor::FactorSrcAlpha },
              { QStringLiteral("one_minus_src_alpha"), KawaiiImageSource::BlendFactor::FactorOneMinusSrcAlpha },
              { QStringLiteral("1-src_alpha"), KawaiiImageSource::BlendFactor::FactorOneMinusSrcAlpha },
              { QStringLiteral("dst_alpha"), KawaiiImageSource::BlendFactor::FactorDstAlpha },
              { QStringLiteral("one_minus_dst_alpha"), KawaiiImageSource::BlendFactor::FactorOneMinusDstAlpha },
              { QStringLiteral("1-dst_alpha"), KawaiiImageSource::BlendFactor::FactorOneMinusDstAlpha },
            };
            if(auto srcColorK = obj.value(QLatin1String("src_color_factor")); srcColorK.isString())
              if(auto el = knownBlendFactors.find(srcColorK.toString()); el != knownBlendFactors.cend())
                imgSrc->setSrcColorFactor(el.value());
            if(auto dstColorK = obj.value(QLatin1String("dst_color_factor")); dstColorK.isString())
              if(auto el = knownBlendFactors.find(dstColorK.toString()); el != knownBlendFactors.cend())
                imgSrc->setDstColorFactor(el.value());
            if(auto srcAlphaK = obj.value(QLatin1String("src_alpha_factor")); srcAlphaK.isString())
              if(auto el = knownBlendFactors.find(srcAlphaK.toString()); el != knownBlendFactors.cend())
                imgSrc->setSrcAlphaFactor(el.value());
            if(auto dstAlphaK = obj.value(QLatin1String("dst_alpha_factor")); dstAlphaK.isString())
              if(auto el = knownBlendFactors.find(dstAlphaK.toString()); el != knownBlendFactors.cend())
                imgSrc->setDstAlphaFactor(el.value());
            KawaiiRenderpass::Subpass subpass = {
              .imgSrc = imgSrc,
              .depthGBuf = std::nullopt
            };
            if(auto outputGBufs = obj.value(QLatin1String("output_gbuffers")); outputGBufs.isArray())
              {
                subpass.outputGBufs.reserve(outputGBufs.toArray().size());
                for(const auto &j: outputGBufs.toArray())
                  if(j.isDouble())
                    subpass.outputGBufs.push_back(j.toInt());
              }
            if(auto inputGBufs = obj.value(QLatin1String("input_gbuffers")); inputGBufs.isArray())
              {
                subpass.inputGBufs.reserve(inputGBufs.toArray().size());
                for(const auto &j: inputGBufs.toArray())
                  if(j.isDouble())
                    subpass.inputGBufs.push_back(j.toInt());
              }
            if(auto storageGBufs = obj.value(QLatin1String("storage_gbuffers")); storageGBufs.isArray())
              {
                subpass.storageGBufs.reserve(storageGBufs.toArray().size());
                for(const auto &j: storageGBufs.toArray())
                  if(j.isDouble())
                    subpass.storageGBufs.push_back(j.toInt());
              }
            if(auto sampledGBufs = obj.value(QLatin1String("sampled_gbuffers")); sampledGBufs.isArray())
              {
                subpass.sampledGBufs.reserve(sampledGBufs.toArray().size());
                for(const auto &j: sampledGBufs.toArray())
                  if(j.isDouble())
                    subpass.sampledGBufs.push_back(j.toInt());
              }
            if(auto depthGBuf = obj.value(QLatin1String("depth_gbuffer")); depthGBuf.isDouble())
              subpass.depthGBuf = depthGBuf.toInt();
            rp.addSubpass(subpass);
          }
    }

  if(rp.subpassCount() == 0)
    return false;

  rp.autodetectDependencies();
  return true;
}
