#ifndef LANDSCAPEINFO_HPP
#define LANDSCAPEINFO_HPP

#include "KawaiiWorlds_global.hpp"
#include <QMetaType>
#include <QVector3D>
#include <QString>
#include <QHash>

struct KAWAIIWORLDS_SHARED_EXPORT LandscapeInfo
{
  QVector3D size;
  QVector3D pos;
  QString id;
  QString material;
  QString heightMapTexture;
  QHash<QString, QString> metadata;
  QVector2D chunkSize = QVector2D(10, 10);
  float step;
  float restitution = 0;
  float friction = 0.5;
  float frictionRolling = 0.33;
  float frictionSpinning = 0.125;

  bool createCollisionShape;
};

Q_DECLARE_METATYPE(LandscapeInfo*);

KAWAIIWORLDS_SHARED_EXPORT QDataStream& operator<<(QDataStream &st, const LandscapeInfo &obj);
KAWAIIWORLDS_SHARED_EXPORT QDataStream& operator>>(QDataStream &st, LandscapeInfo &obj);

#endif // LANDSCAPEINFO_HPP
