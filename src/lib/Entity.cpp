#include "Entity.hpp"
#include "World.hpp"

#include <Kawaii3D/KawaiiRoot.hpp>
#include <sib_utils/lowestHighest.hpp>

namespace {
  class EntityMotionState: public btMotionState
  {
    Entity &e;

    // btMotionState interface
  public:
    EntityMotionState(Entity &e): e(e) {}

    void getWorldTransform(btTransform &worldTrans) const override final
    {
      worldTrans.setIdentity();
      worldTrans.setOrigin(btVector3(e.getPosition().x(), e.getPosition().y(), e.getPosition().z()));
      worldTrans.setRotation(btQuaternion(
                               e.getRotation().x(), e.getRotation().y(), e.getRotation().z(),
                               e.getRotation().scalar()));
    }

    void setWorldTransform(const btTransform &worldTrans) override final
    {
      e.setPosition({ worldTrans.getOrigin().x(), worldTrans.getOrigin().y(), worldTrans.getOrigin().z() });
      e.setRotation({ worldTrans.getRotation().w(),
                      worldTrans.getRotation().x(), worldTrans.getRotation().y(), worldTrans.getRotation().z() });

      if(Q_LIKELY(e.getBody()))
        e.getBody()->setVisualTransformation(e.getPosition(), e.getRotation());
    }
  };
}

Entity::Entity():
  mutex(std::make_unique<QRecursiveMutex>()),
  body(nullptr),
  world(nullptr),
  location(nullptr),
  scriptWrapper(nullptr),
  mass(0),
  restitution(0),
  friction(0.5),
  frictionRolling(0.33),
  frictionSpinning(0.125),
  lookingForwardFactorX(-1),
  lookingForwardFactorY(-1),
  lookingForwardFactorZ(1),
  trIsDirty(true),
  lookForward(false),
  clickOpacity(false),
  recentBulletTick(false)
{
  connect(this, &Entity::tick, this, &Entity::internalTick, Qt::DirectConnection);
  connect(this, &sib_utils::TreeNode::parentUpdated, this, &Entity::onParentChanged, Qt::DirectConnection);
}

Entity::~Entity()
{
  emit aboutToBeDestroyed(this);

  location = nullptr;
  setWorld(nullptr);

  if(scriptWrapper)
    {
      auto _scriptWrapper = scriptWrapper;
      scriptWrapper = nullptr;
      delete _scriptWrapper;
    }

  if(body)
    delete body;
}

const QQuaternion &Entity::getRotation() const
{
  QMutexLocker l(mutex.get());
  return rotation;
}

void Entity::setRotation(const QQuaternion &newRotation)
{
  QMutexLocker l(mutex.get());
  if(!qFuzzyCompare(rotation, newRotation))
    {
      rotation = newRotation;
      updateAnglesToQuaternion();
      trIsDirty = true;
      emit rotationChanged();
    }
}

void Entity::changeRotation(const std::function<bool (QQuaternion &)> &func)
{
  QMutexLocker l(mutex.get());
  if(Q_LIKELY(func))
    if(func(rotation))
      {
        updateAnglesToQuaternion();
        trIsDirty = true;
        emit rotationChanged();
      }
}

const QVector3D &Entity::getEulerAngles() const
{
  QMutexLocker l(mutex.get());
  return eulerAngles;
}

void Entity::setEulerAngles(const QVector3D &newEulerAngles)
{
  QMutexLocker l(mutex.get());
  if(!qFuzzyCompare(eulerAngles, newEulerAngles))
    {
      eulerAngles = newEulerAngles;
      updateQuaternionToAngles();
      emit rotationChanged();
    }
}

void Entity::changeEulerAngles(const std::function<bool (QVector3D &)> &func)
{
  QMutexLocker l(mutex.get());
  if(Q_LIKELY(func))
    if(func(eulerAngles))
      {
        updateQuaternionToAngles();
        emit rotationChanged();
      }
}

const QVector3D &Entity::getPosition() const
{
  QMutexLocker l(mutex.get());
  return position;
}

void Entity::setPosition(const QVector3D &newPosition)
{
  QMutexLocker l(mutex.get());
  if(!qFuzzyCompare(position, newPosition))
    {
      position = newPosition;
      trIsDirty = true;
      emit positionChanged();
    }
}

void Entity::changePosition(const std::function<bool (QVector3D &)> &func)
{
  QMutexLocker l(mutex.get());
  if(Q_LIKELY(func))
    if(func(position))
      {
        trIsDirty = true;
        emit positionChanged();
      }
}

const QVector3D &Entity::getVelocity() const
{
  QMutexLocker l(mutex.get());
  return velocity;
}

void Entity::setVelocity(const QVector3D &newVelocity, bool writeBtObj)
{
  QMutexLocker l(mutex.get());
  if(!qFuzzyCompare(velocity, newVelocity))
    {
      velocity = newVelocity;
      if(writeBtObj && body)
        {
          body->setVelocity(velocity);
          if(!velocity.isNull() && body->hasCollisionObject())
            body->getBtCollisionObject().activate();
        }
      emit velocityChanged();
    }
}

const QVector3D &Entity::getAngularVel() const
{
  QMutexLocker l(mutex.get());
  return angularVel;
}

void Entity::setAngularVel(const QVector3D &newAngularVel, bool writeBtObj)
{
  QMutexLocker l(mutex.get());
  if(!qFuzzyCompare(angularVel, newAngularVel))
    {
      angularVel = newAngularVel;
      if(writeBtObj && body)
        {
          body->setAngularVelocity(angularVel);
          if(!angularVel.isNull() && body->hasCollisionObject())
            body->getBtCollisionObject().activate();
        }
      emit angularVelChanged();
    }
}

const QString &Entity::getMetadata(const QString &key) const
{
  QMutexLocker l(mutex.get());

  static const QString nullString;
  if(auto el = metadata.find(key); el != metadata.end())
    return el.value();
  else
    return nullString;
}

void Entity::setMetadata(const QString &key, const QString &value)
{
  QMutexLocker l(mutex.get());
  if(value.isNull())
    metadata.remove(key);
  else
    metadata[key] = value;
}

void Entity::setMetadata(const QHash<QString, QString> &value)
{
  QMutexLocker l(mutex.get());
  metadata = value;
}

bool Entity::getLookForward() const
{
  QMutexLocker l(mutex.get());
  return lookForward;
}

void Entity::setLookForward(bool newLookForward)
{
  QMutexLocker l(mutex.get());
  if(lookForward == newLookForward) return;

  lookForward = newLookForward;
  if(getBody() && getBody()->hasCollisionObject())
    setupLookForwardAngularFactor();
  emit lookForwardChanged();
}

float Entity::getLookingForwardFactorX() const
{
  return lookingForwardFactorX;
}

void Entity::setLookingForwardFactorX(float newLookingForwardFactorX)
{
  if (qFuzzyCompare(lookingForwardFactorX, newLookingForwardFactorX))
    return;
  lookingForwardFactorX = newLookingForwardFactorX;
  emit lookingForwardFactorXChanged();
}

float Entity::getLookingForwardFactorY() const
{
  return lookingForwardFactorY;
}

void Entity::setLookingForwardFactorY(float newLookingForwardFactorY)
{
  if (qFuzzyCompare(lookingForwardFactorY, newLookingForwardFactorY))
    return;
  lookingForwardFactorY = newLookingForwardFactorY;
  emit lookingForwardFactorYChanged();
}

float Entity::getLookingForwardFactorZ() const
{
  return lookingForwardFactorZ;
}

void Entity::setLookingForwardFactorZ(float newLookingForwardFactorZ)
{
  if (qFuzzyCompare(lookingForwardFactorZ, newLookingForwardFactorZ))
    return;
  lookingForwardFactorZ = newLookingForwardFactorZ;
  emit lookingForwardFactorZChanged();
}

bool Entity::getClickOpacity() const
{
  QMutexLocker l(mutex.get());
  return clickOpacity;
}

void Entity::setClickOpacity(bool newClickOpacity)
{
  QMutexLocker l(mutex.get());
  if(clickOpacity == newClickOpacity) return;

  clickOpacity = newClickOpacity;
  emit clickOpacityChanged();
}

World *Entity::getWorld() const
{
  QMutexLocker l(mutex.get());
  return world;
}

Location *Entity::getLocation() const
{
  QMutexLocker l(mutex.get());
  return location;
}

Body *Entity::getBody() const
{
  QMutexLocker l(mutex.get());
  return body;
}

void Entity::setBody(Body *newBody)
{
  QMutexLocker l(mutex.get());

  if (body == newBody) return;

  emit bodyIsAboutToBeChanged();
  if(body)
    delete body;
  body = newBody;
  onBodyBtObjChange();
  emit bodyChanged();
}

float Entity::getMass() const
{
  QMutexLocker l(mutex.get());
  return mass;
}

void Entity::setMass(float newMass)
{
  QMutexLocker l(mutex.get());
  if(qFuzzyCompare(mass, newMass))
    return;
  mass = newMass;
  emit massChanged();
}

float Entity::getRestitution() const
{
  QMutexLocker l(mutex.get());
  return restitution;
}

void Entity::setRestitution(float newRestitution)
{
  QMutexLocker l(mutex.get());
  if (qFuzzyCompare(restitution, newRestitution))
    return;
  restitution = newRestitution;
  if(Q_LIKELY(body && body->hasCollisionObject()))
    body->getBtCollisionObject().setRestitution(restitution);
  emit restitutionChanged();
}

float Entity::getFriction() const
{
  QMutexLocker l(mutex.get());
  return friction;
}

void Entity::setFriction(float newFriction)
{
  QMutexLocker l(mutex.get());
  if(qFuzzyCompare(friction, newFriction))
    return;
  friction = newFriction;
  if(Q_LIKELY(body && body->hasCollisionObject()))
    body->getBtCollisionObject().setFriction(friction);
  emit frictionChanged();
}

float Entity::getFrictionRolling() const
{
  QMutexLocker l(mutex.get());
  return frictionRolling;
}

void Entity::setFrictionRolling(float newFrictionRolling)
{
  QMutexLocker l(mutex.get());
  if(qFuzzyCompare(frictionRolling, newFrictionRolling))
    return;
  frictionRolling = newFrictionRolling;
  if(Q_LIKELY(body && body->hasCollisionObject()))
    body->getBtCollisionObject().setRollingFriction(frictionRolling);
  emit frictionRollingChanged();
}

float Entity::getFrictionSpinning() const
{
  QMutexLocker l(mutex.get());
  return frictionSpinning;
}

void Entity::setFrictionSpinning(float newFrictionSpinning)
{
  QMutexLocker l(mutex.get());
  if(qFuzzyCompare(frictionSpinning, newFrictionSpinning))
    return;
  frictionSpinning = newFrictionSpinning;
  if(Q_LIKELY(body && body->hasCollisionObject()))
    body->getBtCollisionObject().setSpinningFriction(frictionSpinning);
  emit frictionSpinningChanged();
}

void Entity::push(const QVector3D &force)
{
  QMutexLocker l(mutex.get());
  if(!qFuzzyIsNull(mass) && !force.isNull())
    setVelocity(getVelocity() + force/mass);
}

std::pair<QVector3D, QVector3D> Entity::getBaseBBox() const
{
  QMutexLocker l(mutex.get());

  btTransform btTrsf;
  btTrsf.setIdentity();
  btVector3 btMin, btMax;

  if(!getBody() || !getBody()->getAabb(btTrsf, btMin, btMax))
    return { QVector3D(), QVector3D() };


  return { {btMin.x(), btMin.y(), btMin.z()}, {btMax.x(), btMax.y(), btMax.z()} };
}

std::pair<QVector3D, QVector3D> Entity::getBBox() const
{
  QMutexLocker l(mutex.get());

  btTransform btTrsf;
  btTrsf.setIdentity();
  btTrsf.setOrigin(btVector3(position.x(), position.y(), position.z()));
  btTrsf.setRotation(btQuaternion(
                   rotation.x(), rotation.y(), rotation.z(),
                   rotation.scalar()));

  btVector3 btMin, btMax;

  if(!getBody() || !getBody()->getAabb(btTrsf, btMin, btMax))
    return { QVector3D(), QVector3D() };

  return { {btMin.x(), btMin.y(), btMin.z()}, {btMax.x(), btMax.y(), btMax.z()} };
}

namespace {
  void toPreScreenSpace(glm::vec4 &v)
  {
    const float k = 0.5 / std::abs(v.w);
    v.x *= k;
    v.y *= -k;
    v.z *= k;
  }
}

std::pair<QVector3D, QVector3D> Entity::getAABB(const glm::mat4 &vp) const
{
  QMutexLocker l(mutex.get());

  if(!getBody() || !getBody()->hasCollisionObject() || !getBody()->getBtCollisionObject().getCollisionShape())
    return { QVector3D(), QVector3D() };

  auto [bbMin, bbMax] = getBBox();

  glm::vec4 a0 = vp * glm::vec4(bbMin.x(), bbMin.y(), bbMin.z(), 1);
  glm::vec4 a1 = vp * glm::vec4(bbMin.x(), bbMin.y(), bbMax.z(), 1);

  glm::vec4 b0 = vp * glm::vec4(bbMax.x(), bbMin.y(), bbMin.z(), 1);
  glm::vec4 b1 = vp * glm::vec4(bbMax.x(), bbMin.y(), bbMax.z(), 1);

  glm::vec4 c0 = vp * glm::vec4(bbMax.x(), bbMax.y(), bbMin.z(), 1);
  glm::vec4 c1 = vp * glm::vec4(bbMax.x(), bbMax.y(), bbMax.z(), 1);

  glm::vec4 d0 = vp * glm::vec4(bbMin.x(), bbMax.y(), bbMin.z(), 1);
  glm::vec4 d1 = vp * glm::vec4(bbMin.x(), bbMax.y(), bbMax.z(), 1);

  toPreScreenSpace(a0);
  toPreScreenSpace(a1);
  toPreScreenSpace(b0);
  toPreScreenSpace(b1);
  toPreScreenSpace(c0);
  toPreScreenSpace(c1);
  toPreScreenSpace(d0);
  toPreScreenSpace(d1);

  using namespace sib_utils;

  glm::vec3 min(getLowest(a0.x, a1.x, b0.x, b1.x, c0.x, c1.x, d0.x, d1.x),
                getLowest(a0.y, a1.y, b0.y, b1.y, c0.y, c1.y, d0.y, d1.y),
                getLowest(a0.z, a1.z, b0.z, b1.z, c0.z, c1.z, d0.z, d1.z));
  min += glm::vec3(0.5, 0.5, 0);


  glm::vec3 max(getHighest(a0.x, a1.x, b0.x, b1.x, c0.x, c1.x, d0.x, d1.x),
                getHighest(a0.y, a1.y, b0.y, b1.y, c0.y, c1.y, d0.y, d1.y),
                getHighest(a0.z, a1.z, b0.z, b1.z, c0.z, c1.z, d0.z, d1.z));
  max += glm::vec3(0.5, 0.5, 0);

  return { {min.x, min.y, min.z}, {max.x, max.y, max.z} };
}

bool Entity::addContact(Entity *withEntity)
{
  if(Q_UNLIKELY(withEntity == this)) return false;

  QMutexLocker l(mutex.get());
  const auto insertRes = contacts.insert(withEntity);
  if(insertRes.second)
    {
      if(lastTickContacts.count(withEntity) == 0)
        emit contact(this, withEntity);
      return true;
    }
  return false;
}

bool Entity::hasContact(Entity *withEntity) const
{
  if(Q_UNLIKELY(withEntity == this)) return false;

  QMutexLocker l(mutex.get());
  return contacts.count(withEntity) > 0 || lastTickContacts.count(withEntity) > 0;
}

bool Entity::hasContacts() const
{
  QMutexLocker l(mutex.get());
  return !contacts.empty() || !lastTickContacts.empty();
}

void Entity::readBulletObj()
{
  QMutexLocker l(mutex.get());

  if(Q_UNLIKELY(!body)) return;

  if(!motionState)
    {
      QVector3D pos;
      QQuaternion rot;
      body->getTransformation(pos, rot);
      setPosition(pos);
      setRotation(rot);
      trIsDirty = false;
    }

  QVector3D vel, angularVel;
  body->getVelocity(vel, angularVel);
  setVelocity(vel, false);
  setAngularVel(angularVel, false);
  recentBulletTick = true;
}

void Entity::writeBulletObj()
{
  if(Q_UNLIKELY(!body)) return;

  if(trIsDirty)
    {
      body->setTransformation(position, rotation);
      trIsDirty = false;

      if(body->hasCollisionObject())
        body->getBtCollisionObject().activate();
    }

}

void Entity::onBodyBtObjChange()
{
  QMutexLocker l(mutex.get());
  if(Q_LIKELY(body))
    {
      if(body->hasCollisionObject())
        {
          body->getBtCollisionObject().setUserPointer(this);
          body->getBtCollisionObject().setFriction(friction);
          body->getBtCollisionObject().setRollingFriction(frictionRolling);
          body->getBtCollisionObject().setSpinningFriction(frictionSpinning);
          body->getBtCollisionObject().setRestitution(restitution);
          setupLookForwardAngularFactor();

          for(auto &el: constraints)
            {
              if(el.second.current)
                emit constraintRemoved(el.second.current.get());
              el.second.current = el.second.creator(&getBody()->getBtCollisionObject());
              if(Q_LIKELY(el.second.current))
                emit constraintAdded(el.second.current.get());
            }
        }

      body->setMass(mass);
      body->setTransformation(position, rotation);
      body->setVelocity(velocity);
      body->setAngularVelocity(angularVel);
      if(!motionState)
        motionState = std::make_unique<EntityMotionState>(*this);
      if(!body->setMotionState(motionState.get()))
        motionState.reset();
    }
}

void Entity::allignToLand(Landscape *landscape)
{
  if(Q_UNLIKELY(!landscape)) return;

  QMutexLocker l(mutex.get());
  auto bb = getBBox();

  const float height = landscape->getHeight(QVector2D(bb.first.x(), bb.first.z()),
                                            QVector2D(bb.second.x(), bb.second.z()));

  bb.first -= getPosition();
  bb.second -= getPosition();

  const float y = height - bb.first.y();

  changePosition([y] (QVector3D &pos) {
    if(!qFuzzyCompare(pos.y(), y)) {
        pos.setY(y);
        return true;
      } else
      return false;
  });
}

void Entity::allignToLand(LandscapeInfo *landscapeInfo)
{
  if(Q_LIKELY(world))
    allignToLand(world->getBringedLandscape(landscapeInfo));
}

float Entity::distanceToLand(Landscape *landscape)
{
  if(Q_UNLIKELY(!landscape))
    return std::numeric_limits<float>::quiet_NaN();

  QMutexLocker l(mutex.get());
  auto bb = getBBox();

  const float height = landscape->getHeight(QVector2D(bb.first.x(), bb.first.z()),
                                            QVector2D(bb.second.x(), bb.second.z()));

  bb.first -= getPosition();
  bb.second -= getPosition();

  const float y = height - bb.first.y();
  return getPosition().y() - y;
}

float Entity::distanceToLand(LandscapeInfo *landscapeInfo)
{
  if(Q_LIKELY(world))
    return distanceToLand(world->getBringedLandscape(landscapeInfo));
  return std::numeric_limits<float>::quiet_NaN();
}

QObject *Entity::getScriptWrapper() const
{
  QMutexLocker l(mutex.get());
  return scriptWrapper;
}

void Entity::changeScriptWrapper(const std::function<void (QObject *&)> &func)
{
  QMutexLocker l(mutex.get());
  if(Q_LIKELY(func))
    func(scriptWrapper);
}

std::unique_ptr<QMutexLocker<QRecursiveMutex>> Entity::lock() const
{
  return std::make_unique<QMutexLocker<QRecursiveMutex>>(mutex.get());
}

bool Entity::hasPointerInteractions() const
{
  return receivers(SIGNAL(pointerPress(Entity*))) > 0
      || receivers(SIGNAL(pointerRelease(Entity*))) > 0
      || receivers(SIGNAL(pointerDblClicked(Entity*))) > 0;
}

void Entity::setConstraint(const std::string &name, Constraint &&constraint)
{
  auto el = constraints.find(name);
  if(el != constraints.end())
    {
      auto tmp = std::move(*el);
      emit constraintRemoved(tmp.second.current.get());
    } else
    el = constraints.insert({name, std::move(constraint)}).first;

  if(Q_LIKELY(el->second.creator) && getBody() && getBody()->hasCollisionObject())
    {
      el->second.current = el->second.creator(&getBody()->getBtCollisionObject());
      if(Q_LIKELY(el->second.current))
        emit constraintAdded(el->second.current.get());
    }
}

void Entity::removeConstraint(const std::string &name)
{
  if(auto el = constraints.find(name); el != constraints.end())
    {
      auto tmp = std::move(*el);
      constraints.erase(el);
      emit constraintRemoved(tmp.second.current.get());
    }
}

const std::unordered_map<std::string, Entity::Constraint> &Entity::getConstraints() const
{
  return constraints;
}

namespace {
  bool qFuzzyIsNull(const QVector3D &vec)
  {
    return ::qFuzzyIsNull(vec.x())
        && ::qFuzzyIsNull(vec.y())
        && ::qFuzzyIsNull(vec.z());
  }
}

void Entity::internalTick(Entity *e, float secElapsed)
{
  QMutexLocker l(e->mutex.get());

  if(!e->recentBulletTick)
    {
      if(!qFuzzyIsNull(e->velocity))
        {
          e->position += e->velocity * secElapsed;
          e->trIsDirty = true;
          emit e->positionChanged();
        }
      if(!qFuzzyIsNull(e->angularVel) && !e->lookForward)
        {
          e->changeEulerAngles([e, secElapsed](QVector3D &angles) {
              angles += e->angularVel * secElapsed;
              return true;
            });
        }
    }

  for(auto *another: e->lastTickContacts)
    if(e->contacts.count(another) == 0)
      emit e->contactLost(e, another);
  bool hasContactsChanged = e->lastTickContacts.empty() != e->contacts.empty();
  e->lastTickContacts = std::move(e->contacts);
  if(hasContactsChanged)
    emit e->hasContactsChanged();

  if(e->lookForward && !qFuzzyIsNull(e->velocity))
    {
      QVector3D dir = e->velocity.normalized();
      dir *= QVector3D(e->lookingForwardFactorX, e->lookingForwardFactorY, e->lookingForwardFactorZ);
      QVector3D up(0,1,0);

      e->setRotation(QQuaternion::fromDirection(dir, up));
    }

  // if(e->getBody() && e->getBody()->hasCollisionObject()
  //    && e->getVelocity().lengthSquared() < 0.01
  //    && e->getAngularVel().lengthSquared() < 0.01
  //    && e->getBody()->getBtCollisionObject().isActive())
  //   e->getBody()->getBtCollisionObject().setActivationState(WANTS_DEACTIVATION);

  e->recentBulletTick = false;
}

void Entity::updateQuaternionToAngles()
{
  QQuaternion q;
  if(!qFuzzyIsNull(eulerAngles.z()))
    q = QQuaternion::fromAxisAndAngle(0,0,1, eulerAngles.z());
  if(!qFuzzyIsNull(eulerAngles.x()))
    q *= QQuaternion::fromAxisAndAngle(1,0,0, eulerAngles.x());
  if(!qFuzzyIsNull(eulerAngles.y()))
    q *= QQuaternion::fromAxisAndAngle(0,1,0, eulerAngles.y());
  rotation = q.normalized();
  trIsDirty = true;
}

void Entity::updateAnglesToQuaternion()
{
  const QVector4D q = rotation.toVector4D();

  eulerAngles = {
    std::atan2(q.z() * q.w() + q.x() * q.y(), 0.5f - (q.y()*q.y() + q.z()*q.z())),
    std::asin(-2.0f * (q.y()*q.w() - q.x() * q.z())),
    std::atan2(q.y() * q.z() + q.x() * q.w(), 0.5f - (q.z()*q.z() + q.w()*q.w())),
  };
}

void Entity::setupLookForwardAngularFactor()
{
  auto rigidBody = btRigidBody::upcast(&getBody()->getBtCollisionObject());
  if(rigidBody)
    {
      if(lookForward)
        {
          rigidBody->setAngularFactor(btVector3(0,0,0));
          rigidBody->setAngularVelocity(btVector3(0,0,0));
        } else
        rigidBody->setAngularFactor(btVector3(1,1,1));
    }
}

void Entity::onParentChanged()
{
  setLocation(KawaiiRoot::getRoot<Location>(parent()));
  setWorld(KawaiiRoot::getRoot<World>(parent()));

  if(location)
    location->registerEntity(this);
  else if(world)
    world->activateEntity(this);
}

void Entity::setWorld(World *newWorld)
{
  if(world == newWorld) return;

  auto _contacts = std::move(contacts);
  _contacts.merge(lastTickContacts);
  lastTickContacts.clear();
  for(auto *another: _contacts)
    {
      QMutexLocker l(another->mutex.get());
      emit contactLost(this, another);

      bool anotherContactLostEmited = false;
      auto el = another->contacts.find(this);
      if(el != another->contacts.end())
        {
          another->contacts.erase(el);
          emit another->contactLost(another, this);
          anotherContactLostEmited = true;
        }

      el = another->lastTickContacts.find(this);
      if(el != another->lastTickContacts.end())
        {
          another->lastTickContacts.erase(el);
          if(!anotherContactLostEmited)
            emit another->contactLost(another, this);
          anotherContactLostEmited = true;
        }
      if(anotherContactLostEmited && !another->hasContacts())
        emit another->hasContactsChanged();
    }
  if(!_contacts.empty())
    emit hasContactsChanged();

  world = newWorld;
  emit worldChanged();
}

void Entity::setLocation(Location *newLocation)
{
  if(location == newLocation) return;

  location = newLocation;
  emit locationChanged();
}


KAWAIIWORLDS_SHARED_EXPORT QDataStream& operator<<(QDataStream &st, const Entity &obj)
{
  st << obj.objectName()
     << obj.getRotation()
     << obj.getPosition()
     << obj.getVelocity()
     << obj.getAngularVel()
     << obj.getMass()
     << obj.getRestitution()
     << obj.getFriction()
     << obj.getFrictionRolling()
     << obj.getFrictionSpinning()
     << obj.getLookingForwardFactorX()
     << obj.getLookingForwardFactorY()
     << obj.getLookingForwardFactorZ();

  if(Q_LIKELY(obj.getBody()))
    obj.getBody()->saveTo(st);
  else
    sib_utils::Serializable::stubSaveTo(st);

  return st;
}

KAWAIIWORLDS_SHARED_EXPORT QDataStream& operator>>(QDataStream &st, Entity &obj)
{
  auto l = obj.lock();

  QString str;
  st >> str;
  obj.setObjectName(str);

  QQuaternion q;
  st >> q;
  obj.setRotation(q);

  QVector3D vec3;
  st >> vec3;
  obj.setPosition(vec3);

  st >> vec3;
  obj.setVelocity(vec3);

  st >> vec3;
  obj.setAngularVel(vec3);

  float f;
  st >> f;
  obj.setMass(f);

  st >> f;
  obj.setRestitution(f);

  st >> f;
  obj.setFriction(f);

  st >> f;
  obj.setFrictionRolling(f);

  st >> f;
  obj.setFrictionSpinning(f);

  st >> f;
  obj.setLookingForwardFactorX(f);
  st >> f;
  obj.setLookingForwardFactorY(f);
  st >> f;
  obj.setLookingForwardFactorZ(f);

  obj.setBody(Body::loadFrom(st).release());

  return st;
}
