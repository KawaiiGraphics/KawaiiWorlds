module MaterialEffectFrag;
precision mediump int; precision highp float;

Model {
    sampler2D diffuse_tex;
} model;

layout(location = 2) in highp vec2 texCoord;

import MaterialInfo from CelIlluminationCore;

MaterialInfo cel_material;

void material_effect(in vec3 normal, in vec3 worldPos)
{
    vec4 texel = texture(diffuse_tex, texCoord.xy);
    if(texel.a <= 0.001)
    {
        discard;
        return;
    }
    cel_material.ambient = 0.05 * texel.rgb;
    cel_material.specular = vec4(0.5, 0.5, 0.5, 0.3);
    cel_material.shininess = 10.0;
    cel_material.diffuse = pow(texel.rgb, vec3(2.2));
}

export material_effect;
