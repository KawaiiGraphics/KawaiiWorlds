module MaterialEffectFrag;
precision mediump int; precision highp float;

Model {
    sampler2D diffuse_tex;
} model;

layout(location = 2) in highp vec2 texCoord;

import MaterialInfo from PbrIlluminationCore;

//albedo; metallic; roughness; ao;
MaterialInfo pbr_material;

void material_effect(in vec3 normal, in vec3 worldPos)
{
    vec4 texel = texture(diffuse_tex, texCoord.xy);
    if(texel.a <= 0.001)
    {
        discard;
        return;
    }
    pbr_material.metallic = 0.0;
    pbr_material.roughness = 0.5;
    pbr_material.ao = 1.0;
    pbr_material.albedo = pow(texel.rgb, vec3(2.2));
}

export material_effect;
