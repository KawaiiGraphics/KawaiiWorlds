module MaterialEffectVert;
precision mediump int; precision highp float;

layout(location = ${SIB_TEXCOORD_ATTR_LOCATION}) in vec3 texcoord;

layout(location = 2) out highp vec2 texCoord;

void material_effect(in vec3 norm, in vec4 vert)
{
    texCoord = texcoord.xy;
}

export material_effect;
