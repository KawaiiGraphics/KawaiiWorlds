module MaterialEffectFrag;
precision mediump int; precision highp float;

Model {
    sampler2D diffuse_tex;
} model;

layout(location = 2) in highp vec2 texCoord;

import MaterialInfo from PbrIlluminationCore;

//ambient; diffuse; specular; shininess;
MaterialInfo phong_material;

void material_effect(in vec3 normal, in vec3 worldPos)
{
    vec4 texel = texture(diffuse_tex, texCoord.xy);
    if(texel.a <= 0.001)
    {
        discard;
        return;
    }
    phong_material.ambient = 0.05 * texel.rgb;
    phong_material.specular = vec3(0.5);
    phong_material.shininess = 1.0;
    phong_material.diffuse = pow(texel.rgb, vec3(2.2));
}

export material_effect;
