module LandscapeVertMain;
precision mediump int; precision highp float;

layout(location = ${SIB_VERTEX_ATTR_LOCATION}) in vec4 qt_Vertex;
layout(location = ${SIB_NORMAL_ATTR_LOCATION}) in vec3 qt_Normal;
layout(location = ${SIB_TEXCOORD_ATTR_LOCATION}) in vec3 texcoord;

Surface {
    mat4 proj_mat;
    mat4 vp_mat;
} surface;

Model {
    vec4 landSz;
    vec4 center;

    sampler2D heights;
} model;

layout(location = 0) out vec3 norm_vec;
layout(location = 1) out vec3 pos_vec;

import material_effect from MaterialEffectVert;

float getHeight(const ivec2 p)
{
    return model.landSz.y * texelFetch(heights, p, 0).r;
}

void main(void)
{
    vec4 vert = qt_Vertex;
    vec2 texCoord = (vert.xz / (model.landSz.xz + vec2(1.0))) + vec2(0.5);
    vert.y = model.center.y + model.landSz.y * texture(heights, texCoord).r;
    vert.xz += model.center.xz;

    ivec2 texSz = textureSize(heights, 0);
    vec2 pixelCoord = texCoord * texSz;
    vec2 v1 = floor(pixelCoord),
            v2 = ceil(pixelCoord);

    if(abs(v1.x - v2.x) < 0.1)
        v1.x -= 1.0;
    if(abs(v1.y - v2.y) < 0.1)
        v1.y -= 1.0;

    vec2 v3 = v2 + vec2(1.0);

//    vec3 hDelta = vec3(2,
//                             getHeight(ivec2(int(v3.x), int(v2.y))) - getHeight(ivec2(int(v1.x), int(v2.y))),
//                             0);

//    vec3 vDelta = vec3(0,
//                             getHeight(ivec2(int(v2.x), int(v3.y))) - getHeight(ivec2(int(v2.x), int(v1.y))),
//                             2);

//    norm_vec = normalize(cross(vDelta, hDelta));

    norm_vec = normalize(vec3(
                             getHeight(ivec2(int(v1.x), int(v2.y))) - getHeight(ivec2(int(v3.x), int(v2.y))),
                             2,
                             getHeight(ivec2(int(v2.x), int(v1.y))) - getHeight(ivec2(int(v2.x), int(v3.y)))));
    material_effect(norm_vec, vert);
    pos_vec = vert.xyz;

    gl_Position = surface.vp_mat * vert;
}
