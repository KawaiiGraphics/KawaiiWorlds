module SolidVertMain;
precision mediump int; precision highp float;

layout(location = ${SIB_VERTEX_ATTR_LOCATION}) in vec4 qt_Vertex;
layout(location = ${SIB_NORMAL_ATTR_LOCATION}) in vec3 qt_Normal;
layout(location = ${SIB_TEXCOORD_ATTR_LOCATION}) in vec3 texcoord;

Surface {
    mat4 proj_mat;
    mat4 vp_mat;
} surface;

Model {
    ivec4 objectIndex[${MAX_PACK_INSTANCES}];
    sampler2D diffuse_tex;
} model;

struct instance_buf {
    vec4 rot;
    vec4 pos;
};

layout(std430, binding = GLOBAL_SSBO_BINDING_1) readonly restrict buffer ObjectLocBufLayout {
    instance_buf model_buf[];
};

vec3 rotate_vec(in vec4 q, in vec3 v)
{
  return v + 2.0*cross(cross(v, q.xyz) + q.w*v, q.xyz);
}

layout(location = 0) out vec3 norm_vec;
layout(location = 1) out vec3 pos_vec;

import material_effect from MaterialEffectVert;

void main(void)
{
    int i = gl_InstanceID / 4;
    int j = gl_InstanceID % 4;

    vec4 model_rot = model_buf[model.objectIndex[i][j]].rot.yzwx;
    vec3 model_pos = model_buf[model.objectIndex[i][j]].pos.xyz;

    vec3 p = rotate_vec(model_rot, qt_Vertex.xyz);
    p += model_pos;

    vec4 vert = vec4(p, qt_Vertex.w);

    norm_vec = rotate_vec(model_rot, qt_Normal);
    material_effect(norm_vec, vert);
    pos_vec = vert.xyz;

    gl_Position = surface.vp_mat * vert;
}
