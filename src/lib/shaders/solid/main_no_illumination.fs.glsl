module SolidFragMain;
precision mediump int; precision highp float;

layout(location = 0) in vec3 norm_vec;
layout(location = 1) in vec3 pos_vec;

layout(location = 0) out vec4 result;

import material_effect from MaterialEffectFrag;

vec3 ownColor = vec3(0.0);

void main()
{
    vec3 n = norm_vec;
    vec3 p = pos_vec;
    material_effect(n, p);
    result = vec4(ownColor, 1.0);
}
