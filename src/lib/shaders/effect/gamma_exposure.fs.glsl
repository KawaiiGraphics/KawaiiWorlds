module MaterialEffectFrag;

input rgba32f gbuf_0;
layout(location = 0) out vec4 result;

Global_1
{
    float exposure;
    float gamma;
    float brightness;
    float contrast;
} global_1;

void material_effect(vec3 norm, vec3 pos)
{
    vec3 c = subpassLoad(INPUT_GBUF_0).xyz;
    vec3 mapped = vec3(1.0) - exp(-c * global_1.exposure);
    mapped = pow(mapped, vec3(1.0 / global_1.gamma));
    mapped = clamp(mapped, vec3(0), vec3(1));
    result = vec4(mapped, 1.0);
}

export material_effect;
