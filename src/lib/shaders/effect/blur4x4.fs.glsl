module MaterialEffectFrag;

sampled rgba32f gbuf_0;
layout(location = 0) out vec4 result;

void material_effect(vec3 norm, vec3 pos)
{
    vec4 val = vec4(0.0);
    ivec2 sz = textureSize(SAMPLED_GBUF_0, 0);

    for (int x = -2; x < 2; ++x)
        for (int y = -2; y < 2; ++y)
        {
            ivec2 texelPos = ivec2(gl_FragCoord.x+x, gl_FragCoord.y+y);
            texelPos = clamp(texelPos, ivec2(0), sz);
            val += texelFetch(SAMPLED_GBUF_0, texelPos, 0);
        }
    result = val / (4.0 * 4.0);
}

export material_effect;
