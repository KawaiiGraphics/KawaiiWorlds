module EffectFragMain;
precision mediump int; precision highp float;

layout(location = 0) out vec4 result;

import get_illumination from ${ILLUMINATION_NAME}_illumination;

Camera
{
    vec4 camPos;
    mat4 view_mat;
    mat4 viewInverted;
    mat3 normal;
} camera;

import material_effect from MaterialEffectFrag;

vec3 ownColor = vec3(0.0);

void main()
{
    vec3 normalVec = vec3(0,0,1), posVec = vec3(0);
    material_effect(normalVec, posVec);

    vec3 c = get_illumination(normalVec, posVec, camera.camPos.xyz) + ownColor;
    result = vec4(c, 1.0);
}
