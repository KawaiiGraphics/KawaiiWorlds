module MaterialEffectFrag;

sampled rgba32f gbuf_0; // position
input rgba32f gbuf_0; // normal

layout(location = 0) out float ao;

#define SAMPLE_COUNT 64
#define RADIUS 0.5
#define DEPTH_BIAS 0.025

const int kernelSize = $SAMPLE_COUNT;
const float radius = $RADIUS;
const float bias = $DEPTH_BIAS;

Surface {
    mat4 projectionMat;
    vec4 samples[$SAMPLE_COUNT];
    vec2 noise[4][4];
} surface;

Camera {
    vec4 camPos;
    mat4 viewMat;
    mat4 viewMatInverted;
    mat3 normalMat;
} camera;

void material_effect(vec3 _norm, vec3 _worldPos)
{
    vec2 texCoords    = vec2(0.25) * gl_FragCoord.xy;

    ivec2 fragCoord   = ivec2(gl_FragCoord.x, gl_FragCoord.y);
    vec4 pos          = vec4(texelFetch(SAMPLED_GBUF_0, fragCoord, 0).xyz, 1.0);
    pos               = (camera.viewMat * pos);
    vec3 fragPos      = pos.xyz / pos.w;
    vec3 normal       = normalize(camera.normalMat * subpassLoad(INPUT_GBUF_0).xyz);
    vec2 noiseCoordF  = mod(gl_FragCoord.xy, vec2(4));
    ivec2 noiseCoord  = ivec2(noiseCoordF.x, noiseCoordF.y);
    vec3 randomVec    = vec3(surface.noise[noiseCoord.x][noiseCoord.y], 0);

    vec3 tangent      = normalize(randomVec - normal * dot(randomVec, normal));
    vec3 bitangent    = cross(normal, tangent);
    mat3 TBN          = mat3(tangent, bitangent, normal);

    float occlusion = 0.0;
    for(int i = 0; i < kernelSize; ++i)
    {
        // get sample position
        vec3 samplePos = TBN * surface.samples[i].xyz; // from tangent to view-space
        samplePos = fragPos + samplePos * radius;

        // project sample position (to sample gbuf) (to get position on screen/gbuf)
        vec4 offset = vec4(samplePos, 1.0);
        offset = surface.projectionMat * offset; // from view to clip-space
        offset.xy /= offset.w; // perspective divide
        offset.xy = 0.5 * offset.xy + vec2(0.5); // transform to range 0.0 - 1.0
        offset.xy = clamp(offset.xy, vec2(0), vec2(1));

        // get sample depth
        vec4 loadedSamplePos = vec4(texture(SAMPLED_GBUF_0, offset.xy).xyz, 1.0);
        loadedSamplePos = camera.viewMat * loadedSamplePos;
        float sampleDepth = loadedSamplePos.z / loadedSamplePos.w; // get depth value of kernel sample

        // range check & accumulate
        float rangeCheck = smoothstep(0.0, 1.0, radius / abs(fragPos.z - sampleDepth));
        occlusion += (sampleDepth >= samplePos.z + bias ? 1.0 : 0.0) * rangeCheck;
    }
    ao = 1.0 - (occlusion / kernelSize);
}

export material_effect;
