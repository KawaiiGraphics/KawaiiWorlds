module EffectFragMain;
precision mediump int; precision highp float;

import material_effect from MaterialEffectFrag;

void main()
{
    vec3 normalVec = vec3(0,0,1), posVec = vec3(0);
    material_effect(normalVec, posVec);
}
