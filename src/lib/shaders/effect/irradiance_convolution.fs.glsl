module MaterialEffectFrag;
precision mediump int; precision highp float;
#define ANGULAR_SAMPLE_DELTA 0.125

const float PI = 3.14159265359;
const float sampleDelta = $ANGULAR_SAMPLE_DELTA;

Surface {
    vec2 resolution;
    samplerCube environmentMap;
} surface;

const mat4 vpMat[6] = mat4[6](
            mat4(0, 0, -1, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1),
            mat4(0, 0, 1, 0, 0, 1, 0, 0, -1, 0, 0, 0, 0, 0, 0, 1),
            mat4(1, 0, 0, 0, 0, 0, 1, 0, 0, -1, 0, 0, 0, 0, 0, 1),
            mat4(1, 0, 0, 0, 0, 0, -1, 0, 0, 1, 0, 0, 0, 0, 0, 1),
            mat4(1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1),
            mat4(-1, 0, 0, 0, 0, 1, 0, 0, 0, 0, -1, 0, 0, 0, 0, 1)
            );

layout(location = 0) out highp vec4 posX;
layout(location = 1) out highp vec4 negX;
layout(location = 2) out highp vec4 posY;
layout(location = 3) out highp vec4 negY;
layout(location = 4) out highp vec4 posZ;
layout(location = 5) out highp vec4 negZ;

void material_effect(in vec3 normal, in vec3 worldPos)
{
    vec3 irradiance[6] = vec3[6](vec3(0),vec3(0),vec3(0),vec3(0),vec3(0),vec3(0));

    vec4 fragPos = vec4(gl_FragCoord.xy / surface.resolution, 1, 1);
    fragPos.xy *= vec2(2);
    fragPos.xy -= vec2(1);

    for(int i = 0; i < 6; ++i)
    {
        vec3 N = normalize(vec3(vpMat[i]*fragPos));

        // tangent space calculation from origin point
        vec3 up    = vec3(0.0, 1.0, 0.0);
        vec3 right = normalize(cross(up, N));
        up         = normalize(cross(N, right));

        float nrSamples = 0.0;
        for(float phi = 0.0; phi < 2.0 * PI; phi += sampleDelta)
        {
            for(float theta = 0.0; theta < 0.5 * PI; theta += sampleDelta)
            {
                // spherical to cartesian (in tangent space)
                vec3 tangentSample = vec3(sin(theta) * cos(phi),  sin(theta) * sin(phi), cos(theta));
                // tangent space to world
                vec3 sampleVec = tangentSample.x * right + tangentSample.y * up + tangentSample.z * N;

                irradiance[i] += texture(environmentMap, sampleVec).rgb * cos(theta) * sin(theta);
                nrSamples++;
            }
        }
        irradiance[i] /= float(nrSamples);
    }
    posX = vec4(irradiance[0], 1);
    negX = vec4(irradiance[1], 1);
    posY = vec4(irradiance[2], 1);
    negY = vec4(irradiance[3], 1);
    posZ = vec4(irradiance[4], 1);
    negZ = vec4(irradiance[5], 1);
}

export material_effect;
