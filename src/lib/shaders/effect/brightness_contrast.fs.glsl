module MaterialEffectFrag;

input rgba32f gbuf_0;
layout(location = 0) out vec4 result;

Global_1
{
    float exposure;
    float gamma;
    float brightness;
    float contrast;
} global_1;

void material_effect(vec3 norm, vec3 pos)
{
    vec3 c = subpassLoad(INPUT_GBUF_0).xyz;
    vec3 mapped = (c - vec3(0.5)) * global_1.contrast + vec3(0.5 + global_1.brightness);
    mapped = clamp(mapped, vec3(0), vec3(1));
    result = vec4(mapped, 1.0);
}

export material_effect;
