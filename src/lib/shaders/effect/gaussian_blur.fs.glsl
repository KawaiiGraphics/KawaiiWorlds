module MaterialEffectFrag;

sampled rgba32f gbuf_0;
layout(location = 0) out vec4 fragColor;

#define WEIGHT 0.227027, 0.1945946, 0.1216216, 0.054054, 0.016216

float weight[] = float[] ($WEIGHT);

#define IS_HORIZONTAL true
const bool horizontal = $IS_HORIZONTAL;

void material_effect(vec3 norm, vec3 pos)
{
    ivec2 sz = textureSize(SAMPLED_GBUF_0, 0);

    vec4 result = vec4(0.0);

    int N = 2*(weight.length()-1);
    ivec2 uDir = horizontal? ivec2(1,0): ivec2(0,1);
    for(int i = 0; i <= N; ++i)
    {
        int index = i - (N/2);
        ivec2 c0 = ivec2(gl_FragCoord.xy) + uDir * index;

        if (c0.x < 0) c0.x = 0;
        if (c0.y < 0) c0.y = 0;
        if (c0.x >= sz.x) c0.x = sz.x - 1;
        if (c0.y >= sz.y) c0.y = sz.y - 1;

        vec4 add = texelFetch(SAMPLED_GBUF_0, c0, 0);
        result += add * weight[abs(index)];
    }
    fragColor = result;
}

export material_effect;
