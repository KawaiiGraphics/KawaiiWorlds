module SolidVertMain;
precision mediump int; precision highp float;

layout(location = ${SIB_VERTEX_ATTR_LOCATION}) in vec4 qt_Vertex;
layout(location = ${SIB_NORMAL_ATTR_LOCATION}) in vec3 qt_Normal;
layout(location = ${SIB_TEXCOORD_ATTR_LOCATION}) in vec3 texcoord;

Camera
{
    vec4 camPos;
    mat4 view_mat;
    mat4 viewInverted;
    mat3 normal;
} camera;

Surface {
    mat4 proj_mat;
    mat4 vp_mat;
} surface;

struct instance_buf {
    vec4 rot;
    vec4 pos;
};

Model {
    instance_buf model_buf[${MAX_PACK_INSTANCES}];
    sampler2D diffuse_tex;
} model;

vec3 rotate_vec(in vec4 q, in vec3 v)
{
  return v + 2.0*cross(cross(v, q.xyz ) + q.w*v, q.xyz);
}

layout(location = 0) out vec3 norm_vec;
layout(location = 1) out vec3 pos_vec;

import material_effect from MaterialEffectVert;

void main(void)
{
    vec4 model_rot = model.model_buf[gl_InstanceID].rot.yzwx;
    vec3 model_pos = model.model_buf[gl_InstanceID].pos.xyz;

    vec3 p = rotate_vec(model_rot, qt_Vertex.xyz);
    p += model_pos;

    vec4 vert = vec4(p, qt_Vertex.w);

    norm_vec = rotate_vec(model_rot, qt_Normal);
    material_effect(norm_vec, vert);
    vert.xyz += camera.camPos.xyz;
    pos_vec = vert.xyz;

    gl_Position = surface.vp_mat * vert;
}
