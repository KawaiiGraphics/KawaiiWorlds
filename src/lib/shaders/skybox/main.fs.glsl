module SkyboxFragMain;
precision mediump int; precision highp float;

layout(location = 0) in vec3 norm_vec;
layout(location = 1) in vec3 pos_vec;

layout(location = 0) out vec4 result;

import get_illumination from ${ILLUMINATION_NAME}_illumination;

Global_1
{
    float exposure;
    float gamma;
    float brightness;
    float contrast;
} global_1;

Camera
{
    vec4 camPos;
    mat4 view_mat;
    mat4 viewInverted;
    mat3 normal;
} camera;

import material_effect from MaterialEffectFrag;

vec3 ownColor = vec3(0.0);

layout (depth_greater) out float gl_FragDepth;

void main()
{
    vec3 n = norm_vec;
    vec3 p = pos_vec;
    material_effect(n, p);

    vec3 c = get_illumination(n, pos_vec, camera.camPos.xyz) + ownColor;
    result = vec4(c, 1.0);
    gl_FragDepth = 0.999;
}
