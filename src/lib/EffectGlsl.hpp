#ifndef EFFECTGLSL_HPP
#define EFFECTGLSL_HPP

#include "KawaiiWorlds_global.hpp"
#include <Kawaii3D/Shaders/KawaiiProgram.hpp>
#include "ShaderEffectSrc.hpp"

class KAWAIIWORLDS_SHARED_EXPORT EffectGlsl : public KawaiiProgram
{
  Q_OBJECT
public:
  EffectGlsl(const ShaderEffectSrc &materialEffectSrc);
};

#endif // EFFECTGLSL_HPP
