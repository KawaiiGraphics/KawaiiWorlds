#include "World.hpp"
#include "input/ClickInput.hpp"

#include <sib_utils/lowestHighest.hpp>
#include <QtConcurrent>
#include <QChildEvent>
#include <set>

World::World():
  objects_mutex(std::make_unique<QReadWriteLock>(QReadWriteLock::Recursive)),
  location(nullptr),
  metadata_mutex(std::make_unique<QReadWriteLock>(QReadWriteLock::Recursive)),
  collisions_mutex(std::make_unique<QReadWriteLock>(QReadWriteLock::Recursive)),
  age_mutex(std::make_unique<QReadWriteLock>()),
  ageSeconds(0),
  ageSeconds_last(0),
  refTickInterval(-1),
  timeIsRunning(false),
  destroying(false)
{
  createChild<ResourcePack>();

  addInput(std::make_unique<ClickInput>());
}

World::~World()
{
  finishTime();

  gearbox.reset();
  setLocation(nullptr);
  for(auto *i: findChildren<Location*>(QString(), Qt::FindDirectChildrenOnly))
    delete i;
  QWriteLocker l(objects_mutex.get());
  destroying = true;
  for(auto *e: std::move(activeEntities))
    delete e;
  l.unlock();
  objects_mutex.reset();
  illumination.clear();
}

ResourcePack &World::res()
{
  Q_ASSERT(resources);
  return *resources;
}

const ResourcePack &World::res() const
{
  Q_ASSERT(resources);
  return *resources;
}

std::list<Entity*>::iterator World::activateEntity(Entity *e)
{
  if(Q_UNLIKELY(e->getWorld() != this))
    return activeEntities.end();

  QWriteLocker l(objects_mutex.get());
  auto el = activeEntities.insert(activeEntities.end(), e);
  indexedEntities.indexObject(e);
  if(e->getBody())
    e->getBody()->enable(*this);
  auto deactivateEntityFunc = [this, el] { World::deactivateEntity(el); };
  connect(e, &QObject::destroyed, this, deactivateEntityFunc);
  connect(e, &Entity::worldChanged, this, deactivateEntityFunc);
  emit entityActiveted(e);
  return el;
}

Entity *World::createEntity(Body *body, const QString &id, float mass)
{
  Entity *e = new Entity;
  e->setObjectName(id);
  e->setBody(body);
  e->setMass(mass);
  e->updateParent(this);
  return e;
}

Entity *World::getEntity(const QString &id) const
{
  QReadLocker l(objects_mutex.get());
  return indexedEntities.find(id);
}

void World::forallEntities(const std::function<void (Entity &)> &func) const
{
  if(Q_LIKELY(func))
    {
      QReadLocker l(objects_mutex.get());
      for(const auto &i: activeEntities)
        func(*i);
    }
}

void World::forallEntitiesP(const std::function<void (Entity &)> &func) const
{
  if(Q_LIKELY(func))
    {
      QReadLocker l(objects_mutex.get());
      QtConcurrent::map(activeEntities, [&func](Entity *e) { func(*e); }).waitForFinished();
    }
}

void World::forallEntityPairs(const std::function<void (Entity &, Entity &)> &func) const
{
  if(Q_LIKELY(func))
    {
      QReadLocker l(objects_mutex.get());
      for(auto i = activeEntities.cbegin(); i != activeEntities.cend(); ++i)
        for(auto j = std::next(i); j != activeEntities.cend(); ++j)
          func(**i, **j);
    }
}

void World::deactivateEntity(std::list<Entity*>::iterator el)
{
  if(destroying) return;

  QWriteLocker l(objects_mutex.get());
  collisions.erase(std::remove_if(collisions.begin(), collisions.end(), [&el](const CollisionInfo &collision) {
      return collision.a == *el || collision.b == *el;
    }), collisions.end());

  auto *e = qobject_cast<Entity*>(*el);
  if(e)
    {
      if(e->getBody())
        e->getBody()->disable();
      disconnect(e, &QObject::destroyed, this, nullptr);
      disconnect(e, &Entity::worldChanged, this, nullptr);
      emit entityDeactiveted(e);
    }

  indexedEntities.unindexObject(*el);
  activeEntities.erase(el);
}

void World::startTime(int tickInterval)
{
  finishTime();
  refTickInterval = tickInterval;

  auto work = [this] {
      ageTimestamp = std::chrono::steady_clock::now();
      timeIsRunning = true;
      QTimer::singleShot(refTickInterval, this, qOverload<>(&World::tick));
    };

  if(QThread::currentThread() == thread())
    work();
  else
    QMetaObject::invokeMethod(this, work);
}

void World::finishTime()
{
  timeIsRunning = false;
}

void World::setLocation(Location *location)
{
  if(this->location == location) return;

  if(this->location)
    {
      disconnect(this->location, &Location::entityAdded, this, &World::activateEntity);
      disconnect(this->location, &Location::entityRemoved, this, &World::onLocationRemovesEntity);
      disconnect(this->location, &Location::landscapeAdded, this, &World::onLocationAddsLandscape);
      disconnect(this->location, &Location::landscapeRemoved, this, &World::onLocationRemovesLandscape);
      disconnect(onCurrentLocationDestroyed);

      this->location->forallEntities([this] (Entity &e) { onLocationRemovesEntity(&e); });
    }

  for(auto i = landscapes.begin(); i != landscapes.end(); ++i)
    {
      i->second->unload(*i->first);
      delete i->second;
    }
  landscapes.clear();

  this->location = location;
  emit locationChanged();
  if(this->location)
    {
      connect(this->location, &Location::entityAdded, this, &World::activateEntity);
      connect(this->location, &Location::entityRemoved, this, &World::onLocationRemovesEntity);
      this->location->forallEntities([this] (Entity &e) { activateEntity(&e); });

      connect(this->location, &Location::landscapeAdded, this, &World::onLocationAddsLandscape);
      connect(this->location, &Location::landscapeRemoved, this, &World::onLocationRemovesLandscape);
      this->location->forallLandscapes([this] (LandscapeInfo &info) { onLocationAddsLandscape(&info); });

      onCurrentLocationDestroyed = connect(this->location, &Location::aboutToBeDestroyed, std::bind(&World::setLocation, this, nullptr));
    }
}

Entity *World::getEntityAt(const QPointF &screenPos) const
{
  if(Q_UNLIKELY(!gearbox))
    return nullptr;

  Entity *e = nullptr;
  res().getMainRenderpass()->forallSubpasses([&screenPos, &e, this] (const KawaiiRenderpass::Subpass &subpass) {
      if(e || !subpass.imgSrc || !subpass.imgSrc->property("clickable").toBool()) return;

      auto *sceneLayer = static_cast<KawaiiSceneLayer*>(subpass.imgSrc);
      e = gearbox->nearestEntityAt(screenPos, *sceneLayer);
    });
  return e;
}

Entity *World::getEntityAt(const QPointF &screenPos, const std::function<bool (Entity &)> &filter) const
{
  if(Q_UNLIKELY(!gearbox))
    return nullptr;

  Entity *e = nullptr;
  res().getMainRenderpass()->forallSubpasses([&screenPos, &e, &filter, this] (const KawaiiRenderpass::Subpass &subpass) {
      if(e || !subpass.imgSrc || !subpass.imgSrc->property("clickable").toBool()) return;

      auto *sceneLayer = static_cast<KawaiiSceneLayer*>(subpass.imgSrc);
      e = gearbox->nearestEntityAt(screenPos, *sceneLayer, filter);
    });
  return e;
}

std::unordered_set<Entity*> World::getEntitiesAt(const QPointF &screenPos) const
{
  if(Q_UNLIKELY(!gearbox))
    return {};

  std::unordered_set<Entity*> result;
  res().getMainRenderpass()->forallSubpasses([&screenPos, &result, this] (const KawaiiRenderpass::Subpass &subpass) {
      if(!subpass.imgSrc || !subpass.imgSrc->property("clickable").toBool()) return;

      auto *sceneLayer = static_cast<KawaiiSceneLayer*>(subpass.imgSrc);
      const auto entities = gearbox->allEntitiesAt(screenPos, *sceneLayer);
      result.insert(entities.cbegin(), entities.cend());
    });
  return result;
}

std::unordered_set<Entity *> World::filterEntitiesAt(const QPointF &screenPos, const std::function<bool (Entity &)> &filter) const
{
  if(Q_UNLIKELY(!gearbox))
    return {};

  std::unordered_set<Entity*> result;
  res().getMainRenderpass()->forallSubpasses([&screenPos, &result, &filter, this] (const KawaiiRenderpass::Subpass &subpass) {
      if(!subpass.imgSrc || !subpass.imgSrc->property("clickable").toBool()) return;

      auto *sceneLayer = static_cast<KawaiiSceneLayer*>(subpass.imgSrc);
      const auto entities = gearbox->allEntitiesAt(screenPos, *sceneLayer, filter);
      result.insert(entities.cbegin(), entities.cend());
    });
  return result;
}

std::vector<std::pair<QVector3D, QVector3D>> World::getEntityAABB(Entity &e) const
{
  std::vector<std::pair<QVector3D, QVector3D>> result;
  res().getMainRenderpass()->forallSubpasses([&e, &result] (const KawaiiRenderpass::Subpass &subpass) {
      if(!subpass.imgSrc || !subpass.imgSrc->property("clickable").toBool()) return;

      auto *sceneLayer = static_cast<KawaiiSceneLayer*>(subpass.imgSrc);
      const auto viewProjMat = sceneLayer->getClipCorrection() * sceneLayer->getViewProjectionMatrix();
      result.push_back(e.getAABB(viewProjMat));
    });
  return result;
}

std::vector<QPointF> World::toScreenAxis(const QVector3D &worldPos) const
{
  std::vector<QPointF> result;
  res().getMainRenderpass()->forallSubpasses([&result, &worldPos] (const KawaiiRenderpass::Subpass &subpass) {
      if(!subpass.imgSrc || !subpass.imgSrc->property("clickable").toBool()) return;
      auto *sceneLayer = static_cast<KawaiiSceneLayer*>(subpass.imgSrc);
      result.push_back(sceneLayer->mapCoord(worldPos));
    });
  return result;
}

std::vector<QVector3D> World::toWorldAxis(const QPointF &screenPos, float depth) const
{
  std::vector<QVector3D> result;
  res().getMainRenderpass()->forallSubpasses([&result, &screenPos, depth] (const KawaiiRenderpass::Subpass &subpass) {
      if(!subpass.imgSrc || !subpass.imgSrc->property("clickable").toBool()) return;
      auto *sceneLayer = static_cast<KawaiiSceneLayer*>(subpass.imgSrc);
      result.push_back(sceneLayer->unmapCoord(screenPos, depth));
    });
  return result;
}

float World::readDepth(const QPointF &screenPos) const
{
  auto *rp = res().getMainRenderpass();
  using namespace std;
  set<float> readedDepth;
  QRect readGbufRequest(min<int>(round(screenPos.x()), rp->getRenderableSize().x-1),
                        min<int>(round(screenPos.y()), rp->getRenderableSize().y-1),
                        1,1);

  rp->forallSubpasses([&readGbufRequest, &readedDepth, rp] (const KawaiiRenderpass::Subpass &subpass) {
      if(!subpass.imgSrc || !subpass.imgSrc->property("clickable").toBool()) return;
      if(Q_LIKELY(subpass.depthGBuf))
        {
          const auto readedGBuf = rp->readGbufF(*subpass.depthGBuf, readGbufRequest);
          if(Q_LIKELY(!readedGBuf.empty()))
            readedDepth.insert(readedGBuf.front());
        }
    });

  return !readedDepth.empty()?
        *readedDepth.cbegin():
        numeric_limits<float>::quiet_NaN();
}

void World::onPointerPress(const QPointF &screenPos) const
{
  auto *e = getEntityAt(screenPos, [](Entity &e) { return e.getClickOpacity() || e.hasPointerInteractions(); });
  if(e)
    emit e->pointerPress(e);
}

void World::onPointerRelease(const QPointF &screenPos) const
{
  auto *e = getEntityAt(screenPos, [](Entity &e) { return e.getClickOpacity() || e.hasPointerInteractions(); });
  if(e)
    emit e->pointerRelease(e);
}

void World::onPointerDoubleClick(const QPointF &screenPos) const
{
  auto *e = getEntityAt(screenPos, [](Entity &e) { return e.getClickOpacity() || e.hasPointerInteractions(); });
  if(e)
    emit e->pointerDblClicked(e);
}

float World::getAgeSeconds() const
{
  QReadLocker l(age_mutex.get());
  return ageSeconds;
}

size_t World::addInput(std::unique_ptr<Input> &&input)
{
  if(input)
    {
      inputs.push_back(std::move(input));
      forallAttachedSurfaces([this] (KawaiiSurface &sfc) {
        inputs.back()->install(*this, sfc);
      });
      return inputs.size() - 1;
    } else
    return std::numeric_limits<size_t>::max();
}

void World::swapInputs(size_t i1, size_t i2)
{
  if(i1 != i2)
    std::swap(inputs.at(i1), inputs.at(i2));
}

Input &World::getInput(size_t i) const
{
  return *inputs.at(i);
}

void World::forallInputs(const std::function<void (Input &)> &func) const
{
  if(func)
    for(const auto &i: inputs)
      func(*i);
}

void World::removeInput(size_t i)
{
  if(i < inputs.size())
    inputs.erase(inputs.cbegin() + i);
}

size_t World::getInputCount() const
{
  return inputs.size();
}

void World::attachSurface(KawaiiSurface &sfc)
{
  attachedSurfaces.push_back(&sfc);

  for(const auto &i: inputs)
    i->install(*this, sfc);
}

void World::forallAttachedSurfaces(const std::function<void (KawaiiSurface &)> &func)
{
  for(auto i = attachedSurfaces.begin(); i != attachedSurfaces.end(); )
    {
      if(!i->isNull())
        {
          func(**i);
          ++i;
        } else
        i = attachedSurfaces.erase(i);
    }
}

void World::addCollision(const CollisionInfo &collision)
{
  QWriteLocker collisionsL(collisions_mutex.get());
  if(collision.a && collision.b)
    {
      collisionsL.unlock();
      bool needAddCollision = collision.a->addContact(collision.b);
      if(collision.b->addContact(collision.a))
        needAddCollision = true;
      collisionsL.relock();
      if(needAddCollision)
        collisions.push_back(collision);
    }
}

void World::forallCollisions(Entity *e, const std::function<void (const CollisionInfo &)> &func) const
{
  QReadLocker collisionsL(collisions_mutex.get());
  if(Q_LIKELY(func))
    for(const auto &i: collisions)
      if(i.a == e || i.b == e)
        {
          collisionsL.unlock();
          func(i);
          collisionsL.relock();
        }
}

void World::forallCollisions(const std::function<void (const CollisionInfo &)> &func) const
{
  QReadLocker collisionsL(collisions_mutex.get());
  if(Q_LIKELY(func))
    for(const auto &i: collisions)
      {
        collisionsL.unlock();
        func(i);
        collisionsL.relock();
      }
}

void World::findCollision(Entity *e, const std::function<bool (const CollisionInfo &)> &func) const
{
  QReadLocker collisionsL(collisions_mutex.get());
  if(Q_LIKELY(func))
    for(const auto &i: collisions)
      if(i.a == e || i.b == e)
        {
          collisionsL.unlock();
          if(func(i))
            break;
          else
            collisionsL.relock();
        }
}

IlluminationModelBase &World::getIlluminationModelBase(size_t i) const
{
  Q_ASSERT(i < illumination.size());
  return *illumination[i];
}

IlluminationModelBase &World::findIlluminationModelBase(const QString &name) const
{
  for(const auto &i: illumination)
    if(i->getName() == name)
      return *i;

  throw std::runtime_error("illumination model not found");
}

void World::removeIlluminationModel(size_t i)
{
  Q_ASSERT(i < illumination.size());
  illumination.erase(illumination.begin() + i);
}

size_t World::illuminationModelsCount() const
{
  return illumination.size();
}

Landscape *World::getBringedLandscape(LandscapeInfo *info) const
{
  if(auto el = landscapes.find(info); el != landscapes.end())
    return el->second;
  else
    return nullptr;
}

const QString &World::getMetadata(const QString &key) const
{
  QReadLocker l(metadata_mutex.get());

  static const QString nullString;
  if(auto el = metadata.find(key); el != metadata.end())
    return el.value();
  else
    return nullString;
}

void World::setMetadata(const QString &key, const QString &value)
{
  QWriteLocker l(metadata_mutex.get());

  if(value.isNull())
    metadata.remove(key);
  else
    metadata[key] = value;
}

void World::setMetadata(const QHash<QString, QString> &value)
{
  QWriteLocker l(metadata_mutex.get());
  metadata = value;
}

bool World::canPlace(KawaiiModel3D *model, const QVector3D &pos) const
{
  if(!model)
    throw std::invalid_argument("World::bringEntity: model not exists");

  if(Q_UNLIKELY(!gearbox))
    return true;

  return gearbox->canPlace(model, pos);
}

bool World::canPlace(Entity &entity, const QVector3D &pos) const
{
  if(Q_UNLIKELY(!gearbox))
    return true;

  if(!entity.getBody() || !entity.getBody()->hasCollisionObject())
    return true;

  return gearbox->canPlace(entity.getBody()->getBtCollisionObject(), pos);
}

bool World::canPlace(const QString &model, const QVector3D &pos) const
{
  auto *model_ptr = res().getModel(model);
  return canPlace(model_ptr, pos);
}

void World::clear()
{
  QWriteLocker objL(objects_mutex.get());
  for(const auto &i: activeEntities)
    i->deleteLater();
  activeEntities.clear();
  objL.unlock();

  setLocation(nullptr);
  for(auto *i: findChildren<Location*>(QString(), Qt::FindDirectChildrenOnly))
    i->deleteLater();

  gearbox.reset();
  illumination.clear();

  QWriteLocker metadataL(metadata_mutex.get());
  metadata.clear();
}

void World::load(QDataStream &st)
{
  clear();

  // Load entities
  quint64 sz;
  st >> sz;
  for(quint64 i = 0; i < sz; ++i)
    {
      Entity *entity = new Entity;
      st >> *entity;
      entity->updateParent(this);
    }

  // Load the locations
  st >> sz;
  std::vector<Location*> locations(sz, nullptr);
  for(quint64 i = 0; i < sz; ++i)
    {
      Location *l = createChild<Location>();
      locations[i] = l;
      st >> *l;
    }

  quint8 ctrlByte;
  st >> ctrlByte;
  if(ctrlByte)
    {
      st >> sz;
      if(sz == std::numeric_limits<quint64>::max())
        {
          Location *l = createChild<Location>();
          st >> *l;
          setLocation(l);
        } else
        setLocation(locations[sz]);
    }

  // Load a gearbox
  setGearbox(Gearbox::loadFrom(st, *this));

  // Load clockworks
  if(Q_UNLIKELY(!gearbox))
    Gearbox::loadClockworksStub(st);

  // Load illumination
  st >> sz;
  for(quint64 i = 0; i < sz; ++i)
    illumination.push_back(IlluminationModelBase::loadFrom(st, *this));

  // Load the age
  QWriteLocker ageL(age_mutex.get());
  st >> ageSeconds;

  //Load the metadata
  QWriteLocker metadataL(metadata_mutex.get());
  st >> metadata;

  emit worldLoaded();
}

void World::save(QDataStream &st) const
{
  // Write the entities
  QReadLocker objectsL(objects_mutex.get());
  quint64 sz = activeEntities.size();
  st << sz;
  objectsL.unlock();
  forallEntities([&st] (Entity &e) { st << e; });

  // Write the locations
  const auto locations = findChildren<Location*>(QString(), Qt::FindDirectChildrenOnly);
  st << static_cast<quint64>(locations.size());
  for(auto *i: locations)
    st << *i;

  if(location)
    {
      st << static_cast<quint8>(1);
      if(auto locationI = std::find(locations.cbegin(), locations.cend(), location); locationI != locations.cend())
        {
          st << static_cast<quint64>(locationI - locations.cend());
        }
      else
        {
          st << std::numeric_limits<quint64>::max();
          st << *location;
        }
    } else
    st << static_cast<quint8>(0);

  // Write the gearbox
  if(Q_LIKELY(gearbox))
    gearbox->saveTo(st);
  else
    {
      sib_utils::Serializable::stubSaveTo(st);
      st << quint64(0);
    }

  // Write the illumination
  st << static_cast<quint64>(illumination.size());
  for(const auto &i: illumination)
    i->saveTo(st);

  // Write the age
  st << getAgeSeconds();

  // Write the metadata
  QReadLocker metadataL(metadata_mutex.get());
  st << metadata;
}

void World::childEvent(QChildEvent *event)
{
  switch(event->type())
    {
    case QEvent::ChildAdded:
      if(auto res_pack = qobject_cast<ResourcePack*>(event->child()); res_pack && res_pack != resources.get())
        resources.reset(res_pack);
      break;

    case QEvent::ChildRemoved:
      if(auto res_pack = qobject_cast<ResourcePack*>(event->child()); res_pack && res_pack == resources.get())
        resources.release();
      break;

    default: break;
    }
}

void World::tick()
{
  if(gearbox && gearbox->getProcessingTick())
    {
      if(timeIsRunning)
        QTimer::singleShot(0.35*refTickInterval, this, qOverload<>(&World::tick));
      return;
    }

  auto newTime = std::chrono::steady_clock::now();
  float sec_elapsed = 0.001 * std::chrono::duration_cast<std::chrono::milliseconds>(newTime - ageTimestamp).count();
  ageTimestamp = newTime;

  tick(sec_elapsed);

  if(timeIsRunning)
    {
      newTime = std::chrono::steady_clock::now();
      sec_elapsed = 0.001 * std::chrono::duration_cast<std::chrono::milliseconds>(newTime - ageTimestamp).count();
      float nextInterval = std::max(0.001, refTickInterval - 1000. * sec_elapsed);
      QTimer::singleShot(nextInterval, this, qOverload<>(&World::tick));
    }
}

bool World::hasGearbox() const
{
  return !!gearbox;
}

Gearbox &World::getGearbox() const
{
  return *gearbox;
}

void World::setGearbox(std::unique_ptr<Gearbox> &&newGearbox)
{
  if(gearbox == newGearbox) return;

  gearbox = std::move(newGearbox);
  if(gearbox)
    connect(gearbox.get(), &Gearbox::destroyed, this, [this] {
        gearbox.release();
        emit gearboxChanged();
      });
  emit gearboxChanged();
}

void World::onLocationRemovesEntity(Entity *e)
{
  QReadLocker l(objects_mutex.get());
  auto el = std::find(activeEntities.begin(), activeEntities.end(), e);
  if(Q_LIKELY(el != activeEntities.end()))
    {
      l.unlock();
      deactivateEntity(el);
    }
}

void World::onLocationAddsLandscape(LandscapeInfo *fromLocation)
{
  auto *l = new Landscape();
  l->updateParent(this);
  l->setHeightMap(res().getTexture2D(fromLocation->heightMapTexture));
  l->build(fromLocation->pos, fromLocation->size, fromLocation->step);
  l->setMaterial(*res().getMaterial(fromLocation->material));
  l->setRestitution(fromLocation->restitution);
  l->setFriction(fromLocation->friction);
  l->setFrictionRolling(fromLocation->frictionRolling);
  l->setFrictionSpinning(fromLocation->frictionSpinning);

  if(fromLocation->createCollisionShape && hasGearbox()) {
      l->createRigidBodies(fromLocation->chunkSize);
      for(const auto &body: l->getRigidBodies())
        gearbox->addCollisionObject(*body);
    }
  landscapes[fromLocation] = l;
}

void World::onLocationRemovesLandscape(LandscapeInfo *fromLocation)
{
  auto el = landscapes.find(fromLocation);
  if(el != landscapes.end())
    {
      if(gearbox)
        for(const auto &body: el->second->getRigidBodies())
          gearbox->removeCollisionObject(*body);
      delete el->second;
      landscapes.erase(el);
    }
}

void World::tick(float sec_elapsed)
{
  QWriteLocker ageL(age_mutex.get());
  ageSeconds += sec_elapsed;
  ageL.unlock();

  collisions.clear();

  for(const auto &i: inputs)
    if(i->onTick)
      i->onTick(sec_elapsed);

  if(gearbox)
    gearbox->tick(sec_elapsed);

  for(auto &i: landscapes)
    i.second->flushHeightMap();
}
