#ifndef MODELINSTANCE_HPP
#define MODELINSTANCE_HPP

#include <Kawaii3D/Geometry/KawaiiMeshInstance.hpp>

#include "KawaiiWorlds_global.hpp"

class KAWAIIWORLDS_SHARED_EXPORT ModelInstance
{
  ModelInstance(const ModelInstance &orig) = delete;
  ModelInstance& operator=(const ModelInstance &orig) = delete;

public:
  const QList<KawaiiMeshInstance*> *meshInstances;

  ModelInstance();
  ModelInstance(const QList<KawaiiMeshInstance*> &meshInstances, QList<ModelInstance*> &modelInstances,
                KawaiiGpuBuf *objectsSSBO, QList<ModelInstance*> &allModelInstances);
  ModelInstance(ModelInstance &&orig);
  ModelInstance& operator=(ModelInstance &&orig);
  ~ModelInstance();

  size_t getInstanceIndex() const;
  size_t getObjectIndex() const;

  QQuaternion &rotation() const;
  QVector4D &position() const;
  KawaiiGpuBuf *getObjectsSSBO() const;
  void updateLocation(bool posDirty, bool rotationDirty);



  // IMPLEMENT
private:
  QList<ModelInstance*> *modelInstances;
  QList<ModelInstance*> *allModelInstances;
  KawaiiGpuBuf *objectsSSBO;
  size_t instanceIndex;
  size_t objectIndex;

  void writeObjectIndexToUbo();
  void destroy();
};

#endif // MODELINSTANCE_HPP
