#include "Location.hpp"
#include "Entity.hpp"
#include <sib_utils/qDataStreamExpansion.hpp>

Location::Location():
  scriptWrapper(nullptr),
  objectsM(std::make_unique<QReadWriteLock>(QReadWriteLock::Recursive)),
  landscapesM(std::make_unique<QReadWriteLock>(QReadWriteLock::Recursive)),
  scriptWrapperM(std::make_unique<QReadWriteLock>(QReadWriteLock::Recursive))
{
}

Location::~Location()
{
  emit aboutToBeDestroyed();
  blockSignals(true);
  clear();
  QWriteLocker l(scriptWrapperM.get());
  if(scriptWrapper)
    {
      auto _scriptWrapper = scriptWrapper;
      scriptWrapper = nullptr;
      delete _scriptWrapper;
    }
  l.unlock();
}

Entity *Location::createEntity(Body *body, const QString &id, float mass)
{
  Entity *e = new Entity;
  e->setObjectName(id);
  e->setBody(body);
  e->setMass(mass);
  e->updateParent(this);
  return e;
}

void Location::registerEntity(Entity *e)
{
  if(e->getLocation() != this) return;

  auto el = std::find(objects.begin(), objects.end(), e);
  if(el != objects.end()) return;

  el = objects.insert(objects.end(), e);
  auto onEntityLost = [this, el] { removeEntity(el); };
  connect(e, &Entity::aboutToBeDestroyed, this, onEntityLost);
  connect(e, &Entity::locationChanged, this, onEntityLost);
  indexedObjects.indexObject(e);
  emit entityAdded(e);
}

void Location::forallEntities(const std::function<void (Entity&)> &func)
{
  if(Q_LIKELY(func))
    {
      QReadLocker l(objectsM.get());
      for(auto &i: objects)
        {
          l.unlock();
          func(*i);
          l.relock();
        }
    }
}

void Location::forallEntitiesConst(const std::function<void (const Entity&)> &func) const
{
  if(Q_LIKELY(func))
    {
      QReadLocker l(objectsM.get());
      for(auto &i: objects)
        func(*i);
    }
}

Entity *Location::findEntity(const QString &id)
{
  QReadLocker l(objectsM.get());
  return indexedObjects.find(id);
}

LandscapeInfo *Location::addLandscapeInfo(const QString &material, const QString &heightMapTexture, const QVector3D &size, bool createCollisionShape)
{
  QWriteLocker l(landscapesM.get());
  landscapes.push_back(LandscapeInfo {
                         .size = size,
                         .material = material,
                         .heightMapTexture = heightMapTexture,
                         .step = 0.05,
                         .createCollisionShape = createCollisionShape
                       });
  emit landscapeAdded(&landscapes.back());
  return &landscapes.back();
}

void Location::removeLandscapeInfo(LandscapeInfo &l)
{
  QWriteLocker lock(landscapesM.get());
  for(auto el = landscapes.begin(); el != landscapes.end(); )
    {
      if (Q_UNLIKELY( &(*el) == &l ))
        {
          emit landscapeRemoved(&*el);
          el = landscapes.erase(el);
          return;
        } else
        ++el;
    }
}

void Location::forallLandscapes(const std::function<void (LandscapeInfo &)> &func)
{
  QReadLocker l(landscapesM.get());
  for(auto &i: landscapes)
    {
      l.unlock();
      func(i);
      l.relock();
    }
}

LandscapeInfo *Location::findLandscapeInfo(const QString &id)
{
  QReadLocker l(landscapesM.get());
  for(auto &i: landscapes)
    if(i.id == id)
      return &i;
  return nullptr;
}

void Location::clear()
{
  QWriteLocker objectsL(objectsM.get());
  QWriteLocker landscapesL(landscapesM.get());
  while(!objects.empty())
    delete objects.back();

  while(!landscapes.empty())
    removeLandscapeInfo(landscapes.back());
}

QObject *Location::getScriptWrapper() const
{
  QReadLocker l(scriptWrapperM.get());
  return scriptWrapper;
}

void Location::changeScriptWrapper(const std::function<void (QObject *&)> &func)
{
  QWriteLocker l(scriptWrapperM.get());
  if(Q_LIKELY(func))
    func(scriptWrapper);
}

void Location::removeEntity(std::list<Entity*>::iterator el)
{
  Entity *removing = *el;
  indexedObjects.unindexObject(*el);
  objects.erase(el);
  emit entityRemoved(removing);
}


KAWAIIWORLDS_SHARED_EXPORT QDataStream& operator<<(QDataStream &st, const Location &obj)
{
  QReadLocker objectsL(obj.objectsM.get());
  st << quint64(obj.objects.size());
  obj.forallEntitiesConst([&st] (const Entity &e) { st << e; });

  QReadLocker landscapesL(obj.landscapesM.get());
  return st << obj.landscapes;
}

KAWAIIWORLDS_SHARED_EXPORT QDataStream& operator>>(QDataStream &st, Location &obj)
{
  obj.clear();

  QWriteLocker objectsL(obj.objectsM.get());
  quint64 sz;
  st >> sz;
  for(quint64 i = 0; i < sz; ++i)
    {
      Entity *e = new Entity;
      st >> *e;
      e->updateParent(&obj);
    }

  QWriteLocker landscapesL(obj.landscapesM.get());
  return st >> obj.landscapes;
}
