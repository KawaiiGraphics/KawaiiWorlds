#ifndef SHADEREFFECTSRC_HPP
#define SHADEREFFECTSRC_HPP

#include <Kawaii3D/Shaders/KawaiiProgram.hpp>
#include <QString>
#include <QHash>

struct ShaderEffectSrc {
  enum class ShadowMode: uint8_t {
    Default = 0,
    Suppress = 1,
    DontSuppress = 2
  };

  QString vertexSrc;
  QString fragmentSrc;
  QString illuminationName;
  QHash<QString, QString> shaderParams;
  KawaiiProgram::CullMode cullMode;
  ShadowMode shadowMode;

  inline bool shadowModeMatches(bool suppress_shadows) const {
    switch(shadowMode)
      {
      case ShadowMode::Default:
        return true;
      case ShadowMode::Suppress:
        return suppress_shadows;
      case ShadowMode::DontSuppress:
        return !suppress_shadows;
      }
    Q_UNREACHABLE();
  }
};


#endif // SHADEREFFECTSRC_HPP
