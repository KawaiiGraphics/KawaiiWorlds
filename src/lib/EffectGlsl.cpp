#include "EffectGlsl.hpp"

EffectGlsl::EffectGlsl(const ShaderEffectSrc &materialEffectSrc)
{
  auto *module = createShaderModule(KawaiiShaderType::Fragment, materialEffectSrc.fragmentSrc);
  for(auto i = materialEffectSrc.shaderParams.cbegin(); i != materialEffectSrc.shaderParams.cend(); ++i)
    module->setParameter(i.key(), i.value());
  if(!materialEffectSrc.illuminationName.isNull())
    createShaderModule(KawaiiShaderType::Fragment, QFile(":/shaders/effect/main.fs.glsl"))->setParameter("ILLUMINATION_NAME", materialEffectSrc.illuminationName);
  else
    createShaderModule(KawaiiShaderType::Fragment, QFile(":/shaders/effect/main_no_illumination.fs.glsl"));
  setCullMode(KawaiiProgram::CullMode::CullBack);
}
