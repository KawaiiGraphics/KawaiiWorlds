#ifndef WORLD_HPP
#define WORLD_HPP

#include "IlluminationModel.hpp"
#include "ResourcePack.hpp"
#include "clockworks/Gearbox.hpp"

#include "Location.hpp"
#include "input/Input.hpp"
#include "Landscape.hpp"

#include <Kawaii3D/Surfaces/KawaiiSurface.hpp>
#include <sib_utils/NamedQObjectIndex.hpp>
#include <sib_utils/PairHash.hpp>
#include <unordered_map>
#include <unordered_set>
#include <QMutex>
#include <vector>
#include <memory>
#include <chrono>

class KAWAIIWORLDS_SHARED_EXPORT World : public KawaiiDataUnit
{
  Q_OBJECT
  KAWAII_UNIT_DEF(World);
  Q_PROPERTY(const QString MementoType MEMBER nodeType DESIGNABLE false CONSTANT);

  World(const World&) = delete;
  World& operator=(const World&) = delete;

public:
  World();
  ~World();

  ResourcePack& res();
  const ResourcePack& res() const;

  std::list<Entity*>::iterator activateEntity(Entity *e);
  Entity *createEntity(Body *body, const QString &id, float mass = 1);
  Entity *getEntity(const QString &id) const;
  void forallEntities(const std::function<void(Entity&)> &func) const;
  void forallEntitiesP(const std::function<void(Entity&)> &func) const;
  void forallEntityPairs(const std::function<void(Entity&, Entity&)> &func) const;

  void startTime(int tickInterval = 10);
  void finishTime();

  void setLocation(Location *location);

  Entity* getEntityAt(const QPointF &screenPos) const;
  Entity* getEntityAt(const QPointF &screenPos, const std::function<bool(Entity&)> &filter) const;
  std::unordered_set<Entity *> getEntitiesAt(const QPointF &screenPos) const;
  std::unordered_set<Entity *> filterEntitiesAt(const QPointF &screenPos, const std::function<bool(Entity&)> &filter) const;
  std::vector<std::pair<QVector3D, QVector3D>> getEntityAABB(Entity &e) const;
  std::vector<QPointF> toScreenAxis(const QVector3D &worldPos) const;
  std::vector<QVector3D> toWorldAxis(const QPointF &screenPos, float depth) const;
  float readDepth(const QPointF &screenPos) const;
  void onPointerPress(const QPointF &screenPos) const;
  void onPointerRelease(const QPointF &screenPos) const;
  void onPointerDoubleClick(const QPointF &screenPos) const;

  float getAgeSeconds() const;

  size_t addInput(std::unique_ptr<Input> &&input);
  void swapInputs(size_t i1, size_t i2);
  Input& getInput(size_t i) const;
  void forallInputs(const std::function<void(Input&)> &func) const;
  void removeInput(size_t i);
  size_t getInputCount() const;

  void attachSurface(KawaiiSurface &sfc);
  void forallAttachedSurfaces(const std::function<void(KawaiiSurface&)> &func);

  void addCollision(const CollisionInfo &collision);
  void forallCollisions(Entity *e, const std::function<void(const CollisionInfo&)> &func) const;
  void forallCollisions(const std::function<void(const CollisionInfo&)> &func) const;
  void findCollision(Entity *e, const std::function<bool(const CollisionInfo&)> &func) const;

  template<typename T>
  IlluminationModel<T>& createIlluminationModel(const QString &name)
  {
    illumination.push_back(std::make_unique<IlluminationModel<T>>(*this, name, illumination.size() + 1));
    return static_cast<IlluminationModel<T>&>(*illumination.back());
  }

  template<typename T>
  IlluminationModel<T>& resetIlluminationModel(size_t i)
  {
    Q_ASSERT(i < illumination.size());
    auto replacement = std::make_unique<IlluminationModel<T>>(*this, illumination[i]->getName(), i+1);
    illumination[i] = std::move(replacement);
    return static_cast<IlluminationModel<T>&>(*illumination[i]);
  }

  IlluminationModelBase& getIlluminationModelBase(size_t i) const;

  template<typename T>
  IlluminationModel<T>& getIlluminationModel(size_t i) const
  {
    return dynamic_cast<IlluminationModel<T>&>(getIlluminationModelBase(i));
  }

  IlluminationModelBase& findIlluminationModelBase(const QString &name) const;

  template<typename T>
  IlluminationModel<T>& findIlluminationModel(const QString &name) const
  {
    return dynamic_cast<IlluminationModel<T>&>(findIlluminationModelBase(name));
  }

  void removeIlluminationModel(size_t i);
  size_t illuminationModelsCount() const;

  template<typename T>
  static T mixFrictionFactor(T a, T b)
  {
    return std::sqrt(a*a + b*b);
  }

  Landscape* getBringedLandscape(LandscapeInfo *info) const;

  const QString &getMetadata(const QString &key) const;
  void setMetadata(const QString &key, const QString &value);
  void setMetadata(const QHash<QString, QString> &value);

  bool canPlace(KawaiiModel3D *model, const QVector3D &pos) const;
  bool canPlace(Entity &entity, const QVector3D &pos) const;
  bool canPlace(const QString &model, const QVector3D &pos) const;

  /// Clears world ignoring ResourcePack
  void clear();

  /// Loads world from data stream. Ignores the ResourcePack
  void load(QDataStream &st);

  /// Saves world to data stream. Ignores the ResourcePack
  void save(QDataStream &st) const;

  bool hasGearbox() const;
  Gearbox& getGearbox() const;
  void setGearbox(std::unique_ptr<Gearbox> &&newGearbox);

signals:
  void worldLoaded();
  void entityActiveted(Entity *e);
  void entityDeactiveted(Entity *e);
  void locationChanged();

  void gearboxChanged();

  // QObject interface
protected:
  void childEvent(QChildEvent *event) override;


private:
  std::unique_ptr<ResourcePack> resources;

  std::unique_ptr<QReadWriteLock> objects_mutex;
  std::list<Entity*> activeEntities;
  sib_utils::NamedTypedQObjectIndex<Entity> indexedEntities;

  std::unordered_map<LandscapeInfo*, Landscape*> landscapes;

  Location *location;
  QMetaObject::Connection onCurrentLocationDestroyed;

  std::unique_ptr<Gearbox> gearbox;
  std::vector<std::unique_ptr<Input>> inputs;
  std::vector<QPointer<KawaiiSurface>> attachedSurfaces;

  QHash<QString, QString> metadata;
  std::unique_ptr<QReadWriteLock> metadata_mutex;

  std::vector<CollisionInfo> collisions;
  std::unique_ptr<QReadWriteLock> collisions_mutex;

  std::vector<std::unique_ptr<IlluminationModelBase>> illumination;

  std::chrono::time_point<std::chrono::steady_clock> ageTimestamp;

  std::unique_ptr<QReadWriteLock> age_mutex;
  float ageSeconds;
  float ageSeconds_last;
  float refTickInterval;
  bool timeIsRunning;

  bool destroying;

  void tick();
  void tick(float sec_elapsed);

  void onLocationRemovesEntity(Entity *e);

  void onLocationAddsLandscape(LandscapeInfo *fromLocation);
  void onLocationRemovesLandscape(LandscapeInfo *fromLocation);

  void deactivateEntity(std::list<Entity*>::iterator el);
};

#endif // WORLD_HPP
