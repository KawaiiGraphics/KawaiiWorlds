#ifndef LANDSCAPE_HPP
#define LANDSCAPE_HPP

#include "LandscapeInfo.hpp"

#include <Kawaii3D/Geometry/KawaiiMeshInstance.hpp>
#include <Kawaii3D/Geometry/KawaiiMesh3D.hpp>
#include <Kawaii3D/Textures/KawaiiImage.hpp>
#include <QPointer>
#include <QVector3D>
#include <QVector2D>

#include <BulletDynamics/Dynamics/btRigidBody.h>
#include <memory>

class KAWAIIWORLDS_SHARED_EXPORT Landscape: public KawaiiDataUnit
{
  Q_OBJECT

public:
  Q_PROPERTY(float restitution READ getRestitution WRITE setRestitution NOTIFY restitutionChanged FINAL)
  Q_PROPERTY(float friction READ getFriction WRITE setFriction NOTIFY frictionChanged FINAL)
  Q_PROPERTY(float frictionRolling READ getFrictionRolling WRITE setFrictionRolling NOTIFY frictionRollingChanged FINAL)
  Q_PROPERTY(float frictionSpinning READ getFrictionSpinning WRITE setFrictionSpinning NOTIFY frictionSpinningChanged FINAL)

  QHash<QString, QString> metadata;
  QObject *jsWrapper;

  Landscape();
  ~Landscape();

  void setHeightMap(KawaiiImage *heightMap);

  void build(const QVector3D &pos, const QVector3D &size, float step);
  const std::vector<std::unique_ptr<btRigidBody>>& createRigidBodies(const QVector2D &chunkSize);
  const std::vector<std::unique_ptr<btRigidBody>>& getRigidBodies() const;
  void removeRigidBodies();

  /// pos should be in the [-sz/2; sz/2] range
  float getHeight(const QVector2D &pos) const;

  /// pos0 and pos1 should be in the [-sz/2; sz/2] range
  float getHeight(const QVector2D &pos0, const QVector2D &pos1) const;

  /// pos0 and pos1 should be in the [-sz/2; sz/2] range
  float getSlope(const QVector2D &pos0, const QVector2D &pos1) const;

  /// pos should be in the [-sz/2; sz/2] range
  void bump(const QVector2D &pos, float val, float radius, float hardness, float pressure);

  void setMaterial(KawaiiMaterial &m);

  bool flushHeightMap();

  void unload(LandscapeInfo &out);

  bool isEq(const LandscapeInfo &val) const;

  float getRestitution() const;
  void setRestitution(float newRestitution);

  float getFriction() const;
  void setFriction(float newFriction);

  float getFrictionRolling() const;
  void setFrictionRolling(float newFrictionRolling);

  float getFrictionSpinning() const;
  void setFrictionSpinning(float newFrictionSpinning);

signals:
  void restitutionChanged();
  void frictionChanged();
  void frictionRollingChanged();
  void frictionSpinningChanged();



  //IMPLEMENT
private:
  struct Uniforms
  {
    QVector4D landSz;
    QVector4D pos;
  };

  QPointer<KawaiiImage> heights;

  std::unique_ptr<KawaiiModel3D> model;
  std::unique_ptr<KawaiiMesh3D> mesh;
  KawaiiMeshInstance *meshInstance;

  QMetaObject::Connection updateBtHeightfieldData;
  std::vector<std::shared_ptr<btCollisionShape>> btShapes;
  std::vector<std::unique_ptr<btRigidBody>> rigidBodies;

  struct HeightfieldChunk {
    std::vector<float> data;
    int szX;
    int szZ;
    float minHeight;
    float maxHeight;
  };
  std::vector<HeightfieldChunk> heightfieldData;

  QPointer<KawaiiMaterial> material;

  QVector3D sz;
  QVector3D pos;
  float step;
  float restitution;
  float friction;
  float frictionRolling;
  float frictionSpinning;
  bool heightMapDirty;

  void initUniforms();

  float convertHeight(float val) const;

  void checkHeightmapFmt() const;

  bool forallHeightmapPixel(const QVector2D &pos0, const QVector2D &pos1, const std::function<void(const QRgb *)> &func) const;

  void createHeightfieldData(const QVector2D &chunkSize);
};

#endif // LANDSCAPE_HPP
