#include "KawaiiQmlLayer.hpp"
#include "EngineController.hpp"

#include <QQuickItem>
#include <QQuickRenderTarget>
#include <QQuickGraphicsDevice>

#include <Kawaii3D/Textures/KawaiiImage.hpp>

KAWAII_COMMON_IMPLEMENT(KawaiiQmlLayer::reg);

KawaiiQmlLayer::KawaiiQmlLayer():
  qmlWnd(&renderCtrl),
  ui(nullptr),
  deferredDraw(false),
  qmlNeedsPolish(false),
  qmlNeedsRender(false)
{
  qmlWnd.setDefaultAlphaBuffer(true);
  qmlWnd.setColor(QColor(0,0,0,0));

  connect(&renderCtrl, &QQuickRenderControl::sceneChanged, this, &KawaiiQmlLayer::requestPolish);
  connect(&renderCtrl, &QQuickRenderControl::renderRequested, this, &KawaiiQmlLayer::requestRender);
  connect(&qmlWnd, &QQuickWindow::afterAnimating, this, &KawaiiQmlLayer::requestRender);

  connect(this, &KawaiiImageSource::renderableSizeChanged, this, &KawaiiQmlLayer::updateQmlWndRenderTarget);
  connect(this, &sib_utils::TreeNode::parentUpdated, this, &KawaiiQmlLayer::tryFinalizeMemLoad);

  rhiTexRender = createChild<KawaiiExternalQRhiRender>();
  rhiTexRender->setObjectName("KawaiiQmlLayer:rhi_render");
  tex = createChild<KawaiiImage>();
  tex->setObjectName("KawaiiQmlLayer:render_target_texture");
  auto excludedChildren = property("ExcludedChildren").value<QList<QVariant>>();
  excludedChildren.push_back(QVariant::fromValue<QObject*>(rhiTexRender));
  excludedChildren.push_back(QVariant::fromValue<QObject*>(tex));
  setProperty("ExcludedChildren", excludedChildren);

  updateQmlWndRenderTarget();
  setTexture(tex);
  rhiTexRender->setRenderTargetTexture(tex);

  connect(rhiTexRender, &KawaiiExternalQRhiRender::rhiChanged, this, &KawaiiQmlLayer::onRhiChanged);
}

KawaiiQmlLayer::~KawaiiQmlLayer()
{
  disconnect(this, &KawaiiImageSource::renderableSizeChanged, this, nullptr);

  delete rhiTexRender;
  delete tex;

  if(ui)
    delete ui;
}

QQuickWindow &KawaiiQmlLayer::getQmlWindow()
{
  return qmlWnd;
}

QQuickItem *KawaiiQmlLayer::getUiItem() const
{
  return ui;
}

void KawaiiQmlLayer::setUiItem(QQuickItem *item)
{
  if(ui == item) return;

  if(ui)
    {
      ui->setParentItem(nullptr);
      if(ui->parent() == this)
        ui->setParent(nullptr);
    }
  ui = item;
  if(ui)
    ui->setParentItem(qmlWnd.contentItem());
}

void KawaiiQmlLayer::onRhiChanged(QRhi *rhi, QRhiRenderTarget *renderTarget)
{
  bool needResetRenderCtrl = false;
  auto quickRenderTarget = QQuickRenderTarget::fromRhiRenderTarget(renderTarget);
  if(rhi->backend() == QRhi::OpenGLES2)
    {
      quickRenderTarget.setMirrorVertically(true);
    }

  if(renderCtrl.rhi() != rhi)
    {
      switch(rhi->backend())
        {
        case QRhi::Vulkan:
          qmlWnd.setGraphicsApi(QSGRendererInterface::Vulkan);
          qmlWnd.setVulkanInstance(static_cast<const QRhiVulkanNativeHandles*>(rhi->nativeHandles())->inst);
          break;

        case QRhi::Metal:
          qmlWnd.setGraphicsApi(QSGRendererInterface::Metal);
          break;

        case QRhi::OpenGLES2:
          qmlWnd.setGraphicsApi(QSGRendererInterface::OpenGL);
          break;

        case QRhi::D3D11:
          qmlWnd.setGraphicsApi(QSGRendererInterface::Direct3D11);
          break;

        case QRhi::D3D12:
          qmlWnd.setGraphicsApi(QSGRendererInterface::Direct3D12);
          break;

        case QRhi::Null:
          qmlWnd.setGraphicsApi(QSGRendererInterface::Null);
          break;
        }
      qmlWnd.setGraphicsDevice(QQuickGraphicsDevice::fromRhi(rhi));
      needResetRenderCtrl = true;
    }
  qmlWnd.setRenderTarget(quickRenderTarget);

  if(needResetRenderCtrl)
    {
      renderCtrl.invalidate();
      renderCtrl.initialize();
    }

  Q_ASSERT(renderCtrl.rhi() == rhi);

  if(deferredDraw)
    draw();
}

void KawaiiQmlLayer::draw()
{
  deferredDraw = !rhiTexRender->render([this](QRhi *rhi, QRhiRenderTarget *renderTarget) { return redraw(rhi, renderTarget); });
}

bool KawaiiQmlLayer::redraw(QRhi*, QRhiRenderTarget*)
{
  if(!qmlNeedsRender && !qmlNeedsPolish)
    return false;

  renderCtrl.beginFrame();
  if(qmlNeedsPolish)
    {
      renderCtrl.polishItems();
      renderCtrl.sync();
      qmlNeedsPolish = false;
    }
  renderCtrl.render();
  renderCtrl.endFrame();

  qmlNeedsRender = false;

  return true;
}

void KawaiiQmlLayer::requestRender()
{
  if(!qmlNeedsRender)
    {
      qmlNeedsRender = true;
      QMetaObject::invokeMethod(this, &KawaiiQmlLayer::draw, Qt::QueuedConnection);
    }
}

void KawaiiQmlLayer::requestPolish()
{
  if(!qmlNeedsPolish)
    {
      qmlNeedsRender = qmlNeedsPolish = true;
      QMetaObject::invokeMethod(this, &KawaiiQmlLayer::draw, Qt::QueuedConnection);
    }
}

void KawaiiQmlLayer::updateQmlWndRenderTarget()
{
  if(tex &&
     (getRenderableSize().x != static_cast<uint32_t>(tex->getResolution()[0])
     || getRenderableSize().y != static_cast<uint32_t>(tex->getResolution()[1])))
    {
      QSize sz(getRenderableSize().x, getRenderableSize().y);
      static_cast<KawaiiImage*>(tex)->setImage(QImage(sz, QImage::Format_RGBA8888));
      qmlWnd.resize(sz);
      requestPolish();
    }
}

void KawaiiQmlLayer::writeOwnMemData(sib_utils::memento::Memento::DataMutator &mem) const
{
  if(ui)
    {
      const auto qmlSrc = ui->property("__src").toStringList();
      mem.setData(QStringLiteral("qml_src"), qmlSrc.front());
      mem.setData(QStringLiteral("qml_fname"), qmlSrc.at(1));
      mem.setData(QStringLiteral("enable_input"), enableInput? QStringLiteral("yes"): QStringLiteral("no"));
    } else
    {
      mem.removeText(QStringLiteral("qml_src"));
      mem.removeText(QStringLiteral("qml_fname"));
      mem.removeText(QStringLiteral("enable_input"));
    }
}

void KawaiiQmlLayer::tryFinalizeMemLoad()
{
  if(!ui)
    {
      auto root = KawaiiRoot::getRoot<EngineController>(this);
      if(root)
        root->startQmlLayer(this);
    }
}

void KawaiiQmlLayer::writeBinary(sib_utils::memento::Memento::DataMutator &mem) const
{
  writeOwnMemData(mem);
}

void KawaiiQmlLayer::writeText(sib_utils::memento::Memento::DataMutator &mem) const
{
  writeOwnMemData(mem);
}

void KawaiiQmlLayer::read(sib_utils::memento::Memento::DataReader &mem)
{
  mem.readText(QStringLiteral("qml_src"), [this] (const QString &data) {
      setUiItem(nullptr);
      setProperty("qml_src", data);
    }).readText(QStringLiteral("qml_fname"), [this] (const QString &data) {
      setUiItem(nullptr);
      setProperty("qml_fname", data);
    }).readText(QStringLiteral("enable_input"), [this] (const QString &data) {
      setUiItem(nullptr);
      const auto lowerStr = data.toLower();
      enableInput = (!data.isNull() && !data.isEmpty()
                     && lowerStr != QStringLiteral("no")
          && lowerStr != QStringLiteral("false")
          && lowerStr != QStringLiteral("0"));
    });

  tryFinalizeMemLoad();
}
