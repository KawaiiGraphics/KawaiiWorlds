#ifndef KAWAIIQMLLAYER_HPP
#define KAWAIIQMLLAYER_HPP

#include <Kawaii3D/Renderpass/KawaiiTexLayer.hpp>
#include <Kawaii3D/Textures/KawaiiExternalQRhiRender.hpp>

#include <QOpenGLFramebufferObject>
#include <QQuickRenderControl>
#include <QQuickWindow>

class KawaiiQmlLayer : public KawaiiTexLayer
{
  Q_OBJECT
  KAWAII_UNIT_DEF(KawaiiQmlLayer);
  Q_PROPERTY(const QString MementoType MEMBER nodeType DESIGNABLE false CONSTANT);

public:
  bool enableInput;

  KawaiiQmlLayer();
  ~KawaiiQmlLayer();

  QQuickWindow& getQmlWindow();

  QQuickItem* getUiItem() const;
  void setUiItem(QQuickItem *item);



  //IMPLEMENT
private:
  KawaiiExternalQRhiRender *rhiTexRender;
  KawaiiTexture *tex;

  QQuickRenderControl renderCtrl;
  QQuickWindow qmlWnd;

  QQuickItem *ui;

  bool deferredDraw;
  bool qmlNeedsPolish;
  bool qmlNeedsRender;

  void onRhiChanged(QRhi *rhi, QRhiRenderTarget *renderTarget);

  void draw();
  bool redraw(QRhi *, QRhiRenderTarget *);

  void requestRender();
  void requestPolish();

  void updateQmlWndRenderTarget();

  void writeOwnMemData(sib_utils::memento::Memento::DataMutator &mem) const;

  void tryFinalizeMemLoad();

  // TreeNode interface
protected:
  void writeBinary(sib_utils::memento::Memento::DataMutator &mem) const override;
  void writeText(sib_utils::memento::Memento::DataMutator &mem) const override;
  void read(sib_utils::memento::Memento::DataReader &mem) override;
};

#endif // KAWAIIQMLLAYER_HPP
