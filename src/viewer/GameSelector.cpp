#include "GameSelector.hpp"

#include <Kawaii3D/Surfaces/KawaiiRender.hpp>
#include <QGuiApplication>
#include <QQuickItem>
#include <QQmlEngine>
#include <QHostInfo>
#include <QPalette>

bool operator== (const GameSelector::StartParameters &a, const GameSelector::StartParameters &b)
{
  if(a.isClient != b.isClient)
    return false;

  const bool isNetParamsEq = a.hostAddr == b.hostAddr
      && a.port == b.port
      && a.rendererPlugin == b.rendererPlugin;

  if(a.isClient)
    return isNetParamsEq;
  else
    return isNetParamsEq && a.filePath == b.filePath;
}

namespace {
  const QStringList &getAvailableKawaiiRenderers()
  {
    static bool initialized = false;
    static QStringList renders ({QStringLiteral("Default")});
    if(!initialized)
      renders.append(KawaiiRender::getAvailableRenderers());
    return renders;
  }
}

GameSelector::GameSelector(QQmlEngine *qmlEngine, QObject *parent)
  : QObject { parent },
    qmlEngine(qmlEngine),
    component(nullptr),
    qItem(nullptr),
    params({
           .port = 0,
           .isClient = false
           })
{
  connect(qmlEngine, &QQmlEngine::exit, &wnd, &QQuickWindow::close);
}

GameSelector::~GameSelector()
{
  removeQmlComponent();
}

GameSelector::StartParameters GameSelector::getParams() const
{
  return params;
}

GameSelector::StartParameters GameSelector::exec()
{
  show();
  QEventLoop gameSelectEvents;
  connect(this, &GameSelector::selected, &gameSelectEvents, &QEventLoop::quit);
  connect(&wnd, &QWindow::visibleChanged, &gameSelectEvents, [&gameSelectEvents] (bool wndVisible) {
      if(!wndVisible)
        gameSelectEvents.quit();
    });
  gameSelectEvents.exec();
  return getParams();
}

void GameSelector::show()
{
  if(!component)
    createQmlComponent();

  params = {
    .port = 0,
    .isClient = false
  };

  wnd.setColor(QGuiApplication::palette().window().color());
  wnd.resize(QSize(1280, 720));
  wnd.show();
}

void GameSelector::createQmlComponent()
{
  component = new QQmlComponent(qmlEngine);
  component->setParent(qmlEngine);
  component->loadUrl(QUrl(QStringLiteral("qrc:/GameSelectorForm.qml")));

  qItem = static_cast<QQuickItem*>(component->create());
  if(!qItem)
    {
      qCritical().noquote() << "Can not show game selector form!";
      qCritical().noquote() << component->errorString();
      throw std::runtime_error("Can not show game selector form!");
    }

  qItem->setProperty("renderers", getAvailableKawaiiRenderers());
  qItem->setParentItem(wnd.contentItem());

  bool goGamesBtnAvailable = false;

  QDir d(QDir::rootPath()+"/usr/games");
  d.setPath(d.canonicalPath());

  if(d.path().isEmpty() || !d.exists() || d.isEmpty())
    {
      d.setPath(QDir::rootPath()+"/games");
      d.setPath(d.canonicalPath());
    } else
    goGamesBtnAvailable = true;

  if(d.path().isEmpty() || !d.exists() || d.isEmpty())
    {
      d.setPath(QCoreApplication::applicationDirPath() + "/../games");
      d.setPath(d.canonicalPath());
    } else
    goGamesBtnAvailable = true;

  if(d.path().isEmpty() || !d.exists())
    {
      d.setPath(QCoreApplication::applicationDirPath() + "/games");
      d.setPath(d.canonicalPath());
    }

  if(d.path().isEmpty() || !d.exists())
    d = QDir::home();
  qItem->setProperty("goGamesBtnAvailable", goGamesBtnAvailable);
  qItem->setProperty("directory", QUrl::fromLocalFile(d.path()));

  connect(qItem, SIGNAL(goHomeClicked()), this, SLOT(goHome()));
  connect(qItem, SIGNAL(goRootClicked()), this, SLOT(goRoot()));
  connect(qItem, SIGNAL(goGamesClicked()), this, SLOT(goGames()));

  disconnect(onFinished);
  onFinished = connect(qItem, SIGNAL(finished(QString)), this, SLOT(gameSelected(QString)));
}

void GameSelector::removeQmlComponent()
{
  if(qItem)
    {
      delete qItem;
      qItem = nullptr;
    }

  if(component)
    {
      delete component;
      component = nullptr;
    }
}

void GameSelector::goHome()
{
  qItem->setProperty("directory", QUrl::fromLocalFile(QDir::homePath()));
}

void GameSelector::goRoot()
{
  QDir d(QCoreApplication::applicationDirPath() + "/../games");
  if(!d.exists())
    d.setPath(QCoreApplication::applicationDirPath() + "/games");
  if(!d.exists())
    d.setPath(QCoreApplication::applicationDirPath());
  d.setPath(d.canonicalPath());
  qItem->setProperty("directory", QUrl::fromLocalFile(d.path()));
}

void GameSelector::goGames()
{
  QDir d(QDir::rootPath()+"/usr/games");
  d.setPath(d.canonicalPath());
  if(!d.exists() || d.isEmpty())
    d.setPath(QDir::rootPath()+"/games");
  d.setPath(d.canonicalPath());
  qItem->setProperty("directory", QUrl::fromLocalFile(d.path()));
}

void GameSelector::gameSelected(const QString &urlLocalPath)
{
  QString addrStr;
  QString portStr;
  bool isLocal;
  if(sender())
    {
      params.rendererPlugin = sender()->property("selectedRenderer").toString();
      params.isClient = sender()->property("isClient").toBool();
      isLocal = sender()->property("isLocal").toBool();
      addrStr = sender()->property("hostname").toString();
      portStr = sender()->property("server_port").toString();
    } else
    {
      qCritical() << "KawaiiWorldsViewer: Internal error! GameSelector::gameSelected was called without a sender";
      return;
    }

  if(isLocal)
    params.hostAddr = QHostAddress();
  else
    {
      if(addrStr.isEmpty())
        {
          params.hostAddr = QHostAddress::LocalHost;
        } else if(!params.hostAddr.setAddress(addrStr))
        {
          const auto hosts = !addrStr.isEmpty()? QHostInfo::fromName(addrStr).addresses(): QHostInfo().addresses();
          params.hostAddr = !hosts.empty()? hosts.first(): QHostAddress::Null;
        }
      bool portReaded;
      params.port = portStr.toUShort(&portReaded);
      if(!portReaded)
        params.port = 25564;
    }

  params.filePath = QUrl(urlLocalPath).toLocalFile();

  wnd.hide();
  emit selected();
}
