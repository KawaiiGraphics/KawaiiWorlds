#ifndef GAMESELECTOR_HPP
#define GAMESELECTOR_HPP

#include <QObject>
#include <QStringList>
#include <QQuickWindow>
#include <QHostAddress>
#include <QQmlComponent>

class GameSelector : public QObject
{
  Q_OBJECT
public:
  struct StartParameters {
    QString filePath;
    QString rendererPlugin;
    QHostAddress hostAddr;
    uint16_t port;
    bool isClient;
  };

  explicit GameSelector(QQmlEngine *qmlEngine, QObject *parent = nullptr);
  ~GameSelector();

  StartParameters getParams() const;
  StartParameters exec();

public slots:
  void show();

  void createQmlComponent();
  void removeQmlComponent();

  void goHome();
  void goRoot();
  void goGames();

signals:
  void selected();



  // IMPLEMENT
private:
  QQuickWindow wnd;

  QMetaObject::Connection onFinished;

  QQmlEngine *qmlEngine;
  QQmlComponent *component;
  QQuickItem *qItem;

  StartParameters params;


private slots:
  void gameSelected(const QString &urlLocalPath);
};

bool operator== (const GameSelector::StartParameters &a, const GameSelector::StartParameters &b);
inline bool operator!= (const GameSelector::StartParameters &a, const GameSelector::StartParameters &b)
{ return !(a == b); }

#endif // GAMESELECTOR_HPP
