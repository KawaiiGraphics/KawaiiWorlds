#ifndef ENGINECONTROLLER_HPP
#define ENGINECONTROLLER_HPP

#include <QObject>
#include <QQmlEngine>
#include <QQuickItem>
#include <unordered_set>
#include <Kawaii3D/KawaiiRoot.hpp>

#include "World.hpp"
#include "WorldDOM.hpp"
#include "GameSelector.hpp"
#include "KawaiiWindow.hpp"
#include "KawaiiQmlLayer.hpp"
#include "ResourcePackJsonLoad.hpp"

class EngineController : public KawaiiDataUnit
{
  Q_OBJECT
public:
  using StartParameters = GameSelector::StartParameters;

  struct Timer {
    KawaiiGpuBuf *buf;
    size_t offset;

    float scale;
    float phase;
  };

  EngineController();

  void start(StartParameters &startParams);

  void startQmlLayer(KawaiiQmlLayer *qmlLayer);

public slots:
  void clear();
  void selectGame();

  inline void start(StartParameters &&startParams)
  { return start(static_cast<StartParameters &>(startParams)); }



  // IMPLEMENT
private:
  class WorldLoader {
  public:
    EngineController *engine;
    std::unique_ptr<QObject> networkObject;

    virtual WorldDOM loadWorld(const StartParameters &parameters) = 0;
    void loadQmlLayer(KawaiiQmlLayer *qmlLayer);
    virtual ~WorldLoader();

  private:
    std::unordered_set<KawaiiQmlLayer*> pendingQmlLayers;
  };
  struct ClientWorldLoader;
  struct ServerWorldLoader;
  struct LocalWorldLoader;

  QQmlEngine qmlEngine;
  std::unique_ptr<WorldLoader> worldLoader;

  std::vector<EngineController::Timer> timers;
  std::function<void(const QJsonValue &val, std::vector<std::byte> &content, KawaiiGpuBuf *buf, size_t &byteIndex)> loadTimerFunc;

  KawaiiRoot root;
  World world;
  QJSValue worldJs;
  std::unique_ptr<QObject> networkObject;
  KawaiiWindow window;


  KawaiiImageSource* loadQmlLayer(QString src, const QString &fname, bool enableInput, KawaiiRenderpass *rp);
  KawaiiImageSource* loadQmlLayer(const QJsonObject &val, KawaiiRenderpass *rp);

  ResourcePackJsonLoader createResPackLoader(const QDir &rootDir);

  Entity *loadEntity(const EntityDOM &dom);

  void parseWorldManifest(const WorldDOM &dom);

  void onWorldReloaded();

  QQuickItem* createQmlItem(const QString &source, const QString &fName);

  void updateGpuTimers(float totalSecElapsed);

  void drawDbgAABB(QPainter &painter);
};

#endif // ENGINECONTROLLER_HPP
