#include "KawaiiWindow.hpp"

#include <Kawaii3D/Renderpass/KawaiiQPainterLayer.hpp>

KawaiiWindow::KawaiiWindow(KawaiiRoot &root, QObject *parent):
  QObject(parent),
  root(root)
{
}

void KawaiiWindow::setupSurface(KawaiiRenderpass *rp, const QString &rendererPluginName)
{
  sfc = std::make_unique<KawaiiSurface>(rp);

  sfc->setFrametimeCounter(&lastFrametime);
  class OnUpdated: public QObject {
  public:
    KawaiiWindow *window;
    bool eventFilter(QObject *, QEvent *e) override final
    {
      if(e->type() == QEvent::UpdateRequest)
        {
          window->totalSecElapsed += (window->lastFrametime) / 1000.0;
          emit window->totalSecElapsedChanged(window->totalSecElapsed);
        }
      return false;
    }
  };
  OnUpdated *onWndUpdated = new OnUpdated;
  onWndUpdated->setParent(this);
  onWndUpdated->window = this;
  sfc->getWindow().installEventFilter(onWndUpdated);

  sfc->updateParent(&root);

  if(rendererPluginName.isNull() || rendererPluginName.toLower() == QStringLiteral("default"))
    root.createRendererImplementation();
  else
    root.createRendererImplementation( {rendererPluginName} );
}

void KawaiiWindow::setIcon(const QIcon &icon)
{
  sfc->getWindow().setIcon(icon);
}

QWindow &KawaiiWindow::getQWindow() const
{
  return sfc->getWindow();
}

KawaiiSurface &KawaiiWindow::getSurface() const
{
  return *sfc;
}

void KawaiiWindow::show(bool windowedMode)
{
  if(windowedMode)
    sfc->showMaximized();
  else
    sfc->showFullScreen();

  sfc->getWindow().setMouseGrabEnabled(true);
}

void KawaiiWindow::close()
{
  sfc->getWindow().close();
}
