#ifndef INPUTTRANSFER_HPP
#define INPUTTRANSFER_HPP

#include "input/Input.hpp"
#include <QQuickWindow>
#include <QPointer>

class InputTransfer: public Input
{
public:
  InputTransfer(QQuickWindow *target);
  ~InputTransfer();

  inline void setFocusedItem(QQuickItem *val)
  { focusedItem = val; }

  // Input interface
public:
  void install(World &world, KawaiiSurface &sfc) override;
  void pushMessages(std::vector<QString> &&msg) override;



  //IMPLEMENT
private:
  QQuickWindow *target;
  QPointer<QQuickItem> focusedItem;

  std::vector<QObject*> eventFilters;
};

#endif // INPUTTRANSFER_HPP
