#ifndef WORLDDOM_HPP
#define WORLDDOM_HPP

#include "IlluminationModel.hpp"

#include <Kawaii3D/Illumination/KawaiiIllumination.hpp>
#include <Kawaii3D/Illumination/KawaiiPhong.hpp>
#include <Kawaii3D/Illumination/KawaiiPBR.hpp>
#include <Kawaii3D/Illumination/KawaiiCel.hpp>

#include <sib_utils/qDataStreamExpansion.hpp>
#include <glm/mat4x4.hpp>
#include <QJsonObject>
#include <QJsonArray>
#include <QVector3D>
#include <QIcon>
#include <QDir>

namespace dom_json_loaders {
  template<typename T>
  void loadVecJson(const QJsonArray &arr, std::vector<T> &vec)
  {
    vec.reserve(arr.size());
    for(const auto &i: arr)
      if(i.isObject())
        {
          vec.emplace_back();
          vec.back().read(i.toObject());
        }
  }
}

struct CameraDOM
{
  QString name;
  glm::mat4 viewMat;

  void read(const QJsonObject &json);
};

struct EntityDOM
{
  QString name;
  QVariantMap body;

  QVector3D position;
  QVector3D rotation;

  QVector3D velocity;
  QVector3D angular_velocity;

  QHash<QString, QString> metadata;

  float mass;
  float restitution;
  float friction;
  float frictionRolling;
  float frictionSpinning;

  float lookingForwardFactorX;
  float lookingForwardFactorY;
  float lookingForwardFactorZ;

  bool lookForward;
  bool clickOpacity;

  void read(const QJsonObject &json);
};

struct LandscapeDOM
{
  QString name;
  QString material;
  QString heightMapTexture;
  QVector3D size;
  QVector3D pos;
  QHash<QString, QString> metadata;
  QVector2D chunkSize;

  float step;

  float restitution;
  float friction;
  float frictionRolling;
  float frictionSpinning;

  bool createCollisionShape;

  void read(const QJsonObject &json);
};

struct LocationDOM
{
  QString name;
  std::vector<EntityDOM> entities;
  std::vector<LandscapeDOM> landscapes;

  void read(const QJsonObject &json);
};

struct ShadowMapperDOM
{
  QVector3D dirLightsCenter;
  QVector3D dirLightsArea;
  QSize depthMapResolution;
  float nearClip;
  float farClip;
  bool enabled;

  void read(const QJsonObject &json);
  void setDefault();
};

struct PbrLightProperties
{
  QVector3D color;

  void read(const QJsonObject &json);

  void write(KawaiiPbrLight &l) const;
};

struct PhongLightProperties
{
  QVector3D diffuse;
  QVector3D specular;

  void read(const QJsonObject &json);

  void write(KawaiiPhongLight &l) const;
};

struct CelLightProperties
{
  QVector3D diffuse;
  QVector3D specular;

  void read(const QJsonObject &json);

  void write(KawaiiCelLight &l) const;
};

struct DirLightProperties
{
  QVector3D ambient;
  QVector3D direction;

  void read(const QJsonObject &json);
};

struct DotLightProperties
{
  QVector3D ambient;
  QVector3D position;

  float constAttenuation;
  float linearAttenuation;
  float quadraticAttenuation;

  void read(const QJsonObject &json);
};

struct SpotLightProperties
{
  QVector3D ambient;
  QVector3D position;
  QVector3D direction;

  float spotExponent;

  float constAttenuation;
  float linearAttenuation;
  float quadraticAttenuation;

  float spotCutoffRadians;

  void read(const QJsonObject &json);
};

template<typename T>
struct DirLightDOM: public T, public DirLightProperties {
  void read(const QJsonObject &json)
  {
    T::read(json);
    DirLightProperties::read(json);
  }

  template<typename U>
  void write(typename KawaiiIlluminationStructs<U>::DirLight &l) const
  {
    T::write(l);
    l.ambient = ambient;
    l.direction = direction;
  }
};

template<typename T>
struct DotLightDOM: public T, public DotLightProperties {
  void read(const QJsonObject &json)
  {
    T::read(json);
    DotLightProperties::read(json);
  }

  template<typename U>
  void write(typename KawaiiIlluminationStructs<U>::Lamp &l) const
  {
    T::write(l);
    l.ambient = ambient;
    l.position = position;
    l.constAttenuation = constAttenuation;
    l.linearAttenuation = linearAttenuation;
    l.quadraticAttenuation = quadraticAttenuation;
  }
};

template<typename T>
struct SpotLightDOM: public T, public SpotLightProperties {
  void read(const QJsonObject &json)
  {
    T::read(json);
    SpotLightProperties::read(json);
  }

  template<typename U>
  void write(typename KawaiiIlluminationStructs<U>::Projector &l) const
  {
    T::write(l);
    l.ambient = ambient;
    l.position = position;
    l.constAttenuation = constAttenuation;
    l.linearAttenuation = linearAttenuation;
    l.quadraticAttenuation = quadraticAttenuation;
    l.direction = direction;
    l.spotCutoffRadians = spotCutoffRadians;
    l.spotExponent = spotExponent;
  }
};

template<typename T>
struct IlluminationModelDOM
{
  QString name;
  std::vector<DirLightDOM<T>> dirLights;
  std::vector<DotLightDOM<T>> dotLights;
  std::vector<SpotLightDOM<T>> spotLights;
  ShadowMapperDOM shadowmapper;

  void read(const QJsonObject &json)
  {
    name = json.value(QLatin1String("name")).toString();
    dom_json_loaders::loadVecJson(json.value(QLatin1String("dir_lights")).toArray(), dirLights);
    dom_json_loaders::loadVecJson(json.value(QLatin1String("dot_lights")).toArray(), dotLights);
    dom_json_loaders::loadVecJson(json.value(QLatin1String("spot_lights")).toArray(), spotLights);
    auto shadowmapperJson = json.value(QLatin1String("shadowmapper"));
    if(shadowmapperJson.isObject())
      shadowmapper.read(shadowmapperJson.toObject());
    else if(shadowmapperJson.isBool())
      {
        if(shadowmapperJson.toBool())
          shadowmapper.setDefault();
        else
          shadowmapper.enabled = false;
      } else
      shadowmapper.enabled = false;
  }

  template<typename U>
  void write(IlluminationModel<U> &illum) const
  {
    using IllumStructs = KawaiiIlluminationStructs<U>;

    size_t n = 0;
    for(const auto &i: dirLights)
      {
        illum.addDirLight();
        illum.changeDirLight(n, [&i] (typename IllumStructs::DirLight &l) {
            i.template write<U>(l);
            return true;
          });
        ++n;
      }
    n = 0;
    for(const auto &i: dotLights)
      {
        illum.addDotLight();
        illum.changeDotLight(n, [&i] (typename IllumStructs::Lamp &l) {
            i.template write<U>(l);
            return true;
          });
        ++n;
      }
    n = 0;
    for(const auto &i: spotLights)
      {
        illum.addSpotLight();
        illum.changeSpotLight(n, [&i] (typename IllumStructs::Projector &l) {
            i.template write<U>(l);
            return true;
          });
        ++n;
      }

    if(shadowmapper.enabled)
      {
        KawaiiShadowMapper *shadow_mapper = illum.createShadowMapper();
        shadow_mapper->setResolution(shadowmapper.depthMapResolution);
        shadow_mapper->setNearClip(shadowmapper.nearClip);
        shadow_mapper->setFarClip(shadowmapper.farClip);
        shadow_mapper->setDirLightsCenter(shadowmapper.dirLightsCenter);
        shadow_mapper->setDirLightsArea(shadowmapper.dirLightsArea);
      }
  }
};

using IllumModelDOM = std::variant<
IlluminationModelDOM<PbrLightProperties>,
IlluminationModelDOM<PhongLightProperties>,
IlluminationModelDOM<CelLightProperties>
>;

struct BarierClockworkDOM
{
  std::optional<float> min_x = {};
  std::optional<float> max_x = {};

  std::optional<float> min_y = {};
  std::optional<float> max_y = {};

  std::optional<float> min_z = {};
  std::optional<float> max_z = {};

  std::optional<float> frictionFactor = {};

  void read(const QJsonObject &json);
};

struct FrictionClockworkDOM
{ };

struct GravityAccelClockworkDOM
{
  QVector3D g;

  void read(const QJsonObject &json);
};

struct HookesLawClockworkDOM
{ };

struct ViscosityClockworkDOM
{
  float viscosity_factor;

  void read(const QJsonObject &json);
};

struct SolidizerClockworkDOM
{ };

using ClockworkDOM = std::variant<
BarierClockworkDOM,
FrictionClockworkDOM,
GravityAccelClockworkDOM,
HookesLawClockworkDOM,
ViscosityClockworkDOM,
SolidizerClockworkDOM
>;

std::optional<ClockworkDOM> readClockworkDOM(const QJsonObject &json);

struct WorldDOM
{
  QDir rootDir;
  QJsonObject respackJson;

  QString name;
  QString version;

  QString selectedRenderer;

  QIcon icon;

  std::vector<CameraDOM> cameras;

  std::vector<EntityDOM> persistentEntities;
  std::vector<LocationDOM> locations;

  std::vector<IllumModelDOM> illuminationModels;

  QVariantMap gearbox;
  std::vector<ClockworkDOM> clockworks;

  QString mainSound;

  QString mainQmlSrc;
  QString mainJsSrc;

  QString mainQmlFname;
  QString mainJsFname;

  QHash<QString, QString> metadata;

  quint64 networkHeartbeat;
  qint32 physicTickInterval;
  qint16 entitiesSyncInterval;
  qint16 worldSyncInterval;

  bool isWindowedMode;
  bool isNetworkSupported;
  bool debugAABB;

  void read(const QString &filePath);

  void writeManifest(QDataStream &st) const;
  void readManifest(QDataStream &st);
};

#endif // WORLDDOM_HPP
