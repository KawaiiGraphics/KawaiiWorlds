#include "InputTransfer.hpp"
#include <Kawaii3D/Surfaces/KawaiiSurface.hpp>
#include <QCoreApplication>
#include <QQuickItem>

namespace {
  qreal getTotalOpacity(QQuickItem *item)
  {
    qreal result = 1.0;
    while(item && result)
      {
        result *= item->opacity();
        item = item->parentItem();
      }
    return result;
  }
}

InputTransfer::InputTransfer(QQuickWindow *target):
  target(target),
  focusedItem(nullptr)
{
}

InputTransfer::~InputTransfer()
{
  auto _eventFilters = std::move(eventFilters);
  for(auto *i: _eventFilters)
    delete i;
}

void InputTransfer::install([[maybe_unused]] World &world, KawaiiSurface &sfc)
{
  class EventFilter: public QObject
  {
    ::InputTransfer &obj;
  public:
    EventFilter(::InputTransfer &obj):
      obj(obj)
    {
    }

    ~EventFilter()
    {
      if(auto el = std::find(obj.eventFilters.begin(), obj.eventFilters.end(), this);
         el != obj.eventFilters.end())
        obj.eventFilters.erase(el);
    }

    static QQuickItem* focusableChild(QQuickItem *parent, qreal x, qreal y)
    {
      const auto children = parent->childItems();
      if(!children.isEmpty())
        for(auto i = children.crbegin(); i != children.crend(); ++i)
          if((*i)->isVisible() && (*i)->isEnabled())
            {
              const auto point = parent->mapToItem(*i, QPointF(x, y));

              if(point.x() >= 0 && (*i)->width() > point.x()
                 && point.y() >= 0 && (*i)->height() > point.y())
                {
                  return *i;
                }
            }
      return nullptr;
    }

    bool eventFilter(QObject *watched, QEvent *event) override
    {
      static_cast<void>(watched);

      bool acquireEvent = false;
      switch(event->type())
        {
        case QEvent::KeyPress:
        case QEvent::KeyRelease:
          if(obj.focusedItem)
            {
              auto el = obj.focusedItem;
              bool b = false;
              while(el)
                {
                  b |= QCoreApplication::sendEvent(el, event);
                  el = el->parentItem();
                }
              b || QCoreApplication::sendEvent(obj.target, event);
            } else
            QCoreApplication::sendEvent(obj.target, event);
          return false;

        case QEvent::MouseButtonPress:
        case QEvent::MouseButtonRelease:
        case QEvent::MouseButtonDblClick:
          {
            auto lastFocused = obj.focusedItem.data();

            const auto pos = static_cast<QMouseEvent*>(event)->position();
            QCoreApplication::sendEvent(obj.target, event);

            auto focusedItem = obj.target->contentItem();
            while(auto ch = focusableChild(focusedItem,
                                           pos.x() - focusedItem->x(),
                                           pos.y() - focusedItem->y()))
              {
                focusedItem = ch;
              }

            if(focusedItem != lastFocused)
              {
                obj.focusedItem = focusedItem;
                if(lastFocused && !lastFocused->isFocusScope())
                  {
                    QFocusEvent focusAboutToChange(QEvent::FocusAboutToChange, Qt::MouseFocusReason);
                    QCoreApplication::sendEvent(lastFocused, &focusAboutToChange);
                  }
                QFocusEvent focusIn(QEvent::FocusIn, Qt::MouseFocusReason);
                QCoreApplication::sendEvent(obj.focusedItem, &focusIn);
                if(lastFocused && !lastFocused->isFocusScope())
                  {
                    QFocusEvent focusOut(QEvent::FocusOut, Qt::MouseFocusReason);
                    QCoreApplication::sendEvent(lastFocused, &focusOut);
                  }
              }

            const bool staticEnabled = obj.focusedItem->isEnabled() && obj.focusedItem->isVisible();
            auto isDynamiclyEnabled = [this] {
                bool result = true;
                if(const auto val = obj.focusedItem->property("enabled"); val.isValid() && !val.isNull())
                  result = val.toBool();
                if(const auto val = obj.focusedItem->property("visible"); val.isValid() && !val.isNull())
                  result = result && val.toBool();
                return result;
              };
            acquireEvent = staticEnabled
                && (obj.focusedItem->acceptedMouseButtons() != Qt::NoButton
                || obj.focusedItem->acceptTouchEvents())
                && isDynamiclyEnabled()
                && getTotalOpacity(obj.focusedItem.data()) > 0;
            return acquireEvent;
          }

        case QEvent::MouseMove:
        case QEvent::Scroll:
        case QEvent::ScrollPrepare:
        case QEvent::Shortcut:
        case QEvent::Clipboard:
        case QEvent::KeyboardLayoutChange:
        case QEvent::FocusIn:
        case QEvent::FocusOut:
        case QEvent::FocusAboutToChange:
        case QEvent::TabletMove:
        case QEvent::TabletPress:
        case QEvent::TabletRelease:
        case QEvent::TabletEnterProximity:
        case QEvent::TabletLeaveProximity:
        case QEvent::TouchBegin:
        case QEvent::TouchEnd:
        case QEvent::TouchCancel:
        case QEvent::TouchUpdate:
        case QEvent::Resize:
        case QEvent::Wheel:
          QCoreApplication::sendEvent(obj.target, event);
          return acquireEvent;

        default: return false;
        }
    }
  };

  auto *filter = new EventFilter(*this);
  QObject::connect(&sfc.getWindow(), &QObject::destroyed, filter, &QObject::deleteLater);
  eventFilters.push_back(filter);
  sfc.getWindow().installEventFilter(filter);
}

void InputTransfer::pushMessages([[maybe_unused]] std::vector<QString> &&msg)
{
  ;
}
