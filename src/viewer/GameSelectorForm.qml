import QtQuick
import Qt.labs.folderlistmodel
import QtQuick.Controls
import QtQuick.Layouts

Item {
    id: rootItem
    anchors.fill: parent

    signal finished(string fileName)

    property alias hostname   : ipEditor.text
    property alias server_port: portEditor.value
    property alias isClient   : clientCheck.checked
    property alias isLocal    : localCheck.checked

    property alias directory: folderModel.folder
    property var renderers: null
    property alias selectedRenderer: rendererSelector.currentText

    property string emojiFont
    function selectEmojiFont()
    {
        const availableFonts = Qt.fontFamilies()
        for(let i = 0; i < availableFonts.length; ++i)
            if([/*"OpenMoji", */"JoyPixels", "Noto Color Emoji", "Segoe UI Emoji", "Firefox Emoji", "Apple Color Emoji"].includes(availableFonts[i]))
            {
                emojiFont = availableFonts[i]
                return
            }
    }

    property alias goGamesBtnAvailable: btnGoGames.visible

    signal goHomeClicked
    signal goRootClicked
    signal goGamesClicked


    Component.onCompleted: selectEmojiFont()

    FolderListModel {
        id: folderModel
        nameFilters: ["*.game.json", "*.world.json"]
        showDirs: true
        showDirsFirst: true
        showDotAndDotDot: false
        onFolderChanged: { listView.currentIndex = -1 }
    }

    SystemPalette {
        id: active_palette
    }

    SystemPalette {
        id: disabled_palette
        colorGroup: SystemPalette.Disabled
    }
    palette: Palette {
        active.accent: active_palette.accent
        active.alternateBase: active_palette.alternateBase
        active.base: active_palette.base
        active.brightText: active_palette.text
        active.button: active_palette.button
        active.buttonText: active_palette.buttonText
        active.dark: active_palette.dark
        active.highlight: active_palette.highlight
        active.highlightedText: active_palette.highlightedText
        active.light: active_palette.light
        active.mid: active_palette.mid
        active.midlight: active_palette.midlight
        active.placeholderText: active_palette.placeholderText
        active.shadow: active_palette.shadow
        active.text: active_palette.text
        active.toolTipBase: active_palette.base
        active.toolTipText: active_palette.text
        active.window: active_palette.window
        active.windowText: active_palette.windowText

        disabled.accent: disabled_palette.accent
        disabled.alternateBase: disabled_palette.alternateBase
        disabled.base: disabled_palette.base
        disabled.brightText: disabled_palette.text
        disabled.button: disabled_palette.button
        disabled.buttonText: disabled_palette.buttonText
        disabled.dark: disabled_palette.dark
        disabled.highlight: disabled_palette.highlight
        disabled.highlightedText: disabled_palette.highlightedText
        disabled.light: disabled_palette.light
        disabled.mid: disabled_palette.mid
        disabled.midlight: disabled_palette.midlight
        disabled.placeholderText: disabled_palette.placeholderText
        disabled.shadow: disabled_palette.shadow
        disabled.text: disabled_palette.text
        disabled.toolTipBase: disabled_palette.base
        disabled.toolTipText: disabled_palette.text
        disabled.window: disabled_palette.window
        disabled.windowText: disabled_palette.windowText
    }

    Shortcut {
        sequence: "Ctrl+q"
        onActivated: Qt.exit(0)
    }


    RowLayout {
        anchors.left: parent.left
        anchors.right: parent.right
        Layout.leftMargin: 15
        Layout.rightMargin: 15
        height: 35

        Label {
            id: lblNetworking
            text: "Network settings (only for supporting worlds): "
            verticalAlignment: Qt.AlignVCenter
        }
        TextField {
            id: ipEditor
            Layout.fillWidth: true

            enabled: !localCheck.checked
            placeholderText: "IP or hostname (default localhost)"
            placeholderTextColor:disabled_palette.text
            selectByMouse: true
            verticalAlignment: Qt.AlignVCenter
            horizontalAlignment: Qt.AlignHCenter
        }
        TextField {
            id: portEditor
            width: 192
            enabled: !localCheck.checked
            validator: IntValidator { bottom: 0; top: 65535 }
            text: "25564"

            property int value: text
            horizontalAlignment: Qt.AlignHCenter
            verticalAlignment: Qt.AlignVCenter
            inputMethodHints: Qt.ImhFormattedNumbersOnly

            placeholderText: "network port"
            placeholderTextColor:disabled_palette.text
            selectByMouse: true
        }

        RadioButton {
            id: localCheck
            width: 110
            text: "Local"
            checked: true
        }
        RadioButton {
            id: serverCheck
            width: 110
            text: "Server"
        }
        RadioButton {
            id: clientCheck
            width: 110
            text: "Client"
        }
    }

    RowLayout {
        y: 45
        anchors.left: parent.left
        anchors.right: parent.right
        Layout.leftMargin: 15
        Layout.rightMargin: 15
        height: 35

        Button {
            id: upBtn
            Layout.fillWidth: false
            Layout.fillHeight: true
            enabled: !clientCheck.checked
            text: emojiFont.length>0 ? "⤴" : "Up"
            font.pixelSize: 32
            font.family: emojiFont.length>0 ? emojiFont : ""
            font.styleName: "Color"
            onClicked: {
                if(folderModel.parentFolder.toString().length > 0)
                    folderModel.folder = folderModel.parentFolder
            }

            ToolTip {
                text: "Go up"
            }
        }

        TextField {
            id: pathEditor
            enabled: !clientCheck.checked
            Layout.fillWidth: true
            Layout.fillHeight: true
            text: folderModel.folder
            placeholderText: "path"
            onTextChanged: { folderModel.folder = text }
            placeholderTextColor:disabled_palette.text
            selectByMouse: true
            verticalAlignment: Qt.AlignVCenter
        }

        Button {
            id: btnGoHome
            text: "📁 Home"
            Layout.fillWidth: false
            Layout.fillHeight: true

            onClicked: rootItem.goHomeClicked()
        }
        Button {
            id: btnGoRoot
            text: "📁 Kawaii Worlds"
            Layout.fillWidth: false
            Layout.fillHeight: true

            onClicked: rootItem.goRootClicked()
        }
        Button {
            id: btnGoGames
            text: "📁 Installed Games"
            Layout.fillWidth: false
            Layout.fillHeight: true

            onClicked: rootItem.goGamesClicked()
        }

        Label {
            id: lblRenderer
            Layout.fillHeight: true
            text: "Renderer:"
            verticalAlignment: Qt.AlignVCenter
        }
        ComboBox {
            id: rendererSelector
            Layout.preferredWidth: 325
            Layout.fillHeight: true
            model: renderers
        }
    }

    Button {
        id: connectBtn
        x: 0
        y: 85

        width: parent.width
        height: parent.height - y
        visible: clientCheck.checked
        text: "Connect"

        onClicked: finished("CLIENT")
    }

    ScrollView {
        id: scrollView
        x: 0
        y: 85

        width: parent.width
        height: parent.height - y
        clip: true
        visible: !clientCheck.checked

        ScrollBar.horizontal.policy: ScrollBar.AlwaysOff
        ScrollBar.vertical.policy: ScrollBar.AlwaysOn

        ListView {
            id: listView
            flickableDirection: Flickable.VerticalFlick
            pixelAligned: true
            reuseItems: true
            clip: true
            highlightMoveDuration: 250

            model: folderModel
            highlight: Rectangle { color: active_palette.highlight }

            delegate: Item {
                required property bool fileIsDir
                required property url fileUrl
                required property string filePath
                required property string fileName
                required property int index

                x: 5
                height: 24
                width: listView.width

                MouseArea {
                    anchors.fill: parent
                    onClicked: listView.currentIndex = index
                    onDoubleClicked: {
                        if(fileIsDir)
                            folderModel.folder = fileUrl
                        else
                            rootItem.finished(fileUrl)
                    }
                }

                Row {
                    id: row1
                    spacing: 10

                    Text {
                        property string foldertext: listView.currentIndex==index? "📂": "📁"
                        text: fileIsDir? foldertext: "🎮"
                        font.family: emojiFont
                        font.styleName: "Color"
                        font.hintingPreference: Font.PreferNoHinting
                        font.pixelSize: 20
                        height: 24
                        color: active_palette.buttonText
                        visible: emojiFont.length > 0
                    }

                    Text {
                        text: fileName
                        anchors.verticalCenter: parent.verticalCenter
                        color: active_palette.text
                    }
                }
            }
        }
    }
}
