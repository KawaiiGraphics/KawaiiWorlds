#ifndef KAWAIIWINDOW_HPP
#define KAWAIIWINDOW_HPP

#include <Kawaii3D/KawaiiRoot.hpp>
#include <Kawaii3D/Surfaces/KawaiiSurface.hpp>

#include <QQuickWindow>
#include <QQuickRenderControl>

class KawaiiWindow : public QObject
{
  Q_OBJECT
public:
  explicit KawaiiWindow(KawaiiRoot &root, QObject *parent = nullptr);

  void setupSurface(KawaiiRenderpass *rp, const QString &rendererPluginName);
  void setIcon(const QIcon &icon);

  QWindow& getQWindow() const;
  KawaiiSurface& getSurface() const;

public slots:
  void show(bool windowedMode = false);
  void close();

signals:
  void totalSecElapsedChanged(float secElapsed);



  // IMPLEMENT
private:
  KawaiiRoot &root;
  std::unique_ptr<KawaiiSurface> sfc;

  float lastFrametime;
  float totalSecElapsed;
};

#endif // KAWAIIWINDOW_HPP
