#include "WorldDOM.hpp"

#include <sib_utils/jsonLoaders.hpp>
#include <sib_utils/dirty_cast.hpp>
#include <sib_utils/ioReadAll.hpp>

#include <QJsonDocument>
#include <iostream>

#include <glm/gtc/matrix_transform.hpp>

namespace {
  QVector2D readVec2(const QJsonValue &val, const QVector2D &defaultVal)
  {
    const auto v2 = sib_utils::json::readVec2(val, glm::vec2(defaultVal.x(), defaultVal.y()));
    return {v2.x, v2.y};
  }

  QVector3D readVec3(const QJsonValue &val, const QVector3D &defaultVal)
  {
    const auto v3 = sib_utils::json::readVec3(val, glm::vec3(defaultVal.x(), defaultVal.y(), defaultVal.z()));
    return {v3.x, v3.y, v3.z};
  }

  void readMetadata(const QJsonObject &obj, QHash<QString, QString> &metadata)
  {
    for(auto i = obj.begin(); i != obj.end(); ++i)
      metadata[i.key()] = i.value().toVariant().toString();
  }
}

void CameraDOM::read(const QJsonObject &json)
{
  using namespace sib_utils::json;

  name = json.value(QLatin1String("name")).toString();
  if(json.contains(QLatin1String("view_mat")))
    {
      viewMat = readMat4(json.value(QLatin1String("view_mat")), glm::mat4(1.0));
    } else
    {
      const auto position = readVec3(json.value(QLatin1String("position")), glm::vec3(0.0));
      const auto viewDir = readVec3(json.value(QLatin1String("view_direction")), glm::vec3(0,0,1));
      const auto upDir = glm::normalize(readVec3(json.value(QLatin1String("up_direction")), glm::vec3(0,1,0)));
      viewMat = glm::lookAt(position, position + viewDir, upDir);
    }
}

void EntityDOM::read(const QJsonObject &json)
{
  name = json.value(QLatin1String("name")).toString();
  body = json.value(QLatin1String("body")).toVariant().toMap();

  velocity = readVec3(json.value(QLatin1String("velocity")), QVector3D());
  mass = json.value(QLatin1String("mass")).toDouble(1.0);
  position = readVec3(json.value(QLatin1String("position")), QVector3D());
  rotation = readVec3(json.value(QLatin1String("rotation")), QVector3D());
  angular_velocity = readVec3(json.value(QLatin1String("angle_velocity")), QVector3D());
  if(auto metadataJson = json.value(QLatin1String("metadata")); metadataJson.isObject())
    readMetadata(metadataJson.toObject(), metadata);
  lookForward = json.value(QLatin1String("look_forward")).toBool(false);
  clickOpacity = json.value(QLatin1String("click_opacity")).toBool(false);
  restitution = json.value(QLatin1String("restitution")).toDouble(0.0);
  friction = json.value(QLatin1String("friction")).toDouble(0.5);
  frictionRolling = json.value(QLatin1String("friction_rolling")).toDouble(0.33);
  frictionSpinning = json.value(QLatin1String("friction_spinning")).toDouble(0.125);

  lookingForwardFactorX = json.value(QLatin1String("lookingForwardFactorX")).toDouble(-1);
  lookingForwardFactorY = json.value(QLatin1String("lookingForwardFactorY")).toDouble(-1);
  lookingForwardFactorZ = json.value(QLatin1String("lookingForwardFactorZ")).toDouble(1);
}

void LandscapeDOM::read(const QJsonObject &json)
{
  name = json.value(QLatin1String("name")).toString();
  material = json.value(QLatin1String("material")).toString();
  heightMapTexture = json.value(QLatin1String("height_map_texture")).toString();
  createCollisionShape = json.value(QLatin1String("create_collision_shape")).toBool(false);
  size = readVec3(json.value(QLatin1String("size")), QVector3D(5,5,5));
  pos = readVec3(json.value(QLatin1String("position")), QVector3D());
  if(auto metadataJson = json.value(QLatin1String("metadata")); metadataJson.isObject())
    readMetadata(metadataJson.toObject(), metadata);
  chunkSize = readVec2(json.value(QLatin1String("chunk_size")), QVector2D(10,10));
  step = json.value(QLatin1String("step")).toDouble(0.05);
  restitution = json.value(QLatin1String("restitution")).toDouble(0.0);
  friction = json.value(QLatin1String("friction")).toDouble(0.5);
  frictionRolling = json.value(QLatin1String("friction_rolling")).toDouble(0.33);
  frictionSpinning = json.value(QLatin1String("friction_spinning")).toDouble(0.125);
}

void LocationDOM::read(const QJsonObject &json)
{
  name = json.value(QLatin1String("name")).toString();
  entities.clear();
  auto entities_json = json.value(QLatin1String("entities")).toArray();
  entities.reserve(entities_json.size());
  for(const auto &i: entities_json)
    if(i.isObject())
      {
        entities.emplace_back();
        entities.back().read(i.toObject());
      }

  auto landscapes_json = json.value(QLatin1String("landscapes")).toArray();
  landscapes.reserve(landscapes_json.size());
  for(const auto &i: landscapes_json)
    if(i.isObject())
      {
        landscapes.emplace_back();
        landscapes.back().read(i.toObject());
      }
}

void ShadowMapperDOM::read(const QJsonObject &json)
{
  enabled = json.value(QLatin1String("enable")).toBool(true);
  if(enabled)
    {
      const auto resolutionJson = json.value(QLatin1String("resolution")).toArray({ 1024, 1024 });
      depthMapResolution.setWidth(resolutionJson.at(0).toInt(1024));
      depthMapResolution.setHeight(resolutionJson.at(1).toInt(1024));

      nearClip = json.value(QLatin1String("near_clip")).toDouble(0.1);
      farClip  = json.value(QLatin1String("far_clip")).toDouble(100);
      dirLightsCenter = readVec3(json.value(QLatin1String("dir_lights_center")), QVector3D());
      dirLightsArea = readVec3(json.value(QLatin1String("dir_lights_area")), QVector3D(10,10,10));
    }
}

void ShadowMapperDOM::setDefault()
{
  enabled = true;
  depthMapResolution = QSize(1024, 1024);
  nearClip = 0.1;
  farClip = 100;
  dirLightsCenter = QVector3D();
  dirLightsArea = QVector3D(10,10,10);
}

void PbrLightProperties::read(const QJsonObject &json)
{
  color = readVec3(json.value(QLatin1String("color")), QVector3D(1,1,1));
}

void PbrLightProperties::write(KawaiiPbrLight &l) const
{
  l.color = color;
}

void PhongLightProperties::read(const QJsonObject &json)
{
  diffuse = readVec3(json.value(QLatin1String("diffuse")), QVector3D(1,1,1));
  specular = readVec3(json.value(QLatin1String("specular")), QVector3D(1,1,1));
}

void PhongLightProperties::write(KawaiiPhongLight &l) const
{
  l.diffuse = diffuse;
  l.specular = specular;
}

void CelLightProperties::read(const QJsonObject &json)
{
  diffuse = readVec3(json.value(QLatin1String("diffuse")), QVector3D(1,1,1));
  specular = readVec3(json.value(QLatin1String("specular")), QVector3D(1,1,1));
}

void CelLightProperties::write(KawaiiCelLight &l) const
{
  l.diffuse = diffuse;
  l.specular = specular;
}

void DirLightProperties::read(const QJsonObject &json)
{
  ambient = readVec3(json.value(QLatin1String("ambient")), QVector3D(0.15, 0.15, 0.15));
  direction = readVec3(json.value(QLatin1String("direction")), QVector3D(0,0,1));
}

void DotLightProperties::read(const QJsonObject &json)
{
  ambient = readVec3(json.value(QLatin1String("ambient")), QVector3D(0.15, 0.15, 0.15));
  position = readVec3(json.value(QLatin1String("position")), QVector3D(0,0,0));
  constAttenuation = json.value(QLatin1String("const_attenuation")).toDouble(0);
  linearAttenuation = json.value(QLatin1String("linear_attenuation")).toDouble(0);
  quadraticAttenuation = json.value(QLatin1String("quadratic_attenuation")).toDouble(1);
}

void SpotLightProperties::read(const QJsonObject &json)
{
  ambient = readVec3(json.value(QLatin1String("ambient")), QVector3D(0.15, 0.15, 0.15));
  position = readVec3(json.value(QLatin1String("position")), QVector3D(0,0,0));
  direction = readVec3(json.value(QLatin1String("direction")), QVector3D(0,0,1));
  constAttenuation = json.value(QLatin1String("const_attenuation")).toDouble(0);
  linearAttenuation = json.value(QLatin1String("linear_attenuation")).toDouble(0);
  quadraticAttenuation = json.value(QLatin1String("quadratic_attenuation")).toDouble(1);

  spotExponent = json.value(QLatin1String("spot_exponent")).toDouble(0);

  static const constexpr double pi = 3.141592653589793238462643383279502884L;
  static const constexpr double toRadians = pi / 180.0L;

  spotCutoffRadians = toRadians * json.value(QLatin1String("spot_cutoff")).toDouble(60);
}

void BarierClockworkDOM::read(const QJsonObject &json)
{
  auto floatProperty = [](const QJsonValue &val) -> std::optional<float> {
      if(val.isDouble())
        return val.toDouble();
      else
        return std::nullopt;
    };

  min_x = floatProperty(json.value(QLatin1String("min_x")));
  max_x = floatProperty(json.value(QLatin1String("max_x")));
  min_y = floatProperty(json.value(QLatin1String("min_y")));
  max_y = floatProperty(json.value(QLatin1String("max_y")));
  min_z = floatProperty(json.value(QLatin1String("min_z")));
  max_z = floatProperty(json.value(QLatin1String("max_z")));
  frictionFactor = floatProperty(json.value(QLatin1String("friction_factor")));
}

void GravityAccelClockworkDOM::read(const QJsonObject &json)
{
  if(json.contains(QLatin1String("acceleration")))
    g = readVec3(json.value(QLatin1String("acceleration")), QVector3D());
  else if(json.contains(QLatin1String("g")))
    g = readVec3(json.value(QLatin1String("g")), QVector3D());
}

void ViscosityClockworkDOM::read(const QJsonObject &json)
{
  if(auto val = json.value(QLatin1String("viscosity_factor")); val.isDouble())
    viscosity_factor = val.toDouble();
  else
    viscosity_factor = 0.5;
}

std::optional<ClockworkDOM> readClockworkDOM(const QJsonObject &json)
{
  const auto type = json.value(QLatin1String("type")).toString();
  if(type == QLatin1String("barier"))
    {
      BarierClockworkDOM dom;
      dom.read(json);
      return dom;
    }
  else if(type == QLatin1String("friction"))
    return FrictionClockworkDOM {};
  else if(type == QLatin1String("gravity_acceleration"))
    {
      GravityAccelClockworkDOM dom;
      dom.read(json);
      return dom;
    }
  else if(type == QLatin1String("hookes_law"))
    return HookesLawClockworkDOM {};
  else if(type == QLatin1String("viscosity"))
    {
      ViscosityClockworkDOM dom;
      dom.read(json);
      return dom;
    }
  else if(type == QLatin1String("solidizer"))
    return SolidizerClockworkDOM {};

  std::cerr << "Unknown clockwork type \"" << type.toStdString() << '\"' << std::endl;

  return std::nullopt;
}

void WorldDOM::read(const QString &filePath)
{
  rootDir = QDir(filePath);
  rootDir.cdUp();

  QJsonParseError parseError;
  auto jsonObject = QJsonDocument::fromJson(sib_utils::ioReadAll(QFile(filePath)), &parseError).object();
  if(Q_UNLIKELY(parseError.error != QJsonParseError::NoError))
    throw parseError;

  auto json_cameras = jsonObject.value(QLatin1String("cameras")).toArray();
  dom_json_loaders::loadVecJson(json_cameras, cameras);

  auto getString = [&jsonObject](const QLatin1String &key) {
      if(auto valJson = jsonObject.value(key); valJson.isString())
        return valJson.toString();
      else
        return QString();
    };

  mainSound = getString(QLatin1String("main_sound"));

  const auto json_illuminationModels = jsonObject.value(QLatin1String("illumination_models")).toArray();
  illuminationModels.reserve(json_illuminationModels.size());
  for(const auto &i: json_illuminationModels)
    if(i.isObject())
      {
        const auto obj = i.toObject();

        const QString illumType = obj.contains(QLatin1String("type"))?
              obj.value(QLatin1String("type")).toString().toLower():
              QString();

        if(illumType == QLatin1String("pbr"))
          {
            IlluminationModelDOM<PbrLightProperties> illum;
            illum.read(obj);
            if(illum.name.isNull())
              illum.name = QStringLiteral("PBR");

            illuminationModels.push_back(illum);
          } else if(illumType == QLatin1String("phong"))
          {
            IlluminationModelDOM<PhongLightProperties> illum;
            illum.read(obj);
            if(illum.name.isNull())
              illum.name = QStringLiteral("Phong");

            illuminationModels.push_back(illum);
          } else if(illumType == QLatin1String("cel"))
          {
            IlluminationModelDOM<CelLightProperties> illum;
            illum.read(obj);
            if(illum.name.isNull())
              illum.name = QStringLiteral("Cel");

            illuminationModels.push_back(illum);
          } else

          std::cerr << "WARNING: ignored unknown illumination type \"" << illumType.toStdString() << std::endl;
      }

  const auto json_gearbox = jsonObject.value(QLatin1String("gearbox")).toObject();
  gearbox = json_gearbox.toVariantMap();
  gearbox.remove(QStringLiteral("clockworks"));

  if(json_gearbox.contains(QStringLiteral("clockworks")))
    {
      const auto json_clockworks = json_gearbox.value(QLatin1String("clockworks")).toArray();
      clockworks.reserve(json_clockworks.size());
      for(const auto &i: json_clockworks)
        if(i.isObject())
          {
            if(auto clockwork = readClockworkDOM(i.toObject()); clockwork)
              clockworks.push_back(*clockwork);
          }
    }

  auto json_locations = jsonObject.value(QLatin1String("locations")).toArray();
  dom_json_loaders::loadVecJson(json_locations, locations);

  auto json_persistentEntities = jsonObject.value(QLatin1String("entities")).toArray();
  dom_json_loaders::loadVecJson(json_persistentEntities, persistentEntities);

  if(auto metadataJson = jsonObject.value(QLatin1String("metadata")); metadataJson.isObject())
    readMetadata(metadataJson.toObject(), metadata);

  name = getString(QLatin1String("name"));
  version = getString(QLatin1String("version"));

  if(jsonObject.contains(QLatin1String("system_icon")))
    icon = QIcon::fromTheme(jsonObject.value(QLatin1String("system_icon")).toString());
  else
    icon = {};

  if(icon.isNull() && jsonObject.contains(QLatin1String("icon")))
    icon = QIcon(jsonObject.value(QLatin1String("icon")).toString());

  if(jsonObject.contains(QLatin1String("windowed")))
    isWindowedMode = jsonObject.value(QLatin1String("windowed")).toBool(true);
  else
    isWindowedMode = false;

  if(jsonObject.contains(QLatin1String("debug_aabb")))
    debugAABB = jsonObject.value(QLatin1String("debug_aabb")).toBool(true);
  else
    debugAABB = false;

  auto netJsonObj = jsonObject.value(QLatin1String("network"));
  if(netJsonObj.isObject())
    {
      isNetworkSupported = true;
      auto obj = netJsonObj.toObject();
      networkHeartbeat = obj.value(QLatin1String("heartbeat_interval")).toInt(10);
      entitiesSyncInterval = obj.value(QLatin1String("entities_sync_interval")).toInt(15);
      worldSyncInterval = obj.value(QLatin1String("world_sync_interval")).toInt(1500);
    } else
    {
      isNetworkSupported = netJsonObj.toBool() || netJsonObj.toInt();
      networkHeartbeat = 10;
      entitiesSyncInterval = 15;
      worldSyncInterval = 1500;
    }

  if(jsonObject.contains(QLatin1String("physic_tick_interval")))
    physicTickInterval = jsonObject.value(QLatin1String("physic_tick_interval")).toInt(10);
  else
    physicTickInterval = 10;

  if(jsonObject.contains(QLatin1String("renderer")))
    selectedRenderer = jsonObject.value(QLatin1String("renderer")).toString();
  else
    selectedRenderer = QString();

  mainJsSrc = mainQmlSrc = QString();

  if(jsonObject.contains(QLatin1String("qml_src")))
    {
      mainQmlSrc = jsonObject.value(QLatin1String("qml_src")).toString().toUtf8();
      mainQmlFname = QString();
    } else if(jsonObject.contains(QLatin1String("qml_file")))
    {
      mainQmlFname = rootDir.absoluteFilePath(jsonObject.value(QLatin1String("qml_file")).toString());
      try {
        mainQmlSrc = sib_utils::ioReadAll(QFile(mainQmlFname));
      } catch(...) { mainQmlSrc = mainQmlFname = QString(); }
    }

  if(jsonObject.contains(QLatin1String("js_src")))
    mainJsSrc = jsonObject.value(QLatin1String("js_src")).toString().toUtf8();
  else if(jsonObject.contains(QLatin1String("js_file")))
    {
      mainJsFname = rootDir.absoluteFilePath(jsonObject.value(QLatin1String("js_file")).toString());
      try {
        mainJsSrc = sib_utils::ioReadAll(QFile(mainJsFname));
      } catch(...) { mainJsSrc = mainJsFname = QString(); }
    }

  if(auto resVal = jsonObject.value(QLatin1String("resources")); resVal.isObject())
    respackJson = resVal.toObject();
}

void WorldDOM::writeManifest(QDataStream &st) const
{
  st << name << version << selectedRenderer
     << icon
     << mainSound
     << mainQmlSrc << mainQmlFname
     << mainJsSrc << mainJsFname
     << isWindowedMode << debugAABB
     << physicTickInterval
     << isNetworkSupported << networkHeartbeat << entitiesSyncInterval << worldSyncInterval;
}

void WorldDOM::readManifest(QDataStream &st)
{
  st >> name >> version >> selectedRenderer
      >> icon
      >> mainSound
      >> mainQmlSrc >> mainQmlFname
      >> mainJsSrc >> mainJsFname
      >> isWindowedMode >> debugAABB
      >> physicTickInterval
      >> isNetworkSupported >> networkHeartbeat >> entitiesSyncInterval >> worldSyncInterval;
}
