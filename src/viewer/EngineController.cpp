#include "EngineController.hpp"

#include "ResourcePackJsonLoad.hpp"
#include "ResourcePack_qml.hpp"
#include "KawaiiQmlLayer.hpp"
#include "InputTransfer.hpp"
#include "Location_qml.hpp"
#include "Camera_qml.hpp"
#include "World_qml.hpp"
#include "WorldDOM.hpp"

#include "clockworks/GravityAccelerationClockwork.hpp"
#include "clockworks/HookesLawClockwork.hpp"
#include "clockworks/SolidizerClockwork.hpp"
#include "clockworks/ViscosityClockwork.hpp"
#include "clockworks/FrictionClockwork.hpp"
#include "clockworks/BarierClockwork.hpp"

#include "networking/NetworkServer.hpp"
#include "networking/NetworkServer_qml.hpp"
#include "networking/NetworkClient.hpp"
#include "networking/NetworkClient_qml.hpp"

#include <Kawaii3D/Renderpass/KawaiiQPainterLayer.hpp>
#include <sib_utils/ioReadAll.hpp>
#include <QCoreApplication>
#include <QJsonDocument>
#include <QtConcurrent>
#include <QJsonObject>
#include <QQuickItem>
#include <QDir>

namespace {
  void loadTimer(std::vector<EngineController::Timer> &timers, const QJsonValue &val, std::vector<std::byte> &content, KawaiiGpuBuf *buf, size_t &byteIndex)
  {
    content.resize(content.size() + sizeof(float));

    const auto timer = val.toObject();
    const float interval = timer.value(QLatin1String("interval_sec")).toDouble(1.0);
    const float phase = timer.value(QLatin1String("phase_sec")).toDouble(0.0);
    EngineController::Timer tim = {
      .buf = buf,
      .offset = byteIndex,
      .scale = 1.0f / interval,
      .phase = phase,
    };
    timers.push_back(tim);

    byteIndex += sizeof(float);
  }
}

EngineController::EngineController():
  window(root, this)
{
  root.updateParent(this);
  world.updateParent(&root);
  qmlEngine.setObjectName("Main Script engine");
  connect(&world, &World::worldLoaded, this, &EngineController::onWorldReloaded);
  using namespace std::placeholders;
  loadTimerFunc = std::bind(&loadTimer, std::ref(timers), _1, _2, _3, _4);
}

void EngineController::WorldLoader::loadQmlLayer(KawaiiQmlLayer *qmlLayer)
{
  pendingQmlLayers.insert(qmlLayer);
}

EngineController::WorldLoader::~WorldLoader()
{
  for(auto *qmlLayer: pendingQmlLayers)
    {
      QString src = qmlLayer->property("qml_src").toString();
      const QString fname = qmlLayer->property("qml_fname").toString();

      if(src.isNull() && !fname.isNull())
        src = sib_utils::ioReadAll(QFile(fname));

      auto *qmlItem = engine->createQmlItem(src, fname);
      qmlLayer->setUiItem(qmlItem);
      if(qmlItem)
        {
          qmlItem->setParent(qmlLayer);

          if(qmlLayer->enableInput)
            {
              size_t uiInput = engine->world.addInput(std::make_unique<InputTransfer>(&qmlLayer->getQmlWindow()));
              static_cast<InputTransfer&>(engine->world.getInput(uiInput)).setFocusedItem(qmlLayer->getUiItem());
            }
        }
    }
}

struct EngineController::ClientWorldLoader: EngineController::WorldLoader
{
  // WorldLoader interface
  WorldDOM loadWorld(const StartParameters &parameters) override final
  {
    auto netClient = setupNetwork(parameters, engine->qmlEngine, engine->world);
    if(!netClient)
      return {};
    const auto bytes = netClient->receiveHello();
    QDir d = QStandardPaths::writableLocation(QStandardPaths::CacheLocation);
    d.mkpath(parameters.hostAddr.toString());
    d.cd(parameters.hostAddr.toString());

    QDataStream st(qUncompress(bytes));
    WorldDOM dom;
    dom.rootDir = d;
    dom.readManifest(st);
    dom.mainQmlSrc = dom.mainQmlFname = QString();

    while(!st.atEnd())
      {
        QString fName;
        st >> fName;

        char *content; qint64 contentLength;
        st.readBytes(content, contentLength);

        const QStringView subdir(fName.constData(), fName.lastIndexOf('/'));
        d.mkpath(subdir.toString());
        QFile f(d.absoluteFilePath(fName));
        f.open(QFile::WriteOnly);
        f.write(content, contentLength);
        f.close();
        delete [] content;
      }

    netClient->setHeartbeatInterval(dom.networkHeartbeat);
    networkObject = std::move(netClient);
    return dom;
  }

  std::unique_ptr<NetworkClient> setupNetwork(const StartParameters &parameters, QQmlEngine &qmlEngine, World &world)
  {
    if(!parameters.hostAddr.isNull())
      {
        auto client = std::make_unique<NetworkClient>(world);
        client->start(parameters.hostAddr, parameters.port);

        auto client_qml = new KawaiiJSWrappers::NetworkClient(*client);
        auto networkingJs = qmlEngine.newQObject(client_qml);
        networkingJs.setProperty("isServer", false);
        qmlEngine.globalObject().setProperty(QStringLiteral("network"), networkingJs);

        return client;
      } else
      return nullptr;
  }
};

struct EngineController::LocalWorldLoader: EngineController::WorldLoader
{
  // WorldLoader interface
  WorldDOM loadWorld(const StartParameters &parameters) override final
  {
    WorldDOM dom;
    if(parameters.filePath.isNull())
      return dom;

    try {
      dom.read(parameters.filePath);
    }
    catch(const QJsonParseError &parseError) {
      qCritical().noquote() << QStringLiteral("JSON parse error!\n\n%1\n").arg(parseError.errorString());
      return {};
    }
    catch(const std::ios_base::failure &ioError) {
      qCritical().noquote() << QStringLiteral("File IO error!\n\n%1\n").arg(QLatin1String(ioError.what()));
      return {};
    }

    return dom;
  }
};

struct EngineController::ServerWorldLoader: EngineController::WorldLoader
{
  // WorldLoader interface
  WorldDOM loadWorld(const StartParameters &parameters) override final
  {
    LocalWorldLoader localLoader;
    auto dom = localLoader.loadWorld(parameters);
    networkObject = setupNetwork(parameters, dom, engine->qmlEngine, engine->world);
    return dom;
  }

  static QByteArray getHelloNetMsg(const QDir &rootDir)
  {
    QByteArray msg;
    QDataStream st(&msg, QIODevice::WriteOnly);

    auto addFile = [&st, &rootDir] (const QString &fName) {
        st << rootDir.relativeFilePath(fName);
        const auto content = sib_utils::ioReadAll(QFile(fName));
        st.writeBytes(content.data(), content.size());
      };

    std::list<QDir> stack = {rootDir};
    while(!stack.empty())
      {
        const auto el = *stack.begin();
        stack.pop_front();

        for(const auto &i: el.entryList({"*.qml", "*.js"}, QDir::Files | QDir::Readable))
          addFile(el.absoluteFilePath(i));

        for(const auto &i: el.entryList(QDir::Dirs | QDir::Readable | QDir::NoDotAndDotDot))
          {
            QDir ch_d = el;
            if(ch_d.cd(i))
              stack.push_front(ch_d);
          }
      }
    st.setDevice(nullptr);
    return msg;
  }

  std::unique_ptr<QObject> setupNetwork(const StartParameters &parameters, const WorldDOM &dom, QQmlEngine &qmlEngine, World &world)
  {
    if(!parameters.hostAddr.isNull())
      {
        auto server = std::make_unique<NetworkServer>(world);
        QByteArray customHelloBytes;
        {
          QDataStream st(&customHelloBytes, QIODevice::WriteOnly);
          dom.writeManifest(st);
        }
        customHelloBytes.append(getHelloNetMsg(dom.rootDir));
        server->customHello = NetworkMessage(NetworkMessageType::CustomHello, qCompress(customHelloBytes, 9));
        server->setHeartbeatInterval(dom.networkHeartbeat);
        server->setEntitiesSyncInterval(dom.entitiesSyncInterval);
        server->setWorldSyncInterval(dom.worldSyncInterval);
        server->start(parameters.hostAddr, parameters.port);

        auto server_qml = new KawaiiJSWrappers::NetworkServer(*server);
        auto networkingJs = qmlEngine.newQObject(server_qml);
        networkingJs.setProperty("isServer", true);
        qmlEngine.globalObject().setProperty(QStringLiteral("network"), networkingJs);
        return server;
      } else
      return nullptr;
  }
};

void EngineController::clear()
{
  worldLoader.reset();
  networkObject.reset();
  timers.clear();
  worldJs = QJSValue::UndefinedValue;
  world.clear();
  world.res().clear();
  qmlEngine.globalObject().setProperty(QStringLiteral("network"), QJSValue::UndefinedValue);
  qmlEngine.globalObject().setProperty(QStringLiteral("world"), QJSValue::UndefinedValue);
}

void EngineController::selectGame()
{
  GameSelector gameSelector(&qmlEngine);
  if(auto startParams = gameSelector.exec(); !startParams.filePath.isNull())
    start(startParams);
}

KawaiiImageSource *EngineController::loadQmlLayer(QString src, const QString &fname, bool enableInput, KawaiiRenderpass *rp)
{
  if(src.isNull() && !fname.isNull())
    src = sib_utils::ioReadAll(QFile(fname));

  if(!src.isNull())
    {
      KawaiiQmlLayer *qmlLayer = rp->createChild<KawaiiQmlLayer>();
      qmlLayer->setProperty("qml_src", src);
      qmlLayer->setProperty("qml_fname", fname);
      qmlLayer->enableInput = enableInput;
      return qmlLayer;
    } else
    return nullptr;
}

KawaiiImageSource *EngineController::loadQmlLayer(const QJsonObject &val, KawaiiRenderpass *rp)
{
  QString qmlSrc, qmlFName;
  if(auto jsonQmlSrc = val.value(QLatin1String("src")); jsonQmlSrc.isString())
    qmlSrc = jsonQmlSrc.toString();

  if(auto jsonQmlFName = val.value(QLatin1String("file")); jsonQmlFName.isString())
    qmlFName = jsonQmlFName.toString();

  auto jsonEnableInput = val.value(QLatin1String("enable_input"));
  bool enableInput = jsonEnableInput.isBool() && jsonEnableInput.toBool();

  return loadQmlLayer(qmlSrc, qmlFName, enableInput, rp);
}

ResourcePackJsonLoader EngineController::createResPackLoader(const QDir &rootDir)
{
  ResourcePackJsonLoader resLoader = {
    .rootDir = rootDir,
    .customUboElementLoaders = {
      { QStringLiteral("timer"), loadTimerFunc },
    },
    .customImgSrcLoaders = {
      { QStringLiteral("qml_item"), [this] (const QJsonObject &val, KawaiiRenderpass *rp) { return loadQmlLayer(val, rp); } },
    }
  };

  resLoader.customUboElementLoaders[QStringLiteral("evalute_js")] =
      [this, &resLoader] (const QJsonValue &val, std::vector<std::byte> &content, KawaiiGpuBuf *buf, size_t &byteIndex) {
      if(val.isString())
        {
          const auto script = val.toString();
          auto result = qmlEngine.evaluate(script);
          if(result.isError())
            qWarning().noquote() << "KawaiiWorlds::ResourcePackJsonLoader::loadBufferElement:loadJsValue: Uncaught exception at line"
                                 << result.property("lineNumber").toInt()
                                 << ":" << result.toString()
                                 << "\nevaluted code:\n" << script;

          if(result.isCallable())
            result = result.call({});

          if(auto qvar = result.toVariant(); qvar.canConvert<QJsonValue>())
            {
              QJsonValue json = qvar.toJsonValue();
              if(json.isArray())
                {
                  for(const auto &i: json.toArray())
                    resLoader.loadBufferElement(i, *static_cast<ResourcePack*>(buf->parent()), buf, content, byteIndex);
                } else
                resLoader.loadBufferElement(json, *static_cast<ResourcePack*>(buf->parent()), buf, content, byteIndex);
            }
        }
    };

  return resLoader;
}

Entity* EngineController::loadEntity(const EntityDOM &dom)
{
  auto *e = new Entity;
  e->setObjectName(dom.name);
  e->setClickOpacity(dom.clickOpacity);
  e->setLookForward(dom.lookForward);
  e->setAngularVel(dom.angular_velocity);
  e->setFriction(dom.friction);
  e->setFrictionRolling(dom.frictionRolling);
  e->setFrictionSpinning(dom.frictionSpinning);
  e->setLookingForwardFactorX(dom.lookingForwardFactorX);
  e->setLookingForwardFactorY(dom.lookingForwardFactorY);
  e->setLookingForwardFactorZ(dom.lookingForwardFactorZ);
  e->setMass(dom.mass);
  e->setPosition(dom.position);
  e->setRestitution(dom.restitution);
  e->setEulerAngles(dom.rotation);
  e->setVelocity(dom.velocity);
  e->setMetadata(dom.metadata);

  if(!dom.body.empty())
    e->setBody(Body::create(dom.body).release());
  return e;
}

void EngineController::start(StartParameters &startParams)
{
  if(startParams.hostAddr.isNull())
    worldLoader = std::make_unique<LocalWorldLoader>();
  else {
      if(startParams.isClient)
        worldLoader = std::make_unique<ClientWorldLoader>();
      else
        worldLoader = std::make_unique<ServerWorldLoader>();
    }

  worldLoader->engine = this;
  auto dom = worldLoader->loadWorld(startParams);
  networkObject = std::move(worldLoader->networkObject);

  for(const auto &i: dom.cameras)
    {
      auto *cam = new KawaiiCamera();
      cam->setViewMat(i.viewMat);
      cam->setObjectName(i.name);
      cam->updateParent(world.res().getPrimaryScene());
    }
  if(!dom.respackJson.empty())
    {
      using namespace std::placeholders;
      ResourcePackJsonLoader resLoader = createResPackLoader(dom.rootDir);
      resLoader.loadJson(dom.respackJson, world.res());
    }

  auto app = QCoreApplication::instance();

  if(!dom.name.isNull())
    app->setApplicationName(dom.name);

  if(!dom.version.isNull())
    app->setApplicationVersion(dom.version);

  if(!dom.selectedRenderer.isNull())
    if(startParams.rendererPlugin.isNull() || startParams.rendererPlugin.toLower() == QStringLiteral("default"))
      startParams.rendererPlugin = dom.selectedRenderer;

  if(startParams.rendererPlugin.toLower() == QStringLiteral("default"))
    startParams.rendererPlugin = QString();

  for(const auto &i: dom.persistentEntities)
    {
      auto entity = loadEntity(i);
      entity->updateParent(&world);
    }

  for(const auto &i: dom.locations)
    {
      auto location = new Location;
      location->setObjectName(i.name);
      location->updateParent(&world);
      for(const auto &j: i.entities)
        {
          auto entity = loadEntity(j);
          entity->updateParent(location);
        }

      for(const auto &j: i.landscapes)
        {
          auto landscape = location->addLandscapeInfo(j.material, j.heightMapTexture, j.size);
          landscape->pos = j.pos;
          landscape->step = j.step;
          landscape->metadata = j.metadata;
          landscape->restitution = j.restitution;
          landscape->friction = j.friction;
          landscape->frictionRolling = j.frictionRolling;
          landscape->frictionSpinning = j.frictionSpinning;
          landscape->chunkSize = j.chunkSize;
          landscape->id = j.name;
          landscape->createCollisionShape = j.createCollisionShape;
        }
    }

  for(const auto &i: dom.illuminationModels)
    {
      if(std::holds_alternative<IlluminationModelDOM<PbrLightProperties>>(i))
        {
          const auto &illumDom = std::get<IlluminationModelDOM<PbrLightProperties>>(i);
          auto illumModel = &world.createIlluminationModel<KawaiiPbrLight>(illumDom.name);
          illumDom.write(*illumModel);
          if(illumModel->getShadowMapper())
            {
              KawaiiSceneLayer *lightsAreaSceneLayer = nullptr;
              world.res().getMainRenderpass()->forallSubpasses([this, &lightsAreaSceneLayer] (const KawaiiRenderpass::Subpass &subpass) {
                  if(lightsAreaSceneLayer) return;

                  auto *sceneLayer = dynamic_cast<KawaiiSceneLayer*>(subpass.imgSrc);
                  if(sceneLayer && world.res().getPrimaryScene() == sceneLayer->getCamera()->getScene())
                    lightsAreaSceneLayer = sceneLayer;
                });
              illumModel->getShadowMapper()->setDirLightsAreaLayer(lightsAreaSceneLayer);
            }

        }
      else if(std::holds_alternative<IlluminationModelDOM<PhongLightProperties>>(i))
        {
          const auto &illumDom = std::get<IlluminationModelDOM<PhongLightProperties>>(i);
          auto illumModel = &world.createIlluminationModel<KawaiiPhongLight>(illumDom.name);
          illumDom.write(*illumModel);
        }
      else if(std::holds_alternative<IlluminationModelDOM<CelLightProperties>>(i))
        {
          const auto &illumDom = std::get<IlluminationModelDOM<CelLightProperties>>(i);
          auto illumModel = &world.createIlluminationModel<KawaiiCelLight>(illumDom.name);
          illumDom.write(*illumModel);
        }
    }

  auto gearbox = Gearbox::create(dom.gearbox.value(QStringLiteral("type")).toString(), world);
  if(gearbox)
    {
      auto *gearboxMetaObj = gearbox->metaObject();
      for(auto i = dom.gearbox.cbegin(); i != dom.gearbox.cend(); ++i)
        {
          auto keyUtf8 = i.key().toUtf8();
          int propertyIndex = gearboxMetaObj->indexOfProperty(keyUtf8.constData());
          if(Q_LIKELY(propertyIndex > -1))
            gearboxMetaObj->property(propertyIndex).write(gearbox.get(), i.value());
        }
    }
  world.setGearbox(std::move(gearbox));

  if(world.hasGearbox())
    for(const auto &i: dom.clockworks)
      {
        std::visit([this](auto &&arg) {
            using T = std::decay_t<decltype(arg)>;
            if constexpr (std::is_same_v<T, BarierClockworkDOM>)
            {
              auto barier = std::make_unique<BarierClockwork>();
              barier->min_x = arg.min_x;
              barier->max_x = arg.max_x;
              barier->min_y = arg.min_y;
              barier->max_y = arg.max_y;
              barier->min_z = arg.min_z;
              barier->max_z = arg.max_z;
              barier->frictionFactor = arg.frictionFactor;
              world.getGearbox().addClockwork(std::move(barier));
            } else if constexpr (std::is_same_v<T, FrictionClockworkDOM>)
            {
              world.getGearbox().addClockwork(std::make_unique<FrictionClockwork>());
            } else if constexpr (std::is_same_v<T, GravityAccelClockworkDOM>)
            {
              world.getGearbox().addClockwork(std::make_unique<GravityAccelerationClockwork>(arg.g));
            } else if constexpr (std::is_same_v<T, HookesLawClockworkDOM>)
            {
              world.getGearbox().addClockwork(std::make_unique<HookesLawClockwork>());
            } else if constexpr (std::is_same_v<T, ViscosityClockworkDOM>)
            {
              world.getGearbox().addClockwork(std::make_unique<ViscosityClockwork>(arg.viscosity_factor));
            }
            else if constexpr (std::is_same_v<T, SolidizerClockworkDOM>)
            {
              world.getGearbox().addClockwork(std::make_unique<SolidizerClockwork>());
            }
          }, i);
      }

  worldJs = qmlEngine.newQObject(new KawaiiJSWrappers::World(world));
  auto resourcesJs = qmlEngine.newQObject(new KawaiiJSWrappers::ResourcePack(world.res()));
  worldJs.setProperty(QStringLiteral("resources"), resourcesJs);
  qmlEngine.globalObject().setProperty(QStringLiteral("world"), worldJs);

  onWorldReloaded();

  if(!dom.mainJsSrc.isNull())
    {
      QJSValue ret = qmlEngine.evaluate(dom.mainJsSrc, dom.mainJsFname);
      if(ret.isError())
        qWarning().noquote() << ret.toString();
    }

  bool rpDepsChanged = false;
  auto *rp = world.res().getMainRenderpass();
  auto *qmlLayer = loadQmlLayer(dom.mainQmlSrc, dom.mainQmlFname, true, rp);

  if(qmlLayer)
    {
      rp->addSubpass(KawaiiRenderpass::Subpass {
                       .imgSrc = qmlLayer,

                       .outputGBufs = {0},
                       .depthGBuf = std::nullopt
                     });
      rpDepsChanged = true;
    }

  if(dom.debugAABB)
    {
      auto dbgLayer = rp->createChild<KawaiiQPainterLayer>(nullptr, std::bind(&EngineController::drawDbgAABB, this, std::placeholders::_1));
      rp->addSubpass(KawaiiRenderpass::Subpass {
                       .imgSrc = dbgLayer,

                       .outputGBufs = {0},
                       .depthGBuf = std::nullopt
                     });
      rpDepsChanged = true;
    }

  if(rpDepsChanged)
    rp->autodetectDependencies();

  worldLoader.reset();

  window.setupSurface(rp, startParams.rendererPlugin);
  if(!dom.icon.isNull())
    window.setIcon(dom.icon);
  connect(&qmlEngine, &QQmlEngine::exit, &window.getQWindow(), &QWindow::close);
  connect(&qmlEngine, &QQmlEngine::quit, &window.getQWindow(), &QWindow::close);
  world.attachSurface(window.getSurface());
  window.show(dom.isWindowedMode);

  world.startTime(dom.physicTickInterval);

  if(!dom.mainSound.isNull())
    if(auto snd = world.res().getSound(dom.mainSound); snd)
      snd->play(std::chrono::milliseconds(0));
}

void EngineController::startQmlLayer(KawaiiQmlLayer *qmlLayer)
{
  if(!worldLoader)
    {
      qCritical("EngineController::loadQmlLayer: ERR: World has already been loaded!");
      return;
    }
  worldLoader->loadQmlLayer(qmlLayer);
}

void EngineController::onWorldReloaded()
{
  worldJs = qmlEngine.globalObject().property(QStringLiteral("world"));

  auto locationsJs = qmlEngine.newObject();
  for(auto *i: world.findChildren<Location*>(QString(), Qt::FindDirectChildrenOnly))
    locationsJs.setProperty(i->objectName(), KawaiiJSWrappers::Location::createWrapper(*i, &qmlEngine));
  worldJs.setProperty(QStringLiteral("locations"), locationsJs);

  QJSValue camerasJs = worldJs.property(QStringLiteral("cameras"));
  if(camerasJs.isNull() || camerasJs.isUndefined())
    {
      camerasJs = qmlEngine.newObject();
      worldJs.setProperty(QStringLiteral("cameras"), camerasJs);
    }

  world.res().forall([&camerasJs, this] (const QString &name, KawaiiCamera *cam) {
      QJSValue camJs = camerasJs.property(name);
      if(camJs.isQObject())
        {
          if(auto camWrapper = dynamic_cast<KawaiiJSWrappers::Camera*>(camJs.toQObject());
             camWrapper && &camWrapper->getObj() == cam)
            return;
          if(camJs.toQObject())
            camJs.toQObject()->deleteLater();
        }
      camerasJs.setProperty(name, qmlEngine.newQObject(new KawaiiJSWrappers::Camera(*cam)));
    });
}

QQuickItem *EngineController::createQmlItem(const QString &source, const QString &fName)
{
  if(!source.isNull())
    {
      auto qmlComponent = std::make_unique<QQmlComponent>(&qmlEngine);
      qmlComponent->setParent(&qmlEngine);
      qmlComponent->setData(source.toUtf8(), QUrl::fromLocalFile(fName));
      auto created = qobject_cast<QQuickItem*>(qmlComponent->create());
      if(!created)
        {
          qCritical() << qmlComponent->errorString();
          qmlComponent.reset();
        } else
        {
          connect(created, &QObject::destroyed, qmlComponent.get(), &QObject::deleteLater);
          created->setProperty("__src", QStringList { source, fName });
          qmlComponent.release();
        }
      return created;
    } else
    return nullptr;
}

void EngineController::updateGpuTimers(float totalSecElapsed)
{
  if(!timers.empty())
    {
      QtConcurrent::map(timers, [totalSecElapsed] (EngineController::Timer &i) {
        const float val = i.scale * (totalSecElapsed + i.phase);
        std::memcpy(static_cast<std::byte*>(i.buf->getData()) + i.offset,
                    &val, sizeof(float));
        i.buf->dataChanged(i.offset, sizeof(float));
      }).waitForFinished();
    }
}

void EngineController::drawDbgAABB(QPainter &painter)
{
  const QVector3D sz(painter.window().width(), painter.window().height(), 1);
  world.forallEntities([this, &painter, &sz] (Entity &e) {
      const auto aabbVec = world.getEntityAABB(e);
      for(auto aabb: aabbVec)
        {
          if(aabb.first.z() < 0 || aabb.second.z() > 1)
            return;

          aabb.first *= sz;
          aabb.second *= sz;
          const QRectF rect(aabb.first.toPointF(), aabb.second.toPointF());
          if(e.getClickOpacity())
            {
              painter.setPen(Qt::red);
              painter.setBrush(QBrush(Qt::red, Qt::Dense4Pattern));
            }
          else if(e.hasPointerInteractions())
            {
              painter.setPen(Qt::yellow);
              painter.setBrush(QBrush(Qt::yellow, Qt::Dense7Pattern));
            }
          else
            {
              painter.setPen(Qt::gray);
              painter.setBrush(Qt::transparent);
            }

          painter.drawRect(rect);
          if(!e.objectName().isNull())
            painter.drawText(rect, e.objectName());
        }
    });
}
