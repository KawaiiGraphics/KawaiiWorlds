#include <QGuiApplication>
#include <QQmlDebuggingEnabler>

#include "EngineController.hpp"

int main(int argc, char *argv[])
{
  QQmlTriviallyDestructibleDebuggingEnabler debuggingEnabler;
  QGuiApplication app(argc, argv);
  app.setAttribute(Qt::AA_SynthesizeMouseForUnhandledTouchEvents);
  app.setAttribute(Qt::AA_SynthesizeMouseForUnhandledTabletEvents);
  app.setApplicationName(APP_NAME);

  EngineController engine;
  if(argc > 1)
    QMetaObject::invokeMethod(&engine, [&engine, argv] { engine.start({ .filePath = argv[1] }); }, Qt::QueuedConnection);
  else
    QMetaObject::invokeMethod(&engine, [&engine] { engine.selectGame(); }, Qt::QueuedConnection);

  return app.exec();
}
